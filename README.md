# Bonsai

Bonsai is a daemon and shared-library for providing and consuming personal
cloud-like services. The target audience are GNOME-desktop users with multiple
devices for which they'd like their content kept in sync.

The Bonsai daemon (`bonsaid`) is meant to be run on a small device in your
home with access to storage and network such as Wi-Fi or Ethernet.

Applications can be built upon the `libbonsai` library to access these services
using a high-level API.

The goal is to have a number of services built upon `libbonsai` that can
be re-used by applications to easily access content. Some examples of
possible services could be:

 * File Storage (available now)
 * Mail
 * Calendar
 * Todo/Checklists/Notes
 * Photo Albums
 * Music, Podcasts, Audiobooks, Radio
 * Videos
 * Search
 * Backup, System Migration
 * VPN


## Security Notes

Nothing in this is secure, or even designed to be secure, yet.

One property we want is to have some level of isolation between applications.
In particular, we would like an app that needs to store notes to not be able
to access a contacts database and leak that to a third-party.


## Building

You can build Bonsai with meson using the following:

```sh
cd bonsai/
meson build --prefix=/opt/gnome --libdir=lib
cd build/
ninja
ninja install
```


## Device Pairing

The `bonsaid` daemon should be running on your personal cloud device. This
is typically a machine in your home with enough disk and CPU to process your
personal data and some reasonable network bandwidth to your other devices.


### Enable Pairing Mode

First, on the device running `bonsaid` as your user, enable pairing mode
using the `bonsai-pair` utility.

```sh
bonsai-pair --allow-pairing
```


### Request Pairing from Client Device

From your device you would like to pair, run the `bonsai-pair` utility with
the host and port of your device running the daemon.

```sh
bonsai-pair --pair-to hostname:port
```

If this program connects and requests a pairing successfully, a unique token
will be printed to the console such as:

> a68364ad-bdd2-4160-9a3a-677ea73c00e3


### Accept the Pairing

On your device running `bonsaid`, the `bonsai-pair --allow-pairing` process
will now be displaying information about the pairing request. The token should
match your client device along with some basic information about it such
as the current user's full name and hostname.

Press 'y' and hit Return to pair the client.

```sh
[user@host ~]$ bonsai-pair --allow-pairing
Waiting for peers to connect...
Pairing requsted:
   Token on Device: 46271b7d-31c0-43c3-888e-11571044f0eb
  Certificate Hash: 59bd6c0ca369381342712466c465966500c357572d2847e60af4ee0970730ec6
               name: 'Christian Hergert'
           hostname: 'inflight'
            chassis: 'tablet'

Enroll peer into Bonsai? [y/n]: y
```
