libbonsai_header_dir = join_paths(include_dir, 'libbonsai')
libbonsai_header_subdir = join_paths('libbonsai-@0@'.format(libbonsai_api_version), 'libbonsai')

libbonsai_enum_headers = [
  'bonsai-types.h',
]

libbonsai_public_headers = [
  'bonsai-anonymous-identity.h',
  'bonsai-client.h',
  'bonsai-client-progress.h',
  'bonsai-end-point.h',
  'bonsai-error.h',
  'bonsai-identity.h',
  'bonsai-macros.h',
  'bonsai-message.h',
  'bonsai-server.h',
  'bonsai-socket-end-point.h',
  'bonsai-tls-certificate.h',
  'bonsai-tls-identity.h',
  'bonsai-types.h',
  'bonsai-utils.h',
  'libbonsai.h',
]

libbosai_private_headers = [
  'gconstructor.h',
]

libbonsai_public_sources = [
  'bonsai-anonymous-identity.c',
  'bonsai-client.c',
  'bonsai-client-progress.c',
  'bonsai-end-point.c',
  'bonsai-error.c',
  'bonsai-identity.c',
  'bonsai-message.c',
  'bonsai-server.c',
  'bonsai-socket-end-point.c',
  'bonsai-tls-certificate.c',
  'bonsai-tls-identity.c',
  'bonsai-utils.c',
]

libbonsai_private_sources = [
  'bonsai-init.c',
]

libbonsai_generated_sources = []

bonsai_pairing_src = gnome.gdbus_codegen('bonsai-pairing',
           sources: 'org.gnome.Bonsai.Pairing.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Bonsai',
    install_header: true,
       install_dir: libbonsai_header_dir,
           docbook: 'bonsai',
       autocleanup: 'all',
)
libbonsai_generated_sources += [bonsai_pairing_src]

bonsai_enrollment_src = gnome.gdbus_codegen('bonsai-enrollment',
           sources: 'org.gnome.Bonsai.Enrollment.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Bonsai',
    install_header: true,
       install_dir: libbonsai_header_dir,
           docbook: 'bonsai',
       autocleanup: 'all',
)
libbonsai_generated_sources += [bonsai_enrollment_src]

bonsai_progress_src = gnome.gdbus_codegen('bonsai-progress',
           sources: 'org.gnome.Bonsai.Progress.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Bonsai',
    install_header: true,
       install_dir: libbonsai_header_dir,
           docbook: 'bonsai',
       autocleanup: 'all',
)
libbonsai_generated_sources += [bonsai_progress_src]

ipc_isolation_src = gnome.gdbus_codegen('ipc-isolation',
           sources: 'org.gnome.Bonsai.Isolation.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Ipc',
    install_header: false,
           docbook: 'bonsai',
       autocleanup: 'all',
)

ipc_agent_src = gnome.gdbus_codegen('ipc-agent',
           sources: 'org.gnome.Bonsai.Agent.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Ipc',
    install_header: false,
           docbook: 'bonsai',
       autocleanup: 'all',
)

marshalers = gnome.genmarshal('bonsai-marshalers',
             sources: 'bonsai-marshalers.list',
              prefix: '_bonsai_marshal',
  valist_marshallers: true,
            internal: true,
)

bonsai_trace_h_conf = configuration_data()
bonsai_trace_h_conf.set10('ENABLE_TRACING', get_option('tracing'))
bonsai_trace_h_conf.set('BUGREPORT_URL', 'https://gitlab.gnome.org/chergert/bonsai/issues')

bonsai_trace_h = configure_file(
          input: 'bonsai-trace.h.in',
         output: 'bonsai-trace.h',
  configuration: bonsai_trace_h_conf,
    install_dir: libbonsai_header_dir,
)

libbonsai_deps = [
  gio_dep,
  gio_unix_dep,
  gnutls_dep,
  gcrypt_dep,
]

libbonsai_enums = gnome.mkenums_simple('bonsai-enums',
     body_prefix: '#include "config.h"',
   header_prefix: '#include <libbonsai.h>',
       decorator: 'extern',
         sources: libbonsai_enum_headers,
  install_header: true,
     install_dir: libbonsai_header_dir,
)

libbonsai_sources = libbonsai_public_sources \
                  + libbonsai_private_sources \
                  + libbonsai_generated_sources \
                  + libbonsai_enums \
                  + marshalers

libbonsai = library('bonsai-@0@'.format(libbonsai_api_version), libbonsai_sources,
           dependencies: libbonsai_deps,
            install_dir: get_option('libdir'),
                install: true,
)

libbonsai_dep = declare_dependency(
              sources: libbonsai_enums + libbonsai_generated_sources,
            link_with: [libbonsai],
         dependencies: libbonsai_deps,
  include_directories: [include_directories('.')],
)

install_headers(libbonsai_public_headers, install_dir: libbonsai_header_dir)

pkgg.generate(libbonsai,
      version: meson.project_version(),
         name: 'Bonsai',
     filebase: 'libbonsai-@0@'.format(libbonsai_api_version),
  description: 'Libbonsai contains a client and server library for working with Bonsai devices.',
     requires: [ 'gio-2.0' ],
      subdirs: [ libbonsai_header_subdir ],
)

libbonsai_gir = gnome.generate_gir(libbonsai,
              sources: libbonsai_sources,
            nsversion: libbonsai_api_version,
            namespace: 'Bonsai',
        symbol_prefix: 'bonsai',
    identifier_prefix: 'Bonsai',
             includes: [ 'Gio-2.0' ],
              install: true,
           extra_args: [ '--c-include=libbonsai.h',
                         '--c-include=config.h',
                         '--pkg-export=libbonsai-@0@'.format(libbonsai_api_version) ]
)

subdir('tests')
