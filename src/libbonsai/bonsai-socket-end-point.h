/* bonsai-socket-end-point.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-end-point.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_SOCKET_END_POINT (bonsai_socket_end_point_get_type())

G_DECLARE_DERIVABLE_TYPE (BonsaiSocketEndPoint, bonsai_socket_end_point, BONSAI, SOCKET_END_POINT, BonsaiEndPoint)

struct _BonsaiSocketEndPointClass
{
  BonsaiEndPointClass parent_class;

  /*< private >*/
  gpointer _reserved[4];
};

BonsaiEndPoint     *bonsai_socket_end_point_new             (GSocketConnectable   *connectable);
GSocketConnectable *bonsai_socket_end_point_get_connectable (BonsaiSocketEndPoint *self);

G_END_DECLS
