/* libbonsai.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define BONSAI_DEFAULT_PORT 29847

#include "bonsai-types.h"

#include "bonsai-anonymous-identity.h"
#include "bonsai-client.h"
#include "bonsai-end-point.h"
#include "bonsai-enrollment.h"
#include "bonsai-error.h"
#include "bonsai-identity.h"
#include "bonsai-macros.h"
#include "bonsai-message.h"
#include "bonsai-pairing.h"
#include "bonsai-progress.h"
#include "bonsai-server.h"
#include "bonsai-socket-end-point.h"
#include "bonsai-tls-certificate.h"
#include "bonsai-tls-identity.h"
#include "bonsai-trace.h"
#include "bonsai-utils.h"

G_END_DECLS
