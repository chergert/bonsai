/* bonsai-tls-identity.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-tls-identity"

#include "config.h"

#include "bonsai-tls-certificate.h"
#include "bonsai-tls-identity.h"

struct _BonsaiTlsIdentity
{
  BonsaiIdentity   parent_instance;
  GTlsCertificate *certificate;
  gchar           *hash;
};

G_DEFINE_TYPE (BonsaiTlsIdentity, bonsai_tls_identity, BONSAI_TYPE_IDENTITY)

enum {
  PROP_0,
  PROP_CERTIFICATE,
  PROP_HASH,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_tls_identity_new:
 * @certificate: a #GTlsCertificate
 *
 * Create a new #BonsaiTlsIdentity using the certificate.
 *
 * Returns: (transfer full): a newly created #BonsaiTlsIdentity
 */
BonsaiIdentity *
bonsai_tls_identity_new (GTlsCertificate *certificate)
{
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE (certificate), NULL);

  return g_object_new (BONSAI_TYPE_TLS_IDENTITY,
                       "certificate", certificate,
                       NULL);
}

static void
bonsai_tls_identity_finalize (GObject *object)
{
  BonsaiTlsIdentity *self = (BonsaiTlsIdentity *)object;

  g_clear_object (&self->certificate);
  g_clear_pointer (&self->hash, g_free);

  G_OBJECT_CLASS (bonsai_tls_identity_parent_class)->finalize (object);
}

static void
bonsai_tls_identity_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  BonsaiTlsIdentity *self = BONSAI_TLS_IDENTITY (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      g_value_set_object (value, self->certificate);
      break;

    case PROP_HASH:
      g_value_set_string (value, bonsai_tls_identity_get_hash (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_tls_identity_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  BonsaiTlsIdentity *self = BONSAI_TLS_IDENTITY (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      self->certificate = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_tls_identity_class_init (BonsaiTlsIdentityClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_tls_identity_finalize;
  object_class->get_property = bonsai_tls_identity_get_property;
  object_class->set_property = bonsai_tls_identity_set_property;

  properties [PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The certificate to use as the identity",
                         G_TYPE_TLS_CERTIFICATE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_HASH] =
    g_param_spec_string ("hash",
                         "Hash",
                         "The hash of the certificate",
                         NULL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_tls_identity_init (BonsaiTlsIdentity *self)
{
}

/**
 * bonsai_tls_identity_get_certificate:
 * @self: a #BonsaiTlsCertificate
 *
 * Returns: (transfer none) (not nullable): a #GTlsCertificate
 *
 * Since: 0.2
 */
GTlsCertificate *
bonsai_tls_identity_get_certificate (BonsaiTlsIdentity *self)
{
  g_return_val_if_fail (BONSAI_IS_TLS_IDENTITY (self), NULL);

  return self->certificate;
}

const gchar *
bonsai_tls_identity_get_hash (BonsaiTlsIdentity *self)
{
  g_return_val_if_fail (BONSAI_IS_TLS_IDENTITY (self), NULL);

  if (self->hash == NULL)
    self->hash = bonsai_tls_certificate_get_hash (self->certificate);

  return self->hash;
}
