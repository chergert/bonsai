/* bonsai-utils.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-utils"

#include "config.h"

#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gcrypt.h>

#include "bonsai-utils.h"
#include "bonsai-trace.h"

gchar *
bonsai_get_machine_info (const gchar *key)
{
  g_autoptr(GDBusConnection) bus = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GVariant) child = NULL;

  if (!(bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, NULL)))
    return NULL;

  reply = g_dbus_connection_call_sync (bus,
                                       "org.freedesktop.hostname1",
                                       "/org/freedesktop/hostname1",
                                       "org.freedesktop.DBus.Properties",
                                       "Get",
                                       g_variant_new ("(ss)", "org.freedesktop.hostname1", key),
                                       G_VARIANT_TYPE_VARIANT,
                                       G_DBUS_CALL_FLAGS_NONE,
                                       -1,
                                       NULL,
                                       NULL);

  if (reply != NULL &&
      (child = g_variant_get_variant (reply)) &&
      g_variant_is_of_type (child, G_VARIANT_TYPE_STRING))
    return g_variant_dup_string (child, NULL);

  return NULL;
}

gchar *
bonsai_object_path_random (const gchar *prefix)
{
  g_autoptr(GChecksum) checksum = NULL;
  guint8 nonce[32];

  g_return_val_if_fail (prefix != NULL, NULL);

  checksum = g_checksum_new (G_CHECKSUM_SHA256);
  gcry_create_nonce (nonce, sizeof nonce);
  g_checksum_update (checksum, nonce, sizeof nonce);

  return g_strdup_printf ("%s%s",
                          prefix,
                          g_checksum_get_string (checksum));
}

/* The following code to serialize and deserialize GFileInfo to
 * and from GVariant is from the GVFS daemon protocol. The original
 * copyright is provided below.
 */

/* GIO - GLib Input, Output and Streaming Library
 *
 * Copyright (C) 2006-2007 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 */


static const char *
dbus_type_from_file_attribute_type (GFileAttributeType type)
{
  const char *dbus_type;

  switch (type)
    {
    case G_FILE_ATTRIBUTE_TYPE_STRING:
      dbus_type = "s";
      break;
    case G_FILE_ATTRIBUTE_TYPE_BYTE_STRING:
      dbus_type = "ay";
      break;
    case G_FILE_ATTRIBUTE_TYPE_STRINGV:
      dbus_type = "as";
      break;
    case G_FILE_ATTRIBUTE_TYPE_BOOLEAN:
      dbus_type = "b";
      break;
    case G_FILE_ATTRIBUTE_TYPE_UINT32:
      dbus_type = "u";
      break;
    case G_FILE_ATTRIBUTE_TYPE_INT32:
      dbus_type = "i";
      break;
    case G_FILE_ATTRIBUTE_TYPE_UINT64:
      dbus_type = "t";
      break;
    case G_FILE_ATTRIBUTE_TYPE_INT64:
      dbus_type = "x";
      break;
    case G_FILE_ATTRIBUTE_TYPE_OBJECT:
      dbus_type = "r";
      break;
    case G_FILE_ATTRIBUTE_TYPE_INVALID:
      dbus_type = "ay";
      break;
    default:
      dbus_type = NULL;
      g_warning ("Invalid attribute type %u, ignoring\n", type);
      break;
    }

  return dbus_type;
}

static const char *
get_object_signature (GObject *obj)
{
  if (G_IS_ICON (obj))
      return "(us)";
  return "(u)";
}

static GVariant *
append_object (GObject *obj)
{
  GVariant *var;

  /* version 1 and 2 are deprecated old themed-icon and file-icon values */
  if (G_IS_ICON (obj))
    {
      g_autofree char *data = NULL;

      data = g_icon_to_string (G_ICON (obj));
      var = g_variant_new ("(us)", 3, data);
    }
  else
    {
      /* NULL or unknown type: */
      if (obj != NULL)
        g_warning ("Unknown attribute object type, ignoring");

      var = g_variant_new ("(u)", 0);
    }

  return var;
}

static GVariant *
append_file_attribute (const char           *attribute,
                       GFileAttributeStatus  status,
                       GFileAttributeType    type,
                       gpointer              value_p)
{
  const gchar *dbus_type;
  GVariant *v;

  dbus_type = dbus_type_from_file_attribute_type (type);

  if (g_variant_type_equal (G_VARIANT_TYPE (dbus_type), G_VARIANT_TYPE_TUPLE))
    dbus_type = get_object_signature ((GObject *)value_p);

  if (g_variant_type_is_tuple (G_VARIANT_TYPE (dbus_type)))
    v = append_object ((GObject *)value_p);
  else if (g_variant_type_is_array (G_VARIANT_TYPE (dbus_type)))
    {
      g_autofree gchar *s = NULL;

      s = g_strdup_printf ("^%s", dbus_type);
      v = g_variant_new (s, value_p);
    }
  else if (g_variant_type_equal (G_VARIANT_TYPE (dbus_type), G_VARIANT_TYPE_UINT32))
    v = g_variant_new (dbus_type, *(guint32 *)value_p);
  else if (g_variant_type_equal (G_VARIANT_TYPE (dbus_type), G_VARIANT_TYPE_INT32))
    v = g_variant_new (dbus_type, *(gint32 *)value_p);
  else if (g_variant_type_equal (G_VARIANT_TYPE (dbus_type), G_VARIANT_TYPE_UINT64))
    v = g_variant_new (dbus_type, *(guint64 *)value_p);
  else if (g_variant_type_equal (G_VARIANT_TYPE (dbus_type), G_VARIANT_TYPE_INT64))
    v = g_variant_new (dbus_type, *(gint64 *)value_p);
  else if (g_variant_type_equal (G_VARIANT_TYPE (dbus_type), G_VARIANT_TYPE_BOOLEAN))
    v = g_variant_new (dbus_type, *(gboolean *)value_p);
  else
    v = g_variant_new (dbus_type, value_p);

  return g_variant_new ("(suv)",
                        attribute,
                        status,
                        v);
}

/**
 * bonsai_file_info_serialize:
 * @info: a #GFileInfo
 *
 * Serializes a #GFileInfo into a form that can be transfered
 * across a D-Bus connection.
 *
 * Returns: (transfer floating): a #GVariant
 *
 * Since: 0.2
 */
GVariant *
bonsai_file_info_serialize (GFileInfo *info)
{
  GVariantBuilder builder;
  g_auto(GStrv) attributes = NULL;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a(suv)"));

  attributes = g_file_info_list_attributes (info, NULL);

  for (guint i = 0; attributes[i]; i++)
    {
      GFileAttributeType type;
      GFileAttributeStatus status;
      gpointer value_p;

      if (g_file_info_get_attribute_data (info, attributes[i], &type, &value_p, &status))
        g_variant_builder_add_value (&builder,
                                     append_file_attribute (attributes[i], status, type, value_p));
    }

  return g_variant_builder_end (&builder);
}

typedef union {
  gboolean boolean;
  guint32 uint32;
  guint64 uint64;
  gpointer ptr;
} GDBusAttributeValue;

static gboolean
get_file_attribute (GVariant              *value,
                    gchar                **attribute,
                    GFileAttributeStatus  *status,
                    GFileAttributeType    *type,
                    GDBusAttributeValue   *attr_value)
{
  gboolean res;
  char *str;
  guint32 obj_type;
  GObject *obj;
  GVariant *v;

  g_assert (value != NULL);
  g_assert (g_variant_is_of_type (value, G_VARIANT_TYPE ("(suv)")));

  g_variant_get (value, "(suv)",
                 attribute,
                 status,
                 &v);

  res = TRUE;
  if (g_variant_is_of_type (v, G_VARIANT_TYPE_STRING))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_STRING;
      g_variant_get (v, "s", &attr_value->ptr);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_BYTESTRING))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_BYTE_STRING;
      g_variant_get (v, "^ay", &attr_value->ptr);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_STRING_ARRAY))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_STRINGV;
      g_variant_get (v, "^as", &attr_value->ptr);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_BYTE))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_INVALID;
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_BOOLEAN))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_BOOLEAN;
      g_variant_get (v, "b", &attr_value->boolean);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_UINT32))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_UINT32;
      g_variant_get (v, "u", &attr_value->uint32);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_INT32))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_INT32;
      g_variant_get (v, "i", &attr_value->ptr);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_UINT64))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_UINT64;
      g_variant_get (v, "t", &attr_value->uint64);
    }
  else if (g_variant_is_of_type (v, G_VARIANT_TYPE_INT64))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_INT64;
      g_variant_get (v, "x", &attr_value->ptr);
    }
  else if (g_variant_is_container (v))
    {
      *type = G_FILE_ATTRIBUTE_TYPE_OBJECT;
      obj_type = G_MAXUINT32;   /* treat it as an error if not set below */
      str = NULL;

      if (g_variant_is_of_type (v, G_VARIANT_TYPE ("(u)")))
        {
          g_variant_get (v, "(u)", &obj_type);
        }
      else if (g_variant_is_of_type (v, G_VARIANT_TYPE ("(us)")))
        {
          g_variant_get (v, "(u&s)", &obj_type, &str);
        }

      obj = NULL;

      /* obj_type 1 and 2 are deprecated and treated as errors */
      if (obj_type == 3)
        {
          if (str != NULL)
            {
              /* serialized G_ICON */
              obj = (GObject *)g_icon_new_for_string (str, NULL);
            }
          else
            {
              g_warning ("Malformed object data in file attribute");
            }
        }
      else
        {
          /* NULL (or unsupported) */
          if (obj_type != 0)
            g_warning ("Unsupported object type in file attribute");
        }
      attr_value->ptr = obj;
    }
  else
    res = FALSE;

  g_variant_unref (v);

  return res;
}

static gpointer
attribute_as_pointer (GFileAttributeType   type,
                      GDBusAttributeValue *value)
{
  switch (type)
    {
    case G_FILE_ATTRIBUTE_TYPE_STRING:
    case G_FILE_ATTRIBUTE_TYPE_BYTE_STRING:
    case G_FILE_ATTRIBUTE_TYPE_OBJECT:
    case G_FILE_ATTRIBUTE_TYPE_STRINGV:
      return value->ptr;

    case G_FILE_ATTRIBUTE_TYPE_INVALID:
    case G_FILE_ATTRIBUTE_TYPE_UINT32:
    case G_FILE_ATTRIBUTE_TYPE_INT32:
    case G_FILE_ATTRIBUTE_TYPE_UINT64:
    case G_FILE_ATTRIBUTE_TYPE_INT64:
    case G_FILE_ATTRIBUTE_TYPE_BOOLEAN:
    default:
      return (gpointer) value;
    }
}

static void
attribute_value_destroy (GFileAttributeType   type,
                         GDBusAttributeValue *value)
{
  switch (type)
    {
    case G_FILE_ATTRIBUTE_TYPE_STRING:
    case G_FILE_ATTRIBUTE_TYPE_BYTE_STRING:
      g_free (value->ptr);
      break;

    case G_FILE_ATTRIBUTE_TYPE_STRINGV:
      g_strfreev (value->ptr);
      break;

    case G_FILE_ATTRIBUTE_TYPE_OBJECT:
      if (value->ptr)
        g_object_unref (value->ptr);
      break;

    case G_FILE_ATTRIBUTE_TYPE_INVALID:
    case G_FILE_ATTRIBUTE_TYPE_UINT32:
    case G_FILE_ATTRIBUTE_TYPE_INT32:
    case G_FILE_ATTRIBUTE_TYPE_UINT64:
    case G_FILE_ATTRIBUTE_TYPE_INT64:
    case G_FILE_ATTRIBUTE_TYPE_BOOLEAN:
    default:
      break;
    }
}

GFileInfo *
bonsai_file_info_deserialize (GVariant  *value,
                              GError   **error)
{
  g_autoptr(GFileInfo) info = NULL;
  GVariantIter iter;
  gpointer childptr;

  g_return_val_if_fail (value != NULL, NULL);

  info = g_file_info_new ();

  g_variant_iter_init (&iter, value);
  while ((childptr = g_variant_iter_next_value (&iter)))
    {
      GDBusAttributeValue attr_value = {0};
      GFileAttributeStatus status = 0;
      GFileAttributeType type = 0;
      g_autofree gchar *attribute = NULL;
      g_autoptr(GVariant) child = childptr;

      if (!get_file_attribute (child, &attribute, &status, &type, &attr_value))
        BONSAI_GOTO (error);

      g_file_info_set_attribute (info, attribute, type, attribute_as_pointer (type, &attr_value));
      if (status)
        g_file_info_set_attribute_status (info, attribute, status);

      attribute_value_destroy (type, &attr_value);
    }

  return g_steal_pointer (&info);

 error:
  g_set_error_literal (error,
                       G_IO_ERROR,
                       G_IO_ERROR_FAILED,
                       _("Invalid file info format"));

  return NULL;
}

