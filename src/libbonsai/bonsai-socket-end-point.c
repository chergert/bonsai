/* bonsai-socket-end-point.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-socket-end-point"

#include "config.h"

#include "bonsai-anonymous-identity.h"
#include "bonsai-identity.h"
#include "bonsai-socket-end-point.h"
#include "bonsai-tls-identity.h"
#include "bonsai-trace.h"

typedef struct
{
  BonsaiIdentity *identity;
  BonsaiIdentity *peer_identity;
  gchar          *negotiated_protocol;
} ConnectData;

typedef struct
{
  GSocketConnectable *connectable;
} BonsaiSocketEndPointPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (BonsaiSocketEndPoint, bonsai_socket_end_point, BONSAI_TYPE_END_POINT)

enum {
  PROP_0,
  PROP_CONNECTABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
connect_data_free (gpointer data)
{
  ConnectData *cd = data;

  BONSAI_ENTRY;

  g_clear_object (&cd->identity);
  g_clear_object (&cd->peer_identity);
  g_clear_pointer (&cd->negotiated_protocol, g_free);
  g_slice_free (ConnectData, cd);

  BONSAI_EXIT;
}

/**
 * bonsai_socket_end_point_new:
 * @connectable: a #GSocketConnectable
 *
 * Create a new #BonsaiSocketEndPoint.
 *
 * Returns: (transfer full): a newly created #BonsaiSocketEndPoint
 *
 * Since: 0.2
 */
BonsaiEndPoint *
bonsai_socket_end_point_new (GSocketConnectable *connectable)
{
  return g_object_new (BONSAI_TYPE_SOCKET_END_POINT,
                       "connectable", connectable,
                       NULL);
}

static gboolean
bonsai_socket_end_point_accept_certificate_cb (GTlsClientConnection *conn,
                                               GTlsCertificate      *peer_cert,
                                               GTlsCertificateFlags  tls_errors,
                                               GTask                *task)
{
  ConnectData *cd;
  gboolean ret = FALSE;

  BONSAI_ENTRY;

  g_assert (G_IS_TLS_CERTIFICATE (peer_cert));
  g_assert (G_IS_TLS_CLIENT_CONNECTION (conn));
  g_assert (G_IS_TASK (task));

  cd = g_task_get_task_data (task);

  if (BONSAI_IS_TLS_IDENTITY (cd->peer_identity))
    {
      GTlsCertificate *expected;

      expected = bonsai_tls_identity_get_certificate (BONSAI_TLS_IDENTITY (cd->peer_identity));

      if (expected != NULL)
        ret = g_tls_certificate_is_same (expected, peer_cert);
    }

  if (ret == FALSE)
    ret = BONSAI_IS_ANONYMOUS_IDENTITY (cd->peer_identity);

  BONSAI_RETURN (ret);
}

static void
bonsai_socket_end_point_event_cb (GSocketClient      *client,
                                  GSocketClientEvent  event,
                                  GSocketConnectable *connectable,
                                  GIOStream          *connection,
                                  GTask              *task)
{
  static const gchar *advertised[] = { "Bonsai/1.0", NULL };
  ConnectData *cd;

  BONSAI_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (G_IS_SOCKET_CONNECTABLE (connectable));
  g_assert (!connection || G_IS_IO_STREAM (connection));
  g_assert (G_IS_SOCKET_CLIENT (client));

  cd = g_task_get_task_data (task);

  switch (event)
    {
    case G_SOCKET_CLIENT_CONNECTING: {
      GSocket *sock = g_socket_connection_get_socket (G_SOCKET_CONNECTION (connection));
      g_socket_set_blocking (sock, FALSE);
      break;
    }

    case G_SOCKET_CLIENT_RESOLVING:
    case G_SOCKET_CLIENT_RESOLVED:
    case G_SOCKET_CLIENT_PROXY_NEGOTIATING:
    case G_SOCKET_CLIENT_PROXY_NEGOTIATED:
    case G_SOCKET_CLIENT_COMPLETE:
      break;

    case G_SOCKET_CLIENT_CONNECTED:
      g_assert (G_IS_TCP_CONNECTION (connection));
      g_tcp_connection_set_graceful_disconnect (G_TCP_CONNECTION (connection), TRUE);
      break;

    case G_SOCKET_CLIENT_TLS_HANDSHAKING: {
      GTlsConnection *tls = G_TLS_CONNECTION (connection);

      g_tls_connection_set_require_close_notify (G_TLS_CONNECTION (connection), TRUE);
      g_tls_connection_set_advertised_protocols (G_TLS_CONNECTION (connection), advertised);

      if (BONSAI_IS_TLS_IDENTITY (cd->identity))
        {
          BonsaiTlsIdentity *tls_identity = BONSAI_TLS_IDENTITY (cd->identity);
          GTlsCertificate *certificate = bonsai_tls_identity_get_certificate (tls_identity);

          g_tls_connection_set_certificate (tls, certificate);
        }

      g_signal_connect_object (connection,
                               "accept-certificate",
                               G_CALLBACK (bonsai_socket_end_point_accept_certificate_cb),
                               task,
                               0);

      break;
    }

    case G_SOCKET_CLIENT_TLS_HANDSHAKED: {
      GTlsConnection *tls = G_TLS_CONNECTION (connection);
      g_clear_pointer (&cd->negotiated_protocol, g_free);
      cd->negotiated_protocol = g_strdup (g_tls_connection_get_negotiated_protocol (tls));
      break;
    }

    default:
      g_assert_not_reached ();
    }

  BONSAI_EXIT;
}

static void
bonsai_socket_end_point_connect_cb (GObject      *object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  GSocketClient *client = (GSocketClient *)object;
  g_autoptr(GSocketConnection) conn = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (G_IS_SOCKET_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(conn = g_socket_client_connect_finish (client, result, &error)))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_steal_pointer (&conn),
                           g_object_unref);

  BONSAI_EXIT;
}

static void
bonsai_socket_end_point_connect_async (BonsaiEndPoint      *end_point,
                                       BonsaiIdentity      *identity,
                                       BonsaiIdentity      *peer_identity,
                                       GCancellable        *cancellable,
                                       GAsyncReadyCallback  callback,
                                       gpointer             user_data)
{
  BonsaiSocketEndPoint *self = (BonsaiSocketEndPoint *)end_point;
  BonsaiSocketEndPointPrivate *priv = bonsai_socket_end_point_get_instance_private (self);
  g_autoptr(GSocketClient) client = NULL;
  g_autoptr(GTask) task = NULL;
  ConnectData *cd;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SOCKET_END_POINT (self));
  g_assert (!identity || BONSAI_IS_IDENTITY (identity));
  g_assert (!peer_identity || BONSAI_IS_IDENTITY (peer_identity));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  cd = g_slice_new0 (ConnectData);
  g_set_object (&cd->identity, identity);
  g_set_object (&cd->peer_identity, peer_identity);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_socket_end_point_connect_async);
  g_task_set_task_data (task, cd, connect_data_free);

  client = g_socket_client_new ();
  g_socket_client_set_tls (client, TRUE);

#if 0
  g_socket_client_set_tls_validation_flags (client,
                                            (G_TLS_CERTIFICATE_EXPIRED |
                                             G_TLS_CERTIFICATE_REVOKED |
                                             G_TLS_CERTIFICATE_INSECURE |
                                             G_TLS_CERTIFICATE_NOT_ACTIVATED));
#endif

  g_signal_connect_object (client,
                           "event",
                           G_CALLBACK (bonsai_socket_end_point_event_cb),
                           task,
                           0);

  g_socket_client_connect_async (client,
                                 priv->connectable,
                                 cancellable,
                                 bonsai_socket_end_point_connect_cb,
                                 g_steal_pointer (&task));

  BONSAI_EXIT;
}

static GIOStream *
bonsai_socket_end_point_connect_finish (BonsaiEndPoint  *end_point,
                                        GAsyncResult    *result,
                                        BonsaiIdentity **identity,
                                        gchar          **negotiated_protocol,
                                        GError         **error)
{
  ConnectData *cd;
  GIOStream *ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SOCKET_END_POINT (end_point));
  g_assert (G_IS_TASK (result));
  g_assert (identity != NULL);

  cd = g_task_get_task_data (G_TASK (result));

  if (negotiated_protocol != NULL)
    *negotiated_protocol = g_steal_pointer (&cd->negotiated_protocol);

  if (identity != NULL)
    *identity = g_steal_pointer (&cd->identity);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

static void
bonsai_socket_end_point_finalize (GObject *object)
{
  BonsaiSocketEndPoint *self = (BonsaiSocketEndPoint *)object;
  BonsaiSocketEndPointPrivate *priv = bonsai_socket_end_point_get_instance_private (self);

  g_clear_object (&priv->connectable);

  G_OBJECT_CLASS (bonsai_socket_end_point_parent_class)->finalize (object);
}

static void
bonsai_socket_end_point_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  BonsaiSocketEndPoint *self = BONSAI_SOCKET_END_POINT (object);

  switch (prop_id)
    {
    case PROP_CONNECTABLE:
      g_value_set_object (value, bonsai_socket_end_point_get_connectable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_socket_end_point_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  BonsaiSocketEndPoint *self = BONSAI_SOCKET_END_POINT (object);
  BonsaiSocketEndPointPrivate *priv = bonsai_socket_end_point_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CONNECTABLE:
      priv->connectable = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_socket_end_point_class_init (BonsaiSocketEndPointClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  BonsaiEndPointClass *end_point_class = BONSAI_END_POINT_CLASS (klass);

  object_class->finalize = bonsai_socket_end_point_finalize;
  object_class->get_property = bonsai_socket_end_point_get_property;
  object_class->set_property = bonsai_socket_end_point_set_property;

  end_point_class->connect_async = bonsai_socket_end_point_connect_async;
  end_point_class->connect_finish = bonsai_socket_end_point_connect_finish;

  /**
   * BonsaiSocketEndPoint:connectable:
   *
   * The :connectable property is the #GSocketConnectable to use
   * when connecting to the end point.
   *
   * Since: 0.2
   */
  properties [PROP_CONNECTABLE] =
    g_param_spec_object ("connectable",
                         "Connectable",
                         "Connectable",
                         G_TYPE_SOCKET_CONNECTABLE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_socket_end_point_init (BonsaiSocketEndPoint *self)
{
}

/**
 * bonsai_socket_end_point_get_connectable:
 *
 * Gets the #GSocketConnectable that is used to connect to the end point.
 *
 * Returns: (transfer none): a #GSocketConnectable
 *
 * Since: 0.2
 */
GSocketConnectable *
bonsai_socket_end_point_get_connectable (BonsaiSocketEndPoint *self)
{
  BonsaiSocketEndPointPrivate *priv = bonsai_socket_end_point_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_SOCKET_END_POINT (self), NULL);

  return priv->connectable;
}
