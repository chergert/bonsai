/* bonsai-anonymous-identity.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-anonymous-identity"

#include "config.h"

#include "bonsai-anonymous-identity.h"

struct _BonsaiAnonymousIdentity
{
  BonsaiIdentity parent_instance;
};

G_DEFINE_TYPE (BonsaiAnonymousIdentity, bonsai_anonymous_identity, BONSAI_TYPE_IDENTITY)

/**
 * bonsai_anonymous_identity_new:
 *
 * Create a new #BonsaiAnonymousIdentity.
 *
 * Returns: (transfer full): a newly created #BonsaiAnonymousIdentity
 */
BonsaiIdentity *
bonsai_anonymous_identity_new (void)
{
  return g_object_new (BONSAI_TYPE_ANONYMOUS_IDENTITY, NULL);
}

static void
bonsai_anonymous_identity_class_init (BonsaiAnonymousIdentityClass *klass)
{
}

static void
bonsai_anonymous_identity_init (BonsaiAnonymousIdentity *self)
{
}
