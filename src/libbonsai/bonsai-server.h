/* bonsai-server.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_SERVER (bonsai_server_get_type())

G_DECLARE_DERIVABLE_TYPE (BonsaiServer, bonsai_server, BONSAI, SERVER, GSocketService)

struct _BonsaiServerClass
{
  GSocketServiceClass parent_class;

  /* Signals */
  BonsaiAuthorization (*authorize_identity) (BonsaiServer   *self,
                                             BonsaiIdentity *identity);
  void                (*client_added)       (BonsaiServer   *self,
                                             BonsaiClient   *client);
  void                (*client_removed)     (BonsaiServer   *self,
                                             BonsaiClient   *client);

  /*< private >*/
  gpointer _reserved[20];
};

BonsaiServer        *bonsai_server_new                (GTlsCertificate        *certificate);
BonsaiAuthorization  bonsai_server_authorize_identity (BonsaiServer           *self,
                                                       BonsaiIdentity         *identity);
void                 bonsai_server_export             (BonsaiServer           *self,
                                                       BonsaiRealm             realm,
                                                       GDBusInterfaceSkeleton *skeleton,
                                                       const gchar            *object_path);
void                 bonsai_server_unexport           (BonsaiServer           *self,
                                                       GDBusInterfaceSkeleton *skeleton);

G_END_DECLS
