/* bonsai-client.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-client"

#include "config.h"

#include "bonsai-anonymous-identity.h"
#include "bonsai-client.h"
#include "bonsai-client-private.h"
#include "bonsai-client-progress.h"
#include "bonsai-end-point.h"
#include "bonsai-progress.h"
#include "bonsai-trace.h"
#include "bonsai-utils.h"

typedef struct
{
  BonsaiIdentity    *identity;
  GDBusConnection   *connection;
  GArray            *service_exports;
  GCancellable      *cancellable;
  BonsaiRealm        realm;
  guint              timeout_handler;
} BonsaiClientPrivate;

typedef struct
{
  GDBusInterfaceSkeleton *skeleton;
  gchar                  *object_path;
} ServiceExport;

G_DEFINE_TYPE_WITH_PRIVATE (BonsaiClient, bonsai_client, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CONNECTION,
  PROP_IDENTITY,
  N_PROPS
};

enum {
  CLOSED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
clear_service_export (gpointer data)
{
  ServiceExport *export = data;

  g_clear_pointer (&export->object_path, g_free);
  g_clear_object (&export->skeleton);
}

/**
 * bonsai_client_new:
 * @identity: (nullable): a #BonsaiIdentity or %NULL
 *
 * Create a new #BonsaiClient.
 *
 * Returns: (transfer full): a newly created #BonsaiClient
 *
 * Since: 0.2
 */
BonsaiClient *
bonsai_client_new (BonsaiIdentity *identity)
{
  g_return_val_if_fail (!identity || BONSAI_IS_IDENTITY (identity), NULL);

  return g_object_new (BONSAI_TYPE_CLIENT,
                       "identity", identity,
                       NULL);
}

/**
 * bonsai_client_new_for_connection:
 * @identity: (nullable): a #BonsaiIdentity or %NULL
 * @connection: a #GDBusConnection or %NULL
 *
 * Create a new #BonsaiClient using the previously created #DBusConnection.
 *
 * Returns: (transfer full): a newly created #BonsaiClient
 *
 * Since: 0.2
 */
BonsaiClient *
bonsai_client_new_for_connection (BonsaiIdentity  *identity,
                                  GDBusConnection *connection)
{
  g_return_val_if_fail (!identity || BONSAI_IS_IDENTITY (identity), NULL);
  g_return_val_if_fail (!connection || G_IS_DBUS_CONNECTION (connection), NULL);

  return g_object_new (BONSAI_TYPE_CLIENT,
                       "connection", connection,
                       "identity", identity,
                       NULL);
}

/**
 * bonsai_client_get_cancellable:
 * @self: a #BonsaiClient
 *
 * Gets the cancellable for the client.
 *
 * The cancellable is cancelled when the client is closed.
 *
 * Returns: (transfer none): a #GCancellable
 *
 * Since: 0.2
 */
GCancellable *
bonsai_client_get_cancellable (BonsaiClient *self)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), NULL);

  return priv->cancellable;
}

static void
bonsai_client_connection_closed_cb (BonsaiClient    *self,
                                    gboolean         remote_peer_vanished,
                                    const GError    *error,
                                    GDBusConnection *connection)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT (self));
  g_assert (G_IS_DBUS_CONNECTION (connection));

  BONSAI_TRACE_MSG ("remote_peer_vanished: %d reason=%s",
                    remote_peer_vanished, error ? error->message : "");

  g_clear_handle_id (&priv->timeout_handler, g_source_remove);

  g_cancellable_cancel (priv->cancellable);
  g_signal_emit (self, signals [CLOSED], 0);

  BONSAI_EXIT;
}

static void
bonsai_client_dispose (GObject *object)
{
  BonsaiClient *self = (BonsaiClient *)object;
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  if (priv->connection != NULL)
    g_dbus_connection_close_sync (priv->connection, NULL, NULL);

  g_clear_handle_id (&priv->timeout_handler, g_source_remove);

  while (priv->service_exports->len)
    {
      const ServiceExport *export;

      export = &g_array_index (priv->service_exports, ServiceExport, 0);
      bonsai_client_unexport (self, export->skeleton);
    }

  G_OBJECT_CLASS (bonsai_client_parent_class)->dispose (object);
}

static void
bonsai_client_finalize (GObject *object)
{
  BonsaiClient *self = (BonsaiClient *)object;
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  if (priv->connection)
    g_object_set_data (G_OBJECT (priv->connection), "BONSAI_CLIENT", NULL);

  g_clear_pointer (&priv->service_exports, g_array_unref);
  g_clear_object (&priv->connection);
  g_clear_object (&priv->identity);
  g_clear_object (&priv->cancellable);

  G_OBJECT_CLASS (bonsai_client_parent_class)->finalize (object);
}

static void
bonsai_client_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  BonsaiClient *self = BONSAI_CLIENT (object);

  switch (prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, bonsai_client_get_connection (self));
      break;

    case PROP_IDENTITY:
      g_value_set_object (value, bonsai_client_get_identity (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  BonsaiClient *self = BONSAI_CLIENT (object);
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CONNECTION:
      priv->connection = g_value_dup_object (value);

      if (priv->connection != NULL)
        {
          g_signal_connect_object (priv->connection,
                                   "closed",
                                   G_CALLBACK (bonsai_client_connection_closed_cb),
                                   self,
                                   G_CONNECT_SWAPPED);
          g_object_set_data (G_OBJECT (priv->connection), "BONSAI_CLIENT", self);
        }
      break;

    case PROP_IDENTITY:
      if (!(priv->identity = g_value_dup_object (value)))
        priv->identity = bonsai_anonymous_identity_new ();
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_class_init (BonsaiClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bonsai_client_dispose;
  object_class->finalize = bonsai_client_finalize;
  object_class->get_property = bonsai_client_get_property;
  object_class->set_property = bonsai_client_set_property;

  /**
   * BonsaiClient:connection:
   *
   * The :connection property is the #GDBusConnection used to communicate with
   * the Bonsai peer.
   *
   * Since: 0.2
   */
  properties [PROP_CONNECTION] =
    g_param_spec_object ("connection",
                         "Connection",
                         "The connection to the peer.",
                         G_TYPE_DBUS_CONNECTION,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiClient:identity:
   *
   * The :identity property is the the identity of the peer, such as a
   * #BonsaiAnonymousIdentity if the peer is unknown, or a #BonsaiTlsIdentity
   * if the peer used a #GTlsCertificate client certificate to authenticate.
   *
   * Since: 0.2
   */
  properties [PROP_IDENTITY] =
    g_param_spec_object ("identity",
                         "Identity",
                         "The identity of the peer",
                         BONSAI_TYPE_IDENTITY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * BonsaiClient::closed:
   *
   * The ::closed signal is emitted when the client connection has been
   * discovered to have been closed.
   *
   * Since: 0.2
   */
  signals [CLOSED] =
    g_signal_new ("closed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (BonsaiClientClass, closed),
                  NULL, NULL, NULL, G_TYPE_NONE, 0);
}

static void
bonsai_client_init (BonsaiClient *self)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  priv->cancellable = g_cancellable_new ();
  priv->realm = BONSAI_REALM_ANONYMOUS;
  priv->service_exports = g_array_new (FALSE, FALSE, sizeof (ServiceExport));
  g_array_set_clear_func (priv->service_exports, clear_service_export);
}

/**
 * bonsai_client_get_connection:
 * @self: a #BonsaiClient
 *
 * Gets the underlying #GDBusConnection used by the clients.
 *
 * If the client has not connected to a peer, then this will be %NULL.
 *
 * Returns: (transfer none): a #GDBusConnection or %NULL
 *
 * Since: 0.2
 */
GDBusConnection *
bonsai_client_get_connection (BonsaiClient *self)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), NULL);

  return priv->connection;
}

/**
 * bonsai_client_get_identity:
 * @self: a #BonsaiClient
 *
 * Gets the #BonsaiIdentity for the client.
 *
 * Returns: (not nullable): a #BonsaiIdentity
 *
 * Since: 0.2
 */
BonsaiIdentity *
bonsai_client_get_identity (BonsaiClient *self)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), NULL);

  return priv->identity;
}

/**
 * bonsai_client_export:
 * @self: a #BonsaiClient
 * @interface: the #GDBusInterfaceSkeleton to be exported
 * @object_path: the object path
 * @error: a location for a #GError, or %NULL
 *
 * This function will export @interface on the underlying connection used
 * by @self. If the client has not yet been connected to a peer, then the
 * interface will be exported once the client has connected to a peer and
 * %TRUE is returned.
 *
 * Returns: %TRUE if successful or the client has not yet connected; otherwise
 *   %FALSE and @error is set.
 *
 * Since: 0.2
 */
gboolean
bonsai_client_export (BonsaiClient            *self,
                      GDBusInterfaceSkeleton  *interface,
                      const gchar             *object_path,
                      GError                 **error)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);
  ServiceExport export;

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (object_path != NULL, FALSE);

  for (guint i = 0; i < priv->service_exports->len; i++)
    {
      const ServiceExport *ele = &g_array_index (priv->service_exports, ServiceExport, i);

      /* Ignore if we already saw this */
      if (ele->skeleton == interface && g_str_equal (object_path, ele->object_path))
        return TRUE;
    }

  export.skeleton = g_object_ref (interface);
  export.object_path = g_strdup (object_path);

  g_array_append_val (priv->service_exports, export);

  if (priv->connection == NULL)
    return TRUE;

  return g_dbus_interface_skeleton_export (interface, priv->connection, object_path, error);
}

/**
 * bonsai_client_unexport:
 * @self: a #BonsaiClient
 * @interface: removes @interface from the exported services
 *
 * Removes @interface from being exported to the peer over the client's
 * #GDBusConnection.
 *
 * If the client has not yet connected, it will no longer be exported to
 * the peer once a connection has completed.
 *
 * Since: 0.2
 */
void
bonsai_client_unexport (BonsaiClient           *self,
                        GDBusInterfaceSkeleton *interface)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_CLIENT (self));
  g_return_if_fail (G_IS_DBUS_INTERFACE_SKELETON (interface));

  if (priv->connection != NULL)
    g_dbus_interface_skeleton_unexport_from_connection (interface, priv->connection);

  for (guint i = priv->service_exports->len; i > 0; i--)
    {
      const ServiceExport *export = &g_array_index (priv->service_exports, ServiceExport, i -1 );

      if (export->skeleton == interface)
        g_array_remove_index_fast (priv->service_exports, i - 1);
    }
}

static gboolean
bonsai_client_authorize_authenticated_peer_cb (GDBusAuthObserver *auth_observer,
                                               GIOStream         *stream,
                                               GCredentials      *credentials,
                                               gpointer           unused)
{
  /* We handle this externally during TLS */
  return TRUE;
}

static gboolean
bonsai_client_allow_mechanism_cb (GDBusAuthObserver *auth_observer,
                                  const gchar       *mechanism,
                                  gpointer           unused)
{
  /* We handle this externally during TLS */
  return g_str_equal (mechanism, "ANONYMOUS");
}

static void
bonsai_client_connection_new_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
  g_autoptr(GDBusConnection) connection = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!(connection = g_dbus_connection_new_finish (result, &error)))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      BONSAI_EXIT;
    }
  else
    {
      g_dbus_connection_set_exit_on_close (connection, FALSE);
      g_task_return_pointer (task, g_steal_pointer (&connection), g_object_unref);
      BONSAI_EXIT;
    }
}

static GIOStream *
wrap_tls_stream (GIOStream *stream)
{
  g_autoptr(GDataInputStream) wrapped_input = NULL;
  g_autoptr(GDataOutputStream) wrapped_output = NULL;
  GIOStream *wrapped;

  wrapped_input = g_data_input_stream_new (g_io_stream_get_input_stream (stream));
  wrapped_output = g_data_output_stream_new (g_io_stream_get_output_stream (stream));
  wrapped = g_simple_io_stream_new (G_INPUT_STREAM (wrapped_input),
                                    G_OUTPUT_STREAM (wrapped_output));

  g_object_set_data_full (G_OBJECT (wrapped),
                          "BASE_IO_STREAM",
                          g_object_ref (stream),
                          g_object_unref);

  return wrapped;
}

static void
bonsai_client_connect_cb (GObject      *object,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  BonsaiEndPoint *end_point = (BonsaiEndPoint *)object;
  g_autofree gchar *protocol_name = NULL;
  g_autoptr(GDBusAuthObserver) auth_observer = NULL;
  g_autoptr(BonsaiIdentity) identity = NULL;
  g_autoptr(GIOStream) stream = NULL;
  g_autoptr(GIOStream) wrapped = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_END_POINT (end_point));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  stream = bonsai_end_point_connect_finish (end_point, result, &identity, &protocol_name, &error);

  if (stream == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      BONSAI_EXIT;
    }

  if (g_strcmp0 (protocol_name, "Bonsai/1.0") != 0)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_SUPPORTED,
                               "Protocol '%s' is not supported",
                               protocol_name ? protocol_name : "");
      BONSAI_EXIT;
    }

  cancellable = g_task_get_cancellable (task);

  auth_observer = g_dbus_auth_observer_new ();
  g_signal_connect (auth_observer,
                    "allow-mechanism",
                    G_CALLBACK (bonsai_client_allow_mechanism_cb),
                    NULL);
  g_signal_connect (auth_observer,
                    "authorize-authenticated-peer",
                    G_CALLBACK (bonsai_client_authorize_authenticated_peer_cb),
                    NULL);

  g_task_set_task_data (task, g_object_ref (stream), g_object_unref);

  /* We need to wrap the GTlsConnection or else GDBusConnection will fail
   * to negotiate and/or handle read/writes correctly.
   *
   * See: https://gitlab.gnome.org/GNOME/glib/issues/1948
   */
  wrapped = wrap_tls_stream (stream);

  g_dbus_connection_new (wrapped,
                         NULL,
                         (G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                          G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING |
                          G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_ALLOW_ANONYMOUS),
                         auth_observer,
                         cancellable,
                         bonsai_client_connection_new_cb,
                         g_steal_pointer (&task));

  BONSAI_EXIT;
}

/**
 * bonsai_client_connect_async:
 * @self: a #BonsaiClient
 * @end_point: a #BonsaiEndPoint
 * @peer_identity: (nullable): a #BonsaiIdentity to expect as the peer
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously connects to a peer.
 *
 * The @callback MUST call bonsai_client_connect_finish() to complete the
 * operation, or the connection will not complete.
 *
 * Since: 0.2
 */
void
bonsai_client_connect_async (BonsaiClient        *self,
                             BonsaiEndPoint      *end_point,
                             BonsaiIdentity      *peer_identity,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_CLIENT (self));
  g_return_if_fail (BONSAI_IS_END_POINT (end_point));
  g_return_if_fail (!peer_identity || BONSAI_IS_IDENTITY (peer_identity));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (BONSAI_IS_IDENTITY (priv->identity));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_connect_async);
  g_task_set_name (task, "[BonsaiClient connect]");

  bonsai_end_point_connect_async (end_point,
                                  priv->identity,
                                  peer_identity,
                                  cancellable,
                                  bonsai_client_connect_cb,
                                  g_steal_pointer (&task));

  BONSAI_EXIT;
}

/**
 * bonsai_client_connect_finish:
 * @self: an #BonsaiClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes a request to connect to a client.
 *
 * This function must be called to complete the connection to a peer unless
 * the client was created with bonsai_client_new_for_connection().
 *
 * Returns: %TRUE if the connection was successful; otherwise %FALSE and
 *   #error is set.
 *
 * Since: 0.2
 */
gboolean
bonsai_client_connect_finish (BonsaiClient  *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);
  g_autoptr(GDBusConnection) ret = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_pointer (G_TASK (result), error);

  /* If we've already connected (perhaps they called connect twice and
   * they raced to complete) then mark this connection as failed saying
   * the connection was closed (bit of a lie, but okay).
   */
  if (priv->connection != NULL)
    {
      BONSAI_TRACE_MSG ("Existing connection found, closing new connection");
      if (ret != NULL)
        g_dbus_connection_close (ret, NULL, NULL, NULL);
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_CONNECTION_CLOSED,
                   "Already connected to peer");
      BONSAI_RETURN (FALSE);
    }

  if (ret == NULL)
    BONSAI_RETURN (FALSE);

  g_signal_connect_object (ret,
                           "closed",
                           G_CALLBACK (bonsai_client_connection_closed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  /* Add all of our pre-registered services to the connection before we
   * start message processing (or else we risk dropping them).
   */
  for (guint i = 0; i < priv->service_exports->len; i++)
    {
      const ServiceExport *export = &g_array_index (priv->service_exports, ServiceExport, i);

      g_dbus_interface_skeleton_export (export->skeleton,
                                        ret,
                                        export->object_path,
                                        NULL);
    }

  /* Save the connection for future use */
  g_object_set_data (G_OBJECT (ret), "BONSAI_CLIENT", self);
  priv->connection = g_object_ref (ret);

  /* Now we can start processing as services are registered */
  g_dbus_connection_start_message_processing (priv->connection);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONNECTION]);

  BONSAI_RETURN (TRUE);
}

static void
bonsai_client_connect_sync_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  BonsaiClient *self = (BonsaiClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_client_connect_finish (self, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

gboolean
bonsai_client_connect (BonsaiClient    *self,
                       BonsaiEndPoint  *end_point,
                       BonsaiIdentity  *peer_identity,
                       GCancellable    *cancellable,
                       GError         **error)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GMainContext) context = NULL;
  gboolean ret;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (BONSAI_IS_END_POINT (end_point), FALSE);
  g_return_val_if_fail (!peer_identity || BONSAI_IS_IDENTITY (peer_identity), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  task = g_task_new (self, cancellable, NULL, NULL);
  g_task_set_source_tag (task, bonsai_client_connect);

  bonsai_client_connect_async (self,
                               end_point,
                               peer_identity,
                               cancellable,
                               bonsai_client_connect_sync_cb,
                               g_object_ref (task));

  context = g_main_context_ref_thread_default ();

  while (!g_task_get_completed (task))
    g_main_context_iteration (context, TRUE);

  ret = g_task_propagate_boolean (task, error);

  BONSAI_RETURN (ret);
}

/**
 * bonsai_client_disconnect:
 * @self: a #BonsaiClient
 * @cancellable: a #GCancellable
 * @error: a location for a #GError or %NULL
 *
 * Disconnects a client synchronously.
 *
 * If the client has an established #GDBusConnection, it will be disconnected.
 *
 * Returns: %TRUE if there was no connection or the connection was
 *   successfully disconnected; otherwise %FALSE and @error is set.
 *
 * Since: 0.2
 */
gboolean
bonsai_client_disconnect (BonsaiClient  *self,
                          GCancellable  *cancellable,
                          GError       **error)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);
  g_autoptr(GDBusConnection) connection = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  if ((connection = g_steal_pointer (&priv->connection)))
    {
      gboolean ret;

      g_object_set_data (G_OBJECT (connection), "BONSAI_CLIENT", NULL);

      BONSAI_TRACE_MSG ("Closing client connection");
      ret = g_dbus_connection_close_sync (connection, cancellable, error);

      BONSAI_RETURN (ret);
    }

  BONSAI_RETURN (TRUE);
}

/**
 * bonsai_client_get_realm:
 * @self: a #BonsaiClient
 *
 * Gets the realm the client has been placed into.
 *
 * Returns: Either %BONSAI_REALM_ANONYMOUS or %BONSAI_REALM_AUTHORIZED.
 *
 * Since: 0.2
 */
BonsaiRealm
bonsai_client_get_realm (BonsaiClient *self)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), BONSAI_REALM_ANONYMOUS);

  return priv->realm;
}

static gboolean
bonsai_client_timeout_cb (BonsaiClient *self)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT (self));

  priv->timeout_handler = 0;

  bonsai_client_disconnect (self, NULL, NULL);

  BONSAI_RETURN (G_SOURCE_REMOVE);
}

void
_bonsai_client_set_realm (BonsaiClient *self,
                          BonsaiRealm   realm)
{
  BonsaiClientPrivate *priv = bonsai_client_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_CLIENT (self));
  g_return_if_fail (realm == BONSAI_REALM_ANONYMOUS ||
                    realm == BONSAI_REALM_AUTHORIZED);

  priv->realm = realm;

  if (realm == BONSAI_REALM_ANONYMOUS)
    {
      if (priv->timeout_handler == 0)
        priv->timeout_handler =
          g_timeout_add_seconds_full (G_PRIORITY_HIGH,
                                      60,
                                      (GSourceFunc) bonsai_client_timeout_cb,
                                      self,
                                      NULL);
    }
}

/**
 * bonsai_client_from_connection:
 *
 * Finds the client that is associated with @connection.
 *
 * Returns: (nullable) (transfer none): an existing #BonsaiClient or %NULL
 *
 * Since: 0.2
 */
BonsaiClient *
bonsai_client_from_connection (GDBusConnection *connection)
{
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);

  return g_object_get_data (G_OBJECT (connection), "BONSAI_CLIENT");
}

/**
 * bonsai_client_create_progress:
 * @self: a #BonsaiClient
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @error: a location for a #GError, or %NULL
 *
 * Creates a new #BonsaiProgress that can be provided to API allowing
 * progress callbacks. The progress object will be exported on the clients
 * D-Bus connection for use by the peer.
 *
 * Returns: (transfer full): a #BonsaiProgress or %NULL
 *
 * Since: 0.2
 */
BonsaiProgress *
bonsai_client_create_progress (BonsaiClient  *self,
                               GCancellable  *cancellable,
                               GError       **error)
{
  g_autoptr(BonsaiProgress) ret = NULL;
  g_autofree gchar *object_path = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_CLIENT (self), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  ret = bonsai_client_progress_new ();
  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Progress/");

  if (!bonsai_client_export (self,
                             G_DBUS_INTERFACE_SKELETON (ret),
                             object_path,
                             error))
    BONSAI_RETURN (NULL);

  BONSAI_RETURN (g_steal_pointer (&ret));
}
