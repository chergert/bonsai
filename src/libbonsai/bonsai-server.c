/* bonsai-server.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-server"

#include "config.h"

#include "bonsai-anonymous-identity.h"
#include "bonsai-client.h"
#include "bonsai-client-private.h"
#include "bonsai-enums.h"
#include "bonsai-identity.h"
#include "bonsai-marshalers.h"
#include "bonsai-server.h"
#include "bonsai-tls-certificate.h"
#include "bonsai-tls-identity.h"
#include "bonsai-trace.h"

#define MAX_ANONYMOUS 3

typedef struct
{
  GCancellable      *shutdown_cancellable;
  GDBusAuthObserver *auth_observer;
  GTlsCertificate   *certificate;
  GSequence         *clients;
  gchar             *guid;
  GArray            *services;
  guint              anonymous_count;
} BonsaiServerPrivate;

typedef struct
{
  GDBusInterfaceSkeleton *skeleton;
  gchar                  *object_path;
  BonsaiRealm             realm;
} Service;

typedef struct
{
  BonsaiServer         *server;
  GTcpConnection       *tcp;
  GTlsServerConnection *tls;
  GDBusConnection      *dbus;
  BonsaiIdentity       *identity;
  BonsaiAuthorization   auth;
  BonsaiRealm           realm;
  gulong                accept_certificate_handler;
} Incoming;

enum {
  PROP_0,
  PROP_CERTIFICATE,
  N_PROPS
};

enum {
  AUTHORIZE_IDENTITY,
  CLIENT_ADDED,
  CLIENT_REMOVED,
  N_SIGNALS
};

G_DEFINE_TYPE_WITH_PRIVATE (BonsaiServer, bonsai_server, G_TYPE_SOCKET_SERVICE)

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
incoming_free (Incoming *incoming)
{
  BONSAI_ENTRY;

  g_clear_signal_handler (&incoming->accept_certificate_handler, incoming->tls);
  g_clear_object (&incoming->dbus);
  g_clear_object (&incoming->tls);
  g_clear_object (&incoming->tcp);
  g_clear_object (&incoming->server);
  g_clear_object (&incoming->identity);

  BONSAI_EXIT;
}

static void
incoming_unref (gpointer data)
{
  g_atomic_rc_box_release_full (data, (GDestroyNotify)incoming_free);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Incoming, incoming_unref)

static void
clear_service (gpointer data)
{
  Service *service = data;

  g_dbus_interface_skeleton_unexport (service->skeleton);
  g_clear_object (&service->skeleton);
  g_clear_pointer (&service->object_path, g_free);
}

static gboolean
bonsai_server_accept_certificate_cb (GTlsServerConnection *connection,
                                     GTlsCertificate      *certificate,
                                     GTlsCertificateFlags  flags,
                                     Incoming             *incoming)
{
  g_autoptr(BonsaiIdentity) identity = NULL;
  BonsaiAuthorization authorization;

  BONSAI_ENTRY;

  g_assert (G_IS_TLS_SERVER_CONNECTION (connection));
  g_assert (G_IS_TLS_CERTIFICATE (certificate));
  g_assert (incoming != NULL);
  g_assert (BONSAI_IS_SERVER (incoming->server));
  g_assert (G_IS_TCP_CONNECTION (incoming->tcp));
  g_assert (G_IS_TLS_SERVER_CONNECTION (incoming->tls));
  g_assert (incoming->dbus == NULL);
  g_assert (incoming->identity == NULL);

  /* We don't care if the certificate is self-signed/etc here because we're
   * just trying to check to see if the certificate matches against something
   * we've already pinned.
   */

  if (flags & (G_TLS_CERTIFICATE_EXPIRED |
               G_TLS_CERTIFICATE_REVOKED |
               G_TLS_CERTIFICATE_INSECURE |
               G_TLS_CERTIFICATE_NOT_ACTIVATED))
    BONSAI_RETURN (FALSE);

  identity = bonsai_tls_identity_new (certificate);
  authorization = bonsai_server_authorize_identity (incoming->server, identity);

  switch (authorization)
    {
    case BONSAI_AUTHORIZATION_AUTHORIZED:
      incoming->identity = g_steal_pointer (&identity);
      incoming->realm = BONSAI_REALM_AUTHORIZED;
      BONSAI_RETURN (TRUE);

    case BONSAI_AUTHORIZATION_PAIRING:
      incoming->identity = bonsai_anonymous_identity_new ();
      incoming->realm = BONSAI_REALM_ANONYMOUS;
      BONSAI_RETURN (TRUE);

    case BONSAI_AUTHORIZATION_NOT_AUTHORIZED:
    default:
      BONSAI_RETURN (FALSE);
    }

  BONSAI_RETURN (FALSE);
}

static gint
compare_by_pointer (gconstpointer a,
                    gconstpointer b,
                    gpointer      user_data)
{
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
}

static void
bonsai_server_client_closed_cb (BonsaiServer *self,
                                BonsaiClient *client)
{
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);
  g_autoptr(BonsaiClient) hold = NULL;
  GSequenceIter *iter;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER (self));
  g_assert (BONSAI_IS_CLIENT (client));

  /* Keep an extra reference for lifetime of function */
  hold = g_object_ref (client);

  /* Remove the client now that we know it has closed */
  iter = g_sequence_search (priv->clients, client, compare_by_pointer, NULL);
  if (iter != NULL && !g_sequence_iter_is_end (iter))
    g_sequence_remove (iter);

  /* Notify listeners and remove services from client */
  g_signal_emit (self, signals [CLIENT_REMOVED], 0, client);

  BONSAI_EXIT;
}

static void
bonsai_server_dbus_new_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  BonsaiServerPrivate *priv;
  g_autoptr(Incoming) incoming = user_data;
  g_autoptr(GDBusConnection) connection = NULL;
  g_autoptr(BonsaiClient) client = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *hash = NULL;
  GTlsCertificate *certificate;

  BONSAI_ENTRY;

  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (incoming != NULL);
  g_assert (G_IS_TLS_SERVER_CONNECTION (incoming->tls));
  g_assert (BONSAI_IS_SERVER (incoming->server));
  g_assert (BONSAI_IS_IDENTITY (incoming->identity));

  priv = bonsai_server_get_instance_private (incoming->server);

  /* Complete request to establish D-Bus connection over TCP+TLS */
  if (!(connection = g_dbus_connection_new_finish (result, &error)))
    {
      g_warning ("%s", error->message);
      BONSAI_EXIT;
    }

  /* We don't want to abort() when this connection is closed */
  g_dbus_connection_set_exit_on_close (connection, FALSE);

  /* Create client using negotiated identity */
  client = bonsai_client_new_for_connection (incoming->identity, connection);

  /* Notify about new connection */
  certificate = g_tls_connection_get_peer_certificate (G_TLS_CONNECTION (incoming->tls));
  hash = bonsai_tls_certificate_get_hash (certificate);
  g_debug ("Accepted client with certificate hash %s", hash);

  /* Apply the client realm */
  if (incoming->auth == BONSAI_AUTHORIZATION_AUTHORIZED)
    _bonsai_client_set_realm (client, BONSAI_REALM_AUTHORIZED);
  else
    _bonsai_client_set_realm (client, BONSAI_REALM_ANONYMOUS);

  /* Track this client now for future processing */
  g_sequence_insert_sorted (priv->clients,
                            g_object_ref (client),
                            compare_by_pointer,
                            NULL);
  g_signal_connect_object (client,
                           "closed",
                           G_CALLBACK (bonsai_server_client_closed_cb),
                           incoming->server,
                           G_CONNECT_SWAPPED);

  /* Notify listeners and default handler to register services */
  g_signal_emit (incoming->server, signals [CLIENT_ADDED], 0, client);

  /* Now start processing messages */
  BONSAI_TRACE_MSG ("Client accepted, processing messages");
  g_dbus_connection_start_message_processing (connection);

  BONSAI_EXIT;
}

static void
bonsai_server_handshake_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  BonsaiServerPrivate *priv;
  GTlsServerConnection *conn = (GTlsServerConnection *)object;
  g_autoptr(Incoming) incoming = user_data;
  g_autoptr(GError) error = NULL;
  const gchar *negotiated;

  BONSAI_ENTRY;

  g_assert (G_IS_TLS_SERVER_CONNECTION (conn));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (incoming != NULL);
  g_assert (BONSAI_IS_SERVER (incoming->server));
  g_assert (G_IS_TCP_CONNECTION (incoming->tcp));
  g_assert (G_IS_TLS_SERVER_CONNECTION (incoming->tls));

  priv = bonsai_server_get_instance_private (incoming->server);

  if (!g_tls_connection_handshake_finish (G_TLS_CONNECTION (conn), result, &error))
    BONSAI_GOTO (failed);

  if (!(negotiated = g_tls_connection_get_negotiated_protocol (G_TLS_CONNECTION (conn))) ||
      g_strcmp0 (negotiated, "Bonsai/1.0") != 0)
    BONSAI_GOTO (failed);

  if (incoming->identity == NULL)
    BONSAI_GOTO (failed);

  /* Authorize the TLS certificate (or anonymous identity) immediately
   * before attempting to create a D-Bus connection.
   */
  incoming->auth = bonsai_server_authorize_identity (incoming->server, incoming->identity);
  if (incoming->auth == BONSAI_AUTHORIZATION_NOT_AUTHORIZED)
    BONSAI_GOTO (failed);

  g_dbus_connection_new (G_IO_STREAM (conn),
                         priv->guid,
                         (G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_SERVER |
                          G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING |
                          G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_ALLOW_ANONYMOUS),
                         priv->auth_observer,
                         priv->shutdown_cancellable,
                         bonsai_server_dbus_new_cb,
                         g_steal_pointer (&incoming));

  BONSAI_EXIT;

failed:
  BONSAI_TRACE_MSG ("Failed to handshake: %s", error ? error->message : "");

  BONSAI_EXIT;
}

static gboolean
bonsai_server_incoming (GSocketService    *service,
                        GSocketConnection *connection,
                        GObject           *source_object)
{
  static const gchar *advertised[] = { "Bonsai/1.0", NULL };
  BonsaiServer *self = (BonsaiServer *)service;
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);
  g_autoptr(GIOStream) tls = NULL;
  g_autoptr(Incoming) incoming = NULL;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER (self));
  g_assert (G_IS_TCP_CONNECTION (connection));

  g_tcp_connection_set_graceful_disconnect (G_TCP_CONNECTION (connection), TRUE);

  /* Create TLS server connection or bail */
  tls = g_tls_server_connection_new (G_IO_STREAM (connection),
                                     priv->certificate,
                                     &error);
  if (tls == NULL)
    BONSAI_RETURN (TRUE);

  /* Create state for handling the multiple handshakes */
  incoming = g_atomic_rc_box_new0 (Incoming);
  incoming->server = g_object_ref (self);
  incoming->tcp = g_object_ref (G_TCP_CONNECTION (connection));
  incoming->tls = g_object_ref (G_TLS_SERVER_CONNECTION (tls));
  incoming->auth = BONSAI_AUTHORIZATION_NOT_AUTHORIZED;

  /* Make sure we coordinate protocols as part of TLS and ensure
   * that require client-side certificate.
   */
  g_object_set (incoming->tls,
                "advertised-protocols", advertised,
                "authentication-mode", G_TLS_AUTHENTICATION_REQUIRED,
                "database", NULL,
                "require-close-notify", FALSE,
                NULL);

  incoming->accept_certificate_handler =
    g_signal_connect (incoming->tls,
                      "accept-certificate",
                      G_CALLBACK (bonsai_server_accept_certificate_cb),
                      incoming);

  g_tls_connection_handshake_async (G_TLS_CONNECTION (tls),
                                    G_PRIORITY_DEFAULT,
                                    priv->shutdown_cancellable,
                                    bonsai_server_handshake_cb,
                                    g_steal_pointer (&incoming));

  BONSAI_RETURN (TRUE);
}

static BonsaiAuthorization
bonsai_server_real_authorize_identity (BonsaiServer   *server,
                                       BonsaiIdentity *identity)
{
  return BONSAI_AUTHORIZATION_NOT_AUTHORIZED;
}

static void
bonsai_server_client_added (BonsaiServer *self,
                            BonsaiClient *client)
{
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);
  BonsaiRealm realm;

  g_assert (BONSAI_IS_SERVER (self));
  g_assert (BONSAI_IS_CLIENT (client));

  realm = bonsai_client_get_realm (client);

  if (realm == BONSAI_REALM_ANONYMOUS)
    {
      priv->anonymous_count++;

      if (priv->anonymous_count >= MAX_ANONYMOUS)
        {
          bonsai_client_disconnect (client, NULL, NULL);
          BONSAI_EXIT;
        }
    }

  for (guint i = 0; i < priv->services->len; i++)
    {
      const Service *service = &g_array_index (priv->services, Service, i);

      if ((service->realm & realm) != 0)
        bonsai_client_export (client, service->skeleton, service->object_path, NULL);
    }
}

static void
bonsai_server_client_removed (BonsaiServer *self,
                              BonsaiClient *client)
{
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);
  BonsaiRealm realm;

  g_assert (BONSAI_IS_SERVER (self));
  g_assert (BONSAI_IS_CLIENT (client));

  realm = bonsai_client_get_realm (client);

  if (realm == BONSAI_REALM_ANONYMOUS)
    priv->anonymous_count--;

  for (guint i = 0; i < priv->services->len; i++)
    {
      const Service *service = &g_array_index (priv->services, Service, i);

      if ((service->realm & realm) != 0)
        bonsai_client_unexport (client, service->skeleton);
    }
}

static gboolean
bonsai_server_allow_mechanism_cb (BonsaiServer      *self,
                                  const gchar       *mechanism,
                                  GDBusAuthObserver *auth_observer)
{
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER (self));
  g_assert (mechanism != NULL);
  g_assert (G_IS_DBUS_AUTH_OBSERVER (auth_observer));

  BONSAI_TRACE_MSG ("Checking mechanism %s", mechanism);

  /* We handle this externally during TLS */
  ret = g_str_equal (mechanism, "ANONYMOUS");

  BONSAI_RETURN (ret);
}

static gboolean
bonsai_server_authorize_authenticated_peer_cb (BonsaiServer      *self,
                                               GIOStream         *stream,
                                               GCredentials      *credentials,
                                               GDBusAuthObserver *auth_observer)
{
  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER (self));
  g_assert (G_IS_IO_STREAM (stream));
  g_assert (!credentials || G_IS_CREDENTIALS (credentials));
  g_assert (G_IS_DBUS_AUTH_OBSERVER (auth_observer));

  /* We handle this externally during TLS */
  BONSAI_RETURN (TRUE);
}

static void
bonsai_server_dispose (GObject *object)
{
  BonsaiServer *self = (BonsaiServer *)object;
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);

  BONSAI_ENTRY;

  g_cancellable_cancel (priv->shutdown_cancellable);

  if (!g_sequence_is_empty (priv->clients))
    g_sequence_remove_range (g_sequence_get_begin_iter (priv->clients),
                             g_sequence_get_end_iter (priv->clients));

  G_OBJECT_CLASS (bonsai_server_parent_class)->dispose (object);

  BONSAI_EXIT;
}

static void
bonsai_server_finalize (GObject *object)
{
  BonsaiServer *self = (BonsaiServer *)object;
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);

  BONSAI_ENTRY;

  g_clear_object (&priv->shutdown_cancellable);
  g_clear_object (&priv->certificate);
  g_clear_object (&priv->auth_observer);
  g_clear_pointer (&priv->guid, g_free);
  g_clear_pointer (&priv->clients, g_sequence_free);

  G_OBJECT_CLASS (bonsai_server_parent_class)->finalize (object);

  BONSAI_EXIT;
}

static void
bonsai_server_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  BonsaiServer *self = BONSAI_SERVER (object);
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      g_value_set_object (value, priv->certificate);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  BonsaiServer *self = BONSAI_SERVER (object);
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      priv->certificate = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_class_init (BonsaiServerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GSocketServiceClass *socket_service_class = G_SOCKET_SERVICE_CLASS (klass);

  object_class->dispose = bonsai_server_dispose;
  object_class->finalize = bonsai_server_finalize;
  object_class->get_property = bonsai_server_get_property;
  object_class->set_property = bonsai_server_set_property;

  socket_service_class->incoming = bonsai_server_incoming;

  klass->authorize_identity = bonsai_server_real_authorize_identity;
  klass->client_added = bonsai_server_client_added;
  klass->client_removed = bonsai_server_client_removed;

  /**
   * BonsaiServer:certificate:
   *
   * The :certificate property contains the #GTlsCertificate that will be
   * provided to clients during the TLS handshake. Some clients may verify
   * that this certificate is who they enrolled with.
   *
   * Since: 0.2
   */
  properties[PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The certificate for the server",
                         G_TYPE_TLS_CERTIFICATE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * BonsaiServer::authorize-identity:
   * @self: a #BonsaiServer
   * @identity: a #BonsaiIdentity
   *
   * The :authorize-identity signal is emitted to validate if an identity
   * is authorized to access the system.
   */
  signals[AUTHORIZE_IDENTITY] =
    g_signal_new ("authorize-identity",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (BonsaiServerClass, authorize_identity),
                  g_signal_accumulator_first_wins,
                  NULL,
                  _bonsai_marshal_ENUM__OBJECT,
                  BONSAI_TYPE_AUTHORIZATION,
                  1,
                  BONSAI_TYPE_IDENTITY);
  g_signal_set_va_marshaller (signals[AUTHORIZE_IDENTITY],
                              G_TYPE_FROM_CLASS (klass),
                              _bonsai_marshal_ENUM__OBJECTv);

  /**
   * BonsaiServer::client-added:
   * @self: a #BonsaiServer
   * @client: a #BonsaiClient
   *
   * The ::client-added signal is emitted when a client is added.
   *
   * Since: 0.2
   */
  signals[CLIENT_ADDED] =
    g_signal_new ("client-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (BonsaiServerClass, client_added),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, BONSAI_TYPE_CLIENT);

  /**
   * BonsaiServer::client-removed:
   * @self: a #BonsaiServer
   * @client: a #BonsaiClient
   *
   * The ::client-removed signal is emitted when a client is removed.
   *
   * Since: 0.2
   */
  signals[CLIENT_REMOVED] =
    g_signal_new ("client-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (BonsaiServerClass, client_added),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1, BONSAI_TYPE_CLIENT);
}

static void
bonsai_server_init (BonsaiServer *self)
{
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);

  BONSAI_ENTRY;

  priv->guid = g_dbus_generate_guid ();
  priv->shutdown_cancellable = g_cancellable_new ();
  priv->clients = g_sequence_new (g_object_unref);

  priv->services = g_array_new (FALSE, FALSE, sizeof (Service));
  g_array_set_clear_func (priv->services, clear_service);

  priv->auth_observer = g_dbus_auth_observer_new ();
  g_signal_connect_object (priv->auth_observer,
                           "allow-mechanism",
                           G_CALLBACK (bonsai_server_allow_mechanism_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (priv->auth_observer,
                           "authorize-authenticated-peer",
                           G_CALLBACK (bonsai_server_authorize_authenticated_peer_cb),
                           self,
                           G_CONNECT_SWAPPED);

  BONSAI_EXIT;
}

/**
 * bonsai_server_new:
 * @certificate: a #GTlsCertificate
 *
 * Creates a new #BonsaiServer that will receive connections using
 * @certificate as the server-side TLS certificate.
 *
 * Returns: (transfer full): a #BonsaiServer
 *
 * Since: 0.2
 */
BonsaiServer *
bonsai_server_new (GTlsCertificate *certificate)
{
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE (certificate), NULL);

  return g_object_new (BONSAI_TYPE_SERVER,
                       "certificate", certificate,
                       NULL);
}

/**
 * bonsai_server_authorize_identity:
 * @self: a #BonsaiServer
 * @identity: a #BonsaiIdentity
 *
 * Requests the server to authorize the identity by emitting the
 * ::authorize-identity signal.
 *
 * Returns: a #BonsaiAuthorization
 *
 * Since: 0.2
 */
BonsaiAuthorization
bonsai_server_authorize_identity (BonsaiServer   *self,
                                  BonsaiIdentity *identity)
{
  BonsaiAuthorization ret = BONSAI_AUTHORIZATION_NOT_AUTHORIZED;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_SERVER (self), ret);
  g_return_val_if_fail (BONSAI_IS_IDENTITY (identity), ret);

  g_signal_emit (self, signals[AUTHORIZE_IDENTITY], 0, identity, &ret);

  BONSAI_RETURN (ret);
}

/**
 * bonsai_server_export:
 * @self: a #BonsaiServer
 * @skeleton: the service D-Bus interface
 * @object_path: the path to export the object
 *
 * Adds @skeleton to all clients within @realm.
 *
 * As clients are connected, if placed into @realm, they will receive
 * access to the service before message processing starts.
 *
 * Since: 0.2
 */
void
bonsai_server_export (BonsaiServer           *self,
                      BonsaiRealm             realm,
                      GDBusInterfaceSkeleton *skeleton,
                      const gchar            *object_path)
{
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);
  Service service;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_SERVER (self));
  g_return_if_fail (realm & BONSAI_REALM_BOTH);
  g_return_if_fail (G_IS_DBUS_INTERFACE_SKELETON (skeleton));
  g_return_if_fail (object_path != NULL);

  service.skeleton = g_object_ref (skeleton);
  service.object_path = g_strdup (object_path);
  service.realm = realm;

  g_array_append_val (priv->services, service);

  for (GSequenceIter *iter = g_sequence_get_begin_iter (priv->clients);
       !g_sequence_iter_is_end (iter);
       iter = g_sequence_iter_next (iter))
    {
      BonsaiClient *client = g_sequence_get (iter);
      BonsaiRealm client_realm = bonsai_client_get_realm (client);

      if ((client_realm & realm) != 0)
        bonsai_client_export (client, skeleton, object_path, NULL);
    }

  BONSAI_EXIT;
}

/**
 * bonsai_server_unexport:
 * @self: a #BonsaiServer
 * @skeleton: the service D-Bus interface
 *
 * Removes @skeleton from all clients.
 *
 * Since: 0.2
 */
void
bonsai_server_unexport (BonsaiServer           *self,
                        GDBusInterfaceSkeleton *skeleton)
{
  BonsaiServerPrivate *priv = bonsai_server_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_SERVER (self));
  g_return_if_fail (G_IS_DBUS_INTERFACE_SKELETON (skeleton));

  for (GSequenceIter *iter = g_sequence_get_begin_iter (priv->clients);
       !g_sequence_iter_is_end (iter);
       iter = g_sequence_iter_next (iter))
    {
      BonsaiClient *client = g_sequence_get (iter);
      bonsai_client_unexport (client, skeleton);
    }
}
