/* bonsai-types.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _BonsaiAnonymousIdentity BonsaiAnonymousIdentity;
typedef struct _BonsaiClient            BonsaiClient;
typedef struct _BonsaiEndPoint          BonsaiEndPoint;
typedef struct _BonsaiIdentity          BonsaiIdentity;
typedef struct _BonsaiProgress          BonsaiProgress;
typedef struct _BonsaiServer            BonsaiServer;
typedef struct _BonsaiSocketEndPoint    BonsaiSocketEndPoint;
typedef struct _BonsaiTlsIdentity       BonsaiTlsIdentity;

typedef enum
{
  BONSAI_AUTHORIZATION_NOT_AUTHORIZED = 0,
  BONSAI_AUTHORIZATION_AUTHORIZED     = 1,
  BONSAI_AUTHORIZATION_PAIRING        = 2,
} BonsaiAuthorization;

typedef enum
{
  BONSAI_REALM_ANONYMOUS  = 1 << 0,
  BONSAI_REALM_AUTHORIZED = 1 << 1,
  BONSAI_REALM_BOTH       = BONSAI_REALM_ANONYMOUS | BONSAI_REALM_AUTHORIZED,
} BonsaiRealm;

G_END_DECLS
