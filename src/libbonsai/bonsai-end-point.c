/* bonsai-end-point.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-end-point"

#include "config.h"

#include "bonsai-end-point.h"
#include "bonsai-identity.h"

G_DEFINE_ABSTRACT_TYPE (BonsaiEndPoint, bonsai_end_point, G_TYPE_OBJECT)

static void
bonsai_end_point_class_init (BonsaiEndPointClass *klass)
{
}

static void
bonsai_end_point_init (BonsaiEndPoint *self)
{
}

/**
 * bonsai_end_point_connect_async:
 * @self: an #BonsaiEndPoint
 * @identity: a #BonsaiIdentity to use when connecting
 * @peer_identity: a #BonsaiIdentity expected of the peer to use when connecting
 * @cancellable: (nullable): a #GCancellable
 * @callback: a #GAsyncReadyCallback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Since: 0.2
 */
void
bonsai_end_point_connect_async (BonsaiEndPoint      *self,
                                BonsaiIdentity      *identity,
                                BonsaiIdentity      *peer_identity,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_return_if_fail (BONSAI_IS_END_POINT (self));
  g_return_if_fail (BONSAI_IS_IDENTITY (identity));
  g_return_if_fail (!peer_identity || BONSAI_IS_IDENTITY (peer_identity));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  BONSAI_END_POINT_GET_CLASS (self)->connect_async (self,
                                                    identity,
                                                    peer_identity,
                                                    cancellable,
                                                    callback,
                                                    user_data);
}

/**
 * bonsai_end_point_connect_finish:
 * @self: an #BonsaiEndPoint
 * @result: a #GAsyncResult provided to callback
 * @identity: (out) (optional) (transfer full): a #BonsaiIdentity
 * @negotiated_protocol: (out) (optional) (transfer full): location for negotiated protocol
 * @error: a location for a #GError, or %NULL
 *
 * @identity is set to the known identity if discovered when connecting.
 * Otherwise it is set to %NULL.
 *
 * Returns: (transfer full): a #GIOStream or %NULL and @error is set.
 *
 * Since: 0.2
 */
GIOStream *
bonsai_end_point_connect_finish (BonsaiEndPoint  *self,
                                 GAsyncResult    *result,
                                 BonsaiIdentity **identity,
                                 gchar          **negotiated_protocol,
                                 GError         **error)
{
  g_return_val_if_fail (BONSAI_IS_END_POINT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  if (negotiated_protocol != NULL)
    *negotiated_protocol = NULL;

  return BONSAI_END_POINT_GET_CLASS (self)->connect_finish (self,
                                                            result,
                                                            identity,
                                                            negotiated_protocol,
                                                            error);
}
