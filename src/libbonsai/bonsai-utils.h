/* bonsai-utils.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define BONSAI_MACHINE_INFO_KEY_CHASSIS "Chassis"

gchar     *bonsai_get_machine_info      (const gchar  *key);
gchar     *bonsai_object_path_random    (const gchar  *prefix);
GVariant  *bonsai_file_info_serialize   (GFileInfo    *info);
GFileInfo *bonsai_file_info_deserialize (GVariant     *variant,
                                         GError      **error);

G_END_DECLS
