/* bonsai-end-point.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_END_POINT (bonsai_end_point_get_type())

G_DECLARE_DERIVABLE_TYPE (BonsaiEndPoint, bonsai_end_point, BONSAI, END_POINT, GObject)

struct _BonsaiEndPointClass
{
  GObjectClass parent_class;

  void       (* connect_async)  (BonsaiEndPoint       *self,
                                 BonsaiIdentity       *identity,
                                 BonsaiIdentity       *peer_identity,
                                 GCancellable         *cancellable,
                                 GAsyncReadyCallback   callback,
                                 gpointer              user_data);
  GIOStream *(* connect_finish) (BonsaiEndPoint       *self,
                                 GAsyncResult         *result,
                                 BonsaiIdentity      **identity,
                                 gchar               **negotiated_protocol,
                                 GError              **error);
};

void       bonsai_end_point_connect_async  (BonsaiEndPoint       *self,
                                            BonsaiIdentity       *identity,
                                            BonsaiIdentity       *peer_identity,
                                            GCancellable         *cancellable,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data);
GIOStream *bonsai_end_point_connect_finish (BonsaiEndPoint       *self,
                                            GAsyncResult         *result,
                                            BonsaiIdentity      **identity,
                                            gchar               **negotiated_protocol,
                                            GError              **error);

G_END_DECLS
