/* test-server.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "test-server"

#include "config.h"

#include <libbonsai.h>

typedef struct
{
  GMainLoop       *main_loop;
  BonsaiServer    *server;
  GCancellable    *cancellable;
  GTlsCertificate *certificate;
} TestServer;

static void
create_certificate_cb (GObject      *object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  TestServer *state = user_data;
  g_autoptr(GError) error = NULL;
  gboolean r;

  state->certificate = bonsai_tls_certificate_new_from_files_or_generate_finish (result, &error);
  g_assert_no_error (error);
  g_assert_true (G_IS_TLS_CERTIFICATE (state->certificate));

  state->server = bonsai_server_new (state->certificate);
  g_assert_true (BONSAI_IS_SERVER (state->server));

  r = g_socket_listener_add_inet_port (G_SOCKET_LISTENER (state->server), BONSAI_DEFAULT_PORT, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);

  g_socket_service_start (G_SOCKET_SERVICE (state->server));

  g_print ("Running...\n");
}

gint
main (gint   argc,
      gchar *argv[])
{
  TestServer state = {0};

  state.main_loop = g_main_loop_new (NULL, FALSE);
  state.cancellable = g_cancellable_new ();

  bonsai_tls_certificate_new_from_files_or_generate_async (".public-key",
                                                           ".private-key",
                                                           "US",
                                                           "GNOME",
                                                           NULL,
                                                           create_certificate_cb,
                                                           &state);

  g_main_loop_run (state.main_loop);

  g_clear_pointer (&state.main_loop, g_main_loop_unref);
  g_clear_object (&state.server);
  g_clear_object (&state.cancellable);
  g_clear_object (&state.certificate);

  return 0;
}
