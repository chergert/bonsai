/* test-client.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <libbonsai.h>

typedef struct
{
  GMainLoop      *main_loop;
  BonsaiClient   *client;
  BonsaiEndPoint *end_point;
  GError         *error;
} TestClient;

static void
test_client_pairing_begin_cb (GObject      *object,
                              GAsyncResult *result,
                              gpointer      user_data)
{
  BonsaiPairing *proxy = BONSAI_PAIRING (object);
  TestClient *state = user_data;
  gboolean r;

  g_assert (state != NULL);
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (BONSAI_IS_PAIRING (proxy));

  r = bonsai_pairing_call_begin_finish (proxy, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  g_main_loop_quit (state->main_loop);
}

static void
test_client_connect_cb (GObject      *object,
                        GAsyncResult *result,
                        gpointer      user_data)
{
  BonsaiClient *client = BONSAI_CLIENT (object);
  TestClient *state = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(BonsaiPairing) proxy = NULL;
  GDBusConnection *connection;
  gboolean r;

  g_assert (state != NULL);
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (BONSAI_IS_CLIENT (client));

  r = bonsai_client_connect_finish (client, result, &error);
  g_assert_no_error (error);
  g_assert_true (r);

  connection = bonsai_client_get_connection (client);
  proxy = bonsai_pairing_proxy_new_sync (connection,
                                         G_DBUS_PROXY_FLAGS_NONE,
                                         NULL,
                                         "/org/gnome/Bonsai/Pairing",
                                         NULL,
                                         &state->error);
  g_assert_no_error (state->error);
  g_assert_true (BONSAI_IS_PAIRING (proxy));

  bonsai_pairing_call_begin (proxy,
                             NULL,
                             test_client_pairing_begin_cb,
                             state);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(GSocketConnectable) connectable = NULL;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(BonsaiIdentity) identity = NULL;
  g_autoptr(GError) error = NULL;
  TestClient state = {0};
  const gchar *addr;

  if (argc > 1)
    addr = argv[1];
  else
    addr = "127.0.0.1";

  if (!(connectable = g_network_address_parse (addr, BONSAI_DEFAULT_PORT, &error)))
    {
      g_printerr ("%s\n", error->message);
      return 1;
    }

  certificate = bonsai_tls_certificate_new_from_files_or_generate (".public-key",
                                                                   ".private-key",
                                                                   "US",
                                                                   "GNOME",
                                                                   NULL,
                                                                   &error);
  g_assert_no_error (error);
  g_assert_true (G_IS_TLS_CERTIFICATE (certificate));

  identity = bonsai_tls_identity_new (certificate);

  state.error = NULL;
  state.main_loop = g_main_loop_new (NULL, FALSE);
  state.client = bonsai_client_new (identity);
  state.end_point = bonsai_socket_end_point_new (connectable);

  bonsai_client_connect_async (state.client,
                               state.end_point,
                               NULL,
                               NULL,
                               test_client_connect_cb,
                               &state);

  g_main_loop_run (state.main_loop);

  g_clear_pointer (&state.main_loop, g_main_loop_unref);
  g_clear_object (&state.client);
  g_clear_object (&state.end_point);

  return 0;
}
