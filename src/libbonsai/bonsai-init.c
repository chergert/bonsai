/* bonsai-init.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "config.h"

#include "gconstructor.h"

#include "bonsai-client.h"
#include "bonsai-end-point.h"
#include "bonsai-identity.h"
#include "bonsai-server.h"
#include "bonsai-socket-end-point.h"

#if defined (G_HAS_CONSTRUCTORS)
# ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
#  pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(bonsai_init_ctor)
# endif
G_DEFINE_CONSTRUCTOR(bonsai_init_ctor)
#else
# error Your platform/compiler is missing constructor support
#endif

static void
bonsai_init_ctor (void)
{
  g_type_ensure (BONSAI_TYPE_CLIENT);
  g_type_ensure (BONSAI_TYPE_IDENTITY);
  g_type_ensure (BONSAI_TYPE_SERVER);
  g_type_ensure (BONSAI_TYPE_END_POINT);
  g_type_ensure (BONSAI_TYPE_SOCKET_END_POINT);
}
