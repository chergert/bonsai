/* bonsai-client-progress.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-client-progress"

#include "config.h"

#include "bonsai-client-progress.h"
#include "bonsai-trace.h"

struct _BonsaiClientProgress
{
  BonsaiProgressSkeleton parent_instance;
};

static gboolean
bonsai_client_progress_handle_finished (BonsaiProgress        *progress,
                                        GDBusMethodInvocation *invocation)
{
  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_PROGRESS (progress));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (progress));
  bonsai_progress_complete_finished (progress, g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
progress_iface_init (BonsaiProgressIface *iface)
{
  iface->handle_finished = bonsai_client_progress_handle_finished;
}

G_DEFINE_TYPE_WITH_CODE (BonsaiClientProgress,
                         bonsai_client_progress,
                         BONSAI_TYPE_PROGRESS_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_PROGRESS, progress_iface_init))

/**
 * bonsai_client_progress_new:
 *
 * Create a new #BonsaiClientProgress.
 *
 * Returns: (transfer full): a newly created #BonsaiClientProgress
 */
BonsaiProgress *
bonsai_client_progress_new (void)
{
  return g_object_new (BONSAI_TYPE_CLIENT_PROGRESS, NULL);
}

static void
bonsai_client_progress_class_init (BonsaiClientProgressClass *klass)
{
}

static void
bonsai_client_progress_init (BonsaiClientProgress *self)
{
}
