/* bonsai-client.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_CLIENT (bonsai_client_get_type())

G_DECLARE_DERIVABLE_TYPE (BonsaiClient, bonsai_client, BONSAI, CLIENT, GObject)

struct _BonsaiClientClass
{
  GObjectClass parent_class;

  /* Signals */
  void (*closed) (BonsaiClient *self);

  /*< private >*/
  gpointer _reserved[20];
};

BonsaiClient    *bonsai_client_from_connection    (GDBusConnection         *connection);
BonsaiClient    *bonsai_client_new                (BonsaiIdentity          *identity);
BonsaiClient    *bonsai_client_new_for_connection (BonsaiIdentity          *identity,
                                                   GDBusConnection         *connection);
GCancellable    *bonsai_client_get_cancellable    (BonsaiClient            *self);
BonsaiIdentity  *bonsai_client_get_identity       (BonsaiClient            *self);
GDBusConnection *bonsai_client_get_connection     (BonsaiClient            *self);
BonsaiRealm      bonsai_client_get_realm          (BonsaiClient            *self);
BonsaiProgress  *bonsai_client_create_progress    (BonsaiClient            *self,
                                                   GCancellable            *cancellable,
                                                   GError                 **error);
gboolean         bonsai_client_export             (BonsaiClient            *self,
                                                   GDBusInterfaceSkeleton  *interface,
                                                   const gchar             *object_path,
                                                   GError                 **error);
void             bonsai_client_unexport           (BonsaiClient            *self,
                                                   GDBusInterfaceSkeleton  *interface);
gboolean         bonsai_client_connect            (BonsaiClient            *self,
                                                   BonsaiEndPoint          *end_point,
                                                   BonsaiIdentity          *peer_identity,
                                                   GCancellable            *cancellable,
                                                   GError                 **error);
void             bonsai_client_connect_async      (BonsaiClient            *self,
                                                   BonsaiEndPoint          *end_point,
                                                   BonsaiIdentity          *peer_identity,
                                                   GCancellable            *cancellable,
                                                   GAsyncReadyCallback      callback,
                                                   gpointer                 user_data);
gboolean         bonsai_client_connect_finish     (BonsaiClient            *self,
                                                   GAsyncResult            *result,
                                                   GError                 **error);
gboolean         bonsai_client_disconnect         (BonsaiClient            *self,
                                                   GCancellable            *cancellable,
                                                   GError                 **error);

G_END_DECLS
