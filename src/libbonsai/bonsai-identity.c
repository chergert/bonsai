/* bonsai-identity.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-identity"

#include "config.h"

#include "bonsai-identity.h"

typedef struct
{
  gchar *app_id;
} BonsaiIdentityPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (BonsaiIdentity, bonsai_identity, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_APP_ID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
bonsai_identity_finalize (GObject *object)
{
  BonsaiIdentity *self = (BonsaiIdentity *)object;
  BonsaiIdentityPrivate *priv = bonsai_identity_get_instance_private (self);

  g_clear_pointer (&priv->app_id, g_free);

  G_OBJECT_CLASS (bonsai_identity_parent_class)->finalize (object);
}

static void
bonsai_identity_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  BonsaiIdentity *self = BONSAI_IDENTITY (object);

  switch (prop_id)
    {
    case PROP_APP_ID:
      g_value_set_string (value, bonsai_identity_get_app_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_identity_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  BonsaiIdentity *self = BONSAI_IDENTITY (object);

  switch (prop_id)
    {
    case PROP_APP_ID:
      bonsai_identity_set_app_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_identity_class_init (BonsaiIdentityClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_identity_finalize;
  object_class->get_property = bonsai_identity_get_property;
  object_class->set_property = bonsai_identity_set_property;

  properties [PROP_APP_ID] =
    g_param_spec_string ("app-id",
                         "Application Id",
                         "The application id of the peer application",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_identity_init (BonsaiIdentity *self)
{
}

/**
 * bonsai_identity_get_app_id:
 * @self: a #BonsaiIdentity
 *
 * Gets the app-id of the peer application, if known.
 *
 * This is only meaningful within the daemon which may use the information
 * to isolate contents between applications.
 *
 * Client connections which call the org.gnome.Bonsai.Isolation.Isolate
 * method can reduce access to services. This can be done by a proxy before
 * handing off a connection to a sandboxed application.
 *
 * Returns: (nullable): the name of the identity, or %NULL
 */
const gchar *
bonsai_identity_get_app_id (BonsaiIdentity *self)
{
  BonsaiIdentityPrivate *priv = bonsai_identity_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_IDENTITY (self), NULL);

  return priv->app_id;
}

/**
 * bonsai_identity_set_app_id:
 * @self: a #BonsaiIdentity
 * @app_id: the app_id of the client application
 *
 * Sets the app_id for the identity. This should only be used by the
 * Bonsai daemon to track the application used by the client.
 */
void
bonsai_identity_set_app_id (BonsaiIdentity *self,
                            const gchar    *app_id)
{
  BonsaiIdentityPrivate *priv = bonsai_identity_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_IDENTITY (self));

  if (g_strcmp0 (app_id, priv->app_id) != 0)
    {
      g_free (priv->app_id);
      priv->app_id = g_strdup (app_id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_APP_ID]);
    }
}
