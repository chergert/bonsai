/* bonsai-message.h
 *
 * Copyright 2017-2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef BONSAI_MESSAGE_H
#define BONSAI_MESSAGE_H

#include <glib.h>

G_BEGIN_DECLS

#pragma pack(push, 8)
typedef struct
{
  char bytes[8];
} BonsaiMessageMagic;

typedef struct
{
  BonsaiMessageMagic magic;
} BonsaiMessageAny;

typedef struct
{
  BonsaiMessageMagic magic;
  const char *val;
} BonsaiMessagePutString;

typedef struct
{
  BonsaiMessageMagic magic;
  const char * const *val;
} BonsaiMessagePutStrv;

typedef struct
{
  BonsaiMessageMagic magic;
  const char **valptr;
} BonsaiMessageGetString;

typedef struct
{
  BonsaiMessageMagic magic;
  char ***valptr;
} BonsaiMessageGetStrv;

typedef struct
{
  BonsaiMessageMagic magic;
  gint32 val;
} BonsaiMessagePutInt32;

typedef struct
{
  BonsaiMessageMagic magic;
  gint32 *valptr;
} BonsaiMessageGetInt32;

typedef struct
{
  BonsaiMessageMagic magic;
  gint64 val;
} BonsaiMessagePutInt64;

typedef struct
{
  BonsaiMessageMagic magic;
  gint64 *valptr;
} BonsaiMessageGetInt64;

typedef struct
{
  BonsaiMessageMagic magic;
  gboolean val;
} BonsaiMessagePutBoolean;

typedef struct
{
  BonsaiMessageMagic magic;
  gboolean *valptr;
} BonsaiMessageGetBoolean;

typedef struct
{
  BonsaiMessageMagic magic;
  double val;
} BonsaiMessagePutDouble;

typedef struct
{
  BonsaiMessageMagic magic;
  double *valptr;
} BonsaiMessageGetDouble;

typedef struct
{
  BonsaiMessageMagic magic;
  GVariantIter **iterptr;
} BonsaiMessageGetIter;

typedef struct
{
  BonsaiMessageMagic magic;
  GVariantDict **dictptr;
} BonsaiMessageGetDict;

typedef struct
{
  BonsaiMessageMagic magic;
  GVariant **variantptr;
} BonsaiMessageGetVariant;
#pragma pack(pop)

#define _BONSAI_MAGIC(s) ("@!^%" s)
#define _BONSAI_MAGIC_C(a,b,c,d) {'@','!','^','%',a,b,c,d}

#define _BONSAI_MESSAGE_PUT_STRING_MAGIC  _BONSAI_MAGIC("PUTS")
#define _BONSAI_MESSAGE_GET_STRING_MAGIC  _BONSAI_MAGIC("GETS")
#define _BONSAI_MESSAGE_PUT_STRV_MAGIC    _BONSAI_MAGIC("PUTZ")
#define _BONSAI_MESSAGE_GET_STRV_MAGIC    _BONSAI_MAGIC("GETZ")
#define _BONSAI_MESSAGE_PUT_INT32_MAGIC   _BONSAI_MAGIC("PUTI")
#define _BONSAI_MESSAGE_GET_INT32_MAGIC   _BONSAI_MAGIC("GETI")
#define _BONSAI_MESSAGE_PUT_INT64_MAGIC   _BONSAI_MAGIC("PUTX")
#define _BONSAI_MESSAGE_GET_INT64_MAGIC   _BONSAI_MAGIC("GETX")
#define _BONSAI_MESSAGE_PUT_BOOLEAN_MAGIC _BONSAI_MAGIC("PUTB")
#define _BONSAI_MESSAGE_GET_BOOLEAN_MAGIC _BONSAI_MAGIC("GETB")
#define _BONSAI_MESSAGE_PUT_DOUBLE_MAGIC  _BONSAI_MAGIC("PUTD")
#define _BONSAI_MESSAGE_GET_DOUBLE_MAGIC  _BONSAI_MAGIC("GETD")
#define _BONSAI_MESSAGE_GET_ITER_MAGIC    _BONSAI_MAGIC("GETT")
#define _BONSAI_MESSAGE_GET_DICT_MAGIC    _BONSAI_MAGIC("GETC")
#define _BONSAI_MESSAGE_GET_VARIANT_MAGIC _BONSAI_MAGIC("GETV")

#define _BONSAI_MESSAGE_PUT_STRING_MAGIC_C  _BONSAI_MAGIC_C('P','U','T','S')
#define _BONSAI_MESSAGE_GET_STRING_MAGIC_C  _BONSAI_MAGIC_C('G','E','T','S')
#define _BONSAI_MESSAGE_PUT_STRV_MAGIC_C    _BONSAI_MAGIC_C('P','U','T','Z')
#define _BONSAI_MESSAGE_GET_STRV_MAGIC_C    _BONSAI_MAGIC_C('G','E','T','Z')
#define _BONSAI_MESSAGE_PUT_INT32_MAGIC_C   _BONSAI_MAGIC_C('P','U','T','I')
#define _BONSAI_MESSAGE_GET_INT32_MAGIC_C   _BONSAI_MAGIC_C('G','E','T','I')
#define _BONSAI_MESSAGE_PUT_INT64_MAGIC_C   _BONSAI_MAGIC_C('P','U','T','X')
#define _BONSAI_MESSAGE_GET_INT64_MAGIC_C   _BONSAI_MAGIC_C('G','E','T','X')
#define _BONSAI_MESSAGE_PUT_BOOLEAN_MAGIC_C _BONSAI_MAGIC_C('P','U','T','B')
#define _BONSAI_MESSAGE_GET_BOOLEAN_MAGIC_C _BONSAI_MAGIC_C('G','E','T','B')
#define _BONSAI_MESSAGE_PUT_DOUBLE_MAGIC_C  _BONSAI_MAGIC_C('P','U','T','D')
#define _BONSAI_MESSAGE_GET_DOUBLE_MAGIC_C  _BONSAI_MAGIC_C('G','E','T','D')
#define _BONSAI_MESSAGE_GET_ITER_MAGIC_C    _BONSAI_MAGIC_C('G','E','T','T')
#define _BONSAI_MESSAGE_GET_DICT_MAGIC_C    _BONSAI_MAGIC_C('G','E','T','C')
#define _BONSAI_MESSAGE_GET_VARIANT_MAGIC_C _BONSAI_MAGIC_C('G','E','T','V')

#define BONSAI_MESSAGE_NEW(first_, ...) \
  bonsai_message_new(first_, __VA_ARGS__, NULL)
#define BONSAI_MESSAGE_NEW_ARRAY(first_, ...) \
  bonsai_message_new_array(first_, __VA_ARGS__, NULL)
#define BONSAI_MESSAGE_PARSE(message, ...) \
  bonsai_message_parse(message,  __VA_ARGS__, NULL)
#define BONSAI_MESSAGE_PARSE_ARRAY(iter, ...) \
  bonsai_message_parse_array(iter, __VA_ARGS__, NULL)

#define BONSAI_MESSAGE_PUT_STRING(_val) \
  (&((BonsaiMessagePutString) { .magic = {_BONSAI_MESSAGE_PUT_STRING_MAGIC_C}, .val = _val }))
#define BONSAI_MESSAGE_GET_STRING(_valptr) \
  (&((BonsaiMessageGetString) { .magic = {_BONSAI_MESSAGE_GET_STRING_MAGIC_C}, .valptr = _valptr }))

#define BONSAI_MESSAGE_PUT_STRV(_val) \
  (&((BonsaiMessagePutStrv) { .magic = {_BONSAI_MESSAGE_PUT_STRV_MAGIC_C}, .val = _val }))
#define BONSAI_MESSAGE_GET_STRV(_valptr) \
  (&((BonsaiMessageGetStrv) { .magic = {_BONSAI_MESSAGE_GET_STRV_MAGIC_C}, .valptr = _valptr }))

#define BONSAI_MESSAGE_PUT_INT32(_val) \
  (&((BonsaiMessagePutInt32) { .magic = {_BONSAI_MESSAGE_PUT_INT32_MAGIC_C}, .val = _val }))
#define BONSAI_MESSAGE_GET_INT32(_valptr) \
  (&((BonsaiMessageGetInt32) { .magic = {_BONSAI_MESSAGE_GET_INT32_MAGIC_C}, .valptr = _valptr }))

#define BONSAI_MESSAGE_PUT_INT64(_val) \
  (&((BonsaiMessagePutInt64) { .magic = {_BONSAI_MESSAGE_PUT_INT64_MAGIC_C}, .val = _val }))
#define BONSAI_MESSAGE_GET_INT64(_valptr) \
  (&((BonsaiMessageGetInt64) { .magic = {_BONSAI_MESSAGE_GET_INT64_MAGIC_C}, .valptr = _valptr }))

#define BONSAI_MESSAGE_PUT_BOOLEAN(_val) \
  (&((BonsaiMessagePutBoolean) { .magic = {_BONSAI_MESSAGE_PUT_BOOLEAN_MAGIC_C}, .val = _val }))
#define BONSAI_MESSAGE_GET_BOOLEAN(_valptr) \
  (&((BonsaiMessageGetBoolean) { .magic = {_BONSAI_MESSAGE_GET_BOOLEAN_MAGIC_C}, .valptr = _valptr }))

#define BONSAI_MESSAGE_PUT_DOUBLE(_val) \
  (&((BonsaiMessagePutDouble) { .magic = {_BONSAI_MESSAGE_PUT_DOUBLE_MAGIC_C}, .val = _val }))
#define BONSAI_MESSAGE_GET_DOUBLE(_valptr) \
  (&((BonsaiMessageGetDouble) { .magic = {_BONSAI_MESSAGE_GET_DOUBLE_MAGIC_C}, .valptr = _valptr }))

#define BONSAI_MESSAGE_GET_ITER(_valptr) \
  (&((BonsaiMessageGetIter) { .magic = {_BONSAI_MESSAGE_GET_ITER_MAGIC_C}, .iterptr = _valptr }))

#define BONSAI_MESSAGE_GET_DICT(_valptr) \
  (&((BonsaiMessageGetDict) { .magic = {_BONSAI_MESSAGE_GET_DICT_MAGIC_C}, .dictptr = _valptr }))

#define BONSAI_MESSAGE_GET_VARIANT(_valptr) \
  (&((BonsaiMessageGetVariant) { .magic = {_BONSAI_MESSAGE_GET_VARIANT_MAGIC_C}, .variantptr = _valptr }))

GVariant *bonsai_message_new         (gconstpointer first_param, ...) G_GNUC_NULL_TERMINATED;

GVariant *bonsai_message_new_array   (gconstpointer first_param, ...) G_GNUC_NULL_TERMINATED;

gboolean  bonsai_message_parse       (GVariant *message, ...) G_GNUC_NULL_TERMINATED;

gboolean  bonsai_message_parse_array (GVariantIter *iter, ...) G_GNUC_NULL_TERMINATED;

G_END_DECLS

#endif /* BONSAI_MESSAGE_H */
