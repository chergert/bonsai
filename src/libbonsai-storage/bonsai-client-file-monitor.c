/* bonsai-client-file-monitor.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-client-file-monitor"

#include "config.h"

#include "bonsai-client-file-monitor.h"
#include "bonsai-file-monitor.h"
#include "bonsai-trace.h"

struct _BonsaiClientFileMonitor
{
  GFileMonitor       parent_instance;
  BonsaiFileMonitor *proxy;
};

G_DEFINE_TYPE (BonsaiClientFileMonitor, bonsai_client_file_monitor, G_TYPE_FILE_MONITOR)

enum {
  PROP_0,
  PROP_PROXY,
  N_PROPS,

  PROP_RATE_LIMIT,
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_client_file_monitor_new:
 * @connection: a #GDBusConnection
 * @object_path: the object path on the bus
 * @error: a location for a #GError
 *
 * Create a new #BonsaiClientFileMonitor.
 *
 * Returns: (transfer full): a newly created #BonsaiClientFileMonitor
 */
GFileMonitor *
bonsai_client_file_monitor_new (GDBusConnection  *connection,
                                const gchar      *object_path,
                                GError          **error)
{
  g_autoptr(BonsaiFileMonitor) proxy = NULL;

  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);
  g_return_val_if_fail (object_path, NULL);

  proxy = bonsai_file_monitor_proxy_new_sync (connection,
                                              G_DBUS_PROXY_FLAGS_NONE,
                                              NULL,
                                              object_path,
                                              NULL,
                                              error);

  if (proxy == NULL)
    return NULL;

  return g_object_new (BONSAI_TYPE_CLIENT_FILE_MONITOR,
                       "proxy", proxy,
                       NULL);
}

static gboolean
bonsai_client_file_monitor_cancel (GFileMonitor *file_monitor)
{
  BonsaiClientFileMonitor *self = (BonsaiClientFileMonitor *)file_monitor;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_MONITOR (self));

  if (self->proxy != NULL)
    bonsai_file_monitor_call_cancel_sync (self->proxy, NULL, NULL);

  BONSAI_RETURN (TRUE);
}

static void
bonsai_client_file_monitor_changed_cb (BonsaiClientFileMonitor *self,
                                       const gchar             *path,
                                       const gchar             *other_path,
                                       guint                    event_type,
                                       BonsaiFileMonitor       *proxy)
{
  g_autofree gchar *file_uri = NULL;
  g_autoptr(GFile) file = NULL;
  g_autoptr(GFile) other_file = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_MONITOR (self));
  g_assert (path != NULL);
  g_assert (other_path != NULL);
  g_assert (BONSAI_IS_FILE_MONITOR_PROXY (proxy));

  if (path[0] == 0)
    BONSAI_GOTO (failure);

  file_uri = g_strdup_printf ("bonsai://%s", path);
  file = g_file_new_for_uri (file_uri);

  if (other_path[0] != 0)
    {
      g_autofree gchar *other_uri = g_strdup_printf ("bonsai://%s", other_path);
      other_file = g_file_new_for_uri (other_uri);
    }

  event_type &= (G_FILE_MONITOR_EVENT_CHANGED |
                 G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT |
                 G_FILE_MONITOR_EVENT_DELETED |
                 G_FILE_MONITOR_EVENT_CREATED |
                 G_FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED |
                 G_FILE_MONITOR_EVENT_PRE_UNMOUNT |
                 G_FILE_MONITOR_EVENT_UNMOUNTED |
                 G_FILE_MONITOR_EVENT_MOVED |
                 G_FILE_MONITOR_EVENT_RENAMED |
                 G_FILE_MONITOR_EVENT_MOVED_IN |
                 G_FILE_MONITOR_EVENT_MOVED_OUT);

  if (event_type == 0)
    BONSAI_GOTO (failure);

  g_file_monitor_emit_event (G_FILE_MONITOR (self), file, other_file, event_type);

failure:
  BONSAI_EXIT;
}

static gint
bonsai_client_file_monitor_get_rate_limit (BonsaiClientFileMonitor *self)
{
  g_assert (BONSAI_IS_CLIENT_FILE_MONITOR (self));

  if (self->proxy == NULL)
    return 0;

  return bonsai_file_monitor_get_rate_limit (self->proxy);
}

static void
bonsai_client_file_monitor_set_rate_limit (BonsaiClientFileMonitor *self,
                                           gint                     rate_limit)
{
  g_assert (BONSAI_IS_CLIENT_FILE_MONITOR (self));

  if (self->proxy == NULL)
    return;

  bonsai_file_monitor_set_rate_limit (self->proxy, rate_limit);
}

static void
bonsai_client_file_monitor_finalize (GObject *object)
{
  BonsaiClientFileMonitor *self = (BonsaiClientFileMonitor *)object;

  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (bonsai_client_file_monitor_parent_class)->finalize (object);
}

static void
bonsai_client_file_monitor_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  BonsaiClientFileMonitor *self = BONSAI_CLIENT_FILE_MONITOR (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      g_value_set_object (value, bonsai_client_file_monitor_get_proxy (self));
      break;

    case PROP_RATE_LIMIT:
      g_value_set_int (value, bonsai_client_file_monitor_get_rate_limit (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_file_monitor_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  BonsaiClientFileMonitor *self = BONSAI_CLIENT_FILE_MONITOR (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      self->proxy = g_value_dup_object (value);
      if (self->proxy != NULL)
        g_signal_connect_object (self->proxy,
                                 "changed",
                                 G_CALLBACK (bonsai_client_file_monitor_changed_cb),
                                 self,
                                 G_CONNECT_SWAPPED);
      break;

    case PROP_RATE_LIMIT:
      bonsai_client_file_monitor_set_rate_limit (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_file_monitor_class_init (BonsaiClientFileMonitorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GFileMonitorClass *file_monitor_class = G_FILE_MONITOR_CLASS (klass);

  object_class->finalize = bonsai_client_file_monitor_finalize;
  object_class->get_property = bonsai_client_file_monitor_get_property;
  object_class->set_property = bonsai_client_file_monitor_set_property;

  file_monitor_class->cancel = bonsai_client_file_monitor_cancel;

  properties [PROP_PROXY] =
    g_param_spec_object ("proxy",
                         "Proxy",
                         "The proxy to communicate with the peer",
                         BONSAI_TYPE_FILE_MONITOR_PROXY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  g_object_class_override_property (object_class, PROP_RATE_LIMIT, "rate-limit");
}

static void
bonsai_client_file_monitor_init (BonsaiClientFileMonitor *self)
{
}

/**
 * bonsai_client_file_monitor_get_proxy:
 *
 * Gets the :proxy property.
 *
 * Returns: (transfer none): a #BonsaiFileMonitorProxy
 *
 * Since: 0.2
 */
BonsaiFileMonitor *
bonsai_client_file_monitor_get_proxy (BonsaiClientFileMonitor *self)
{
  g_return_val_if_fail (BONSAI_IS_CLIENT_FILE_MONITOR (self), NULL);

  return self->proxy;
}
