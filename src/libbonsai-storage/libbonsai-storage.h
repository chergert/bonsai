/* libbonsai-storage.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <libbonsai.h>

G_BEGIN_DECLS

#include "bonsai-client-file-enumerator.h"
#include "bonsai-client-file-monitor.h"
#include "bonsai-client-input-stream.h"
#include "bonsai-client-output-stream.h"
#include "bonsai-file-enumerator.h"
#include "bonsai-file-monitor.h"
#include "bonsai-input-stream.h"
#include "bonsai-output-stream.h"
#include "bonsai-server-file-enumerator.h"
#include "bonsai-server-file-monitor.h"
#include "bonsai-server-input-stream.h"
#include "bonsai-server-output-stream.h"
#include "bonsai-storage.h"

G_END_DECLS
