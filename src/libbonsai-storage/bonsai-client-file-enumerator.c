/* bonsai-client-file-enumerator.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-client-file-enumerator"

#include "config.h"

#include "bonsai-client-file-enumerator.h"
#include "bonsai-file-enumerator.h"
#include "bonsai-trace.h"
#include "bonsai-utils.h"

struct _BonsaiClientFileEnumerator
{
  GFileEnumerator       parent_instance;
  BonsaiFileEnumerator *proxy;
  GQueue                infos;
  guint                 in_disposal : 1;
};

G_DEFINE_TYPE (BonsaiClientFileEnumerator, bonsai_client_file_enumerator, G_TYPE_FILE_ENUMERATOR)

enum {
  PROP_0,
  PROP_PROXY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
bonsai_client_file_enumerator_close_fn (GFileEnumerator  *enumerator,
                                        GCancellable     *cancellable,
                                        GError          **error)
{
  BonsaiClientFileEnumerator *self = (BonsaiClientFileEnumerator *)enumerator;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_ENUMERATOR (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (self->proxy == NULL)
    BONSAI_RETURN (TRUE);

  /* If we're in disposal, we want to try to fire this off and ignore the
   * response, or else we risk keeping this object alive for too long. We
   * can just fire off the async request to the peer and return now. For
   * situations where people care about cleanly closing, they can call
   * close manually to get the expected behavior.
   */
  if (self->in_disposal)
    {
      bonsai_file_enumerator_call_close (self->proxy, NULL, NULL, NULL);
      BONSAI_RETURN (TRUE);
    }
  else
    {
      gboolean ret = bonsai_file_enumerator_call_close_sync (self->proxy, cancellable, error);
      BONSAI_RETURN (ret);
    }
}

static void
bonsai_client_file_enumerator_close_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  BonsaiFileEnumerator *proxy = (BonsaiFileEnumerator *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_FILE_ENUMERATOR_PROXY (proxy));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_file_enumerator_call_close_finish (proxy, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

static void
bonsai_client_file_enumerator_close_async (GFileEnumerator     *enumerator,
                                           int                  io_priority,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  BonsaiClientFileEnumerator *self = (BonsaiClientFileEnumerator *)enumerator;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_ENUMERATOR (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_priority (task, io_priority);

  bonsai_file_enumerator_call_close (self->proxy,
                                     cancellable,
                                     bonsai_client_file_enumerator_close_cb,
                                     g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gboolean
bonsai_client_file_enumerator_close_finish (GFileEnumerator  *enumerator,
                                            GAsyncResult     *result,
                                            GError          **error)
{
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

static GFileInfo *
bonsai_client_file_enumerator_next_file (GFileEnumerator  *enumerator,
                                         GCancellable     *cancellable,
                                         GError          **error)
{
  BonsaiClientFileEnumerator *self = (BonsaiClientFileEnumerator *)enumerator;
  GFileInfo *ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_ENUMERATOR (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (g_file_enumerator_is_closed (enumerator))
    return NULL;

  if (g_queue_is_empty (&self->infos))
    {
      g_autoptr(GVariant) infos = NULL;
      GVariantIter iter;
      GVariant *info;

      if (!bonsai_file_enumerator_call_next_files_sync (self->proxy, 100, &infos, cancellable, error))
        BONSAI_RETURN (NULL);

      g_variant_iter_init (&iter, infos);
      while (g_variant_iter_loop (&iter, "@(suv)", &info))
        {
          GFileInfo *file_info;

          if (!(file_info = bonsai_file_info_deserialize (info, error)))
            BONSAI_RETURN (NULL);

          g_queue_push_tail (&self->infos, file_info);
        }
    }

  ret = g_queue_peek_head (&self->infos);

  BONSAI_RETURN (ret);
}

static void
free_info_list (gpointer data)
{
  g_list_free_full (data, g_object_unref);
}

static void
bonsai_client_file_enumerator_next_files_cb (GObject      *object,
                                             GAsyncResult *result,
                                             gpointer      user_data)
{
  BonsaiFileEnumerator *proxy = (BonsaiFileEnumerator *)object;
  g_autoqueue(GFileInfo) ret = NULL;
  g_autoptr(GVariant) infos = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  GVariantIter iter;
  gpointer infoptr;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_FILE_ENUMERATOR (proxy));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_file_enumerator_call_next_files_finish (proxy, &infos, result, &error))
    BONSAI_GOTO (return_error);

  ret = g_queue_new ();

  g_variant_iter_init (&iter, infos);
  while ((infoptr = g_variant_iter_next_value (&iter)))
    {
      g_autoptr(GVariant) info = infoptr;
      GFileInfo *file_info;

      if (!g_variant_is_of_type (info, G_VARIANT_TYPE ("a(suv)")))
        {
          g_set_error (&error,
                       G_DBUS_ERROR,
                       G_DBUS_ERROR_INVALID_ARGS,
                       "Invalid type %s for info",
                       g_variant_get_type_string (info));
          BONSAI_GOTO (return_error);
        }

      if (!(file_info = bonsai_file_info_deserialize (info, &error)))
        BONSAI_GOTO (return_error);

      g_queue_push_tail (ret, file_info);
    }

  g_task_return_pointer (task, ret->head, free_info_list);

  ret->head = NULL;
  ret->tail = NULL;
  ret->length = 0;

  BONSAI_EXIT;

return_error:
  g_task_return_error (task, g_steal_pointer (&error));

  BONSAI_EXIT;
}

static void
bonsai_client_file_enumerator_next_files_async (GFileEnumerator     *enumerator,
                                                int                  num_files,
                                                int                  io_priority,
                                                GCancellable        *cancellable,
                                                GAsyncReadyCallback  callback,
                                                gpointer             user_data)
{
  BonsaiClientFileEnumerator *self = (BonsaiClientFileEnumerator *)enumerator;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_ENUMERATOR (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_file_enumerator_next_files_async);
  g_task_set_priority (task, io_priority);

  if (!g_queue_is_empty (&self->infos))
    {
      GList *stolen = self->infos.head;

      self->infos.head = NULL;
      self->infos.tail = NULL;
      self->infos.length = 0;

      g_task_return_pointer (task, stolen, free_info_list);

      BONSAI_EXIT;
    }

  if (num_files == 0)
    num_files = 100;

  bonsai_file_enumerator_call_next_files (self->proxy,
                                          num_files,
                                          cancellable,
                                          bonsai_client_file_enumerator_next_files_cb,
                                          g_steal_pointer (&task));

  BONSAI_EXIT;
}

static GList *
bonsai_client_file_enumerator_next_files_finish (GFileEnumerator  *enumerator,
                                                 GAsyncResult     *result,
                                                 GError          **error)
{
  GList *ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_pointer (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

/**
 * bonsai_client_file_enumerator_new:
 * @connection: a #GDBusConnection
 * @object_path: the path on the bus
 * @cancellable: (nullable): a #GCancellable or %NULL
 *
 * Create a new #BonsaiClientFileEnumerator.
 *
 * Returns: (transfer full): a newly created #BonsaiClientFileEnumerator
 */
GFileEnumerator *
bonsai_client_file_enumerator_new (GDBusConnection  *connection,
                                   const gchar      *object_path,
                                   GCancellable     *cancellable,
                                   GError          **error)
{
  g_autoptr(BonsaiFileEnumerator) proxy = NULL;

  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);
  g_return_val_if_fail (object_path != NULL, NULL);

  proxy = bonsai_file_enumerator_proxy_new_sync (connection,
                                                 (G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
                                                  G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS),
                                                 NULL,
                                                 object_path,
                                                 cancellable,
                                                 error);
  if (proxy == NULL)
    return NULL;

  g_return_val_if_fail (BONSAI_IS_FILE_ENUMERATOR_PROXY (proxy), NULL);

  return g_object_new (BONSAI_TYPE_CLIENT_FILE_ENUMERATOR,
                       "proxy", proxy,
                       NULL);
}

static void
bonsai_client_file_enumerator_dispose (GObject *object)
{
  BonsaiClientFileEnumerator *self = (BonsaiClientFileEnumerator *)object;

  BONSAI_ENTRY;

  self->in_disposal = TRUE;

  G_OBJECT_CLASS (bonsai_client_file_enumerator_parent_class)->dispose (object);

  BONSAI_EXIT;
}

static void
bonsai_client_file_enumerator_finalize (GObject *object)
{
  BonsaiClientFileEnumerator *self = (BonsaiClientFileEnumerator *)object;

  BONSAI_ENTRY;

  g_queue_clear_full (&self->infos, g_object_unref);
  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (bonsai_client_file_enumerator_parent_class)->finalize (object);

  BONSAI_EXIT;
}

static void
bonsai_client_file_enumerator_get_property (GObject    *object,
                                            guint       prop_id,
                                            GValue     *value,
                                            GParamSpec *pspec)
{
  BonsaiClientFileEnumerator *self = BONSAI_CLIENT_FILE_ENUMERATOR (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      g_value_set_object (value, self->proxy);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_file_enumerator_set_property (GObject      *object,
                                            guint         prop_id,
                                            const GValue *value,
                                            GParamSpec   *pspec)
{
  BonsaiClientFileEnumerator *self = BONSAI_CLIENT_FILE_ENUMERATOR (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      self->proxy = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_file_enumerator_class_init (BonsaiClientFileEnumeratorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GFileEnumeratorClass *enumerator_class = G_FILE_ENUMERATOR_CLASS (klass);

  object_class->dispose = bonsai_client_file_enumerator_dispose;
  object_class->finalize = bonsai_client_file_enumerator_finalize;
  object_class->get_property = bonsai_client_file_enumerator_get_property;
  object_class->set_property = bonsai_client_file_enumerator_set_property;

  enumerator_class->close_fn = bonsai_client_file_enumerator_close_fn;
  enumerator_class->close_async = bonsai_client_file_enumerator_close_async;
  enumerator_class->close_finish = bonsai_client_file_enumerator_close_finish;
  enumerator_class->next_file = bonsai_client_file_enumerator_next_file;
  enumerator_class->next_files_async = bonsai_client_file_enumerator_next_files_async;
  enumerator_class->next_files_finish = bonsai_client_file_enumerator_next_files_finish;

  properties [PROP_PROXY] =
    g_param_spec_object ("proxy",
                         "Proxy",
                         "Proxy",
                         BONSAI_TYPE_FILE_ENUMERATOR_PROXY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_client_file_enumerator_init (BonsaiClientFileEnumerator *self)
{
}
