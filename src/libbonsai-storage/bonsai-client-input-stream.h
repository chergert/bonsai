/* bonsai-client-input-stream.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define BONSAI_TYPE_CLIENT_INPUT_STREAM (bonsai_client_input_stream_get_type())

G_DECLARE_FINAL_TYPE (BonsaiClientInputStream, bonsai_client_input_stream, BONSAI, CLIENT_INPUT_STREAM, GInputStream)

GInputStream    *bonsai_client_input_stream_new             (GDBusConnection         *connection,
                                                             const gchar             *object_path);
GDBusConnection *bonsai_client_input_stream_get_connection  (BonsaiClientInputStream *self);
const gchar     *bonsai_client_input_stream_get_object_path (BonsaiClientInputStream *self);

G_END_DECLS
