/* bonsai-client-output-stream.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-client-output-stream"

#include "config.h"

#include "bonsai-client-output-stream.h"
#include "bonsai-output-stream.h"
#include "bonsai-trace.h"

struct _BonsaiClientOutputStream
{
  GOutputStream            parent_instance;
  BonsaiOutputStreamProxy *proxy;
};

static void seekable_iface_init (GSeekableIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiClientOutputStream, bonsai_client_output_stream, G_TYPE_OUTPUT_STREAM,
                         G_IMPLEMENT_INTERFACE (G_TYPE_SEEKABLE, seekable_iface_init))

enum {
  PROP_0,
  PROP_PROXY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_client_output_stream_new:
 * @connection: a #GDBusConnection
 * @object_path: an object path on the bus
 *
 * Create a new #BonsaiClientOutputStream.
 *
 * Returns: (transfer full): a newly created #BonsaiClientOutputStream
 *
 * Since: 0.2
 */
GOutputStream *
bonsai_client_output_stream_new (GDBusConnection *connection,
                                 const gchar     *object_path)
{
  g_autoptr(BonsaiOutputStream) proxy = NULL;

  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);
  g_return_val_if_fail (object_path != NULL, NULL);

  proxy = bonsai_output_stream_proxy_new_sync (connection,
                                               (G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START_AT_CONSTRUCTION |
                                                G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
                                                G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS),
                                               NULL,
                                               object_path,
                                               NULL,
                                               NULL);

  g_return_val_if_fail (BONSAI_IS_OUTPUT_STREAM_PROXY (proxy), NULL);

  g_object_set (proxy,
                "g-default-timeout", 60 * 1000,
                NULL);

  return g_object_new (BONSAI_TYPE_CLIENT_OUTPUT_STREAM,
                       "proxy", proxy,
                       NULL);
}

static gssize
bonsai_client_output_stream_write_fn (GOutputStream  *stream,
                                      const void     *buffer,
                                      gsize           count,
                                      GCancellable   *cancellable,
                                      GError        **error)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)stream;
  g_autoptr(GVariant) vbuffer = NULL;
  guint64 n_written = 0;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_assert (buffer != NULL);
  g_assert (count > 0);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  vbuffer = g_variant_take_ref (g_variant_new_fixed_array (G_VARIANT_TYPE_BYTE,
                                                           buffer,
                                                           count,
                                                           sizeof (guint8)));

  if (!bonsai_output_stream_call_write_sync (BONSAI_OUTPUT_STREAM (self->proxy),
                                             vbuffer,
                                             &n_written,
                                             cancellable,
                                             error))
    return -1;

  BONSAI_RETURN (n_written);
}

static gboolean
bonsai_client_output_stream_close_fn (GOutputStream  *stream,
                                      GCancellable   *cancellable,
                                      GError        **error)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)stream;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  ret = bonsai_output_stream_call_close_sync (BONSAI_OUTPUT_STREAM (self->proxy),
                                              cancellable,
                                              error);

  BONSAI_RETURN (ret);
}

static gboolean
bonsai_client_output_stream_flush (GOutputStream  *stream,
                                   GCancellable   *cancellable,
                                   GError        **error)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)stream;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  ret = bonsai_output_stream_call_flush_sync (BONSAI_OUTPUT_STREAM (self->proxy),
                                              cancellable,
                                              error);

  BONSAI_RETURN (ret);
}

static void
bonsai_client_output_stream_write_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  BonsaiOutputStream *stream = (BonsaiOutputStream *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  guint64 written;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_output_stream_call_write_finish (stream, &written, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_int (task, written);

  BONSAI_EXIT;
}

static void
bonsai_client_output_stream_write_async (GOutputStream       *stream,
                                         const void          *buffer,
                                         gsize                count,
                                         int                  io_priority,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)stream;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_output_stream_write_async);

  bonsai_output_stream_call_write (BONSAI_OUTPUT_STREAM (self->proxy),
                                   g_variant_new_fixed_array (G_VARIANT_TYPE_BYTE, buffer, count, sizeof (guint8)),
                                   cancellable,
                                   bonsai_client_output_stream_write_cb,
                                   g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gssize
bonsai_client_output_stream_write_finish (GOutputStream  *stream,
                                          GAsyncResult   *result,
                                          GError        **error)
{
  gssize ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (stream));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_int (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

static void
bonsai_client_output_stream_flush_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  BonsaiOutputStream *stream = (BonsaiOutputStream *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_output_stream_call_flush_finish (stream, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

static void
bonsai_client_output_stream_flush_async (GOutputStream       *stream,
                                         int                  io_priority,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)stream;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_output_stream_flush_async);

  bonsai_output_stream_call_flush (BONSAI_OUTPUT_STREAM (self->proxy),
                                   cancellable,
                                   bonsai_client_output_stream_flush_cb,
                                   g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gboolean
bonsai_client_output_stream_flush_finish (GOutputStream  *stream,
                                          GAsyncResult   *result,
                                          GError        **error)
{
  gssize ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (stream));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

static void
bonsai_client_output_stream_close_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  BonsaiOutputStream *stream = (BonsaiOutputStream *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_output_stream_call_close_finish (stream, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

static void
bonsai_client_output_stream_close_async (GOutputStream       *stream,
                                         int                  io_priority,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)stream;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_output_stream_close_async);

  bonsai_output_stream_call_close (BONSAI_OUTPUT_STREAM (self->proxy),
                                   cancellable,
                                   bonsai_client_output_stream_close_cb,
                                   g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gboolean
bonsai_client_output_stream_close_finish (GOutputStream  *stream,
                                          GAsyncResult   *result,
                                          GError        **error)
{
  gssize ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (stream));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

static void
bonsai_client_output_stream_finalize (GObject *object)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)object;

  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (bonsai_client_output_stream_parent_class)->finalize (object);
}

static void
bonsai_client_output_stream_get_property (GObject    *object,
                                          guint       prop_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
  BonsaiClientOutputStream *self = BONSAI_CLIENT_OUTPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      g_value_set_object (value, self->proxy);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_output_stream_set_property (GObject      *object,
                                          guint         prop_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
  BonsaiClientOutputStream *self = BONSAI_CLIENT_OUTPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      self->proxy = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_output_stream_class_init (BonsaiClientOutputStreamClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GOutputStreamClass *output_stream_class = G_OUTPUT_STREAM_CLASS (klass);

  object_class->finalize = bonsai_client_output_stream_finalize;
  object_class->get_property = bonsai_client_output_stream_get_property;
  object_class->set_property = bonsai_client_output_stream_set_property;

  output_stream_class->write_fn = bonsai_client_output_stream_write_fn;
  output_stream_class->close_fn = bonsai_client_output_stream_close_fn;
  output_stream_class->flush = bonsai_client_output_stream_flush;
  output_stream_class->write_async = bonsai_client_output_stream_write_async;
  output_stream_class->write_finish = bonsai_client_output_stream_write_finish;
  output_stream_class->flush_async = bonsai_client_output_stream_flush_async;
  output_stream_class->flush_finish = bonsai_client_output_stream_flush_finish;
  output_stream_class->close_async = bonsai_client_output_stream_close_async;
  output_stream_class->close_finish = bonsai_client_output_stream_close_finish;

  /**
   * BonsaiClientOutputStream:proxy:
   *
   * The :proxy property is used to communicate with the D-Bus peer.
   *
   * Since: 0.2
   */
  properties [PROP_PROXY] =
    g_param_spec_object ("proxy",
                         "Proxy",
                         "The proxy to communicate with the peer.",
                         BONSAI_TYPE_OUTPUT_STREAM_PROXY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_client_output_stream_init (BonsaiClientOutputStream *self)
{
}

static gboolean
bonsai_client_output_stream_can_seek (GSeekable *seekable)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)seekable;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));

  return bonsai_output_stream_get_can_seek (BONSAI_OUTPUT_STREAM (self->proxy));
}

static gboolean
bonsai_client_output_stream_can_truncate (GSeekable *seekable)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)seekable;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));

  return bonsai_output_stream_get_can_truncate (BONSAI_OUTPUT_STREAM (self->proxy));
}

static gboolean
bonsai_client_output_stream_seek (GSeekable     *seekable,
                                  goffset        offset,
                                  GSeekType      type,
                                  GCancellable  *cancellable,
                                  GError       **error)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)seekable;
  guint64 position = 0;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  ret = bonsai_output_stream_call_seek_sync (BONSAI_OUTPUT_STREAM (self->proxy),
                                             offset,
                                             type,
                                             &position,
                                             cancellable,
                                             error);

  BONSAI_RETURN (ret);
}

static goffset
bonsai_client_output_stream_tell (GSeekable *seekable)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)seekable;
  guint64 position = 0;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));

  if (!bonsai_output_stream_call_tell_sync (BONSAI_OUTPUT_STREAM (self->proxy), &position, NULL, NULL))
    BONSAI_RETURN (-1);
  else
    BONSAI_RETURN (position);
}

static gboolean
bonsai_client_output_stream_truncate_fn (GSeekable     *seekable,
                                         goffset        offset,
                                         GCancellable  *cancellable,
                                         GError       **error)
{
  BonsaiClientOutputStream *self = (BonsaiClientOutputStream *)seekable;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_OUTPUT_STREAM (self));

  ret = bonsai_output_stream_call_truncate_sync (BONSAI_OUTPUT_STREAM (self->proxy), offset, cancellable, error);

  BONSAI_RETURN (ret);
}

static void
seekable_iface_init (GSeekableIface *iface)
{
  iface->can_seek = bonsai_client_output_stream_can_seek;
  iface->can_truncate = bonsai_client_output_stream_can_truncate;
  iface->seek = bonsai_client_output_stream_seek;
  iface->tell = bonsai_client_output_stream_tell;
  iface->truncate_fn = bonsai_client_output_stream_truncate_fn;
}
