/* bonsai-server-output-stream.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-server-output-stream"

#include "config.h"

#include "bonsai-trace.h"

#include "bonsai-server-output-stream.h"

struct _BonsaiServerOutputStream
{
  BonsaiOutputStreamSkeleton  parent_instance;
  GOutputStream              *base_stream;
};

static void output_stream_iface_init (BonsaiOutputStreamIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiServerOutputStream,
                         bonsai_server_output_stream,
                         BONSAI_TYPE_OUTPUT_STREAM_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_OUTPUT_STREAM,
                                                output_stream_iface_init))

enum {
  PROP_0,
  PROP_BASE_STREAM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_server_output_stream_new:
 *
 * Create a new #BonsaiServerOutputStream.
 *
 * Returns: (transfer full): a newly created #BonsaiServerOutputStream
 */
BonsaiServerOutputStream *
bonsai_server_output_stream_new (GOutputStream *base_stream)
{
  g_return_val_if_fail (G_IS_OUTPUT_STREAM (base_stream), NULL);

  return g_object_new (BONSAI_TYPE_SERVER_OUTPUT_STREAM,
                       "base-stream", base_stream,
                       NULL);
}

static void
bonsai_server_output_stream_finalize (GObject *object)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)object;

  BONSAI_ENTRY;

  g_clear_object (&self->base_stream);

  G_OBJECT_CLASS (bonsai_server_output_stream_parent_class)->finalize (object);

  BONSAI_EXIT;
}

static void
bonsai_server_output_stream_get_property (GObject    *object,
                                          guint       prop_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
  BonsaiServerOutputStream *self = BONSAI_SERVER_OUTPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_BASE_STREAM:
      g_value_set_object (value, bonsai_server_output_stream_get_base_stream (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_output_stream_set_property (GObject      *object,
                                          guint         prop_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
  BonsaiServerOutputStream *self = BONSAI_SERVER_OUTPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_BASE_STREAM:
      self->base_stream = g_value_dup_object (value);
      bonsai_output_stream_set_can_seek (BONSAI_OUTPUT_STREAM (self),
                                         G_IS_SEEKABLE (self->base_stream) &&
                                         g_seekable_can_seek (G_SEEKABLE (self->base_stream)));
      bonsai_output_stream_set_can_truncate (BONSAI_OUTPUT_STREAM (self),
                                             G_IS_SEEKABLE (self->base_stream) &&
                                             g_seekable_can_truncate (G_SEEKABLE (self->base_stream)));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_output_stream_class_init (BonsaiServerOutputStreamClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_server_output_stream_finalize;
  object_class->get_property = bonsai_server_output_stream_get_property;
  object_class->set_property = bonsai_server_output_stream_set_property;

  /**
   * BonsaiServerOutputStream:base-stream:
   *
   * The :base-stream property contains the underlying output stream that
   * can be accessed via D-Bus.
   *
   * Since: 0.2
   */
  properties [PROP_BASE_STREAM] =
    g_param_spec_object ("base-stream",
                         "Base Stream",
                         "Base Stream",
                         G_TYPE_OUTPUT_STREAM,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_server_output_stream_init (BonsaiServerOutputStream *self)
{
}

/**
 * bonsai_server_output_stream_get_base_stream:
 * @self: a #BonsaiServerOutputStream
 *
 * Gets the #GOutputStream that is exported over D-Bus.
 *
 * Returns: (transfer none): a #GOutputStream
 *
 * Since: 0.2
 */
GOutputStream *
bonsai_server_output_stream_get_base_stream (BonsaiServerOutputStream *self)
{
  g_return_val_if_fail (BONSAI_IS_SERVER_OUTPUT_STREAM (self), NULL);

  return self->base_stream;
}

static gboolean
bonsai_server_output_stream_get_can_seek (BonsaiOutputStream *stream)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));

  ret = G_IS_SEEKABLE (self->base_stream) &&
        g_seekable_can_seek (G_SEEKABLE (self->base_stream));

  BONSAI_RETURN (ret);
}

static gboolean
bonsai_server_output_stream_handle_seek (BonsaiOutputStream    *stream,
                                         GDBusMethodInvocation *invocation,
                                         gint64                 offset,
                                         guint                  seek_type)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (seek_type != G_SEEK_END &&
      seek_type != G_SEEK_CUR &&
      seek_type != G_SEEK_SET)
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "SeekType is unknown");
      BONSAI_RETURN (TRUE);
    }

  if (!bonsai_server_output_stream_get_can_seek (stream))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_IO_ERROR,
                                             G_IO_ERROR_NOT_SUPPORTED,
                                             "Seek is not supported on stream");
      BONSAI_RETURN (TRUE);
    }

  if (!g_seekable_seek (G_SEEKABLE (self), offset, seek_type, NULL, &error))
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_RETURN (TRUE);
    }

  bonsai_output_stream_complete_seek (stream,
                                      g_steal_pointer (&invocation),
                                      g_seekable_tell (G_SEEKABLE (self->base_stream)));

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_server_output_stream_get_can_truncate (BonsaiOutputStream *stream)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));

  ret = G_IS_SEEKABLE (self->base_stream) &&
        g_seekable_can_truncate (G_SEEKABLE (self->base_stream));

  BONSAI_RETURN (ret);
}

static gboolean
bonsai_server_output_stream_handle_truncate (BonsaiOutputStream    *stream,
                                             GDBusMethodInvocation *invocation,
                                             guint64                offset)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!bonsai_server_output_stream_get_can_truncate (stream))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_IO_ERROR,
                                             G_IO_ERROR_NOT_SUPPORTED,
                                             "Truncate is not supported on stream");
      BONSAI_RETURN (TRUE);
    }

  if (!g_seekable_truncate (G_SEEKABLE (self->base_stream), offset, NULL, &error))
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_RETURN (TRUE);
    }

  bonsai_output_stream_complete_truncate (stream, g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_server_output_stream_handle_tell (BonsaiOutputStream    *stream,
                                         GDBusMethodInvocation *invocation)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  bonsai_output_stream_complete_tell (stream,
                                      g_steal_pointer (&invocation),
                                      g_seekable_tell (G_SEEKABLE (self->base_stream)));

  BONSAI_RETURN (TRUE);
}

static void
bonsai_server_output_stream_handle_write_cb (GObject      *object,
                                             GAsyncResult *result,
                                             gpointer      user_data)
{
  GOutputStream *stream = (GOutputStream *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;
  gsize n_written = 0;

  BONSAI_ENTRY;

  g_assert (G_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_output_stream_write_all_finish (stream, result, &n_written, &error))
    {
      if (n_written == 0)
        {
          g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
          BONSAI_EXIT;
        }
    }

  bonsai_output_stream_complete_write (NULL, g_steal_pointer (&invocation), n_written);

  BONSAI_EXIT;
}

static gboolean
bonsai_server_output_stream_handle_write (BonsaiOutputStream    *stream,
                                          GDBusMethodInvocation *invocation,
                                          GVariant              *bytes)

{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;
  const guint8 *buffer;
  gsize count;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (bytes != NULL);
  g_assert (g_variant_is_of_type (bytes, G_VARIANT_TYPE_BYTESTRING));

  /* Prolong bytes lifetime to match invocation */
  g_object_set_data_full (G_OBJECT (invocation),
                          "STREAM_BYTES",
                          g_variant_ref (bytes),
                          (GDestroyNotify) g_variant_unref);

  /* Variant contains "ay", but it has no particular encoding */
  buffer = g_variant_get_fixed_array (bytes, &count, sizeof (guint8));

  g_output_stream_write_all_async (self->base_stream,
                                   buffer,
                                   count,
                                   G_PRIORITY_DEFAULT,
                                   NULL,
                                   bonsai_server_output_stream_handle_write_cb,
                                   g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsai_server_output_stream_close_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  GOutputStream *stream = (GOutputStream *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_output_stream_close_finish (stream, result, &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  else
    bonsai_output_stream_complete_close (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsai_server_output_stream_handle_close (BonsaiOutputStream    *stream,
                                          GDBusMethodInvocation *invocation)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_output_stream_close_async (self->base_stream,
                               G_PRIORITY_DEFAULT,
                               NULL,
                               bonsai_server_output_stream_close_cb,
                               g_steal_pointer (&invocation));

  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self));

  BONSAI_RETURN (TRUE);
}

static void
bonsai_server_output_stream_flush_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  GOutputStream *stream = (GOutputStream *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_output_stream_flush_finish (stream, result, &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  else
    bonsai_output_stream_complete_flush (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsai_server_output_stream_handle_flush (BonsaiOutputStream    *stream,
                                          GDBusMethodInvocation *invocation)
{
  BonsaiServerOutputStream *self = (BonsaiServerOutputStream *)stream;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_OUTPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_output_stream_flush_async (self->base_stream,
                               G_PRIORITY_DEFAULT,
                               NULL,
                               bonsai_server_output_stream_flush_cb,
                               g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
output_stream_iface_init (BonsaiOutputStreamIface *iface)
{
  iface->get_can_seek = bonsai_server_output_stream_get_can_seek;
  iface->get_can_truncate = bonsai_server_output_stream_get_can_truncate;
  iface->handle_seek = bonsai_server_output_stream_handle_seek;
  iface->handle_tell = bonsai_server_output_stream_handle_tell;
  iface->handle_truncate = bonsai_server_output_stream_handle_truncate;
  iface->handle_write = bonsai_server_output_stream_handle_write;
  iface->handle_close = bonsai_server_output_stream_handle_close;
  iface->handle_flush = bonsai_server_output_stream_handle_flush;
}
