/* bonsai-client-input-stream.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-client-input-stream"

#include "config.h"

#include "bonsai-input-stream.h"
#include "bonsai-client-input-stream.h"
#include "bonsai-trace.h"

struct _BonsaiClientInputStream
{
  GInputStream       parent_instance;
  BonsaiInputStream *proxy;
};

static void seekable_iface_init (GSeekableIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiClientInputStream, bonsai_client_input_stream, G_TYPE_INPUT_STREAM,
                         G_IMPLEMENT_INTERFACE (G_TYPE_SEEKABLE, seekable_iface_init))

enum {
  PROP_0,
  PROP_PROXY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_client_input_stream_new:
 *
 * Create a new #BonsaiClientInputStream.
 *
 * Returns: (transfer full): a newly created #BonsaiClientInputStream
 */
GInputStream *
bonsai_client_input_stream_new (GDBusConnection *connection,
                                const gchar     *object_path)
{
  g_autoptr(BonsaiInputStream) proxy = NULL;

  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);
  g_return_val_if_fail (object_path != NULL, NULL);

  /* This shouldn't be able to fail/block */
  proxy = bonsai_input_stream_proxy_new_sync (connection,
                                              (G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START_AT_CONSTRUCTION |
                                               G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
                                               G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES),
                                              NULL,
                                              object_path,
                                              NULL,
                                              NULL);

  g_return_val_if_fail (BONSAI_IS_INPUT_STREAM_PROXY (proxy), NULL);

  g_object_set (proxy,
                "g-default-timeout", 60 * 1000,
                NULL);

  return g_object_new (BONSAI_TYPE_CLIENT_INPUT_STREAM,
                       "proxy", proxy,
                       NULL);
}

static gboolean
bonsai_client_input_stream_close_fn (GInputStream  *stream,
                                     GCancellable  *cancellable,
                                     GError       **error)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)stream;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  ret = bonsai_input_stream_call_close_sync (BONSAI_INPUT_STREAM (self->proxy),
                                             cancellable,
                                             error);

  BONSAI_RETURN (ret);
}

static void
bonsai_client_input_stream_close_cb (GObject      *object,
                                     GAsyncResult *result,
                                     gpointer      user_data)
{
  BonsaiInputStream *stream = (BonsaiInputStream *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_INPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_input_stream_call_close_finish (stream, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

static void
bonsai_client_input_stream_close_async (GInputStream        *stream,
                                        int                  io_priority,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)stream;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_input_stream_close_async);

  bonsai_input_stream_call_close (BONSAI_INPUT_STREAM (self->proxy),
                                  cancellable,
                                  bonsai_client_input_stream_close_cb,
                                  g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gboolean
bonsai_client_input_stream_close_finish (GInputStream  *stream,
                                         GAsyncResult  *result,
                                         GError       **error)
{
  gssize ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (stream));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

static gssize
bonsai_client_input_stream_read_fn (GInputStream  *stream,
                                    void          *buffer,
                                    gsize          count,
                                    GCancellable  *cancellable,
                                    GError       **error)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)stream;
  g_autoptr(GVariant) out_buffer = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (bonsai_input_stream_call_read_sync (self->proxy, count, &out_buffer, cancellable, error))
    {
      const guint8 *data;
      gsize n_elements = 0;

      if ((data = g_variant_get_fixed_array (out_buffer, &n_elements, sizeof (guint8))))
        {
          memcpy (buffer, data, MIN (count, n_elements));
          BONSAI_RETURN (n_elements);
        }
    }

  BONSAI_RETURN (-1);
}

static gssize
bonsai_client_input_stream_skip (GInputStream  *stream,
                                 gsize          count,
                                 GCancellable  *cancellable,
                                 GError       **error)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)stream;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (bonsai_input_stream_call_seek_sync (self->proxy, count, G_SEEK_CUR, NULL, cancellable, error))
    BONSAI_RETURN (count);
  else
    BONSAI_RETURN (-1);
}

static void
bonsai_client_input_stream_skip_cb (GObject      *object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  BonsaiInputStreamProxy *proxy = (BonsaiInputStreamProxy *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;
  gsize count;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_INPUT_STREAM_PROXY (proxy));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  count = GPOINTER_TO_SIZE (g_task_get_task_data (task));

  if (!bonsai_input_stream_call_seek_finish (BONSAI_INPUT_STREAM (proxy), NULL, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_int (task, count);

  BONSAI_EXIT;
}

static void
bonsai_client_input_stream_skip_async (GInputStream        *stream,
                                       gsize                count,
                                       int                  io_priority,
                                       GCancellable        *cancellable,
                                       GAsyncReadyCallback  callback,
                                       gpointer             user_data)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)stream;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_input_stream_skip_async);
  g_task_set_task_data (task, GSIZE_TO_POINTER (count), NULL);

  bonsai_input_stream_call_seek (self->proxy,
                                 count,
                                 G_SEEK_CUR,
                                 cancellable,
                                 bonsai_client_input_stream_skip_cb,
                                 g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gssize
bonsai_client_input_stream_skip_finish (GInputStream  *stream,
                                        GAsyncResult  *result,
                                        GError       **error)
{
  g_autoptr(GError) local_error = NULL;
  gssize ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (stream));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_int (G_TASK (result), &local_error);

  if (local_error)
    {
      g_propagate_error (error, g_steal_pointer (&local_error));
      return -1;
    }

  BONSAI_RETURN (ret);
}

static void
bonsai_client_input_stream_read_cb (GObject      *object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  BonsaiInputStream *stream = (BonsaiInputStream *)object;
  g_autoptr(GVariant) buffer = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_INPUT_STREAM_PROXY (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!bonsai_input_stream_call_read_finish (stream, &buffer, result, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_steal_pointer (&buffer),
                           (GDestroyNotify) g_variant_unref);

  BONSAI_EXIT;
}

static void
bonsai_client_input_stream_read_async (GInputStream        *stream,
                                       void                *buffer,
                                       gsize                count,
                                       int                  io_priority,
                                       GCancellable        *cancellable,
                                       GAsyncReadyCallback  callback,
                                       gpointer             user_data)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)stream;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsai_client_input_stream_read_async);
  g_task_set_task_data (task, buffer, NULL);
  g_object_set_data (G_OBJECT (task), "COUNT", GSIZE_TO_POINTER (count));

  bonsai_input_stream_call_read (self->proxy,
                                 count,
                                 cancellable,
                                 bonsai_client_input_stream_read_cb,
                                 g_steal_pointer (&task));

  BONSAI_EXIT;
}

static gssize
bonsai_client_input_stream_read_finish (GInputStream  *stream,
                                        GAsyncResult  *result,
                                        GError       **error)
{
  g_autoptr(GVariant) variant = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (stream));
  g_assert (G_IS_TASK (result));

  if ((variant = g_task_propagate_pointer (G_TASK (result), error)))
    {
      void *buffer = g_task_get_task_data (G_TASK (result));
      gsize count = GPOINTER_TO_SIZE (g_object_get_data (G_OBJECT (result), "COUNT"));
      const guint8 *data;
      gsize n_elements = 0;

      g_assert (g_variant_is_of_type (variant, G_VARIANT_TYPE_BYTESTRING));

      if ((data = g_variant_get_fixed_array (variant, &n_elements, sizeof (guint8))))
        {
          count = MIN (count, n_elements);
          memcpy (buffer, data, count);
          BONSAI_RETURN (count);
        }
    }

  BONSAI_RETURN (-1);
}

static void
bonsai_client_input_stream_finalize (GObject *object)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)object;

  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (bonsai_client_input_stream_parent_class)->finalize (object);
}

static void
bonsai_client_input_stream_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  BonsaiClientInputStream *self = BONSAI_CLIENT_INPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      g_value_set_object (value, self->proxy);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_input_stream_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  BonsaiClientInputStream *self = BONSAI_CLIENT_INPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_PROXY:
      self->proxy = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_client_input_stream_class_init (BonsaiClientInputStreamClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GInputStreamClass *input_stream_class = G_INPUT_STREAM_CLASS (klass);

  object_class->finalize = bonsai_client_input_stream_finalize;
  object_class->get_property = bonsai_client_input_stream_get_property;
  object_class->set_property = bonsai_client_input_stream_set_property;

  input_stream_class->read_fn = bonsai_client_input_stream_read_fn;
  input_stream_class->skip = bonsai_client_input_stream_skip;
  input_stream_class->close_fn = bonsai_client_input_stream_close_fn;
  input_stream_class->read_async = bonsai_client_input_stream_read_async;
  input_stream_class->read_finish = bonsai_client_input_stream_read_finish;
  input_stream_class->skip_async = bonsai_client_input_stream_skip_async;
  input_stream_class->skip_finish = bonsai_client_input_stream_skip_finish;
  input_stream_class->close_async = bonsai_client_input_stream_close_async;
  input_stream_class->close_finish = bonsai_client_input_stream_close_finish;

  /**
   * BonsaiClientInputStream:proxy:
   *
   * The :proxy property is used to communicate with the D-Bus peer.
   *
   * Since: 0.2
   */
  properties [PROP_PROXY] =
    g_param_spec_object ("proxy",
                         "Proxy",
                         "The proxy to communicate with the peer.",
                         BONSAI_TYPE_INPUT_STREAM_PROXY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_client_input_stream_init (BonsaiClientInputStream *self)
{
}

static gboolean
bonsai_client_input_stream_can_seek (GSeekable *seekable)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)seekable;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));

  return bonsai_input_stream_get_can_seek (BONSAI_INPUT_STREAM (self->proxy));
}

static gboolean
bonsai_client_input_stream_can_truncate (GSeekable *seekable)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)seekable;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));

  return FALSE;
}

static gboolean
bonsai_client_input_stream_seek (GSeekable     *seekable,
                                 goffset        offset,
                                 GSeekType      type,
                                 GCancellable  *cancellable,
                                 GError       **error)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)seekable;
  G_GNUC_UNUSED guint64 position = 0;
  gboolean ret;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  ret = bonsai_input_stream_call_seek_sync (BONSAI_INPUT_STREAM (self->proxy),
                                            offset,
                                            type,
                                            &position,
                                            cancellable,
                                            error);

  BONSAI_RETURN (ret);
}

static goffset
bonsai_client_input_stream_tell (GSeekable *seekable)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)seekable;
  guint64 position = 0;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));

  if (!bonsai_input_stream_call_tell_sync (BONSAI_INPUT_STREAM (self->proxy), &position, NULL, NULL))
    BONSAI_RETURN (-1);
  else
    BONSAI_RETURN (position);
}

static gboolean
bonsai_client_input_stream_truncate_fn (GSeekable     *seekable,
                                        goffset        offset,
                                        GCancellable  *cancellable,
                                        GError       **error)
{
  BonsaiClientInputStream *self = (BonsaiClientInputStream *)seekable;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT_INPUT_STREAM (self));

  g_set_error (error,
               G_IO_ERROR,
               G_IO_ERROR_NOT_SUPPORTED,
               "Truncating not supported on input stream");

  BONSAI_RETURN (FALSE);
}

static void
seekable_iface_init (GSeekableIface *iface)
{
  iface->can_seek = bonsai_client_input_stream_can_seek;
  iface->can_truncate = bonsai_client_input_stream_can_truncate;
  iface->seek = bonsai_client_input_stream_seek;
  iface->tell = bonsai_client_input_stream_tell;
  iface->truncate_fn = bonsai_client_input_stream_truncate_fn;
}
