/* bonsai-server-input-stream.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-server-input-stream"

#include "config.h"

#include "bonsai-server-input-stream.h"
#include "bonsai-trace.h"

struct _BonsaiServerInputStream
{
  BonsaiInputStreamSkeleton  parent_instance;
  GInputStream              *base_stream;
};

static void input_stream_iface (BonsaiInputStreamIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiServerInputStream, bonsai_server_input_stream, BONSAI_TYPE_INPUT_STREAM_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_INPUT_STREAM, input_stream_iface))

enum {
  PROP_0,
  PROP_BASE_STREAM,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_server_input_stream_new:
 * @base_stream: a #GInputStream
 *
 * Create a new #BonsaiServerInputStream.
 *
 * Returns: (transfer full): a newly created #BonsaiServerInputStream
 */
BonsaiInputStream *
bonsai_server_input_stream_new (GInputStream *base_stream)
{
  g_return_val_if_fail (G_IS_INPUT_STREAM (base_stream), NULL);

  return g_object_new (BONSAI_TYPE_SERVER_INPUT_STREAM,
                       "base-stream", base_stream,
                       NULL);
}

static void
bonsai_server_input_stream_finalize (GObject *object)
{
  BonsaiServerInputStream *self = (BonsaiServerInputStream *)object;

  g_clear_object (&self->base_stream);

  G_OBJECT_CLASS (bonsai_server_input_stream_parent_class)->finalize (object);
}

static void
bonsai_server_input_stream_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  BonsaiServerInputStream *self = BONSAI_SERVER_INPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_BASE_STREAM:
      g_value_set_object (value, self->base_stream);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_input_stream_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  BonsaiServerInputStream *self = BONSAI_SERVER_INPUT_STREAM (object);

  switch (prop_id)
    {
    case PROP_BASE_STREAM:
      self->base_stream = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_input_stream_class_init (BonsaiServerInputStreamClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_server_input_stream_finalize;
  object_class->get_property = bonsai_server_input_stream_get_property;
  object_class->set_property = bonsai_server_input_stream_set_property;

  properties [PROP_BASE_STREAM] =
    g_param_spec_object ("base-stream",
                         "Base Stream",
                         "Base Stream",
                         G_TYPE_INPUT_STREAM,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_server_input_stream_init (BonsaiServerInputStream *self)
{
}

/**
 * bonsai_server_input_stream_get_base_stream:
 * @self: a #BonsaiServerInputStream
 *
 * Gets the underlying stream.
 *
 * Returns: (transfer none): a #GInputStream
 *
 * Since: 0.2
 */
GInputStream *
bonsai_server_input_stream_get_base_stream (BonsaiServerInputStream *self)
{
  g_return_val_if_fail (BONSAI_IS_SERVER_INPUT_STREAM (self), NULL);

  return self->base_stream;
}

static gboolean
bonsai_server_input_stream_get_can_seek (BonsaiInputStream *stream)
{
  BonsaiServerInputStream *self = (BonsaiServerInputStream *)stream;
  gboolean ret = FALSE;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_INPUT_STREAM (self));

  if (G_IS_SEEKABLE (self->base_stream))
    ret = g_seekable_can_seek (G_SEEKABLE (self->base_stream));

  BONSAI_RETURN (ret);
}

static void
bonsai_server_input_stream_handle_close_cb (GObject      *object,
                                            GAsyncResult *result,
                                            gpointer      user_data)
{
  GInputStream *stream = (GInputStream *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_INPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_input_stream_close_finish (stream, result, &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  else
    bonsai_input_stream_complete_close (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsai_server_input_stream_handle_close (BonsaiInputStream     *stream,
                                         GDBusMethodInvocation *invocation)
{
  BonsaiServerInputStream *self = (BonsaiServerInputStream *)stream;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_INPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_input_stream_close_async (self->base_stream,
                              G_PRIORITY_DEFAULT,
                              NULL,
                              bonsai_server_input_stream_handle_close_cb,
                              g_steal_pointer (&invocation));

  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self));

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_server_input_stream_handle_tell (BonsaiInputStream     *stream,
                                        GDBusMethodInvocation *invocation)
{
  BonsaiServerInputStream *self = (BonsaiServerInputStream *)stream;
  gsize offset;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_INPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (G_IS_SEEKABLE (self->base_stream))
    offset = g_seekable_tell (G_SEEKABLE (self->base_stream));
  else
    offset = 0;

  bonsai_input_stream_complete_tell (stream,
                                     g_steal_pointer (&invocation),
                                     offset);

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_server_input_stream_handle_seek (BonsaiInputStream     *stream,
                                        GDBusMethodInvocation *invocation,
                                        gint64                 position,
                                        guint                  seek_type)
{
  BonsaiServerInputStream *self = (BonsaiServerInputStream *)stream;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_INPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if ((seek_type != G_SEEK_END &&
       seek_type != G_SEEK_CUR &&
       seek_type != G_SEEK_SET) ||
      !G_IS_SEEKABLE (self->base_stream))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "Invalid seek request");
      BONSAI_RETURN (TRUE);
    }

  if (!g_seekable_seek (G_SEEKABLE (self->base_stream),
                        position, seek_type, NULL, &error))
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_RETURN (TRUE);
    }

  bonsai_input_stream_complete_seek (stream,
                                     g_steal_pointer (&invocation),
                                     g_seekable_tell (G_SEEKABLE (self->base_stream)));

  BONSAI_RETURN (TRUE);
}

static void
bonsai_server_input_stream_handle_read_cb (GObject      *object,
                                           GAsyncResult *result,
                                           gpointer      user_data)
{
  GInputStream *stream = (GInputStream *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;
  void *buffer;
  gssize n_read;

  BONSAI_ENTRY;

  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_INPUT_STREAM (stream));

  buffer = g_object_get_data (G_OBJECT (invocation), "BUFFER");
  n_read = g_input_stream_read_finish (stream, result, &error);

  if (error != NULL)
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  else
    bonsai_input_stream_complete_read (NULL,
                                       g_steal_pointer (&invocation),
                                       g_variant_new_fixed_array (G_VARIANT_TYPE_BYTE,
                                                                  buffer,
                                                                  n_read,
                                                                  sizeof (guint8)));

  BONSAI_EXIT;
}

static gboolean
bonsai_server_input_stream_handle_read (BonsaiInputStream     *stream,
                                        GDBusMethodInvocation *invocation,
                                        guint64                count)
{
  BonsaiServerInputStream *self = (BonsaiServerInputStream *)stream;
  void *buffer;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_INPUT_STREAM (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  count = MIN (count, 4096 * 32);
  buffer = g_try_malloc (count);

  if (buffer == 0)
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_NO_MEMORY,
                                             "Failed to allocate read buffer");
      BONSAI_RETURN (TRUE);
    }

  g_object_set_data_full (G_OBJECT (invocation),
                          "BUFFER",
                          buffer,
                          g_free);

  g_input_stream_read_all_async (self->base_stream,
                                 buffer,
                                 count,
                                 G_PRIORITY_DEFAULT,
                                 NULL,
                                 bonsai_server_input_stream_handle_read_cb,
                                 g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
input_stream_iface (BonsaiInputStreamIface *iface)
{
  iface->get_can_seek = bonsai_server_input_stream_get_can_seek;
  iface->handle_close = bonsai_server_input_stream_handle_close;
  iface->handle_tell = bonsai_server_input_stream_handle_tell;
  iface->handle_read = bonsai_server_input_stream_handle_read;
  iface->handle_seek = bonsai_server_input_stream_handle_seek;
}
