/* bonsai-server-file-enumerator.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-server-file-enumerator"

#include "config.h"

#include "bonsai-server-file-enumerator.h"
#include "bonsai-trace.h"
#include "bonsai-utils.h"

struct _BonsaiServerFileEnumerator
{
  BonsaiFileEnumeratorSkeleton  parent_instance;
  GFileEnumerator              *enumerator;
};

static void file_enumerator_iface_init (BonsaiFileEnumeratorIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiServerFileEnumerator,
                         bonsai_server_file_enumerator,
                         BONSAI_TYPE_FILE_ENUMERATOR_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_FILE_ENUMERATOR,
                                                file_enumerator_iface_init))

enum {
  PROP_0,
  PROP_ENUMERATOR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_server_file_enumerator_new:
 * @enumeator: a #GFileEnumerator
 *
 * Create a new #BonsaiServerFileEnumerator.
 *
 * Returns: (transfer full): a newly created #BonsaiServerFileEnumerator
 */
BonsaiFileEnumerator *
bonsai_server_file_enumerator_new (GFileEnumerator *enumerator)
{
  g_return_val_if_fail (G_IS_FILE_ENUMERATOR (enumerator), NULL);

  return g_object_new (BONSAI_TYPE_SERVER_FILE_ENUMERATOR,
                       "enumerator", enumerator,
                       NULL);
}

static void
bonsai_server_file_enumerator_finalize (GObject *object)
{
  BonsaiServerFileEnumerator *self = (BonsaiServerFileEnumerator *)object;

  g_clear_object (&self->enumerator);

  G_OBJECT_CLASS (bonsai_server_file_enumerator_parent_class)->finalize (object);
}

static void
bonsai_server_file_enumerator_get_property (GObject    *object,
                                            guint       prop_id,
                                            GValue     *value,
                                            GParamSpec *pspec)
{
  BonsaiServerFileEnumerator *self = BONSAI_SERVER_FILE_ENUMERATOR (object);

  switch (prop_id)
    {
    case PROP_ENUMERATOR:
      g_value_set_object (value, self->enumerator);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_file_enumerator_set_property (GObject      *object,
                                            guint         prop_id,
                                            const GValue *value,
                                            GParamSpec   *pspec)
{
  BonsaiServerFileEnumerator *self = BONSAI_SERVER_FILE_ENUMERATOR (object);

  switch (prop_id)
    {
    case PROP_ENUMERATOR:
      self->enumerator = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_file_enumerator_class_init (BonsaiServerFileEnumeratorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_server_file_enumerator_finalize;
  object_class->get_property = bonsai_server_file_enumerator_get_property;
  object_class->set_property = bonsai_server_file_enumerator_set_property;

  properties [PROP_ENUMERATOR] =
    g_param_spec_object ("enumerator",
                         "Enumerator",
                         "Enumerator",
                         G_TYPE_FILE_ENUMERATOR,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_server_file_enumerator_init (BonsaiServerFileEnumerator *self)
{
}

static void
bonsai_server_file_enumerator_handle_next_files_cb (GObject      *object,
                                                    GAsyncResult *result,
                                                    gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autolist(GFileInfo) files = NULL;
  g_autoptr(GError) error = NULL;
  GVariantBuilder builder;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  files = g_file_enumerator_next_files_finish (enumerator, result, &error);

  if (error != NULL)
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_EXIT;
    }

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa(suv)"));
  for (const GList *iter = files; iter; iter = iter->next)
    {
      GFileInfo *info = iter->data;
      g_variant_builder_add_value (&builder, bonsai_file_info_serialize (info));
    }

  bonsai_file_enumerator_complete_next_files (NULL,
                                              g_steal_pointer (&invocation),
                                              g_variant_builder_end (&builder));

  BONSAI_EXIT;
}

static gboolean
bonsai_server_file_enumerator_handle_next_files (BonsaiFileEnumerator  *enumerator,
                                                 GDBusMethodInvocation *invocation,
                                                 guint                  num_files)
{
  BonsaiServerFileEnumerator *self = (BonsaiServerFileEnumerator *)enumerator;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_FILE_ENUMERATOR (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (num_files == 0)
    num_files = 100;

  g_file_enumerator_next_files_async (self->enumerator,
                                      num_files,
                                      G_PRIORITY_DEFAULT,
                                      NULL,
                                      bonsai_server_file_enumerator_handle_next_files_cb,
                                      g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsai_server_file_enumerator_handle_close_cb (GObject      *object,
                                               GAsyncResult *result,
                                               gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_file_enumerator_close_finish (enumerator, result, &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  else
    bonsai_file_enumerator_complete_close (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsai_server_file_enumerator_handle_close (BonsaiFileEnumerator  *enumerator,
                                            GDBusMethodInvocation *invocation)
{
  BonsaiServerFileEnumerator *self = (BonsaiServerFileEnumerator *)enumerator;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_FILE_ENUMERATOR (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_file_enumerator_close_async (self->enumerator,
                                 G_PRIORITY_DEFAULT,
                                 NULL,
                                 bonsai_server_file_enumerator_handle_close_cb,
                                 g_steal_pointer (&invocation));

  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self));

  BONSAI_RETURN (TRUE);
}

static void
file_enumerator_iface_init (BonsaiFileEnumeratorIface *iface)
{
  iface->handle_close = bonsai_server_file_enumerator_handle_close;
  iface->handle_next_files = bonsai_server_file_enumerator_handle_next_files;
}
