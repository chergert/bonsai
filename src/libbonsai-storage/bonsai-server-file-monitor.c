/* bonsai-server-file-monitor.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-server-file-monitor"

#include "config.h"

#include "bonsai-server-file-monitor.h"
#include "bonsai-trace.h"

struct _BonsaiServerFileMonitor
{
  BonsaiFileMonitorSkeleton  parent_instance;
  GFileMonitor              *monitor;
  GFile                     *root;
};

static void file_monitor_iface_init (BonsaiFileMonitorIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiServerFileMonitor,
                         bonsai_server_file_monitor,
                         BONSAI_TYPE_FILE_MONITOR_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_FILE_MONITOR,
                                                file_monitor_iface_init))

enum {
  PROP_0,
  PROP_ROOT,
  PROP_MONITOR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * bonsai_server_file_monitor_new:
 * @monitor: a #GFileMonitor
 * @root: the root of the storage system
 *
 * Create a new #BonsaiServerFileMonitor.
 *
 * Returns: (transfer full): a newly created #BonsaiServerFileMonitor
 *
 * Since: 0.2
 */
BonsaiFileMonitor *
bonsai_server_file_monitor_new (GFileMonitor *monitor,
                                GFile        *root)
{
  g_return_val_if_fail (G_IS_FILE_MONITOR (monitor), NULL);

  return g_object_new (BONSAI_TYPE_SERVER_FILE_MONITOR,
                       "root", root,
                       "monitor", monitor,
                       NULL);
}

static void
bonsai_server_file_monitor_changed_cb (BonsaiServerFileMonitor *self,
                                       GFile                   *file,
                                       GFile                   *other_file,
                                       GFileMonitorEvent        event_type,
                                       GFileMonitor            *monitor)
{
  g_autofree gchar *path = NULL;
  g_autofree gchar *other_path = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_FILE_MONITOR (self));
  g_assert (G_IS_FILE (file));
  g_assert (!other_file || G_IS_FILE (other_file));
  g_assert (G_IS_FILE_MONITOR (monitor));

  path = g_file_get_relative_path (self->root, file);
  if (path == NULL)
    return;

  if (other_file != NULL)
    {
      other_path = g_file_get_relative_path (self->root, other_file);
      if (other_path == NULL)
        return;
    }

  bonsai_file_monitor_emit_changed (BONSAI_FILE_MONITOR (self),
                                    path,
                                    other_path ? other_path : "",
                                    event_type);

  BONSAI_EXIT;
}

/**
 * bonsai_server_file_monitor_get_monitor:
 * @self: a #BonsaiServerFileMonitor
 *
 * Gets the :monitor property.
 *
 * Returns: (transfer none): a #GFileMonitor
 *
 * Since: 0.2
 */
GFileMonitor *
bonsai_server_file_monitor_get_monitor (BonsaiServerFileMonitor *self)
{
  g_return_val_if_fail (BONSAI_IS_SERVER_FILE_MONITOR (self), NULL);

  return self->monitor;
}

static void
bonsai_server_file_monitor_set_monitor (BonsaiServerFileMonitor *self,
                                        GFileMonitor            *monitor)
{
  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_FILE_MONITOR (self));
  g_assert (!monitor || G_IS_FILE_MONITOR (monitor));
  g_assert (self->monitor == NULL);

  if (g_set_object (&self->monitor, monitor))
    {
      g_object_bind_property (self, "rate-limit", monitor, "rate-limit", 0);
      bonsai_file_monitor_set_rate_limit (BONSAI_FILE_MONITOR (self), 800);
      g_signal_connect_object (self->monitor,
                               "changed",
                               G_CALLBACK (bonsai_server_file_monitor_changed_cb),
                               self,
                               G_CONNECT_SWAPPED);
    }

  BONSAI_EXIT;
}

static void
bonsai_server_file_monitor_finalize (GObject *object)
{
  BonsaiServerFileMonitor *self = (BonsaiServerFileMonitor *)object;

  g_clear_object (&self->monitor);

  G_OBJECT_CLASS (bonsai_server_file_monitor_parent_class)->finalize (object);
}

static void
bonsai_server_file_monitor_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  BonsaiServerFileMonitor *self = BONSAI_SERVER_FILE_MONITOR (object);

  switch (prop_id)
    {
    case PROP_ROOT:
      g_value_set_object (value, self->root);
      break;

    case PROP_MONITOR:
      g_value_set_object (value, bonsai_server_file_monitor_get_monitor (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_file_monitor_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  BonsaiServerFileMonitor *self = BONSAI_SERVER_FILE_MONITOR (object);

  switch (prop_id)
    {
    case PROP_ROOT:
      self->root = g_value_dup_object (value);
      break;

    case PROP_MONITOR:
      bonsai_server_file_monitor_set_monitor (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_server_file_monitor_class_init (BonsaiServerFileMonitorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_server_file_monitor_finalize;
  object_class->get_property = bonsai_server_file_monitor_get_property;
  object_class->set_property = bonsai_server_file_monitor_set_property;

  properties [PROP_ROOT] =
    g_param_spec_object ("root",
                         "Root",
                         "The root of the storage system",
                         G_TYPE_FILE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_MONITOR] =
    g_param_spec_object ("monitor",
                         "Monitor",
                         "The monitor to be exported over D-Bus",
                         G_TYPE_FILE_MONITOR,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_server_file_monitor_init (BonsaiServerFileMonitor *self)
{
}

static gint
bonsai_server_file_monitor_get_rate_limit (BonsaiFileMonitor *monitor)
{
  BonsaiServerFileMonitor *self = (BonsaiServerFileMonitor *)monitor;
  gint rate_limit;

  g_object_get (self->monitor,
                "rate-limit", &rate_limit,
                NULL);

  return rate_limit;
}

static gboolean
bonsai_server_file_monitor_handle_cancel (BonsaiFileMonitor     *monitor,
                                          GDBusMethodInvocation *invocation)
{
  BonsaiServerFileMonitor *self = (BonsaiServerFileMonitor *)monitor;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_SERVER_FILE_MONITOR (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_file_monitor_cancel (self->monitor);
  bonsai_file_monitor_complete_cancel (monitor, g_steal_pointer (&invocation));
  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self));

  BONSAI_RETURN (TRUE);
}

static void
file_monitor_iface_init (BonsaiFileMonitorIface *iface)
{
  iface->get_rate_limit = bonsai_server_file_monitor_get_rate_limit;
  iface->handle_cancel = bonsai_server_file_monitor_handle_cancel;
}
