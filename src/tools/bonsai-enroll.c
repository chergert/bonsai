/* bonsai-enroll.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-enroll"

#include "config.h"

#include <libbonsai.h>
#include <stdlib.h>

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(BonsaiEnrollment) enrollment = NULL;
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GError) error = NULL;
  g_auto(GStrv) unenroll = NULL;
  g_auto(GStrv) enroll = NULL;
  GOptionEntry entries[] = {
    { "enroll", 'e', 0, G_OPTION_ARG_FILENAME_ARRAY, &enroll, "Enroll a certificate", "CERTIFICATE_FILE" },
    { "unenroll", 'u', 0, G_OPTION_ARG_FILENAME_ARRAY, &unenroll, "Unenroll a certificate", "CERTIFICATE_FILE" },
    { 0 }
  };

  context = g_option_context_new ("- Manage certificates");
  g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);

  g_option_context_set_summary (context, "\
  The bonsai-enroll utility allows the user to enroll a TLS certificate\n\
  on the device running the bonsai daemon (bonsaid). This is performed over\n\
  D-Bus and does not require the bonsai daemon to have a socket listening.\n\
\n\
  If bonsaid is not running, it will be spawned automatically.\n\
\n\
  Examples:\n\
\n\
    bonsai-enroll --enroll ~/.config/bonsai/public.key\n\
    bonsai-enroll --unenroll ~/.config/bonsai/public.key");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (enroll == NULL && unenroll == NULL)
    {
      g_autoptr(GSettings) settings = g_settings_new ("org.gnome.bonsai.daemon");
      g_auto(GStrv) enrolled = g_settings_get_strv (settings, "enrolled");

      for (guint i = 0; enrolled[i]; i++)
        g_print ("%s\n", enrolled[i]);
      g_print ("%d enrolled certificates.\n", g_strv_length (enrolled));

      return EXIT_SUCCESS;
    }

  enrollment = bonsai_enrollment_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                                         (G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
                                                          G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
                                                          G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START_AT_CONSTRUCTION),
                                                         "org.gnome.Bonsai.Daemon",
                                                         "/org/gnome/Bonsai/Enrollment",
                                                         NULL,
                                                         &error);

  if (enrollment == NULL)
    {
      g_printerr ("Accessing Bonsai daemon failed: %s\n", error->message);
      return EXIT_FAILURE;
    }

  if (enroll != NULL)
    {
      for (guint i = 0; enroll[i]; i++)
        {
          const gchar *filename = enroll[i];
          g_autoptr(GTlsCertificate) cert = NULL;
          g_autofree gchar *hash = NULL;
          g_autofree gchar *pem = NULL;

          if (!(cert = g_tls_certificate_new_from_file (filename, &error)))
            {
              g_printerr ("Failed to load certificate \"%s\"\n", filename);
              g_clear_error (&error);
              continue;
            }

          g_object_get (cert, "certificate-pem", &pem, NULL);

          if (!bonsai_enrollment_call_enroll_sync (enrollment, pem, NULL, &error))
            {
              g_printerr ("Failed to enroll certificate \"%s\": %s\n", filename, error->message);
              g_clear_error (&error);
              continue;
            }

          hash = bonsai_tls_certificate_get_hash (cert);
          g_print ("Enrolled \"%s\" with hash %s\n", filename, hash);
        }
    }

  if (unenroll != NULL)
    {
      for (guint i = 0; unenroll[i]; i++)
        {
          const gchar *filename = unenroll[i];
          g_autoptr(GTlsCertificate) cert = NULL;
          g_autofree gchar *hash = NULL;
          g_autofree gchar *pem = NULL;

          if (!(cert = g_tls_certificate_new_from_file (filename, &error)))
            {
              g_printerr ("Failed to load certificate \"%s\"", filename);
              g_clear_error (&error);
              continue;
            }

          g_object_get (cert, "certificate-pem", &pem, NULL);

          if (!bonsai_enrollment_call_unenroll_sync (enrollment, pem, NULL, &error))
            {
              g_printerr ("Failed to unenroll certificate \"%s\": %s\n", filename, error->message);
              g_clear_error (&error);
              continue;
            }

          hash = bonsai_tls_certificate_get_hash (cert);
          g_print ("Unenrolled \"%s\" with hash %s\n", filename, hash);
        }
    }

  return EXIT_SUCCESS;
}
