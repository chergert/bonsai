/* bonsai-dao-sync.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-sync"

#include "config.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <libbonsai.h>
#include <libbonsai-dao.h>

static GMainLoop *main_loop;

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GError) error = NULL;
  const gchar *pair_to;
  const gchar *repository_id;
  const gchar *repository_path;

  main_loop = g_main_loop_new (NULL, FALSE);
  context = g_option_context_new ("- Sync a Bonsai DAO repository");

  g_option_context_set_summary (context, "\
  The bonsai-dao-sync utility allows you to sync a DAO repository\n\
  from a remote Bonsai node to a local database.\n\
\n\
  Examples:\n\
\n\
    bonsai-dao-sync hostname:port com.example.App path/to/local/repository");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (argc != 4)
    {
      g_printerr ("Invalid parameters, see --help\n");
      return EXIT_FAILURE;
    }

  pair_to = argv[1];
  repository_id = argv[2];
  repository_path = argv[3];

  if (!g_application_id_is_valid (repository_id))
    {
      g_printerr ("Invalid repository name \"%s\"\n", repository_id);
      return EXIT_FAILURE;
    }

  if (g_mkdir_with_parents (repository_path, 0750) != 0)
    {
      int errsv = errno;
      g_printerr ("Failed to create local repository: %s\n",
                  g_strerror (errsv));
      return EXIT_FAILURE;
    }

  if (!(repository = bonsai_dao_repository_new_for_path (repository_id, repository_path, NULL, &error)))
    {
      g_printerr ("Failed to load local repository: %s\n", error->message);
      return EXIT_FAILURE;
    }
  else
    {
      g_autoptr(BonsaiClient) client = NULL;
      g_autoptr(GSocketConnectable) connectable = NULL;
      g_autoptr(GTlsCertificate) certificate = NULL;
      g_autoptr(BonsaiIdentity) identity = NULL;
      g_autoptr(BonsaiEndPoint) end_point = NULL;
      g_autofree gchar *revision = NULL;
      GDBusConnection *connection;

      if (!(connectable = g_network_address_parse (pair_to, BONSAI_DEFAULT_PORT, &error)))
        {
          g_printerr ("Invalid network address \"%s\": %s", pair_to, error->message);
          return EXIT_FAILURE;
        }

      if (!(certificate = bonsai_tls_certificate_new_for_user (NULL, &error)))
        {
          g_printerr ("Failed to load TLS certificate: %s\n", error->message);
          return EXIT_FAILURE;
        }

      identity = bonsai_tls_identity_new (certificate);
      client = bonsai_client_new (identity);
      end_point = bonsai_socket_end_point_new (connectable);

      if (!bonsai_client_connect (client, end_point, NULL, NULL, &error))
        {
          g_printerr ("Failed to connect to peer: %s\n", error->message);
          return EXIT_FAILURE;
        }

      connection = bonsai_client_get_connection (client);

      if (!bonsai_dao_repository_synchronize (repository, connection, NULL, &revision, NULL, &error))
        {
          g_printerr ("Failed to synchronize repository: %s\n", error->message);
          return EXIT_FAILURE;
        }

      g_printerr ("Now at revision \"%s\"\n", revision);
    }

  return EXIT_SUCCESS;
}
