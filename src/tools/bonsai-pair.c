/* bonsai-pair.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-pair"

#include "config.h"

#include <libbonsai.h>
#include <stdio.h>
#include <stdlib.h>

static GMainLoop *main_loop;
static gint exit_code = EXIT_FAILURE;

static void
on_pairing_requested_cb (BonsaiEnrollment *enrollment,
                         const gchar      *certificate_pem,
                         GVariant         *info,
                         const gchar      *token)
{
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autofree gchar *hash = NULL;
  GVariantIter iter;
  GVariant *value;
  gchar *key;
  gunichar ch;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_ENROLLMENT (enrollment));
  g_assert (certificate_pem != NULL);
  g_assert (token != NULL);
  g_assert (info != NULL);

  if (!(certificate = g_tls_certificate_new_from_pem (certificate_pem, -1, NULL)))
    {
      g_warning ("Received invalid certificate from daemon, ignoring");
      BONSAI_EXIT;
    }

  hash = bonsai_tls_certificate_get_hash (certificate);

  g_print ("Pairing requsted:\n");
  g_print ("   Token on Device: %s\n", token);
  g_print ("  Certificate Hash: %s\n", hash);

  g_variant_iter_init (&iter, info);
  while (g_variant_iter_loop (&iter, "{sv}", &key, &value))
    {
      if (g_str_equal (key, "certificate-pem"))
        continue;
      else
        {
          g_autofree gchar *str = g_variant_print (value, FALSE);
          g_autofree gchar *escape = g_strescape (str, NULL);

          g_print (" %18s: %s\n", key, escape);
        }
    }

  g_print ("\n");

  goto first;

again:
  while (ch != '\n')
    ch = getchar ();

first:
  g_print ("Enroll peer into Bonsai? [y]es/[n]o/[s]how certificate: ");

  ch = getchar ();

  if (ch == 's')
    {
      g_print ("%s\n", certificate_pem);
      goto again;
    }
  else if (ch == 'y' || ch == 'Y')
    {
      g_autoptr(GError) error = NULL;

      if (!bonsai_enrollment_call_enroll_sync (enrollment, certificate_pem, NULL, &error))
        g_printerr ("Failed to enroll certificate: %s\n", error->message);
      else
        exit_code = EXIT_SUCCESS;

      g_main_loop_quit (main_loop);
    }
  else if (ch == 'n' || ch == 'N')
    {
      g_main_loop_quit (main_loop);
    }
  else
    goto again;

  BONSAI_EXIT;
}

static void
on_pairing_ended_cb (BonsaiPairing *pairing)
{
  BONSAI_ENTRY;

  if (!bonsai_pairing_get_pairing (pairing))
    {
      g_print ("Pairing ended.\n");
      g_main_loop_quit (main_loop);
    }

  BONSAI_EXIT;
}

static gboolean
allow_pairing_via_dbus (BonsaiPairing     *pairing,
                        BonsaiEnrollment  *enrollment,
                        GError           **error)
{
  BONSAI_ENTRY;

  g_signal_connect (enrollment,
                    "pairing-requested",
                    G_CALLBACK (on_pairing_requested_cb),
                    NULL);
  g_signal_connect (pairing,
                    "notify::pairing",
                    G_CALLBACK (on_pairing_ended_cb),
                    NULL);

  if (!bonsai_pairing_call_begin_sync (pairing, NULL, error))
    BONSAI_RETURN (FALSE);

  g_print ("Waiting for peers to connect...\n");

  g_main_loop_run (main_loop);

  BONSAI_RETURN (exit_code == EXIT_SUCCESS);
}

static gboolean
pair_to_via_dbus (BonsaiPairing    *pairing,
                  GTlsCertificate  *certificate,
                  GError          **error)
{
  g_autofree gchar *chassis = NULL;
  g_autofree gchar *token = NULL;
  g_autofree gchar *pem = NULL;
  GVariantDict dict = G_VARIANT_DICT_INIT (NULL);

  g_assert (BONSAI_IS_PAIRING (pairing));
  g_assert (G_IS_TLS_CERTIFICATE (certificate));

  g_object_get (certificate, "certificate-pem", &pem, NULL);

  if (!bonsai_pairing_call_request_token_sync (pairing, pem, &token, NULL, error))
    return FALSE;

  chassis = bonsai_get_machine_info (BONSAI_MACHINE_INFO_KEY_CHASSIS);

  g_variant_dict_insert (&dict, "name", "s", g_get_real_name () ?: "");
  g_variant_dict_insert (&dict, "hostname", "s", g_get_host_name () ?: "");
  g_variant_dict_insert (&dict, "chassis", "s", chassis ?: "desktop");

  g_print ("Pair Token: %s\n", token);

  return bonsai_pairing_call_pair_sync (pairing,
                                        g_variant_dict_end (&dict),
                                        NULL,
                                        error);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *pair_to = NULL;
  gboolean allow_pairing = FALSE;
  GOptionEntry entries[] = {
    { "allow-pairing", 'a', 0, G_OPTION_ARG_NONE, &allow_pairing, "Allow the Bonsai daemon to accept new devices" },
    { "pair-to", 'h', 0, G_OPTION_ARG_STRING, &pair_to, "The hostname and port of the bonsai peer", "HOST:PORT" },
    { NULL }
  };

  main_loop = g_main_loop_new (NULL, FALSE);
  context = g_option_context_new ("- Begin pairing a bonsai peer");
  g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);

  g_option_context_set_summary (context, "\
  The bonsai-pair utility allows you to enable pairing on the device\n\
  running the bonsai daemon (bonsaid) as well pair a client device to\n\
  the device running the bonsai daemon.\n\
\n\
  Examples:\n\
\n\
    # On the device running the bonsai daemon to allow pairing\n\
    # for 60 seconds\n\
    bonsai-pair --allow-pairing\n\
\n\
    # After a client conencts to the daemon, you will be presented\n\
    # with the ability to pair the device.\n\
\n\
    # On the client device, to be paired to the bonsai daemon\n\
    bonsai-pair --pair-to hostname:port");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (allow_pairing && pair_to)
    {
      g_printerr ("--allow-pairing and --pair-to cannot be combined\n");
      return EXIT_FAILURE;
    }

  if (allow_pairing)
    {
      g_autoptr(BonsaiPairing) pairing = NULL;
      g_autoptr(BonsaiEnrollment) enrollment = NULL;

      if (!(enrollment = bonsai_enrollment_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                                                   G_DBUS_PROXY_FLAGS_NONE,
                                                                   "org.gnome.Bonsai.Daemon",
                                                                   "/org/gnome/Bonsai/Enrollment",
                                                                   NULL,
                                                                   &error)) ||
         !(pairing = bonsai_pairing_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                                            G_DBUS_PROXY_FLAGS_NONE,
                                                            "org.gnome.Bonsai.Daemon",
                                                            "/org/gnome/Bonsai/Pairing",
                                                            NULL,
                                                            &error)))
        g_error ("%s", error->message);

      if (!allow_pairing_via_dbus (pairing, enrollment, &error))
        g_error ("%s", error ? error->message : "No device was paired");

      return EXIT_SUCCESS;
    }
  else if (pair_to)
    {
      g_autoptr(BonsaiClient) client = NULL;
      g_autoptr(BonsaiPairing) pairing = NULL;
      g_autoptr(GSocketConnectable) connectable = NULL;
      g_autoptr(GTlsCertificate) certificate = NULL;
      g_autoptr(BonsaiIdentity) identity = NULL;
      g_autoptr(BonsaiEndPoint) end_point = NULL;
      GDBusConnection *connection;

      if (!(connectable = g_network_address_parse (pair_to, BONSAI_DEFAULT_PORT, &error)))
        g_error ("%s", error->message);

      if (!(certificate = bonsai_tls_certificate_new_for_user (NULL, &error)))
        g_error ("%s", error->message);

      identity = bonsai_tls_identity_new (certificate);
      client = bonsai_client_new (identity);
      end_point = bonsai_socket_end_point_new (connectable);

      if (!bonsai_client_connect (client, end_point, NULL, NULL, &error))
        g_error("%s", error->message);

      connection = bonsai_client_get_connection (client);
      pairing = bonsai_pairing_proxy_new_sync (connection,
                                               G_DBUS_PROXY_FLAGS_NONE,
                                               NULL,
                                               "/org/gnome/Bonsai/Pairing",
                                               NULL,
                                               &error);

      if (pairing == NULL)
        g_error ("%s", error->message);

      if (!pair_to_via_dbus (pairing, certificate, &error))
        g_printerr ("%s\n", error ? error->message : "Device was not paired.");
      else
        exit_code = EXIT_SUCCESS;
    }
  else
    {
      g_autofree gchar *help = g_option_context_get_help (context, FALSE, NULL);

      g_printerr ("%s\n", help);
      return EXIT_FAILURE;
    }

  return exit_code;
}
