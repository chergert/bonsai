/* bonsai-dao-collection.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_COLLECTION (bonsai_dao_collection_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoCollection, bonsai_dao_collection, BONSAI, DAO_COLLECTION, GObject)

const gchar         *bonsai_dao_collection_get_name        (BonsaiDaoCollection  *self);
BonsaiDaoRepository *bonsai_dao_collection_ref_repository  (BonsaiDaoCollection  *self);
GType                bonsai_dao_collection_get_object_type (BonsaiDaoCollection  *self);
gboolean             bonsai_dao_collection_ensure_index    (BonsaiDaoCollection  *self,
                                                            BonsaiDaoTransaction *transaction,
                                                            GCancellable         *cancellable,
                                                            GError              **error,
                                                            const gchar          *first_property,
                                                            ...) G_GNUC_NULL_TERMINATED;

G_END_DECLS
