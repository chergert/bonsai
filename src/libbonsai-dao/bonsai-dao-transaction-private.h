/* bonsai-dao-transaction-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"
#include "bonsai-dao-transaction-log-private.h"

#include <lmdb.h>

G_BEGIN_DECLS

BonsaiDaoTransaction    *_bonsai_dao_transaction_new        (BonsaiDaoRepository     *repository,
                                                             gboolean                 writeable);
MDB_txn                 *_bonsai_dao_transaction_get_native (BonsaiDaoTransaction    *self);
gboolean                 _bonsai_dao_transaction_revert_log (BonsaiDaoTransaction    *self,
                                                             BonsaiDaoTransactionLog *log);
void                     _bonsai_dao_transaction_reset_log  (BonsaiDaoTransaction    *self);

G_END_DECLS
