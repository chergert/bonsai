/* bonsai-dao-utils.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-utils"

#include "config.h"

#include "bonsai-dao-utils.h"

#define DEFINE_INT_TRANSFORM(type, name)                              \
static gboolean                                                       \
variant_to_##name (GVariant *variant,                                 \
                   GValue   *value)                                   \
{                                                                     \
  type v;                                                             \
                                                                      \
  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT16))           \
    v = g_variant_get_int16 (variant);                                \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT32))      \
    v = g_variant_get_int32 (variant);                                \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT64))      \
    v = g_variant_get_int64 (variant);                                \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT16))     \
    v = g_variant_get_uint16 (variant);                               \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT32))     \
    v = g_variant_get_uint32 (variant);                               \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT64))     \
    v = g_variant_get_uint64 (variant);                               \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_DOUBLE))     \
    v = g_variant_get_double (variant);                               \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BOOLEAN))    \
    v = g_variant_get_boolean (variant);                              \
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BYTE))       \
    v = g_variant_get_byte (variant);                                 \
  else                                                                \
    return FALSE;                                                     \
                                                                      \
  g_value_set_##name (value, v);                                      \
                                                                      \
  return TRUE;                                                        \
}

DEFINE_INT_TRANSFORM(gint, int)
DEFINE_INT_TRANSFORM(guint, uint)
DEFINE_INT_TRANSFORM(gint64, int64)
DEFINE_INT_TRANSFORM(guint64, uint64)
DEFINE_INT_TRANSFORM(glong, long)
DEFINE_INT_TRANSFORM(gulong, ulong)
DEFINE_INT_TRANSFORM(gdouble, double)
DEFINE_INT_TRANSFORM(gfloat, float)
DEFINE_INT_TRANSFORM(guint, enum)
DEFINE_INT_TRANSFORM(guint, flags)
DEFINE_INT_TRANSFORM(gboolean, boolean)

static gboolean
variant_to_string (GVariant *variant,
                   GValue   *value)
{
  gchar *s = NULL;

  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING))
    s = g_variant_dup_string (variant, NULL);
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("ms")))
    g_variant_get (variant, "ms", &s);
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("()")))
    { /* Do Nothing */ }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BYTESTRING))
    s = g_variant_dup_bytestring (variant, NULL);
  else
    return FALSE;

  g_value_take_string (value, s);

  return TRUE;
}

static gboolean
variant_to_strv (GVariant *variant,
                 GValue   *value)
{
  g_autofree const gchar **strv = NULL;

  if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("mas")))
    {
      gchar **r = NULL;
      g_variant_get (variant, "mas", &r, NULL);
      g_value_take_boxed (value, r);
      return TRUE;
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE ("()")))
    {
      g_value_take_boxed (value, NULL);
      return TRUE;
    }
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_STRING_ARRAY))
    strv = g_variant_get_strv (variant, NULL);
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_BYTESTRING_ARRAY))
    strv = g_variant_get_bytestring_array (variant, NULL);
  else
    return FALSE;

  g_value_set_boxed (value, strv);

  return TRUE;
}

static gboolean
variant_to_variant (GVariant *variant,
                    GValue   *value)
{
  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARIANT))
    g_value_take_variant (value, g_variant_get_variant (variant));
  else
    g_value_set_variant (value, variant);

  return TRUE;
}

gboolean
bonsai_dao_variant_to_value (GVariant *variant,
                             GValue   *value)
{
  g_autoptr(GVariant) child = NULL;

  if (!G_VALUE_TYPE (value))
    {
      g_critical ("value must be initialized to the destination type, variant type is %s",
                  variant ? g_variant_get_type_string (variant) : "(null)");
      return FALSE;
    }

  /* Unwrap the container if necessary */
  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARIANT))
    {
      child = g_variant_get_variant (variant);
      variant = child;
    }

  switch (G_VALUE_TYPE (value))
    {
    case G_TYPE_INT:       return variant_to_int (variant, value);
    case G_TYPE_UINT:      return variant_to_uint (variant, value);
    case G_TYPE_INT64:     return variant_to_int64 (variant, value);
    case G_TYPE_UINT64:    return variant_to_uint64 (variant, value);
    case G_TYPE_LONG:      return variant_to_long (variant, value);
    case G_TYPE_ULONG:     return variant_to_ulong (variant, value);
    case G_TYPE_STRING:    return variant_to_string (variant, value);
    case G_TYPE_DOUBLE:    return variant_to_double (variant, value);
    case G_TYPE_FLOAT:     return variant_to_float (variant, value);
    case G_TYPE_BOOLEAN:   return variant_to_boolean (variant, value);
    case G_TYPE_VARIANT:   return variant_to_variant (variant, value);

    default:
      if (G_VALUE_HOLDS (value, G_TYPE_STRV))
        return variant_to_strv (variant, value);
      else if (G_VALUE_HOLDS (value, G_TYPE_ENUM))
        return variant_to_enum (variant, value);
      else if (G_VALUE_HOLDS (value, G_TYPE_FLAGS))
        return variant_to_flags (variant, value);
      else if (G_VALUE_HOLDS (value, G_TYPE_DATE_TIME))
        {
          if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT64))
            {
              gint64 v = g_variant_get_int64 (variant);
              if (v == -1)
                g_value_set_boxed (value, NULL);
              else
                g_value_take_boxed (value, g_date_time_new_from_unix_utc (v));
              return TRUE;
            }
        }

      break;
    }

  return FALSE;
}

GVariant *
bonsai_dao_value_to_variant (const GValue *value)
{
  g_return_val_if_fail (value != NULL, NULL);
  g_return_val_if_fail (G_IS_VALUE (value), NULL);

  switch (G_VALUE_TYPE (value))
    {
    case G_TYPE_BOOLEAN: return g_variant_new_boolean (g_value_get_boolean (value));
    case G_TYPE_INT: return g_variant_new_int32 (g_value_get_int (value));
    case G_TYPE_UINT: return g_variant_new_uint32 (g_value_get_uint (value));
    case G_TYPE_INT64: return g_variant_new_int64 (g_value_get_int64 (value));
    case G_TYPE_UINT64: return g_variant_new_uint64 (g_value_get_uint64 (value));
    case G_TYPE_LONG: return g_variant_new_int64 (g_value_get_long (value));
    case G_TYPE_ULONG: return g_variant_new_uint64 (g_value_get_ulong (value));
    case G_TYPE_DOUBLE: return g_variant_new_double (g_value_get_double (value));
    case G_TYPE_FLOAT: return g_variant_new_double (g_value_get_float (value));
    case G_TYPE_VARIANT: return g_variant_new_variant (g_value_get_variant (value));
    case G_TYPE_CHAR: return g_variant_new_int32 (g_value_get_schar (value));
    case G_TYPE_UCHAR: return g_variant_new_uint32 (g_value_get_uchar (value));
    case G_TYPE_STRING: {
      const gchar *s = g_value_get_string (value);
      /* Normally we would do "ms" for a maybe-string here, but that
       * cannot be serialized over D-Bus. So instead we use the empty
       * tuple to represent the null string and deal with it when decoding.
       */
      if (s)
        return g_variant_new_string (s);
      else
        return g_variant_new_tuple (NULL, 0);
    }

    default:
      if (G_VALUE_HOLDS_ENUM (value))
        return g_variant_new_uint32 (g_value_get_enum (value));
      else if (G_VALUE_HOLDS_FLAGS (value))
        return g_variant_new_uint32 (g_value_get_flags (value));
      else if (G_VALUE_HOLDS (value, G_TYPE_STRV))
        {
          const gchar * const *strv = g_value_get_boxed (value);

          if (strv)
            return g_variant_new_strv (strv, -1);
          else
            return g_variant_new_tuple (NULL, 0);
        }
      else if (G_VALUE_HOLDS (value, G_TYPE_DATE_TIME))
        {
          GDateTime *dt = g_value_get_boxed (value);
          gint64 v = dt ? g_date_time_to_unix (dt) : -1;
          return g_variant_new_int64 (v);
        }

      return NULL;
    }
}

GVariant *
bonsai_dao_increment (GVariant *variant,
                      gint64    by)
{
  if (variant == NULL)
    return g_variant_new_int64 (by);

  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT16))
    return g_variant_new_int16 (by + g_variant_get_int16 (variant));
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT32))
    return g_variant_new_int32 (by + g_variant_get_int32 (variant));
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_INT64))
    return g_variant_new_int64 (by + g_variant_get_int64 (variant));
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT16))
    return g_variant_new_uint16 (by + g_variant_get_uint16 (variant));
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT32))
    return g_variant_new_uint32 (by + g_variant_get_uint32 (variant));
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_UINT64))
    return g_variant_new_uint64 (by + g_variant_get_uint64 (variant));
  else if (g_variant_is_of_type (variant, G_VARIANT_TYPE_DOUBLE))
    return g_variant_new_double (by + g_variant_get_double (variant));

  return NULL;
}

void
bonsai_dao_variant_builder_append_value (GVariantBuilder *builder,
                                         const GValue    *value)
{
  g_assert (builder != NULL);
  g_assert (value != NULL);

  g_variant_builder_open (builder, G_VARIANT_TYPE_VARIANT);

  if (G_VALUE_HOLDS_INT (value))
    g_variant_builder_add (builder, "i", g_value_get_int (value));
  else if (G_VALUE_HOLDS_UINT (value))
    g_variant_builder_add (builder, "u", g_value_get_uint (value));
  else if (G_VALUE_HOLDS_INT64 (value))
    g_variant_builder_add (builder, "x", g_value_get_int64 (value));
  else if (G_VALUE_HOLDS_UINT64 (value))
    g_variant_builder_add (builder, "t", g_value_get_uint64 (value));
  else if (G_VALUE_HOLDS_BOOLEAN (value))
    g_variant_builder_add (builder, "b", g_value_get_boolean (value));
  else if (G_VALUE_HOLDS_STRING (value))
    {
      const gchar *s = g_value_get_string (value);
      if (s)
        g_variant_builder_add (builder, "s", s);
      else
        g_variant_builder_add (builder, "()");
    }
  else if (G_VALUE_HOLDS_ENUM (value))
    g_variant_builder_add (builder, "u", g_value_get_enum (value));
  else if (G_VALUE_HOLDS_FLAGS (value))
    g_variant_builder_add (builder, "u", g_value_get_flags (value));
  else if (G_VALUE_HOLDS_CHAR (value))
    g_variant_builder_add (builder, "i", g_value_get_schar (value));
  else if (G_VALUE_HOLDS_UCHAR (value))
    g_variant_builder_add (builder, "u", g_value_get_uchar (value));
  else if (G_VALUE_HOLDS_LONG (value))
    g_variant_builder_add (builder, "x", (gint64)g_value_get_long (value));
  else if (G_VALUE_HOLDS_ULONG (value))
    g_variant_builder_add (builder, "t", (guint64)g_value_get_ulong (value));
  else if (G_VALUE_HOLDS_FLOAT (value))
    g_variant_builder_add (builder, "d", (gdouble)g_value_get_float (value));
  else if (G_VALUE_HOLDS_DOUBLE (value))
    g_variant_builder_add (builder, "d", g_value_get_double (value));
  else if (G_VALUE_HOLDS_GTYPE (value))
    g_variant_builder_add (builder, "s", g_type_name (g_value_get_gtype (value)));
  else if (G_VALUE_HOLDS_VARIANT (value))
    g_variant_builder_add (builder, "v", g_value_get_variant (value));
  else if (G_VALUE_HOLDS (value, G_TYPE_DATE_TIME))
    {
      GDateTime *dt = g_value_get_boxed (value);
      gint64 v = dt ? g_date_time_to_unix (dt) : -1;
      g_variant_builder_add (builder, "x", v);
    }
  else if (G_VALUE_HOLDS (value, G_TYPE_STRV))
    {
      const gchar * const *strv = g_value_get_boxed (value);

      if (strv != NULL)
        {
          g_variant_builder_add (builder, "as", strv);
        }
      else
        {
          g_variant_builder_add (builder, "()");
        }
    }
  else
    g_critical ("Unsupported property type: %s", G_VALUE_TYPE_NAME (value));

  g_variant_builder_close (builder);
}

const gchar *
bonsai_dao_get_variant_type_string (GType type)
{
  switch (type)
    {
    case G_TYPE_BOOLEAN: return "b";
    case G_TYPE_INT: return "i";
    case G_TYPE_UINT: return "u";
    case G_TYPE_INT64: return "x";
    case G_TYPE_UINT64: return "t";
    case G_TYPE_LONG: return "x";
    case G_TYPE_ULONG: return "t";
    case G_TYPE_DOUBLE: return "d";
    case G_TYPE_FLOAT: return "d";
    case G_TYPE_VARIANT: return "v";
    case G_TYPE_CHAR: return "i";
    case G_TYPE_UCHAR: return "u";
    case G_TYPE_STRING: return "ms";

    default:
      if (G_TYPE_IS_ENUM (type) || G_TYPE_IS_FLAGS (type))
        return "u";
      else if (type == G_TYPE_STRV)
        return "as";
      else if (type == G_TYPE_DATE_TIME)
        return "x";

      return NULL;
    }
}
