/* bonsai-dao-index-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <lmdb.h>

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_INDEX (bonsai_dao_index_get_type())

G_DECLARE_DERIVABLE_TYPE (BonsaiDaoIndex, bonsai_dao_index, BONSAI, DAO_INDEX, GObject)

struct _BonsaiDaoIndexClass
{
  GObjectClass parent_class;

  gboolean (*create_cursor) (BonsaiDaoIndex  *self,
                             MDB_txn         *txn,
                             MDB_cursor     **cursor);
  gboolean (*move_first)    (BonsaiDaoIndex  *self,
                             BonsaiDaoQuery  *query,
                             MDB_cursor      *cursor,
                             MDB_val         *key,
                             MDB_val         *val);
  gboolean (*move_next)     (BonsaiDaoIndex  *self,
                             MDB_cursor      *cursor,
                             MDB_val         *key,
                             MDB_val         *val);
  gboolean (*insert)        (BonsaiDaoIndex  *self,
                             MDB_txn         *txn,
                             const gchar     *id,
                             GVariant        *variant,
                             GError         **error);
  gboolean (*delete)        (BonsaiDaoIndex  *self,
                             MDB_txn         *txn,
                             const gchar     *id,
                             GVariant        *variant,
                             GError         **error);
  gboolean (*has_pspec)     (BonsaiDaoIndex  *self,
                             GParamSpec      *pspec);
};

BonsaiDaoIndex *_bonsai_dao_index_new_for_id    (BonsaiDaoRepository  *repository,
                                                 MDB_dbi               slot);
gboolean        _bonsai_dao_index_create_cursor (BonsaiDaoIndex       *self,
                                                 MDB_txn              *txn,
                                                 MDB_cursor          **cursor);
gboolean        _bonsai_dao_index_move_first    (BonsaiDaoIndex       *self,
                                                 BonsaiDaoQuery       *query,
                                                 MDB_cursor           *cursor,
                                                 MDB_val              *key,
                                                 MDB_val              *val);
gboolean        _bonsai_dao_index_move_next     (BonsaiDaoIndex       *self,
                                                 MDB_cursor           *cursor,
                                                 MDB_val              *key,
                                                 MDB_val              *val);
gboolean        _bonsai_dao_index_move_prev     (BonsaiDaoIndex       *self,
                                                 MDB_cursor           *cursor,
                                                 MDB_val              *key,
                                                 MDB_val              *val);
gboolean        _bonsai_dao_index_move_last     (BonsaiDaoIndex       *self,
                                                 MDB_cursor           *cursor,
                                                 MDB_val              *key,
                                                 MDB_val              *val);
gboolean        _bonsai_dao_index_insert        (BonsaiDaoIndex       *self,
                                                 MDB_txn              *txn,
                                                 const gchar          *id,
                                                 GVariant             *variant,
                                                 GError              **error);
gboolean        _bonsai_dao_index_delete        (BonsaiDaoIndex       *self,
                                                 MDB_txn              *txn,
                                                 const gchar          *id,
                                                 GVariant             *variant,
                                                 GError              **error);
gboolean        _bonsai_dao_index_has_pspec     (BonsaiDaoIndex       *self,
                                                 GParamSpec           *pspec);

G_END_DECLS
