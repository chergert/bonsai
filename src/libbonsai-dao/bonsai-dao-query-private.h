/* bonsai-dao-query-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

#include <lmdb.h>

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_QUERY (bonsai_dao_query_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoQuery, bonsai_dao_query, BONSAI, DAO_QUERY, GObject)

BonsaiDaoQuery  *_bonsai_dao_query_new               (BonsaiDaoRepository   *repository,
                                                      BonsaiDaoCollection   *collection,
                                                      BonsaiDaoTransaction  *transaction,
                                                      BonsaiDaoQueryClause  *clause);
gboolean         _bonsai_dao_query_create_cursor     (BonsaiDaoQuery        *query,
                                                      MDB_cursor           **cursor,
                                                      BonsaiDaoIndex       **index,
                                                      GError               **error);
gboolean         _bonsai_dao_query_matches           (BonsaiDaoQuery        *self,
                                                      const gchar           *id,
                                                      GVariant              *variant);
gboolean         _bonsai_dao_query_get_bounds        (BonsaiDaoQuery        *self,
                                                      GParamSpec            *pspec,
                                                      GValue                *lower,
                                                      GValue                *upper);
GParamSpec     **_bonsai_dao_query_pspecs            (BonsaiDaoQuery        *self,
                                                      guint                 *n_pspecs);
gboolean         _bonsai_dao_query_clause_get_bounds (BonsaiDaoQueryClause  *clause,
                                                      GParamSpec            *pspec,
                                                      GValue                *lower,
                                                      GValue                *upper);
gchar           *_bonsai_dao_query_to_string         (BonsaiDaoQuery        *self);

G_END_DECLS
