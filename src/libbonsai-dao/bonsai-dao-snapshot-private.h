/* bonsai-dao-snapshot-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <lmdb.h>

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

void               _bonsai_dao_snapshot_append_take_value     (BonsaiDaoSnapshot  *self,
                                                               GParamSpec         *pspec,
                                                               GValue             *value);
BonsaiDaoObject   *_bonsai_dao_snapshot_create_object         (BonsaiDaoSnapshot  *self,
                                                               const gchar        *id,
                                                               GType               object_type);
BonsaiDaoSnapshot *_bonsai_dao_snapshot_dup_for_id            (BonsaiDaoSnapshot  *self,
                                                               const gchar        *id);
GVariant          *_bonsai_dao_snapshot_get_object_as_variant (BonsaiDaoSnapshot  *self,
                                                               const gchar        *id,
                                                               GError            **error);

G_END_DECLS
