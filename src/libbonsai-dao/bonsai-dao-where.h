/* bonsai-dao-where.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <gobject/gvaluecollector.h>

#include "bonsai-dao-object.h"
#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#ifndef __GI_SCANNER__

static inline BonsaiDaoQueryClause *
_BONSAI_DAO_WHERE_BOOLEAN (BonsaiDaoQueryClause *left,
                           BonsaiDaoQueryClause *right)
{
  BonsaiDaoQueryClause *ret;

  g_return_val_if_fail (left != NULL, NULL);

  ret = g_slice_new0 (BonsaiDaoQueryClause);
  ret->boolean.lhs = left;
  ret->boolean.rhs = right;

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_NOT (BonsaiDaoQueryClause *lhs)
{
  BonsaiDaoQueryClause *ret;

  if ((ret = _BONSAI_DAO_WHERE_BOOLEAN (lhs, NULL)))
    ret->oper = BONSAI_DAO_OPERATOR_NOT;

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_AND (BonsaiDaoQueryClause *left,
                      BonsaiDaoQueryClause *right)
{
  BonsaiDaoQueryClause *ret;

  if ((ret = _BONSAI_DAO_WHERE_BOOLEAN (left, right)))
    ret->oper = BONSAI_DAO_OPERATOR_AND;

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_OR (BonsaiDaoQueryClause *left,
                     BonsaiDaoQueryClause *right)
{
  BonsaiDaoQueryClause *ret;

  if ((ret = _BONSAI_DAO_WHERE_BOOLEAN (left, right)))
    ret->oper = BONSAI_DAO_OPERATOR_OR;

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_NOR (BonsaiDaoQueryClause *left,
                      BonsaiDaoQueryClause *right)
{
  return BONSAI_DAO_WHERE_NOT (BONSAI_DAO_WHERE_OR (left, right));
}

static inline BonsaiDaoQueryClause *
_BONSAI_DAO_WHERE_PREDICATE (GType        type,
                             const gchar *property,
                             va_list     *args)
{
  g_autoptr(GTypeClass) klass = NULL;
  BonsaiDaoQueryClause *ret;
  GParamSpec *pspec;
  gchar *errstr = NULL;

  g_return_val_if_fail (G_TYPE_IS_OBJECT (type), NULL);
  g_return_val_if_fail (g_type_is_a (type, BONSAI_TYPE_DAO_OBJECT), NULL);

  if (!(klass = g_type_class_ref (type)) ||
      !(pspec = g_object_class_find_property (G_OBJECT_CLASS (klass), property)))
    {
      g_critical ("Type %s has no property %s", g_type_name (type), property);
      return NULL;
    }


  ret = g_slice_new0 (BonsaiDaoQueryClause);
  ret->predicate.lhs = g_param_spec_ref (pspec);

  G_VALUE_COLLECT_INIT (&ret->predicate.rhs, pspec->value_type, *args, 0, &errstr);

  if (errstr)
    {
      g_param_spec_unref (ret->predicate.lhs);
      g_slice_free (BonsaiDaoQueryClause, ret);
      return NULL;
    }

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_EQUAL (GType        type,
                        const gchar *property,
                        ...)
{
  BonsaiDaoQueryClause *ret;
  va_list args;

  va_start (args, property);
  if ((ret = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    ret->oper = BONSAI_DAO_OPERATOR_EQUAL;
  va_end (args);

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_NOT_EQUAL (GType        type,
                            const gchar *property,
                            ...)
{
  BonsaiDaoQueryClause *ret;
  va_list args;

  va_start (args, property);
  if ((ret = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    ret->oper = BONSAI_DAO_OPERATOR_EQUAL;
  va_end (args);

  return BONSAI_DAO_WHERE_NOT (ret);
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_GREATER_THAN (GType        type,
                               const gchar *property,
                               ...)
{
  BonsaiDaoQueryClause *ret;
  va_list args;

  va_start (args, property);
  if ((ret = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    ret->oper = BONSAI_DAO_OPERATOR_GREATER_THAN;
  va_end (args);

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_GREATER_THAN_OR_EQUAL (GType        type,
                                        const gchar *property,
                                        ...)
{
  BonsaiDaoQueryClause *lt;
  BonsaiDaoQueryClause *eq;
  va_list args;

  va_start (args, property);
  if ((lt = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    lt->oper = BONSAI_DAO_OPERATOR_GREATER_THAN;
  va_end (args);

  va_start (args, property);
  if ((eq = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    eq->oper = BONSAI_DAO_OPERATOR_EQUAL;
  va_end (args);

  return BONSAI_DAO_WHERE_OR (lt, eq);
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_LESS_THAN (GType        type,
                            const gchar *property,
                            ...)
{
  BonsaiDaoQueryClause *ret;
  va_list args;

  va_start (args, property);
  if ((ret = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    ret->oper = BONSAI_DAO_OPERATOR_LESS_THAN;
  va_end (args);

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_LESS_THAN_OR_EQUAL (GType        type,
                                     const gchar *property,
                                     ...)
{
  BonsaiDaoQueryClause *lt;
  BonsaiDaoQueryClause *eq;
  va_list args;

  va_start (args, property);
  if ((lt = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    lt->oper = BONSAI_DAO_OPERATOR_LESS_THAN;
  va_end (args);

  va_start (args, property);
  if ((eq = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    eq->oper = BONSAI_DAO_OPERATOR_EQUAL;
  va_end (args);

  return BONSAI_DAO_WHERE_OR (lt, eq);
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_IN (GType        type,
                     const gchar *property,
                     ...)
{
  BonsaiDaoQueryClause *ret;
  va_list args;

  va_start (args, property);
  if ((ret = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    ret->oper = BONSAI_DAO_OPERATOR_IN;
  va_end (args);

  return ret;
}

static inline BonsaiDaoQueryClause *
BONSAI_DAO_WHERE_NOT_IN (GType        type,
                         const gchar *property,
                         ...)
{
  BonsaiDaoQueryClause *ret;
  va_list args;

  va_start (args, property);
  if ((ret = _BONSAI_DAO_WHERE_PREDICATE (type, property, &args)))
    ret->oper = BONSAI_DAO_OPERATOR_IN;
  va_end (args);

  return BONSAI_DAO_WHERE_NOT (ret);
}

static inline void
bonsai_dao_query_clause_free (BonsaiDaoQueryClause *where)
{
  switch (where->oper)
    {
    case BONSAI_DAO_OPERATOR_OR:
    case BONSAI_DAO_OPERATOR_AND:
      bonsai_dao_query_clause_free (where->boolean.lhs);
      bonsai_dao_query_clause_free (where->boolean.rhs);
      break;

    case BONSAI_DAO_OPERATOR_NOT:
      bonsai_dao_query_clause_free (where->boolean.lhs);
      break;

    case BONSAI_DAO_OPERATOR_EQUAL:
    case BONSAI_DAO_OPERATOR_IN:
    case BONSAI_DAO_OPERATOR_LESS_THAN:
    case BONSAI_DAO_OPERATOR_GREATER_THAN:
      g_param_spec_unref (where->predicate.lhs);
      g_value_unset (&where->predicate.rhs);
      break;

    default:
      g_assert_not_reached ();
    }

  g_slice_free (BonsaiDaoQueryClause, where);
}

#endif

G_END_DECLS
