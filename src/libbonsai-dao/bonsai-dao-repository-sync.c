/* bonsai-dao-repository-sync.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-repository-sync"

#include "config.h"

#include <lmdb.h>

#include "bonsai-dao-repository.h"
#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-changeset.h"
#include "bonsai-dao-transaction.h"
#include "bonsai-dao-transaction-log-private.h"
#include "bonsai-dao-transaction-private.h"

#include "ipc-dao-log.h"
#include "ipc-dao-sync.h"

static gboolean
revert_local_changes (BonsaiDaoRepository   *self,
                      BonsaiDaoTransaction  *txn,
                      GPtrArray             *changes,
                      GError               **error)
{
  MDB_dbi slot = 0;
  MDB_cursor *cursor = NULL;
  MDB_txn *native;
  MDB_val key, val;
  gboolean ret = FALSE;
  gint r;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_REPOSITORY (self));
  g_assert (BONSAI_IS_DAO_TRANSACTION (txn));
  g_assert (changes != NULL);

  native = _bonsai_dao_transaction_get_native (txn);

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (native, "org.gnome.Bonsai.Dao.Log", 0, &slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_cursor_open (native, slot, &cursor), error, failure);

  r = mdb_cursor_get (cursor, &key, &val, MDB_LAST);

  do
    {
      g_autoptr(BonsaiDaoTransactionLog) log = NULL;
      g_autoptr(GVariant) variant = NULL;
      g_autoptr(GBytes) bytes = NULL;

      if (r == MDB_NOTFOUND)
        break;

      if (r != 0)
        {
          g_set_error_literal (error,
                               BONSAI_DAO_REPOSITORY_ERROR,
                               r,
                               mdb_strerror (r));
          BONSAI_GOTO (failure);
        }

      bytes = g_bytes_new (val.mv_data, val.mv_size);
      variant = g_variant_new_from_bytes (G_VARIANT_TYPE ("a(sa{sv})"), bytes, TRUE);
      log = bonsai_dao_transaction_log_from_variant (variant);

      if (!_bonsai_dao_transaction_revert_log (txn, log))
        BONSAI_GOTO (failure);

      g_ptr_array_add (changes, g_object_ref (log));

      r = mdb_cursor_get (cursor, &key, &val, MDB_PREV);
    }
  while (TRUE);

  ret = TRUE;

failure:

  g_clear_pointer (&cursor, mdb_cursor_close);

  BONSAI_RETURN (ret);
}

static gboolean
clear_local_changes (BonsaiDaoRepository   *self,
                     BonsaiDaoTransaction  *txn,
                     GError               **error)
{
  MDB_cursor *cursor;
  MDB_txn *native;
  MDB_dbi slot;
  MDB_val key, val;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_REPOSITORY (self));
  g_assert (BONSAI_IS_DAO_TRANSACTION (txn));

  native = _bonsai_dao_transaction_get_native (txn);

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (native, "org.gnome.Bonsai.Dao.Log", 0, &slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_cursor_open (native, slot, &cursor), error, failure);

  if (mdb_cursor_get (cursor, &key, &val, MDB_FIRST) == 0)
    {
      do
        GOTO_LABEL_IF_NON_ZERO (mdb_cursor_del (cursor, 0), error, failure);
      while (mdb_cursor_get (cursor, &key, &val, MDB_NEXT) == 0);
    }

  BONSAI_RETURN (TRUE);

failure:
  BONSAI_RETURN (FALSE);
}

static gboolean
bonsai_dao_repository_synchronize_log (BonsaiDaoRepository   *self,
                                       IpcDaoLog             *dao_log,
                                       BonsaiDaoTransaction  *txn,
                                       GCancellable          *cancellable,
                                       gchar                **new_revision,
                                       GError               **error)
{
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GPtrArray) changes = NULL;
  g_autoptr(GError) local_error = NULL;
  g_autofree gchar *identifier = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_REPOSITORY (self));
  g_assert (IPC_IS_DAO_LOG (dao_log));
  g_assert (BONSAI_IS_DAO_TRANSACTION (txn));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (new_revision != NULL);

  *new_revision = NULL;

  /*
   * The synchronization is performed using the following steps.
   *
   * - First we need to rollback all changes that we've had since we
   *   last checked in with the Bonsai peer. In particular, we can read
   *   our transaction log in reverse and revert each entry one-by-one.
   *   For each object we revert, we need to keep a copy of the most
   *   recent version so that we can use it in case we have a merge
   *   conflict as the duplicate form.
   *
   * - Then we apply changesets from the peer as we read them from the
   *   peer D-Bus service.
   *
   * - Re-apply our changesets that we previous reverted. If we have a
   *   failure (merge conflict) then we need to consult about what to do.
   *   In particulary, we will typically duplicate the object and take
   *   our stashed object as the new object (with new object-id) to insert.
   *
   * - Next we empty out our entire transaction log database and update
   *   the "sync token" in our metadata collection.
   *
   * - Finally we commit the transaction.
   */

  changes = g_ptr_array_new_with_free_func (g_object_unref);
  if (!revert_local_changes (self, txn, changes, &local_error))
    BONSAI_GOTO (failure);

  /* TODO: Stash the local changes to be re-applied */

  /* Clear our local changes so our new applications will be re-inserted
   * into the local changes log.
   */
  if (!clear_local_changes (self, txn, &local_error))
    BONSAI_GOTO (failure);

  _bonsai_dao_transaction_reset_log (txn);

  /* Now apply the upstream changes first */
  while (ipc_dao_log_call_read_sync (dao_log, &reply, cancellable, &local_error))
    {
      g_autoptr(BonsaiDaoTransactionLog) log = NULL;

      g_assert (reply != NULL);
      g_assert (g_variant_is_of_type (reply, G_VARIANT_TYPE ("a(sa{sv})")));

      if (g_variant_n_children (reply) == 0)
        break;

      if (!(log = bonsai_dao_transaction_log_from_variant (reply)))
        {
          local_error = g_error_new_literal (G_IO_ERROR,
                                             G_IO_ERROR_INVALID_DATA,
                                             "Failed to decode transaction log");
          BONSAI_GOTO (failure);
        }

      bonsai_dao_transaction_apply_log (txn, log, FALSE);

      if (bonsai_dao_transaction_has_error (txn))
        BONSAI_GOTO (failure);

      g_clear_pointer (&identifier, g_free);
      identifier = g_strdup (bonsai_dao_transaction_log_get_identifier (log));
    }

  /* Reset the transaction log so we only get our new entries written */
  _bonsai_dao_transaction_reset_log (txn);

  /* Now reapply our local changes on-top of the remote changes */
  for (guint i = changes->len; i > 0; i--)
    {
      BonsaiDaoTransactionLog *log = g_ptr_array_index (changes, i - 1);

      if (!bonsai_dao_transaction_apply_log (txn, log, TRUE))
        BONSAI_GOTO (failure);
    }

  /* Stash the revsision we made it to so that we start from here
   * the next time that we apply upstream changes.
   */
  if (identifier == NULL)
    {
      identifier = _bonsai_dao_repository_get_revision (self, txn);
      if (identifier == NULL)
        identifier = g_strdup ("");
    }
  _bonsai_dao_repository_set_revision (self, txn, identifier, &local_error);
  *new_revision = g_strdup (identifier);

failure:
  if (local_error)
    {
      g_propagate_error (error, g_steal_pointer (&local_error));
      BONSAI_RETURN (FALSE);
    }

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_dao_repository_push_changes (BonsaiDaoRepository      *self,
                                    BonsaiDaoTransaction     *txn,
                                    IpcDaoSync               *proxy,
                                    const gchar              *revision,
                                    BonsaiDaoTransactionLog  *log,
                                    GCancellable             *cancellable,
                                    GError                  **error)
{
  g_autoptr(GVariant) variant = NULL;
  g_autofree gchar *new_revision = NULL;
  const gchar *id;

  g_assert (BONSAI_IS_DAO_REPOSITORY (self));
  g_assert (revision != NULL);
  g_assert (BONSAI_IS_DAO_TRANSACTION_LOG (log));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  variant = g_variant_take_ref (bonsai_dao_transaction_log_to_variant (log, ""));
  id = bonsai_dao_repository_get_identifier (self);

  if (!ipc_dao_sync_call_push_sync (proxy, id, revision, variant, &new_revision, cancellable, error))
    return FALSE;

  return _bonsai_dao_repository_set_revision (self, txn, new_revision, error);
}

/**
 * bonsai_dao_repository_synchronize:
 * @self: a #BonsaiDaoRepository
 * @connection: a #GDBusConnection
 * @bus_name: (nullable): a name on the bus, or NULL if using a direct
 *   connection to a peer.
 * @sync_revision: (out): the revision synchronized to
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @error: a location for a #GError
 *
 * This function will block while synchronizing the repository with a peer
 * Bonsai device.
 *
 * Synchronization happens by rolling the transaction log back to the state
 * of the last transaction and then replaying local changes on top of the
 * updated transaction state.
 *
 * Whe conflicts occur, the default resolution is to duplicate the local
 * object. Applications, in most cases, can provide UI to allow the user
 * to easily delete duplicated content if that is their desire.
 *
 * Some work in the future will allow for the repository to handle what
 * to do on a merge conflict.
 *
 * Applications will want to refresh objects that are user visible after
 * synchronizing to ensure they have up to date contents. However, it
 * may be useful to check to see if the object in question has changed
 * by verifying the #BonsaiDaoObjectVersion field.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
bonsai_dao_repository_synchronize (BonsaiDaoRepository  *self,
                                   GDBusConnection      *connection,
                                   const gchar          *bus_name,
                                   gchar               **sync_revision,
                                   GCancellable         *cancellable,
                                   GError              **error)
{
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(IpcDaoSync) dao_sync = NULL;
  g_autoptr(IpcDaoLog) dao_log = NULL;
  g_autofree gchar *object_path = NULL;
  g_autofree gchar *revision = NULL;
  g_autofree gchar *new_revision = NULL;
  BonsaiDaoTransactionLog *log;
  const gchar *identifier;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), FALSE);
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  if (sync_revision)
    *sync_revision = NULL;

  if (!(txn = bonsai_dao_repository_begin_readwrite (self, error)))
    BONSAI_RETURN (FALSE);

  if (!(revision = _bonsai_dao_repository_get_revision (self, txn)))
    revision = g_strdup ("");

  identifier = bonsai_dao_repository_get_identifier (self);

  dao_sync = ipc_dao_sync_proxy_new_sync (connection,
                                          (G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
                                           G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES),
                                          bus_name,
                                          "/org/gnome/Bonsai/Dao/Sync",
                                          cancellable,
                                          error);
  if (dao_sync == NULL)
    BONSAI_RETURN (FALSE);

  /* First we need to get a handle to the transaction log of the
   * Bonsai Dao peer. Technically you could rebase upon any peer,
   * but realistically you only want to do this from the primary.
   */
  if (!ipc_dao_sync_call_begin_sync (dao_sync, identifier, revision, &object_path, cancellable, error))
    BONSAI_RETURN (FALSE);

  /* Now we can create our proxy to the transaction log. We want to
   * read the operations from the log and apply them. We will roll
   * our changes back to the state matching the logs starting point
   * and then try to replay our changes on-top.
   */
  dao_log = ipc_dao_log_proxy_new_sync (connection,
                                        (G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
                                         G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES),
                                        bus_name,
                                        object_path,
                                        cancellable,
                                        error);
  if (dao_log == NULL)
    BONSAI_RETURN (FALSE);

  if (!bonsai_dao_repository_synchronize_log (self, dao_log, txn, cancellable, &new_revision, error) ||
      !ipc_dao_log_call_close_sync (dao_log, cancellable, error) ||
      bonsai_dao_transaction_get_error (txn, error))
    {
      bonsai_dao_transaction_cancel (txn);
      BONSAI_RETURN (FALSE);
    }

  /* Try to push our changes to the peer on-top of our current version */
  if ((log = bonsai_dao_transaction_get_log (txn)) &&
      !bonsai_dao_transaction_log_is_empty (log) &&
      !bonsai_dao_repository_push_changes (self, txn, dao_sync, new_revision, log, cancellable, error))
    BONSAI_RETURN (FALSE);

  /* Now we can clear our local changes because they've been pushed */
  if (!clear_local_changes (self, txn, error))
    BONSAI_RETURN (FALSE);

  _bonsai_dao_transaction_reset_log (txn);

  if (!bonsai_dao_transaction_commit (txn, cancellable, error))
    BONSAI_RETURN (FALSE);

  if (sync_revision != NULL)
    *sync_revision = g_steal_pointer (&new_revision);

  BONSAI_RETURN (TRUE);
}
