/* bonsai-dao-transaction.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_TRANSACTION (bonsai_dao_transaction_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoTransaction, bonsai_dao_transaction, BONSAI, DAO_TRANSACTION, GObject)

BonsaiDaoRepository     *bonsai_dao_transaction_get_repository (BonsaiDaoTransaction     *self);
gboolean                 bonsai_dao_transaction_has_error      (BonsaiDaoTransaction     *self);
gboolean                 bonsai_dao_transaction_get_error      (BonsaiDaoTransaction     *self,
                                                                GError                  **error);
void                     bonsai_dao_transaction_cancel         (BonsaiDaoTransaction     *self);
gboolean                 bonsai_dao_transaction_commit         (BonsaiDaoTransaction     *self,
                                                                GCancellable             *cancellable,
                                                                GError                  **error);
void                     bonsai_dao_transaction_insert         (BonsaiDaoTransaction     *self,
                                                                BonsaiDaoCollection      *collection,
                                                                BonsaiDaoObject          *object);
void                     bonsai_dao_transaction_update         (BonsaiDaoTransaction     *self,
                                                                BonsaiDaoCollection      *collection,
                                                                BonsaiDaoObject          *object);
void                     bonsai_dao_transaction_delete         (BonsaiDaoTransaction     *self,
                                                                BonsaiDaoCollection      *collection,
                                                                BonsaiDaoObject          *object);
BonsaiDaoCursor         *bonsai_dao_transaction_query          (BonsaiDaoTransaction     *self,
                                                                BonsaiDaoCollection      *collection,
                                                                BonsaiDaoQueryClause     *first_clause,
                                                                ...) G_GNUC_NULL_TERMINATED;
BonsaiDaoObject         *bonsai_dao_transaction_lookup         (BonsaiDaoTransaction     *self,
                                                                BonsaiDaoCollection      *collection,
                                                                const gchar              *object_id);
gboolean                 bonsai_dao_transaction_get_writeable  (BonsaiDaoTransaction     *self);
BonsaiDaoTransactionLog *bonsai_dao_transaction_get_log        (BonsaiDaoTransaction     *self);
gboolean                 bonsai_dao_transaction_apply_log      (BonsaiDaoTransaction     *self,
                                                                BonsaiDaoTransactionLog  *log,
                                                                gboolean                  dup_on_conflict);

G_END_DECLS
