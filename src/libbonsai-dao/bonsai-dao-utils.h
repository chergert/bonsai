/* bonsai-dao-utils.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

void         bonsai_dao_variant_builder_append_value (GVariantBuilder *builder,
                                                      const GValue    *value);
GVariant    *bonsai_dao_value_to_variant             (const GValue    *value);
gboolean     bonsai_dao_variant_to_value             (GVariant        *variant,
                                                      GValue          *value);
GVariant    *bonsai_dao_increment                    (GVariant        *variant,
                                                      gint64           by);
const gchar *bonsai_dao_get_variant_type_string      (GType            type);

G_END_DECLS
