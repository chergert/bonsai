/* bonsai-dao-cursor.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_CURSOR (bonsai_dao_cursor_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoCursor, bonsai_dao_cursor, BONSAI, DAO_CURSOR, GObject)

BonsaiDaoObject *bonsai_dao_cursor_next       (BonsaiDaoCursor     *self,
                                               GCancellable        *cancellable,
                                               GError             **error);
GVariant        *bonsai_dao_cursor_next_value (BonsaiDaoCursor     *self,
                                               const GVariantType  *expected_type,
                                               GCancellable        *cancellable,
                                               gchar              **id,
                                               GError             **error);
gint64           bonsai_dao_cursor_count      (BonsaiDaoCursor     *self,
                                               GCancellable        *cancellable,
                                               GError             **error);
void             bonsai_dao_cursor_reset      (BonsaiDaoCursor     *self);

G_END_DECLS
