/* bonsai-dao-changeset.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_CHANGESET (bonsai_dao_changeset_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoChangeset, bonsai_dao_changeset, BONSAI, DAO_CHANGESET, GObject)

BonsaiDaoChangeset *bonsai_dao_changeset_new                    (const gchar         *object_id);
BonsaiDaoChangeset *bonsai_dao_changeset_from_variant           (GType                object_type,
                                                                 GVariant            *variant);
BonsaiDaoChangeset *bonsai_dao_changeset_revert                 (BonsaiDaoChangeset  *self);
const gchar        *bonsai_dao_changeset_get_object_id          (BonsaiDaoChangeset  *self);
gboolean            bonsai_dao_changeset_is_sealed              (BonsaiDaoChangeset  *self);
void                bonsai_dao_changeset_seal                   (BonsaiDaoChangeset  *self);
gboolean            bonsai_dao_changeset_is_empty               (BonsaiDaoChangeset  *self);
GVariant           *bonsai_dao_changeset_to_variant             (BonsaiDaoChangeset  *self);
gboolean            bonsai_dao_changeset_apply                  (BonsaiDaoChangeset  *self,
                                                                 GVariantDict        *dict,
                                                                 GError             **error);
void                bonsai_dao_changeset_append_overwrite_value (BonsaiDaoChangeset  *self,
                                                                 const gchar         *property_name,
                                                                 const GValue        *from,
                                                                 const GValue        *to);
void                bonsai_dao_changeset_append_overwrite       (BonsaiDaoChangeset  *self,
                                                                 GParamSpec          *pspec,
                                                                 ...);
void                bonsai_dao_changeset_append_string_diff     (BonsaiDaoChangeset  *self,
                                                                 GParamSpec          *pspec,
                                                                 const gchar         *before,
                                                                 const gchar         *after);
void                bonsai_dao_changeset_append_increment       (BonsaiDaoChangeset  *self,
                                                                 const gchar         *property_name,
                                                                 gint64               value);
void                bonsai_dao_changeset_append_unset           (BonsaiDaoChangeset  *self,
                                                                 const gchar         *property_name,
                                                                 const GValue        *from);
void                bonsai_dao_changeset_append_push            (BonsaiDaoChangeset  *self,
                                                                 const gchar         *property_name,
                                                                 const GValue        *value);
void                bonsai_dao_changeset_append_pop             (BonsaiDaoChangeset  *self,
                                                                 const gchar         *property_name,
                                                                 const GValue        *value);

G_END_DECLS
