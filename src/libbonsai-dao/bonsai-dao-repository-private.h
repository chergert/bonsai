/* bonsai-dao-repository-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <lmdb.h>
#include <libbonsai.h>

#include "bonsai-dao-types.h"
#include "bonsai-dao-repository.h"

G_BEGIN_DECLS

#define GOTO_LABEL_IF_NON_ZERO(stmt, err, label)           \
  G_STMT_START {                                           \
    int __r = (stmt);                                      \
    if (__r != 0)                                          \
      {                                                    \
        g_set_error_literal ((err),                        \
                             BONSAI_DAO_REPOSITORY_ERROR,  \
                             __r,                          \
                             mdb_strerror (__r));          \
        BONSAI_GOTO (label);                               \
      }                                                    \
  } G_STMT_END

MDB_txn             *_bonsai_dao_repository_create_transaction (BonsaiDaoRepository  *repository,
                                                                gboolean              writeable,
                                                                GError              **error);
MDB_dbi              _bonsai_dao_repository_get_log            (BonsaiDaoRepository  *self);
BonsaiDaoCollection *_bonsai_dao_repository_get_collection     (BonsaiDaoRepository  *self,
                                                                const gchar          *name);
gboolean             _bonsai_dao_repository_set_revision       (BonsaiDaoRepository  *self,
                                                                BonsaiDaoTransaction *txn,
                                                                const gchar          *revision,
                                                                GError              **error);
gchar               *_bonsai_dao_repository_get_revision       (BonsaiDaoRepository  *self,
                                                                BonsaiDaoTransaction *txn);
void                 _bonsai_dao_repository_wrlock             (BonsaiDaoRepository  *self);
void                 _bonsai_dao_repository_wrunlock           (BonsaiDaoRepository  *self);

G_END_DECLS
