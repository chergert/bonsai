/* bonsai-dao-object-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

void       _bonsai_dao_object_set_snapshot (BonsaiDaoObject    *self,
                                            BonsaiDaoSnapshot  *snapshot);
GVariant  *_bonsai_dao_object_get_original (BonsaiDaoObject    *self,
                                            GError            **error);
void       _bonsai_dao_object_stash        (BonsaiDaoObject    *self);
GPtrArray *_bonsai_dao_object_diff         (BonsaiDaoObject    *self);

G_END_DECLS
