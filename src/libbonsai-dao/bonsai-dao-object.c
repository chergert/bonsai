/* bonsai-dao-object.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-object"

#include "config.h"

#include <libbonsai.h>
#include <string.h>

#include "bonsai-dao-object.h"
#include "bonsai-dao-object-private.h"
#include "bonsai-dao-snapshot.h"
#include "bonsai-dao-snapshot-private.h"
#include "bonsai-dao-utils.h"

typedef struct
{
  gchar             *id;
  BonsaiDaoSnapshot *snapshot;
  BonsaiDaoVersion   upstream_version;
} BonsaiDaoObjectPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (BonsaiDaoObject, bonsai_dao_object, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
bonsai_dao_object_real_inflate_property (BonsaiDaoObjectClass  *klass,
                                         const gchar           *key,
                                         GVariant              *variant,
                                         GParamSpec           **pspec,
                                         GValue                *value)
{
  g_assert (BONSAI_IS_DAO_OBJECT_CLASS (klass));
  g_assert (key != NULL);
  g_assert (variant != NULL);
  g_assert (pspec != NULL);
  g_assert (value != NULL);

  if (!(*pspec = g_object_class_find_property (G_OBJECT_CLASS (klass), key)))
    return FALSE;

  g_value_init (value, (*pspec)->value_type);

  return bonsai_dao_variant_to_value (variant, value);
}

static BonsaiDaoObject *
bonsai_dao_object_real_inflate (BonsaiDaoObjectClass *klass,
                                const gchar          *id,
                                GVariant             *variant)
{
  g_autoptr(GPtrArray) names = NULL;
  g_autoptr(GArray) values = NULL;
  GVariantIter iter;
  GVariant *value;
  GObject *ret = NULL;
  gchar *key;
  GValue val = G_VALUE_INIT;

  g_assert (BONSAI_IS_DAO_OBJECT_CLASS (klass));
  g_assert (id != NULL);
  g_assert (variant != NULL);
  g_assert (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARDICT));

  names = g_ptr_array_new ();
  values = g_array_new (FALSE, FALSE, sizeof (GValue));
  g_array_set_clear_func (values, (GDestroyNotify)g_value_unset);

  /* Always set ID property first */
  g_value_init (&val, G_TYPE_STRING);
  g_value_set_string (&val, id);
  g_array_append_val (values, val);
  g_ptr_array_add (names, (gchar *)"id");

  g_variant_iter_init (&iter, variant);
  while (g_variant_iter_loop (&iter, "{sv}", &key, &value))
    {
      GParamSpec *pspec = NULL;

      /* Don't allow setting :id twice */
      if (g_strcmp0 (key, "id") == 0)
        continue;

      memset (&val, 0, sizeof val);

      if (!klass->inflate_property (klass, key, value, &pspec, &val))
        {
          g_free (key);
          g_variant_unref (value);
          BONSAI_GOTO (failure);
        }

      g_ptr_array_add (names, (gchar *)pspec->name);
      g_array_append_val (values, val);
    }

  ret = g_object_new_with_properties (G_TYPE_FROM_CLASS (klass),
                                      names->len,
                                      (const gchar **)names->pdata,
                                      &g_array_index (values, GValue, 0));

failure:
  BONSAI_RETURN (BONSAI_DAO_OBJECT (g_steal_pointer (&ret)));
}

static void
bonsai_dao_object_real_snapshot (BonsaiDaoObject   *self,
                                 BonsaiDaoSnapshot *snapshot)
{
  BonsaiDaoObjectAutoProperty *prop;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_OBJECT (self));
  g_assert (BONSAI_IS_DAO_SNAPSHOT (snapshot));

  for (prop = BONSAI_DAO_OBJECT_GET_CLASS (self)->auto_properties;
       prop != NULL;
       prop = prop->next)
    {
      GValue value = G_VALUE_INIT;

      g_object_get_property (G_OBJECT (self), prop->pspec->name, &value);
      _bonsai_dao_snapshot_append_take_value (snapshot, prop->pspec, &value);
    }

  BONSAI_EXIT;
}

static void
bonsai_dao_object_finalize (GObject *object)
{
  BonsaiDaoObject *self = (BonsaiDaoObject *)object;
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);
  g_clear_object (&priv->snapshot);

  G_OBJECT_CLASS (bonsai_dao_object_parent_class)->finalize (object);
}

static void
bonsai_dao_object_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  BonsaiDaoObject *self = BONSAI_DAO_OBJECT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, bonsai_dao_object_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_object_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  BonsaiDaoObject *self = BONSAI_DAO_OBJECT (object);
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_object_class_init (BonsaiDaoObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_object_finalize;
  object_class->get_property = bonsai_dao_object_get_property;
  object_class->set_property = bonsai_dao_object_set_property;

  klass->inflate = bonsai_dao_object_real_inflate;
  klass->inflate_property = bonsai_dao_object_real_inflate_property;
  klass->snapshot = bonsai_dao_object_real_snapshot;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "Identifier for the Dao object",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class, PROP_ID, properties [PROP_ID]);
}

static void
bonsai_dao_object_init (BonsaiDaoObject *self)
{
}

const gchar *
bonsai_dao_object_get_id (BonsaiDaoObject *self)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_DAO_OBJECT (self), NULL);

  return priv->id;
}

void
bonsai_dao_object_snapshot (BonsaiDaoObject   *self,
                            BonsaiDaoSnapshot *snapshot)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_OBJECT (self));
  g_return_if_fail (BONSAI_IS_DAO_SNAPSHOT (snapshot));

  /* push_object() returns FALSE if we've seen this object */
  if (bonsai_dao_snapshot_push_object (snapshot, priv->id))
    {
      BONSAI_DAO_OBJECT_GET_CLASS (self)->snapshot (self, snapshot);
      bonsai_dao_snapshot_pop (snapshot);
    }

  BONSAI_EXIT;
}

/*
 * _bonsai_dao_object_set_snapshot:
 * @self: a #BonsaiDaoObject
 * @snapshot: a #BonsaiDaoSnapshot
 *
 * Sets the base snapshot the object was created from. This is useful
 * so that changes can be differenced when storing the object back to
 * underlying storage.
 */
void
_bonsai_dao_object_set_snapshot (BonsaiDaoObject   *self,
                                 BonsaiDaoSnapshot *snapshot)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_DAO_OBJECT (self));
  g_return_if_fail (!snapshot || BONSAI_IS_DAO_SNAPSHOT (snapshot));

  g_set_object (&priv->snapshot, snapshot);
}

/**
 * bonsai_dao_object_get_upstream_version:
 * @self: a #BonsaiDaoObject
 *
 * Gets the version of the object where it branched from upstrem.
 *
 * This can be used when syncing to help determine how to replay operations.
 *
 * Returns: a monotonic version token
 */
BonsaiDaoVersion
bonsai_dao_object_get_upstream_version (BonsaiDaoObject *self)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_DAO_OBJECT (self), 0);

  return priv->upstream_version;
}

void
bonsai_dao_object_class_install_auto_property (BonsaiDaoObjectClass         *klass,
                                               guint                         prop_id,
                                               GParamSpec                   *pspec,
                                               BonsaiDaoObjectPropertyFlags  flags)
{
  BonsaiDaoObjectAutoProperty *auto_prop;
  GType type;

  g_return_if_fail (BONSAI_IS_DAO_OBJECT_CLASS (klass));
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));

  g_object_class_install_property (G_OBJECT_CLASS (klass), prop_id, pspec);

  type = pspec->value_type;

  if (!(type == G_TYPE_INT ||
        type == G_TYPE_UINT ||
        type == G_TYPE_INT64 ||
        type == G_TYPE_UINT64 ||
        type == G_TYPE_LONG ||
        type == G_TYPE_ULONG ||
        type == G_TYPE_FLOAT ||
        type == G_TYPE_DOUBLE ||
        type == G_TYPE_BOOLEAN ||
        type == G_TYPE_STRING ||
        type == G_TYPE_CHAR ||
        type == G_TYPE_UCHAR ||
        type == G_TYPE_ENUM ||
        type == G_TYPE_FLAGS ||
        type == G_TYPE_VARIANT ||
        type == G_TYPE_GTYPE ||
        type == G_TYPE_DATE_TIME ||
        type == G_TYPE_STRV ||
        G_TYPE_IS_ENUM (type) ||
        G_TYPE_IS_FLAGS (type)))
    {
      g_critical ("Unsupported type %s", g_type_name (type));
      return;
    }

  auto_prop = g_slice_new0 (BonsaiDaoObjectAutoProperty);
  auto_prop->next = klass->auto_properties;
  auto_prop->prop_id = prop_id;
  auto_prop->pspec = g_param_spec_ref (pspec);
  auto_prop->flags = flags;

  klass->auto_properties = auto_prop;
}

gpointer
bonsai_dao_object_new_from_snapshot (GType              object_type,
                                     const gchar       *id,
                                     BonsaiDaoSnapshot *snapshot)
{
  g_return_val_if_fail (g_type_is_a (object_type, BONSAI_TYPE_DAO_OBJECT), NULL);
  g_return_val_if_fail (object_type != BONSAI_TYPE_DAO_OBJECT, NULL);

  return _bonsai_dao_snapshot_create_object (snapshot, id, object_type);
}

gpointer
bonsai_dao_object_new_from_variant (GType        object_type,
                                    const gchar *id,
                                    GVariant    *variant)
{
  g_autoptr(GTypeClass) klass = NULL;
  g_autoptr(GVariant) taken = NULL;

  g_return_val_if_fail (object_type != BONSAI_TYPE_DAO_OBJECT, NULL);
  g_return_val_if_fail (g_type_is_a (object_type, BONSAI_TYPE_DAO_OBJECT), NULL);
  g_return_val_if_fail (id != NULL, NULL);
  g_return_val_if_fail (variant != NULL, NULL);
  g_return_val_if_fail (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARDICT), NULL);

  if (g_variant_is_floating (variant))
    taken = g_variant_take_ref (variant);

  if (!(klass = g_type_class_ref (object_type)))
    return NULL;

  return BONSAI_DAO_OBJECT_CLASS (klass)->inflate (BONSAI_DAO_OBJECT_CLASS (klass), id, variant);
}

GPtrArray *
_bonsai_dao_object_diff (BonsaiDaoObject *self)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);
  g_autoptr(BonsaiDaoSnapshot) snapshot = NULL;
  GPtrArray *changes;

  g_return_val_if_fail (BONSAI_IS_DAO_OBJECT (self), NULL);

  if (priv->snapshot == NULL)
    return g_ptr_array_new_with_free_func (g_object_unref);

  snapshot = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (self, snapshot);

  changes = bonsai_dao_snapshot_diff (priv->snapshot, snapshot);
  g_ptr_array_set_free_func (changes, g_object_unref);

  return g_steal_pointer (&changes);
}

void
_bonsai_dao_object_stash (BonsaiDaoObject *self)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);
  g_autoptr(BonsaiDaoSnapshot) snapshot = NULL;

  g_return_if_fail (BONSAI_IS_DAO_OBJECT (self));

  snapshot = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (self, snapshot);
  g_clear_object (&priv->snapshot);
  priv->snapshot = g_steal_pointer (&snapshot);
}

GVariant *
_bonsai_dao_object_get_original (BonsaiDaoObject  *self,
                                 GError          **error)
{
  BonsaiDaoObjectPrivate *priv = bonsai_dao_object_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_DAO_OBJECT (self), NULL);

  /* Gets the original object values. This is useful when deleting an object
   * from the undelrying storage as we need the old values for removing
   * the secondary index keys.
   */

  if (!priv->snapshot)
    {
      g_autoptr(BonsaiDaoSnapshot) snapshot = bonsai_dao_snapshot_new ();
      bonsai_dao_object_snapshot (self, snapshot);
      return _bonsai_dao_snapshot_get_object_as_variant (snapshot, priv->id, error);
    }

  return _bonsai_dao_snapshot_get_object_as_variant (priv->snapshot, priv->id, error);
}
