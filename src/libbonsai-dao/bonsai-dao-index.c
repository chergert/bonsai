/* bonsai-dao-index.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-index"

#include "config.h"

#include <lmdb.h>
#include <libbonsai.h>
#include <string.h>

#include "bonsai-dao-index-private.h"
#include "bonsai-dao-object.h"
#include "bonsai-dao-query-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-repository-private.h"

typedef struct
{
  MDB_dbi slot;
} BonsaiDaoIndexPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (BonsaiDaoIndex, bonsai_dao_index, G_TYPE_OBJECT)

static gboolean
bonsai_dao_index_real_create_cursor (BonsaiDaoIndex  *self,
                                     MDB_txn         *txn,
                                     MDB_cursor     **cursor)
{
  BonsaiDaoIndexPrivate *priv = bonsai_dao_index_get_instance_private (self);

  g_assert (BONSAI_IS_DAO_INDEX (self));
  g_assert (txn != NULL);
  g_assert (cursor != NULL);

  /* The default cursor is just for the ID which is the primary key
   * of the objects within the collection. We use priv->slot which is
   * the MDB_dbi identifying the object collection.
   */

  return mdb_cursor_open (txn, priv->slot, cursor) == 0;
}

static gboolean
bonsai_dao_index_real_move_first (BonsaiDaoIndex *self,
                                  BonsaiDaoQuery *query,
                                  MDB_cursor     *cursor,
                                  MDB_val        *key,
                                  MDB_val        *val)
{
  /* If the query has an "id" with a GT or EQ operator, we can use that
   * to advance to the first item matching that key instead of walking
   * the whole table.
   */
  if (query != NULL)
    {
      g_autoptr(GTypeClass) type_class = g_type_class_ref (BONSAI_TYPE_DAO_OBJECT);
      g_auto(GValue) id_lower = G_VALUE_INIT;
      g_auto(GValue) id_upper = G_VALUE_INIT;
      GParamSpec *id_pspec;

      id_pspec = g_object_class_find_property (G_OBJECT_CLASS (type_class), "id");

      if (_bonsai_dao_query_get_bounds (query, id_pspec, &id_lower, &id_upper))
        {
          if (G_VALUE_HOLDS_STRING (&id_lower) && g_value_get_string (&id_lower))
            {
              key->mv_data = (gpointer)g_value_get_string (&id_lower);
              key->mv_size = strlen (key->mv_data) + 1;

              return mdb_cursor_get (cursor, key, val, MDB_SET_RANGE) == 0;
            }
        }
    }

  return mdb_cursor_get (cursor, key, val, MDB_FIRST) == 0;
}

static gboolean
bonsai_dao_index_real_move_next (BonsaiDaoIndex *self,
                                 MDB_cursor     *cursor,
                                 MDB_val        *key,
                                 MDB_val        *val)
{
  /* TODO: If we had :id in the query, we should be able to know when
   * we've past our upper bound (if there is one). When reached (or
   * passed), we should stop iteration immediately to avoid full table
   * scans of the content.
   *
   * This is a pretty important thing, and we need to plumb access to the
   * data and keep it fast (as it's going to be run a lot).
   */

  return mdb_cursor_get (cursor, key, val, MDB_NEXT) == 0;
}

gboolean
_bonsai_dao_index_move_last (BonsaiDaoIndex *self,
                             MDB_cursor     *cursor,
                             MDB_val        *key,
                             MDB_val        *val)
{
  return mdb_cursor_get (cursor, key, val, MDB_LAST) == 0;
}

gboolean
_bonsai_dao_index_move_prev (BonsaiDaoIndex *self,
                             MDB_cursor     *cursor,
                             MDB_val        *key,
                             MDB_val        *val)
{
  return mdb_cursor_get (cursor, key, val, MDB_PREV) == 0;
}

static gboolean
bonsai_dao_index_real_insert (BonsaiDaoIndex  *self,
                              MDB_txn         *txn,
                              const gchar     *id,
                              GVariant        *variant,
                              GError         **error)
{
  BonsaiDaoIndexPrivate *priv = bonsai_dao_index_get_instance_private (self);
  MDB_val key, val;

  g_assert (BONSAI_IS_DAO_INDEX (self));
  g_assert (txn != NULL);
  g_assert (id != NULL);
  g_assert (variant != NULL);

  key.mv_data = (gpointer)id;
  key.mv_size = strlen (id) + 1;

  val.mv_data = (gpointer)g_variant_get_data (variant);
  val.mv_size = g_variant_get_size (variant);

  /* Insert the document into the collection */
  GOTO_LABEL_IF_NON_ZERO (mdb_put (txn, priv->slot, &key, &val, MDB_NOOVERWRITE), error, failure);

  return TRUE;

failure:
  return FALSE;
}

static gboolean
bonsai_dao_index_real_delete (BonsaiDaoIndex  *self,
                              MDB_txn         *txn,
                              const gchar     *id,
                              GVariant        *variant,
                              GError         **error)
{
  BonsaiDaoIndexPrivate *priv = bonsai_dao_index_get_instance_private (self);
  MDB_val key;

  g_assert (BONSAI_IS_DAO_INDEX (self));
  g_assert (txn != NULL);
  g_assert (id != NULL);
  g_assert (variant != NULL);

  key.mv_data = (gpointer)id;
  key.mv_size = strlen (id) + 1;

  /* Insert the document into the collection */
  GOTO_LABEL_IF_NON_ZERO (mdb_del (txn, priv->slot, &key, NULL), error, failure);

  return TRUE;

failure:
  return FALSE;
}

static void
bonsai_dao_index_class_init (BonsaiDaoIndexClass *klass)
{
  klass->create_cursor = bonsai_dao_index_real_create_cursor;
  klass->move_next = bonsai_dao_index_real_move_next;
  klass->move_first = bonsai_dao_index_real_move_first;
  klass->insert = bonsai_dao_index_real_insert;
  klass->delete = bonsai_dao_index_real_delete;
}

static void
bonsai_dao_index_init (BonsaiDaoIndex *self)
{
}

BonsaiDaoIndex *
_bonsai_dao_index_new_for_id (BonsaiDaoRepository *repository,
                              MDB_dbi              slot)
{
  BonsaiDaoIndex *self;
  BonsaiDaoIndexPrivate *priv;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (repository), NULL);

  self = g_object_new (BONSAI_TYPE_DAO_INDEX, NULL);
  priv = bonsai_dao_index_get_instance_private (self);
  priv->slot = slot;

  return g_steal_pointer (&self);
}

gboolean
_bonsai_dao_index_create_cursor (BonsaiDaoIndex  *self,
                                 MDB_txn         *txn,
                                 MDB_cursor     **cursor)
{
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self), FALSE);
  g_return_val_if_fail (txn != NULL, FALSE);
  g_return_val_if_fail (cursor != NULL, FALSE);

  return BONSAI_DAO_INDEX_GET_CLASS (self)->create_cursor (self, txn, cursor);
}

gboolean
_bonsai_dao_index_move_first (BonsaiDaoIndex *self,
                              BonsaiDaoQuery *query,
                              MDB_cursor     *cursor,
                              MDB_val        *key,
                              MDB_val        *val)
{
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self), FALSE);
  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (query), FALSE);
  g_return_val_if_fail (cursor != NULL, FALSE);

  return BONSAI_DAO_INDEX_GET_CLASS (self)->move_first (self, query, cursor, key, val);
}

gboolean
_bonsai_dao_index_move_next (BonsaiDaoIndex *self,
                             MDB_cursor     *cursor,
                             MDB_val        *key,
                             MDB_val        *val)
{
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self), FALSE);
  g_return_val_if_fail (cursor != NULL, FALSE);

  return BONSAI_DAO_INDEX_GET_CLASS (self)->move_next (self, cursor, key, val);
}

gboolean
_bonsai_dao_index_insert (BonsaiDaoIndex  *self,
                          MDB_txn         *txn,
                          const gchar     *id,
                          GVariant        *variant,
                          GError         **error)
{
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self), FALSE);
  g_return_val_if_fail (txn != NULL, FALSE);
  g_return_val_if_fail (id != NULL, FALSE);
  g_return_val_if_fail (variant != NULL, FALSE);

  return BONSAI_DAO_INDEX_GET_CLASS (self)->insert (self, txn, id, variant, error);
}

gboolean
_bonsai_dao_index_delete (BonsaiDaoIndex  *self,
                          MDB_txn         *txn,
                          const gchar     *id,
                          GVariant        *variant,
                          GError         **error)
{
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self), FALSE);
  g_return_val_if_fail (txn != NULL, FALSE);
  g_return_val_if_fail (id != NULL, FALSE);
  g_return_val_if_fail (variant != NULL, FALSE);

  return BONSAI_DAO_INDEX_GET_CLASS (self)->delete (self, txn, id, variant, error);
}

gboolean
_bonsai_dao_index_has_pspec (BonsaiDaoIndex *self,
                             GParamSpec     *pspec)
{
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self), FALSE);

  if (BONSAI_DAO_INDEX_GET_CLASS (self)->has_pspec)
    return BONSAI_DAO_INDEX_GET_CLASS (self)->has_pspec (self, pspec);

  return FALSE;
}
