/* test-dao-synchronize.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "test-dao-synchronize"

#include "config.h"

#include <libbonsai-dao.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <glib-unix.h>

#include "ipc-dao-log.h"
#include "ipc-dao-sync.h"

#include "bonsai-dao-transaction-log-private.h"
#include "mock-sync.h"
#include "test-note.h"

#define REPO_ID "org.gnome.Bonsai.Test.Synchronize"

G_GNUC_UNUSED static inline void
dump_log (BonsaiDaoRepository *repo)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(BonsaiDaoCollection) col = NULL;
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;
  GVariant *val;
  gchar *id = NULL;
  guint i = 0;

  col = bonsai_dao_repository_load_collection (repo, "org.gnome.Bonsai.Dao.Log", BONSAI_TYPE_DAO_OBJECT, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (col));

  txn = bonsai_dao_repository_begin_read (repo, &error);
  g_assert_no_error (error);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (txn));

  cursor = bonsai_dao_transaction_query (txn, col, NULL, NULL);
  g_assert_true (BONSAI_IS_DAO_CURSOR (cursor));

  while ((val = bonsai_dao_cursor_next_value (cursor, G_VARIANT_TYPE ("a(sa{sv})"), NULL, &id, &error)))
    {
      g_autoptr(BonsaiDaoTransactionLog) log = bonsai_dao_transaction_log_from_variant (val);
      g_autofree gchar *str = NULL;

      g_assert_no_error (error);
      g_assert (log != NULL);

      g_print ("LOG[%d]: %s\n", i++, id);
      str = bonsai_dao_transaction_log_print (log);
      g_print ("%s\n", str);

      g_free (id);
      g_variant_unref (val);
    }
}

static void
assert_log_empty (BonsaiDaoRepository *repo)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(BonsaiDaoCollection) col = NULL;
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;

  col = bonsai_dao_repository_load_collection (repo, "org.gnome.Bonsai.Dao.Log", BONSAI_TYPE_DAO_OBJECT, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (col));

  txn = bonsai_dao_repository_begin_read (repo, &error);
  g_assert_no_error (error);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (txn));

  cursor = bonsai_dao_transaction_query (txn, col, NULL, NULL);
  g_assert_true (BONSAI_IS_DAO_CURSOR (cursor));

  g_assert_cmpint (0, ==, bonsai_dao_cursor_count (cursor, NULL, &error));
  g_assert_no_error (error);
}

static void
make_p2p_dbus (GDBusConnection **conn1,
               GDBusConnection **conn2)
{
  g_autoptr(GInputStream) a_in = NULL;
  g_autoptr(GInputStream) b_in = NULL;
  g_autoptr(GOutputStream) a_out = NULL;
  g_autoptr(GOutputStream) b_out = NULL;
  g_autoptr(GIOStream) a_stream = NULL;
  g_autoptr(GIOStream) b_stream = NULL;
  g_autoptr(GError) error = NULL;
  int a[2];
  int b[2];

  if (pipe (a) != 0)
    g_assert_not_reached ();

  if (pipe (b) != 0)
    g_assert_not_reached ();

  g_unix_set_fd_nonblocking (a[0], TRUE, &error);
  g_assert_no_error (error);
  g_unix_set_fd_nonblocking (a[1], TRUE, &error);
  g_assert_no_error (error);
  g_unix_set_fd_nonblocking (b[0], TRUE, &error);
  g_assert_no_error (error);
  g_unix_set_fd_nonblocking (b[1], TRUE, &error);
  g_assert_no_error (error);

  a_in = g_unix_input_stream_new (a[0], TRUE);
  a_out = g_unix_output_stream_new (a[1], TRUE);
  b_in = g_unix_input_stream_new (b[0], TRUE);
  b_out = g_unix_output_stream_new (b[1], TRUE);

  a_stream = g_simple_io_stream_new (a_in, b_out);
  b_stream = g_simple_io_stream_new (b_in, a_out);

  *conn1 = g_dbus_connection_new_sync (a_stream,
                                       NULL,
                                       G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING,
                                       NULL,
                                       NULL,
                                       &error);
  g_assert_no_error (error);

  *conn2 = g_dbus_connection_new_sync (b_stream,
                                       NULL,
                                       G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING,
                                       NULL,
                                       NULL,
                                       &error);
  g_assert_no_error (error);

  g_dbus_connection_start_message_processing (*conn1);
  g_dbus_connection_start_message_processing (*conn2);
}

static void
add_items (BonsaiDaoRepository *repository,
           BonsaiDaoCollection *collection,
           const gchar         *title)
{
  g_autoptr(GError) error = NULL;

  /*
   * Use a new transaction for each insert to create some chatter in
   * the log to synchronize later. If you were creating a bunch of
   * objects in your app, you'd probably want to batch these into a
   * single transaction.
   */

  for (guint i = 0; i < 100; i++)
    {
      g_autofree gchar *id = g_uuid_string_random ();
      g_autoptr(TestNote) note = NULL;
      BonsaiDaoTransaction *txn = NULL;
      gboolean r;

      note = g_object_new (TEST_TYPE_NOTE,
                           "id", id,
                           "title", title,
                           NULL);

      txn = bonsai_dao_repository_begin_readwrite (repository, &error);
      g_assert_no_error (error);
      g_assert_true (BONSAI_IS_DAO_TRANSACTION (txn));

      bonsai_dao_transaction_insert (txn, collection, BONSAI_DAO_OBJECT (note));

      r = bonsai_dao_transaction_commit (txn, NULL, &error);
      g_assert_no_error (error);
      g_assert_true (r);

      g_assert_finalize_object (txn);
    }
}

typedef struct
{
  BonsaiDaoRepository *repo;
  GDBusConnection *conn;
  GMainLoop *loop;
} ThreadSync;

static gpointer
sync_in_thread (gpointer data)
{
  ThreadSync *state = data;
  g_autoptr(GError) error = NULL;
  gboolean r;

  r = bonsai_dao_repository_synchronize (state->repo, state->conn, NULL, NULL, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);

  g_main_loop_quit (state->loop);

  return NULL;
}

static void
thread_sync (ThreadSync *state)
{
  GThread *thread;

  thread = g_thread_new ("sync-thread", sync_in_thread, state);
  g_main_loop_run (state->loop);
  g_thread_join (thread);

  assert_log_empty (state->repo);
}

static gint64
count_items (BonsaiDaoCollection  *collection,
             BonsaiDaoQueryClause *clause)
{
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;
  g_autoptr(GError) error = NULL;
  gint64 count;

  g_assert (BONSAI_IS_DAO_COLLECTION (collection));

  repository = bonsai_dao_collection_ref_repository (collection);
  g_assert (BONSAI_IS_DAO_REPOSITORY (repository));

  txn = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);

  cursor = bonsai_dao_transaction_query (txn, collection, clause, NULL);
  count = bonsai_dao_cursor_count (cursor, NULL, NULL);

  g_assert_finalize_object (cursor);
  g_assert_finalize_object (txn);

  cursor = NULL;
  txn = NULL;

  return count;
}

static void
update_notes (BonsaiDaoCollection *collection)
{
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;
  g_autoptr(GError) error = NULL;
  gpointer noteptr;
  gboolean r;

  g_assert (BONSAI_IS_DAO_COLLECTION (collection));

  repository = bonsai_dao_collection_ref_repository (collection);
  g_assert (BONSAI_IS_DAO_REPOSITORY (repository));

  txn = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);

  cursor = bonsai_dao_transaction_query (txn, collection, NULL, NULL);

  while ((noteptr = bonsai_dao_cursor_next (cursor, NULL, &error)))
    {
      g_autoptr(TestNote) note = noteptr;

      g_object_set (note,
                    "title", "this is an updated note title",
                    NULL);

      bonsai_dao_transaction_update (txn, collection, BONSAI_DAO_OBJECT (note));
    }

  g_assert_no_error (error);

  r = bonsai_dao_transaction_commit (txn, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);

  g_assert_finalize_object (cursor), cursor = NULL;
  g_assert_finalize_object (txn), txn = NULL;
}

static void
test_synchronize (void)
{
  g_autoptr(BonsaiDaoRepository) repo1 = NULL;
  g_autoptr(BonsaiDaoRepository) repo2 = NULL;
  g_autoptr(BonsaiDaoCollection) col1 = NULL;
  g_autoptr(BonsaiDaoCollection) col2 = NULL;
  g_autoptr(GError) error = NULL;
  gchar base_path[] = "test-dao-synchronize-XXXXXX";
  g_autofree gchar *repo1_path = NULL;
  g_autofree gchar *repo2_path = NULL;
  g_autofree gchar *rm_rf = NULL;
  g_autoptr(GDBusConnection) conn1 = NULL;
  g_autoptr(GDBusConnection) conn2 = NULL;
  g_autoptr(MockSync) service = NULL;
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  ThreadSync state;
  gint r;

  g_mkdtemp (base_path);
  r = g_mkdir_with_parents (base_path, 0750);
  g_assert_cmpint (r, ==, 0);

  repo1_path = g_build_filename (base_path, "repo1", NULL);
  repo2_path = g_build_filename (base_path, "repo2", NULL);

  repo1 = bonsai_dao_repository_new_for_path (REPO_ID, repo1_path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repo1);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repo1));

  repo2 = bonsai_dao_repository_new_for_path (REPO_ID, repo2_path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repo2);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repo2));

  col1 = bonsai_dao_repository_load_collection (repo1, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (col1);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (col1));

  col2 = bonsai_dao_repository_load_collection (repo2, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (col2);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (col2));

  /* Add items to repo1, our "primary" */
  add_items (repo1, col1, "col1");

  /* Create our mock service to export "repo1" */
  make_p2p_dbus (&conn1, &conn2);
  service = mock_sync_new (repo1);
  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (service), conn1, "/org/gnome/Bonsai/Dao/Sync", &error);
  g_assert_no_error (error);

  /* Setup state for sync */
  state.repo = repo2;
  state.conn = conn2;
  state.loop = main_loop;

  /* Now request that repo2 synchronize items from "primary" */
  thread_sync (&state);

  /* Add items to repo2, our "secondary" so we can ensure they replay during sync */
  add_items (repo2, col2, "col2");

  /* Now count the number of items in repo2.notes */
  g_assert_cmpint (200, ==, count_items (col2, NULL));
  g_assert_cmpint (100, ==, count_items (col1, NULL));

  /* Now push our repo2 changes to repo1 */
  thread_sync (&state);

  /* col2 should have been pushed into col1 */
  g_assert_cmpint (200, ==, count_items (col2, NULL));
  g_assert_cmpint (200, ==, count_items (col1, NULL));

  assert_log_empty (repo2);

  /* Update the notes in the collection */
  update_notes (col1);

  /* Now request that repo2 synchronize the updated items */
  thread_sync (&state);

  /* Now count the number of items in repo2.notes */
  g_assert_cmpint (200, ==, count_items (col1, BONSAI_DAO_WHERE_EQUAL (TEST_TYPE_NOTE, "title", "this is an updated note title")));
  g_assert_cmpint (200, ==, count_items (col2, BONSAI_DAO_WHERE_EQUAL (TEST_TYPE_NOTE, "title", "this is an updated note title")));

  assert_log_empty (repo2);

  rm_rf = g_strdup_printf ("rm -rf '%s'", base_path);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

gint
main (gint argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Bonsai/Dao/synchronize", test_synchronize);
  return g_test_run ();
}
