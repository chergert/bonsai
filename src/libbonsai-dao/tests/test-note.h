/* test-note.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libbonsai-dao.h>

G_BEGIN_DECLS

#define TEST_TYPE_NOTE (test_note_get_type())

G_DECLARE_FINAL_TYPE (TestNote, test_note, TEST, NOTE, BonsaiDaoObject)

TestNote            *test_note_new            (void);
GDateTime           *test_note_get_created_at (TestNote *self);
const gchar         *test_note_get_body       (TestNote *self);
const gchar         *test_note_get_title      (TestNote *self);
const gchar * const *test_note_get_tags       (TestNote *self);

G_END_DECLS
