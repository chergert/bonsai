/* test-dao.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <libbonsai-dao.h>

#include "bonsai-dao-object-private.h"

#include "test-note.h"

#define REPO_ID "org.gnome.Bonsai.Dao.Tests"

static const gchar *tags_abc[] = { "a", "b", "c", NULL };
static const gchar *tags_def[] = { "d", "e", "f", NULL };

static void
test_snapshot_new_object (void)
{
  g_autofree gchar *id = g_uuid_string_random ();
  g_autoptr(BonsaiDaoSnapshot) empty = bonsai_dao_snapshot_new ();
  g_autoptr(BonsaiDaoSnapshot) snapshot = NULL;
  g_autoptr(GPtrArray) diff = NULL;
  g_autoptr(TestNote) note = NULL;

  note = g_object_new (TEST_TYPE_NOTE,
                       "id", id,
                       "title", "title 1",
                       "body", "this\nis\nthe\nbody\nof\ntext.",
                       "tags", tags_abc,
                       NULL);
  snapshot = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), snapshot);

  diff = bonsai_dao_snapshot_diff (empty, snapshot);
  g_assert_nonnull (diff);
  g_ptr_array_set_free_func (diff, g_object_unref);

  g_assert_cmpint (diff->len, ==, 1);

  for (guint i = 0; i < diff->len; i++)
    {
      BonsaiDaoChangeset *changeset = g_ptr_array_index (diff, i);
      const gchar *object_id = bonsai_dao_changeset_get_object_id (changeset);
      g_assert_cmpstr (object_id, ==, id);
    }
}

static void
test_snapshot_changes (void)
{
  g_autofree gchar *id = g_uuid_string_random ();
  g_autoptr(BonsaiDaoSnapshot) snapshot = NULL;
  g_autoptr(BonsaiDaoSnapshot) snapshot2 = NULL;
  g_autoptr(GPtrArray) diff = NULL;
  g_autoptr(TestNote) note = NULL;

  note = g_object_new (TEST_TYPE_NOTE,
                       "id", id,
                       "title", "title 1",
                       "body", "this\nis\nthe\nbody\nof\ntext.",
                       "tags", tags_abc,
                       NULL);
  snapshot = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), snapshot);

  g_object_set (note,
                "title", "title 1 changed",
                "body", "this\nis\nthe\nbody\nof\ntext.\nand more.",
                "tags", tags_def,
                NULL);
  snapshot2 = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), snapshot2);

  diff = bonsai_dao_snapshot_diff (snapshot, snapshot2);
  g_assert_nonnull (diff);
  g_ptr_array_set_free_func (diff, g_object_unref);

  g_assert_cmpint (diff->len, ==, 1);

  for (guint i = 0; i < diff->len; i++)
    {
      BonsaiDaoChangeset *changeset = g_ptr_array_index (diff, i);
      const gchar *object_id = bonsai_dao_changeset_get_object_id (changeset);
      g_assert_cmpstr (object_id, ==, id);
    }
}

static void
test_restore_compare (void)
{
  g_autofree gchar *id = g_uuid_string_random ();
  g_autoptr(BonsaiDaoSnapshot) snapshot = NULL;
  g_autoptr(BonsaiDaoSnapshot) snapshot2 = NULL;
  g_autoptr(GPtrArray) diff = NULL;
  g_autoptr(TestNote) note = NULL;
  g_autoptr(TestNote) note2 = NULL;

  note = g_object_new (TEST_TYPE_NOTE,
                       "id", id,
                       "title", "title 1",
                       "body", "this\nis\nthe\nbody\nof\ntext.",
                       NULL);
  snapshot = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), snapshot);

  note2 = bonsai_dao_object_new_from_snapshot (TEST_TYPE_NOTE, id, snapshot);
  snapshot2 = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note2), snapshot2);

  diff = bonsai_dao_snapshot_diff (snapshot, snapshot2);
  g_assert_nonnull (diff);
  g_ptr_array_set_free_func (diff, g_object_unref);
  g_assert_cmpint (diff->len, ==, 0);

  g_assert_nonnull (test_note_get_created_at (note));
  g_assert_nonnull (test_note_get_created_at (note2));
  g_assert_true (test_note_get_created_at (note) == test_note_get_created_at (note2));
}

static void
test_create_repository (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_load_collection (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_create_transaction (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoTransaction) transaction = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;
  g_autofree gchar *id = g_uuid_string_random ();
  g_autoptr(TestNote) note = NULL;
  g_autoptr(BonsaiDaoObject) lookup = NULL;
  g_autoptr(BonsaiDaoSnapshot) before = NULL;
  g_autoptr(BonsaiDaoSnapshot) after = NULL;
  g_autoptr(GPtrArray) changes = NULL;
  gboolean r;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  /* Empty transaction */
  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  /* Add an object to transaction */
  note = g_object_new (TEST_TYPE_NOTE,
                       "id", id,
                       "title", "title 1",
                       "body", "this\nis\nthe\nbody\nof\ntext.",
                       NULL);
  before = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), before);
  bonsai_dao_transaction_insert (transaction, collection, BONSAI_DAO_OBJECT (note));

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  /* Now query the object back and see that we got what we expected */
  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  lookup = bonsai_dao_transaction_lookup (transaction, collection, id);
  g_assert_nonnull (lookup);
  g_assert_cmpstr (G_OBJECT_TYPE_NAME (lookup), ==, "TestNote");

  /* Test object for similarities */
  after = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (lookup), after);
  changes = bonsai_dao_snapshot_diff (before, after);
  g_assert_nonnull (changes);
  g_assert_cmpint (changes->len, ==, 0);

  /* Make sure the date-time round-tripped correctly */
  g_assert_nonnull (test_note_get_created_at (note));
  g_assert_nonnull (test_note_get_created_at (TEST_NOTE (lookup)));
  g_assert_true (test_note_get_created_at (note) != test_note_get_created_at (TEST_NOTE (lookup)));
  g_assert_cmpint (g_date_time_to_unix (test_note_get_created_at (note)),
                   ==,
                   g_date_time_to_unix (test_note_get_created_at (TEST_NOTE (lookup))));

  bonsai_dao_transaction_cancel (transaction);
  g_clear_object (&transaction);

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_update_object (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoTransaction) transaction = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;
  g_autofree gchar *id = g_uuid_string_random ();
  g_autoptr(TestNote) note = NULL;
  g_autoptr(BonsaiDaoObject) lookup = NULL;
  g_autoptr(BonsaiDaoSnapshot) before = NULL;
  gboolean r;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  /* Add an object to transaction */
  note = g_object_new (TEST_TYPE_NOTE,
                       "id", id,
                       "title", "title 1",
                       "body", "this\nis\nthe\nbody\nof\ntext.",
                       NULL);
  before = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), before);
  bonsai_dao_transaction_insert (transaction, collection, BONSAI_DAO_OBJECT (note));

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  /* Now transform the object and update/track changes */
  g_object_set (note,
                "title", "updated title",
                NULL);

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  bonsai_dao_transaction_update (transaction, collection, BONSAI_DAO_OBJECT (note));

  /* Commit the object changes to the data store */
  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  /* Now read back the committed object */
  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  lookup = bonsai_dao_transaction_lookup (transaction, collection, id);
  g_assert_nonnull (lookup);
  g_assert_cmpstr (G_OBJECT_TYPE_NAME (lookup), ==, "TestNote");
  g_assert_cmpstr (bonsai_dao_object_get_id (lookup), ==, id);
  g_assert_cmpstr (test_note_get_title (TEST_NOTE (lookup)), ==, "updated title");

  bonsai_dao_transaction_cancel (transaction);
  g_clear_object (&transaction);

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_delete_object (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoTransaction) transaction = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;
  g_autofree gchar *id = g_uuid_string_random ();
  g_autoptr(TestNote) note = NULL;
  g_autoptr(BonsaiDaoObject) lookup = NULL;
  g_autoptr(BonsaiDaoSnapshot) before = NULL;
  gboolean r;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  /* Add an object to transaction */
  note = g_object_new (TEST_TYPE_NOTE,
                       "id", id,
                       "title", "title 1",
                       "body", "this\nis\nthe\nbody\nof\ntext.",
                       NULL);
  before = bonsai_dao_snapshot_new ();
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), before);
  bonsai_dao_transaction_insert (transaction, collection, BONSAI_DAO_OBJECT (note));

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  bonsai_dao_transaction_delete (transaction, collection, BONSAI_DAO_OBJECT (note));

  /* Commit the object changes to the data store */
  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  /* Now read back the committed object */
  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  lookup = bonsai_dao_transaction_lookup (transaction, collection, id);
  g_assert_null (lookup);

  bonsai_dao_transaction_cancel (transaction);
  g_clear_object (&transaction);

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

typedef struct
{
  gchar *id;
  gchar *title;
} IdAndTitle;

static void
clear_id_and_title (gpointer data)
{
  IdAndTitle *info = data;
  g_free (info->id);
  g_free (info->title);
}

static gint
compare_id_and_title (gconstpointer a,
                      gconstpointer b)
{
  const IdAndTitle *at = a;
  const IdAndTitle *bt = b;

  return g_strcmp0 (at->id, bt->id);
}

static void
test_query (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoTransaction) transaction = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;
  g_autoptr(GArray) titles = NULL;
  gpointer objectptr;
  gboolean r;
  gint count = 0;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  titles = g_array_new (FALSE, FALSE, sizeof (IdAndTitle));
  g_array_set_clear_func (titles, clear_id_and_title);

  for (guint i = 0; i < 10; i++)
    {
      g_autofree gchar *title = g_strdup_printf ("note %u", i);
      g_autofree gchar *id = g_uuid_string_random ();
      g_autoptr(TestNote) note = g_object_new (TEST_TYPE_NOTE,
                                               "id", id,
                                               "title", title,
                                               "body", "this is the body",
                                               NULL);
      IdAndTitle info = { g_strdup (id), g_strdup (title) };
      g_array_append_val (titles, info);
      bonsai_dao_transaction_insert (transaction, collection, BONSAI_DAO_OBJECT (note));
    }

  g_array_sort (titles, compare_id_and_title);

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  cursor = bonsai_dao_transaction_query (transaction, collection, NULL, NULL);
  g_assert_no_error (error);
  g_assert_nonnull (cursor);
  g_assert_true (BONSAI_IS_DAO_CURSOR (cursor));

  while ((objectptr = bonsai_dao_cursor_next (cursor, NULL, &error)))
    {
      g_autoptr(BonsaiDaoObject) object = objectptr;
      const IdAndTitle *info = &g_array_index (titles, IdAndTitle, count);

      g_assert_nonnull (object);
      g_assert_cmpstr (G_OBJECT_TYPE_NAME (object), ==, "TestNote");
      g_assert_true (TEST_IS_NOTE (object));

      g_assert_cmpstr (info->title, ==, test_note_get_title (TEST_NOTE (object)));

      count++;
    }

  g_assert_cmpint (count, ==, 10);

  bonsai_dao_cursor_reset (cursor);
  count = bonsai_dao_cursor_count (cursor, NULL, &error);
  g_assert_no_error (error);
  g_assert_cmpint (count, ==, 10);

  bonsai_dao_transaction_cancel (transaction);
  g_clear_object (&transaction);

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_where (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoTransaction) transaction = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;
  g_autoptr(GDateTime) now = g_date_time_new_now_local ();
  g_autoptr(GDateTime) now_plus_one = g_date_time_add_seconds (now, 1);
  g_autoptr(GDateTime) now_minus_one = g_date_time_add_seconds (now, -1);
  gpointer objectptr;
  gboolean r;
  gint count = 0;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  /* Add a secondary index with just the "created-at" property */
  r = bonsai_dao_collection_ensure_index (collection, transaction, NULL, &error, "created-at", NULL);
  g_assert_no_error (error);
  g_assert_true (r);

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  for (guint i = 0; i < 10; i++)
    {
      g_autofree gchar *title = g_strdup_printf ("note %u", i);
      g_autofree gchar *id = g_uuid_string_random ();
      g_autoptr(TestNote) note = g_object_new (TEST_TYPE_NOTE,
                                               "id", id,
                                               "created-at", now,
                                               "title", title,
                                               "body", "this is the body",
                                               NULL);
      bonsai_dao_transaction_insert (transaction, collection, BONSAI_DAO_OBJECT (note));
    }

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  cursor = bonsai_dao_transaction_query (transaction, collection,
                                         BONSAI_DAO_WHERE_EQUAL (TEST_TYPE_NOTE, "title", "note 3"),
                                         BONSAI_DAO_WHERE_EQUAL (TEST_TYPE_NOTE, "body", "this is the body"),
                                         BONSAI_DAO_WHERE_LESS_THAN_OR_EQUAL (TEST_TYPE_NOTE, "created-at", now_plus_one),
                                         NULL);
  g_assert_nonnull (cursor);
  g_assert_true (BONSAI_IS_DAO_CURSOR (cursor));

  while ((objectptr = bonsai_dao_cursor_next (cursor, NULL, &error)))
    {
      g_autoptr(BonsaiDaoObject) object = objectptr;

      g_assert_nonnull (object);
      g_assert_cmpstr (G_OBJECT_TYPE_NAME (object), ==, "TestNote");
      g_assert_true (TEST_IS_NOTE (object));

      g_assert_cmpstr (test_note_get_title (TEST_NOTE (object)), ==, "note 3");
      g_assert_cmpstr (test_note_get_body (TEST_NOTE (object)), ==, "this is the body");

      count++;
    }

  g_clear_object (&cursor);
  g_assert_cmpint (count, ==, 1);
  count = 0;

  cursor = bonsai_dao_transaction_query (transaction, collection,
                                         BONSAI_DAO_WHERE_EQUAL (TEST_TYPE_NOTE, "title", "note 3"),
                                         BONSAI_DAO_WHERE_EQUAL (TEST_TYPE_NOTE, "body", "this is the body"),
                                         BONSAI_DAO_WHERE_GREATER_THAN (TEST_TYPE_NOTE, "created-at", now_minus_one),
                                         NULL);
  g_assert_nonnull (cursor);
  g_assert_true (BONSAI_IS_DAO_CURSOR (cursor));

  while ((objectptr = bonsai_dao_cursor_next (cursor, NULL, &error)))
    {
      g_autoptr(BonsaiDaoObject) object = objectptr;

      g_assert_nonnull (object);
      g_assert_cmpstr (G_OBJECT_TYPE_NAME (object), ==, "TestNote");
      g_assert_true (TEST_IS_NOTE (object));

      g_assert_cmpstr (test_note_get_title (TEST_NOTE (object)), ==, "note 3");
      g_assert_cmpstr (test_note_get_body (TEST_NOTE (object)), ==, "this is the body");

      count++;
    }

  g_clear_object (&cursor);
  g_assert_cmpint (count, ==, 1);
  count = 0;

  cursor = bonsai_dao_transaction_query (transaction, collection,
                                         BONSAI_DAO_WHERE_LESS_THAN (TEST_TYPE_NOTE, "created-at", now),
                                         NULL);
  g_assert_nonnull (cursor);
  g_assert_true (BONSAI_IS_DAO_CURSOR (cursor));
  while ((objectptr = bonsai_dao_cursor_next (cursor, NULL, &error)))
    g_assert_not_reached ();
  g_clear_object (&cursor);

  bonsai_dao_transaction_cancel (transaction);
  g_clear_object (&transaction);

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_delete (void)
{
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoTransaction) transaction = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  gchar dirtmpl[] = "test-dao-XXXXXX";
  g_autofree gchar *rm_rf = NULL;
  g_autoptr(GDateTime) now = g_date_time_new_now_local ();
  g_autoptr(GPtrArray) notes = NULL;
  gboolean r;

  g_mkdtemp (dirtmpl);

  path = g_build_filename (dirtmpl, "repository", NULL);
  repository = bonsai_dao_repository_new_for_path (REPO_ID, path, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (repository);
  g_assert_true (BONSAI_IS_DAO_REPOSITORY (repository));

  collection = bonsai_dao_repository_load_collection (repository, "notes", TEST_TYPE_NOTE, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (collection);
  g_assert_true (BONSAI_IS_DAO_COLLECTION (collection));

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  /* Add a secondary index with just the "created-at" property */
  r = bonsai_dao_collection_ensure_index (collection, transaction, NULL, &error, "created-at", NULL);
  g_assert_no_error (error);
  g_assert_true (r);

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  notes = g_ptr_array_new_with_free_func (g_object_unref);

  for (guint i = 0; i < 10; i++)
    {
      g_autofree gchar *title = g_strdup_printf ("note %u", i);
      g_autofree gchar *id = g_uuid_string_random ();
      g_autoptr(TestNote) note = g_object_new (TEST_TYPE_NOTE,
                                               "id", id,
                                               "created-at", now,
                                               "title", title,
                                               "body", "this is the body",
                                               NULL);
      bonsai_dao_transaction_insert (transaction, collection, BONSAI_DAO_OBJECT (note));
      g_ptr_array_add (notes, g_object_ref (note));
    }

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_readwrite (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  for (guint i = 0; i < notes->len; i++)
    {
      TestNote *note = g_ptr_array_index (notes, i);

      bonsai_dao_transaction_delete (transaction, collection, BONSAI_DAO_OBJECT (note));
    }

  r = bonsai_dao_transaction_commit (transaction, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (r);
  g_clear_object (&transaction);

  transaction = bonsai_dao_repository_begin_read (repository, &error);
  g_assert_no_error (error);
  g_assert_nonnull (transaction);
  g_assert_true (BONSAI_IS_DAO_TRANSACTION (transaction));

  for (guint i = 0; i < notes->len; i++)
    {
      TestNote *note = g_ptr_array_index (notes, i);
      const gchar *object_id = bonsai_dao_object_get_id (BONSAI_DAO_OBJECT (note));
      g_autoptr(BonsaiDaoObject) obj = NULL;

      obj = bonsai_dao_transaction_lookup (transaction, collection, object_id);
      g_assert_null (obj);
    }

  rm_rf = g_strdup_printf ("rm -rf '%s'", dirtmpl);
  g_message ("%s", rm_rf);
  system (rm_rf);
}

static void
test_changeset_serialization (void)
{
  g_autoptr(BonsaiDaoSnapshot) first = NULL;
  g_autoptr(BonsaiDaoSnapshot) second = NULL;
  g_autoptr(GPtrArray) diff = NULL;

  first = bonsai_dao_snapshot_new ();
  second = bonsai_dao_snapshot_new ();

  for (guint i = 0; i < 50; i++)
    {
      g_autofree gchar *id = g_strdup_printf ("note-%u", i);
      g_autofree gchar *body = NULL;
      g_autoptr(TestNote) note = g_object_new (TEST_TYPE_NOTE,
                                               "id", id,
                                               "title", id,
                                               "body", NULL,
                                               NULL);
      bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), first);
      body = g_strdup_printf ("this is note\n%u.\n", i);
      g_object_set (note, "body", body, NULL);
      bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), second);
    }

  diff = bonsai_dao_snapshot_diff (first, second);
  g_assert_nonnull (diff);
  g_ptr_array_set_free_func (diff, g_object_unref);

  g_assert_cmpint (diff->len, >, 0);

  for (guint i = 0; i < diff->len; i++)
    {
      BonsaiDaoChangeset *changeset = g_ptr_array_index (diff, i);
      g_autoptr(GVariant) serialized = bonsai_dao_changeset_to_variant (changeset);
      g_autoptr(BonsaiDaoChangeset) deserialized = bonsai_dao_changeset_from_variant (TEST_TYPE_NOTE, serialized);

      g_assert_nonnull (serialized);
      g_assert_nonnull (deserialized);

      g_assert_true (g_variant_is_of_type (serialized, G_VARIANT_TYPE ("(sa(usv))")));
      g_assert_true (BONSAI_IS_DAO_CHANGESET (deserialized));
    }
}

static BonsaiDaoChangeset *
get_changeset (BonsaiDaoSnapshot *first,
               BonsaiDaoSnapshot *second,
               const gchar       *object_id)
{
  g_autoptr(GPtrArray) diff = NULL;

  diff = bonsai_dao_snapshot_diff (first, second);
  g_assert_nonnull (diff);
  g_ptr_array_set_free_func (diff, g_object_unref);

  g_assert_cmpint (diff->len, >, 0);

  for (guint i = 0; i < diff->len; i++)
    {
      BonsaiDaoChangeset *changeset = g_ptr_array_index (diff, i);

      if (g_strcmp0 (object_id, bonsai_dao_changeset_get_object_id (changeset)) == 0)
        return g_object_ref (changeset);
    }

  return NULL;
}

static void
test_changeset_revert (void)
{
  g_autoptr(BonsaiDaoSnapshot) first = bonsai_dao_snapshot_new ();
  g_autoptr(BonsaiDaoSnapshot) second = bonsai_dao_snapshot_new ();
  g_autoptr(BonsaiDaoSnapshot) third = bonsai_dao_snapshot_new ();
  g_autofree gchar *id = g_strdup_printf ("note-%u", 0);
  g_autofree gchar *body = NULL;
  g_autoptr(BonsaiDaoChangeset) changeset = NULL;
  g_autoptr(BonsaiDaoChangeset) revert = NULL;
  g_autoptr(GDateTime) now = g_date_time_new_now_local ();
  g_autoptr(GDateTime) now_plus_1 = g_date_time_add_seconds (now, 1);
  g_autoptr(GVariant) original = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GPtrArray) diff = NULL;
  GVariantDict dict;
  g_autoptr(TestNote) inflated = NULL;
  g_autoptr(TestNote) note = g_object_new (TEST_TYPE_NOTE,
                                           "created-at", now,
                                           "id", id,
                                           "title", id,
                                           "body", NULL,
                                           NULL);
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), first);
  body = g_strdup_printf ("this is note\n%u.\n", 0);
  g_object_set (note, "body", body, "created-at", now_plus_1, NULL);
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (note), second);

  changeset = get_changeset (first, second, id);
  g_assert_nonnull (changeset);
  g_assert_true (BONSAI_IS_DAO_CHANGESET (changeset));

  revert = bonsai_dao_changeset_revert (changeset);
  g_assert_nonnull (revert);
  g_assert_true (BONSAI_IS_DAO_CHANGESET (revert));

  /* revert and ensure we are back to original */
  _bonsai_dao_object_stash (BONSAI_DAO_OBJECT (note));
  original = _bonsai_dao_object_get_original (BONSAI_DAO_OBJECT (note), &error);
  g_assert_no_error (error);
  g_assert_nonnull (original);

  g_variant_dict_init (&dict, original);
  bonsai_dao_changeset_apply (revert, &dict, &error);
  g_assert_no_error (error);

  inflated = bonsai_dao_object_new_from_variant (TEST_TYPE_NOTE, id, g_variant_dict_end (&dict));
  g_assert_nonnull (inflated);
  g_assert_true (TEST_IS_NOTE (inflated));
  bonsai_dao_object_snapshot (BONSAI_DAO_OBJECT (inflated), third);

  diff = bonsai_dao_snapshot_diff (first, third);
  g_assert_nonnull (diff);
  for (guint i = 0; i < diff->len; i++)
    {
      BonsaiDaoChangeset *cs = g_ptr_array_index (diff, i);
      g_autoptr(GVariant) v = NULL;

      g_assert_nonnull (cs);
      g_assert_true (BONSAI_IS_DAO_CHANGESET (cs));

      v = bonsai_dao_changeset_to_variant (cs);
      g_assert_nonnull (v);

      g_print ("%s\n", g_variant_print (v, TRUE));
    }
  g_assert_cmpint (diff->len, ==, 0);
}

gint
main (gint argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Bonsai/Dao/snapshot_new_object", test_snapshot_new_object);
  g_test_add_func ("/Bonsai/Dao/snapshot_test_changes", test_snapshot_changes);
  g_test_add_func ("/Bonsai/Dao/restore_compare", test_restore_compare);
  g_test_add_func ("/Bonsai/Dao/create_repository", test_create_repository);
  g_test_add_func ("/Bonsai/Dao/load_collection", test_load_collection);
  g_test_add_func ("/Bonsai/Dao/create_transaction", test_create_transaction);
  g_test_add_func ("/Bonsai/Dao/update_object", test_update_object);
  g_test_add_func ("/Bonsai/Dao/delete_object", test_delete_object);
  g_test_add_func ("/Bonsai/Dao/query", test_query);
  g_test_add_func ("/Bonsai/Dao/index", test_where);
  g_test_add_func ("/Bonsai/Dao/delete", test_delete);
  g_test_add_func ("/Bonsai/Dao/changeset_serialization", test_changeset_serialization);
  g_test_add_func ("/Bonsai/Dao/changeset_revert", test_changeset_revert);
  return g_test_run ();
}
