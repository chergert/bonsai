/* mock-log.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "mock-log"

#include "config.h"

#include "mock-log.h"

struct _MockLog
{
  IpcDaoLogSkeleton parent;
  BonsaiDaoRepository *repository;
  BonsaiDaoCursor *cursor;
  gchar *token;
};

static gboolean
mock_log_handle_close (IpcDaoLog             *service,
                       GDBusMethodInvocation *invocation)
{
  MockLog *self = (MockLog *)service;

  g_assert (MOCK_IS_LOG (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self));
  g_clear_object (&self->repository);
  g_clear_object (&self->cursor);
  g_clear_pointer (&self->token, g_free);

  ipc_dao_log_complete_close (service, invocation);

  return TRUE;
}

static gboolean
mock_log_handle_read (IpcDaoLog             *service,
                      GDBusMethodInvocation *invocation)
{
  MockLog *self = (MockLog *)service;
  g_autoptr(GVariant) entry = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *id = NULL;

  g_assert (MOCK_IS_LOG (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (self->cursor == NULL)
    g_dbus_method_invocation_return_error (invocation, G_IO_ERROR, G_IO_ERROR_FAILED, "No cursor available");
  else if ((entry = bonsai_dao_cursor_next_value (self->cursor, G_VARIANT_TYPE ("a(sa{sv})"), NULL, &id, &error)))
    ipc_dao_log_complete_read (service, invocation, entry);
  else if (error != NULL)
    g_dbus_method_invocation_return_gerror (invocation, error);
  else
    {
      GVariantBuilder builder;

      g_variant_builder_init (&builder, G_VARIANT_TYPE ("a(sa{sv})"));
      ipc_dao_log_complete_read (service, invocation, g_variant_builder_end (&builder));
    }

  return TRUE;
}

static void
log_iface_init (IpcDaoLogIface *iface)
{
  iface->handle_close = mock_log_handle_close;
  iface->handle_read = mock_log_handle_read;
}

G_DEFINE_TYPE_WITH_CODE (MockLog, mock_log, IPC_TYPE_DAO_LOG_SKELETON,
                         G_IMPLEMENT_INTERFACE (IPC_TYPE_DAO_LOG, log_iface_init))

MockLog *
mock_log_new (BonsaiDaoRepository  *repository,
              const gchar          *token,
              GError              **error)
{
  MockLog *self = g_object_new (MOCK_TYPE_LOG, NULL);
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(BonsaiDaoCollection) collection = NULL;
  g_autoptr(BonsaiDaoCursor) cursor = NULL;

  if (!(txn = bonsai_dao_repository_begin_read (repository, error)))
    return NULL;

  if (!(collection = bonsai_dao_repository_load_collection (repository, "org.gnome.Bonsai.Dao.Log", BONSAI_TYPE_DAO_OBJECT, NULL, error)))
    return NULL;

  if (token == NULL || *token == 0)
    cursor = bonsai_dao_transaction_query (txn, collection, NULL, NULL);
  else
    cursor = bonsai_dao_transaction_query (txn, collection,
                                           BONSAI_DAO_WHERE_GREATER_THAN (BONSAI_TYPE_DAO_OBJECT, "id", token),
                                           NULL);

  self->repository = g_object_ref (repository);
  self->token = g_strdup (token);
  self->cursor = g_object_ref (cursor);

  return self;
}

static void
mock_log_finalize (GObject *object)
{
  MockLog *self = (MockLog *)object;

  g_clear_object (&self->repository);
  g_clear_pointer (&self->token, g_free);
  g_clear_object (&self->cursor);

  G_OBJECT_CLASS (mock_log_parent_class)->finalize (object);
}

static void
mock_log_class_init (MockLogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mock_log_finalize;
}

static void
mock_log_init (MockLog *self)
{
}
