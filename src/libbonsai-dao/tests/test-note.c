/* test-note.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "test-note.h"

struct _TestNote
{
  BonsaiDaoObject parent;
  GDateTime *created_at;
  gchar *title;
  gchar *body;
  gchar **tags;
};

G_DEFINE_TYPE (TestNote, test_note, BONSAI_TYPE_DAO_OBJECT)

enum {
  PROP_0,
  PROP_BODY,
  PROP_CREATED_AT,
  PROP_TAGS,
  PROP_TITLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/**
 * test_note_new:
 *
 * Create a new #TestNote.
 *
 * Returns: (transfer full): a newly created #TestNote
 */
TestNote *
test_note_new (void)
{
  return g_object_new (TEST_TYPE_NOTE, NULL);
}

static void
test_note_finalize (GObject *object)
{
  TestNote *self = (TestNote *)object;

  g_clear_pointer (&self->body, g_free);
  g_clear_pointer (&self->tags, g_strfreev);
  g_clear_pointer (&self->title, g_free);

  G_OBJECT_CLASS (test_note_parent_class)->finalize (object);
}

static void
test_note_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  TestNote *self = TEST_NOTE (object);

  switch (prop_id)
    {
    case PROP_BODY:
      g_value_set_string (value, self->body);
      break;

    case PROP_CREATED_AT:
      g_value_set_boxed (value, self->created_at);
      break;

    case PROP_TITLE:
      g_value_set_string (value, self->title);
      break;

    case PROP_TAGS:
      g_value_set_boxed (value, self->tags);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
test_note_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  TestNote *self = TEST_NOTE (object);

  switch (prop_id)
    {
    case PROP_BODY:
      g_free (self->body);
      self->body = g_value_dup_string (value);
      break;

    case PROP_CREATED_AT:
      g_date_time_unref (self->created_at);
      self->created_at = g_value_dup_boxed (value);
      break;

    case PROP_TITLE:
      g_free (self->title);
      self->title = g_value_dup_string (value);
      break;

    case PROP_TAGS:
      g_strfreev (self->tags);
      self->tags = g_value_dup_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
test_note_class_init (TestNoteClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  BonsaiDaoObjectClass *dao_class = BONSAI_DAO_OBJECT_CLASS (klass);

  object_class->finalize = test_note_finalize;
  object_class->get_property = test_note_get_property;
  object_class->set_property = test_note_set_property;

  properties [PROP_BODY] =
    g_param_spec_string ("body",
                         "Body",
                         "Body",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  bonsai_dao_object_class_install_auto_property (dao_class,
                                                 PROP_BODY,
                                                 properties [PROP_BODY],
                                                 BONSAI_DAO_OBJECT_PROPERTY_STRING_DIFF);

  properties [PROP_CREATED_AT] =
    g_param_spec_boxed ("created-at",
                        "Created At",
                        "Created At",
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  bonsai_dao_object_class_install_auto_property (dao_class,
                                                 PROP_CREATED_AT,
                                                 properties [PROP_CREATED_AT],
                                                 BONSAI_DAO_OBJECT_PROPERTY_OVERWRITE);

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "Title",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  bonsai_dao_object_class_install_auto_property (dao_class,
                                                 PROP_TITLE,
                                                 properties [PROP_TITLE],
                                                 BONSAI_DAO_OBJECT_PROPERTY_OVERWRITE);

  properties [PROP_TAGS] =
    g_param_spec_boxed ("tags",
                         "tags",
                         "tags",
                         G_TYPE_STRV,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  bonsai_dao_object_class_install_auto_property (dao_class,
                                                 PROP_TAGS,
                                                 properties [PROP_TAGS],
                                                 BONSAI_DAO_OBJECT_PROPERTY_OVERWRITE);
}

static void
test_note_init (TestNote *self)
{
  self->created_at = g_date_time_new_now_local ();
}

const gchar *
test_note_get_body (TestNote *self)
{
  g_return_val_if_fail (TEST_IS_NOTE (self), NULL);

  return self->body;
}

const gchar *
test_note_get_title (TestNote *self)
{
  g_return_val_if_fail (TEST_IS_NOTE (self), NULL);

  return self->title;
}

GDateTime *
test_note_get_created_at (TestNote *self)
{
  g_return_val_if_fail (TEST_IS_NOTE (self), NULL);

  return self->created_at;
}

const gchar * const *
test_note_get_tags (TestNote *self)
{
  g_return_val_if_fail (TEST_IS_NOTE (self), NULL);
  return (const gchar * const *)self->tags;
}
