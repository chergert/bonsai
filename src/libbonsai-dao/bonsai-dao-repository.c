/* bonsai-dao-repository.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-repository"

#include "config.h"

#include <libbonsai.h>
#include <string.h>

#include "bonsai-dao-collection.h"
#include "bonsai-dao-collection-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-transaction.h"
#include "bonsai-dao-transaction-private.h"

typedef struct
{
  GMutex      mutex;
  gchar      *path;
  MDB_env    *env;
  gchar      *identifier;
  MDB_dbi     log_slot;
  GHashTable *collections;
  GMutex      wrlock;
} BonsaiDaoRepositoryPrivate;

static void initable_iface_init (GInitableIface *iface);

G_DEFINE_QUARK ("bonsai-dao-repository-error", bonsai_dao_repository_error)
G_DEFINE_TYPE_WITH_CODE (BonsaiDaoRepository, bonsai_dao_repository, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (BonsaiDaoRepository)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, initable_iface_init))

#define MAX_COLLECTIONS 128

enum {
  PROP_0,
  PROP_IDENTIFIER,
  PROP_PATH,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
bonsai_dao_repository_finalize (GObject *object)
{
  BonsaiDaoRepository *self = (BonsaiDaoRepository *)object;
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_clear_pointer (&priv->identifier, g_free);
  g_clear_pointer (&priv->path, g_free);
  g_clear_pointer (&priv->env, mdb_env_close);
  g_clear_pointer (&priv->collections, g_hash_table_unref);

  g_mutex_clear (&priv->mutex);
  g_mutex_clear (&priv->wrlock);

  G_OBJECT_CLASS (bonsai_dao_repository_parent_class)->finalize (object);
}

static void
bonsai_dao_repository_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  BonsaiDaoRepository *self = BONSAI_DAO_REPOSITORY (object);

  switch (prop_id)
    {
    case PROP_IDENTIFIER:
      g_value_set_string (value, bonsai_dao_repository_get_identifier (self));
      break;

    case PROP_PATH:
      g_value_set_string (value, bonsai_dao_repository_get_path (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_repository_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  BonsaiDaoRepository *self = BONSAI_DAO_REPOSITORY (object);
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_IDENTIFIER: {
      const gchar *id = g_value_get_string (value);
      if (g_application_id_is_valid (id))
        priv->identifier = g_strdup (id);
      break;
    }

    case PROP_PATH:
      priv->path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_repository_class_init (BonsaiDaoRepositoryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_repository_finalize;
  object_class->get_property = bonsai_dao_repository_get_property;
  object_class->set_property = bonsai_dao_repository_set_property;

  /**
   * BonsaiDaoRepository:identifier:
   *
   * The :identifier of the repository. This should be a reverse
   * domain name style identifier of your application with the
   * optionally a suffix. For example, "org.example.MyApp". It must
   * pass g_application_id_is_valid().
   */
  properties [PROP_IDENTIFIER] =
    g_param_spec_string ("identifier",
                         "Identifier",
                         "The identifier of the repository",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoRepository:path:
   *
   * The :path property contains the directory on disk that should be used
   * to store collections and transaction logs as well as synchronization
   * state.
   */
  properties [PROP_PATH] =
    g_param_spec_string ("path",
                         "Path",
                         "The path of the data store for the repository",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_dao_repository_init (BonsaiDaoRepository *self)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_mutex_init (&priv->mutex);
  g_mutex_init (&priv->wrlock);

  priv->collections = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

BonsaiDaoRepository *
bonsai_dao_repository_new_for_path (const gchar   *identifier,
                                    const gchar   *path,
                                    GCancellable  *cancellable,
                                    GError       **error)
{
  g_autoptr(BonsaiDaoRepository) self = NULL;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  self = g_object_new (BONSAI_TYPE_DAO_REPOSITORY,
                       "identifier", identifier,
                       "path", path,
                       NULL);

  if (!g_initable_init (G_INITABLE (self), cancellable, error))
    return NULL;

  return g_steal_pointer (&self);
}

/**
 * bonsai_dao_repository_get_path:
 * @self: a #BonsaiDaoRepository
 *
 * Gets the :path property.
 *
 * Returns: a string containing the repository directory
 */
const gchar *
bonsai_dao_repository_get_path (BonsaiDaoRepository *self)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);

  return priv->path;
}

static gboolean
bonsai_dao_repository_initable_init (GInitable     *initable,
                                     GCancellable  *cancellable,
                                     GError       **error)
{
  BonsaiDaoRepository *self = (BonsaiDaoRepository *)initable;
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);
  g_autoptr(GMutexLocker) locker = NULL;
  MDB_txn *txn;
  MDB_dbi slot = 0;

  g_assert (BONSAI_IS_DAO_REPOSITORY (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (priv->path == NULL)
    {
      g_set_error_literal (error,
                           G_IO_ERROR,
                           G_IO_ERROR_INVALID_FILENAME,
                           "BonsaiDaoRepository must be initialized with a path");
      return FALSE;
    }

  locker = g_mutex_locker_new (&priv->mutex);

  /* Make sure the directory exists */
  if (!g_file_test (priv->path, G_FILE_TEST_IS_DIR))
    {
      g_autoptr(GFile) file = g_file_new_for_path (priv->path);
      if (!g_file_make_directory_with_parents (file, cancellable, error))
        return FALSE;
    }

  GOTO_LABEL_IF_NON_ZERO (mdb_env_create (&priv->env), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_env_set_maxdbs (priv->env, MAX_COLLECTIONS), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_env_open (priv->env, priv->path, 0, 0640), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_env_set_userctx (priv->env, self), error, failure);

  /* Create the transaction log if this is first run */
  GOTO_LABEL_IF_NON_ZERO (mdb_txn_begin (priv->env, NULL, 0, &txn), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (txn, "org.gnome.Bonsai.Dao.Log", MDB_CREATE, &slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_txn_commit (txn), error, failure);

  priv->log_slot = slot;

  return TRUE;

failure:
  return FALSE;
}

static void
initable_iface_init (GInitableIface *iface)
{
  iface->init = bonsai_dao_repository_initable_init;
}

/**
 * bonsai_dao_repository_load_collection:
 * @self: a #BonsaiDaoRepository
 * @name: the name of the collection, 64-bytes or less
 * @object_type: the type of objects to store
 * @cancellable: a #GCancellable
 * @error: a location for a #GError, or %NULL
 *
 * Loads a collection from the repository, possibly creating it
 * if necessary.
 *
 * Returns: (transfer full): a #BonsaiDaoCollection or %NULL
 *   and @error is set.
 */
BonsaiDaoCollection *
bonsai_dao_repository_load_collection (BonsaiDaoRepository  *self,
                                       const gchar          *name,
                                       GType                 object_type,
                                       GCancellable         *cancellable,
                                       GError              **error)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);
  g_autoptr(BonsaiDaoCollection) ret = NULL;
  g_autoptr(GMutexLocker) locker = NULL;
  MDB_txn *txn = NULL;
  MDB_dbi slot;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);
  g_return_val_if_fail (name != NULL, NULL);
  g_return_val_if_fail (strlen (name) <= 64, NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  locker = g_mutex_locker_new (&priv->mutex);

  GOTO_LABEL_IF_NON_ZERO (mdb_txn_begin (priv->env, NULL, 0, &txn), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (txn, name, MDB_CREATE, &slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_txn_commit (txn), error, failure);

  ret = _bonsai_dao_collection_new (self, name, object_type, slot);

  g_hash_table_insert (priv->collections, g_strdup (name), g_object_ref (ret));

  return g_steal_pointer (&ret);

failure:
  return NULL;
}

/**
 * bonsai_dao_repository_begin_read:
 * @self: a #BonsaiDaoRepository
 * @error: a location for a #GError, or %NULL
 *
 * Begins a new transaction and returns a #BonsaiDaoTransaction to
 * represent it.
 *
 * This transaction is read-only.
 *
 * Read-only transactions may happen concurrently on multiple threads,
 * but only a single transaction on a single thread may occur.
 *
 * Returns: (transfer full): a #BonsaiDaoTransaction
 */
BonsaiDaoTransaction *
bonsai_dao_repository_begin_read (BonsaiDaoRepository  *self,
                                  GError              **error)
{
  g_autoptr(BonsaiDaoTransaction) ret = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);

  ret = _bonsai_dao_transaction_new (self, FALSE);
  if (!g_initable_init (G_INITABLE (ret), NULL, error))
    BONSAI_RETURN (NULL);

  BONSAI_RETURN (g_steal_pointer (&ret));
}

/**
 * bonsai_dao_repository_begin_readwrite:
 * @self: a #BonsaiDaoRepository
 * @error: a location for a #GError, or %NULL
 *
 * Begins a new transaction and returns a #BonsaiDaoTransaction to
 * represent it.
 *
 * This transaction is read-write.
 *
 * Only a single transaction may be used on a single thread at a time.
 *
 * This function will block while any other write transaction is active
 * as only a single writer transaction may be processed at a time.
 *
 * Returns: (transfer full): a #BonsaiDaoTransaction
 */
BonsaiDaoTransaction *
bonsai_dao_repository_begin_readwrite (BonsaiDaoRepository  *self,
                                       GError              **error)
{
  g_autoptr(BonsaiDaoTransaction) ret = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);

  ret = _bonsai_dao_transaction_new (self, TRUE);
  if (!g_initable_init (G_INITABLE (ret), NULL, error))
    BONSAI_RETURN (NULL);

  BONSAI_RETURN (g_steal_pointer (&ret));
}

static gboolean
check_disposition_locked (BonsaiDaoRepository  *self,
                          GError              **error)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_assert (BONSAI_IS_DAO_REPOSITORY (self));

  if (priv->env == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_INITIALIZED,
                   "repository has not been loaded or has been closed");
      return FALSE;
    }

  return TRUE;
}

MDB_txn *
_bonsai_dao_repository_create_transaction (BonsaiDaoRepository  *self,
                                           gboolean              writeable,
                                           GError              **error)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);
  g_autoptr(GMutexLocker) locker = NULL;
  MDB_txn *ret = NULL;
  guint flags = 0;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);

  locker = g_mutex_locker_new (&priv->mutex);

  if (!check_disposition_locked (self, error))
    return NULL;

  if (!writeable)
    flags |= MDB_RDONLY;

  GOTO_LABEL_IF_NON_ZERO (mdb_txn_begin (priv->env, NULL, flags, &ret), error, failure);

  return g_steal_pointer (&ret);

failure:
  return NULL;
}

const gchar *
bonsai_dao_repository_get_identifier (BonsaiDaoRepository *self)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);

  return priv->identifier;
}

MDB_dbi
_bonsai_dao_repository_get_log (BonsaiDaoRepository *self)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), 0);

  return priv->log_slot;
}

BonsaiDaoCollection *
_bonsai_dao_repository_get_collection (BonsaiDaoRepository *self,
                                       const gchar         *name)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);
  BonsaiDaoCollection *ret;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);
  g_return_val_if_fail (name != NULL, NULL);

  g_mutex_lock (&priv->mutex);
  ret = g_hash_table_lookup (priv->collections, name);
  if (ret != NULL)
    g_object_ref (ret);
  g_mutex_unlock (&priv->mutex);

  return g_steal_pointer (&ret);
}

gchar *
_bonsai_dao_repository_get_revision (BonsaiDaoRepository  *self,
                                     BonsaiDaoTransaction *transaction)
{
  MDB_txn *txn;
  MDB_dbi slot = 0;
  MDB_val key, val;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (transaction), NULL);

  txn = _bonsai_dao_transaction_get_native (transaction);

  key.mv_data = (gpointer)"revision";
  key.mv_size = strlen ("revision");

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (txn, "org.gnome.Bonsai.Dao.Metadata", 0, &slot), NULL, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_get (txn, slot, &key, &val), NULL, failure);

  return g_strndup (val.mv_data, val.mv_size);

failure:
  return NULL;
}

gboolean
_bonsai_dao_repository_set_revision (BonsaiDaoRepository  *self,
                                     BonsaiDaoTransaction *transaction,
                                     const gchar          *revision,
                                     GError              **error)
{
  MDB_txn *txn;
  MDB_dbi slot = 0;
  MDB_val key, val;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), FALSE);
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (transaction), FALSE);
  g_return_val_if_fail (revision != NULL, FALSE);

  txn = _bonsai_dao_transaction_get_native (transaction);

  key.mv_data = (gpointer)"revision";
  key.mv_size = strlen ("revision");

  val.mv_data = (gpointer)revision;
  val.mv_size = strlen (revision);

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (txn, "org.gnome.Bonsai.Dao.Metadata", MDB_CREATE, &slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_put (txn, slot, &key, &val, 0), error, failure);

  BONSAI_RETURN (TRUE);

failure:
  BONSAI_RETURN (FALSE);
}

gchar *
bonsai_dao_repository_get_last_revision (BonsaiDaoRepository  *self,
                                         BonsaiDaoTransaction *txn)
{
  MDB_cursor *cursor = NULL;
  MDB_txn *native;
  MDB_dbi slot;
  MDB_val key, val;
  gchar *ret = NULL;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (self), NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (txn), NULL);

  native = _bonsai_dao_transaction_get_native (txn);

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (native, "org.gnome.Bonsai.Dao.Log", 0, &slot), NULL, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_cursor_open (native, slot, &cursor), NULL, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_cursor_get (cursor, &key, &val, MDB_LAST), NULL, failure);

  ret = g_strndup (key.mv_data, MAX (1, key.mv_size) - 1);

failure:
  g_clear_pointer (&cursor, mdb_cursor_close);

  return ret;
}

void
_bonsai_dao_repository_wrlock (BonsaiDaoRepository *self)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_DAO_REPOSITORY (self));

  g_mutex_lock (&priv->wrlock);
}

void
_bonsai_dao_repository_wrunlock (BonsaiDaoRepository *self)
{
  BonsaiDaoRepositoryPrivate *priv = bonsai_dao_repository_get_instance_private (self);

  g_return_if_fail (BONSAI_IS_DAO_REPOSITORY (self));

  g_mutex_unlock (&priv->wrlock);
}
