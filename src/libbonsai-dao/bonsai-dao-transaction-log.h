/* bonsai-dao-transaction-log.h
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_TRANSACTION_LOG (bonsai_dao_transaction_log_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoTransactionLog, bonsai_dao_transaction_log, BONSAI, DAO_TRANSACTION_LOG, GObject)

typedef enum
{
  BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT = 1,
  BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE = 2,
  BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE = 3,
} BonsaiDaoTransactionLogEntryKind;

typedef struct
{
  /* Which union field to use */
  BonsaiDaoTransactionLogEntryKind kind;

  /* The object_id that is being mutated */
  gchar *id;

  union {
    /* This is a serialized BonsaiDaoChangeset that can be applied
     * to the backing object in a way that will help avoid conflicts
     * for common operations (like "newest value wins" or "push this
     * onto a string array").
     */
    GVariant *update;

    /* This is the complete document to be inserted. It will always
     * fail to replay if another object of the same identifier exists
     * unless they contain the same data.
     */
    GVariant *insert;

    /* This is the object before it was deleted. We use this to ensure
     * that we don't delete content that has changed from under us.
     */
    GVariant *delete;
  };
} BonsaiDaoTransactionLogEntry;

typedef gboolean (*BonsaiDaoTransactionLogForeach) (const gchar                        *collection,
                                                    const BonsaiDaoTransactionLogEntry *entry,
                                                    gpointer                            user_data);

const gchar             *bonsai_dao_transaction_log_get_identifier (BonsaiDaoTransactionLog         *self);
gboolean                 bonsai_dao_transaction_log_is_empty       (BonsaiDaoTransactionLog         *self);
BonsaiDaoTransactionLog *bonsai_dao_transaction_log_from_variant   (GVariant                        *variant);
GVariant                *bonsai_dao_transaction_log_to_variant     (BonsaiDaoTransactionLog         *self,
                                                                    const gchar                     *identifier);
gboolean                 bonsai_dao_transaction_log_foreach        (BonsaiDaoTransactionLog         *self,
                                                                    gboolean                         reverse,
                                                                    BonsaiDaoTransactionLogForeach   foreach_func,
                                                                    gpointer                         foreach_func_data);
gchar                   *bonsai_dao_transaction_log_print          (BonsaiDaoTransactionLog         *self);


G_END_DECLS
