/* bonsai-dao-transaction-log.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-transaction-log"

#include "config.h"

#include <libbonsai.h>

#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-transaction-log-private.h"

struct _BonsaiDaoTransactionLog
{
  GObject     parent_instance;
  GHashTable *entries_by_collection;
  gchar      *identifier;
  guint       is_empty : 1;
};

G_DEFINE_TYPE (BonsaiDaoTransactionLog, bonsai_dao_transaction_log, G_TYPE_OBJECT)

BonsaiDaoTransactionLog *
_bonsai_dao_transaction_log_new (void)
{
  return g_object_new (BONSAI_TYPE_DAO_TRANSACTION_LOG, NULL);
}

static void
bonsai_dao_transaction_log_finalize (GObject *object)
{
  BonsaiDaoTransactionLog *self = (BonsaiDaoTransactionLog *)object;

  g_clear_pointer (&self->entries_by_collection, g_hash_table_unref);
  g_clear_pointer (&self->identifier, g_free);

  G_OBJECT_CLASS (bonsai_dao_transaction_log_parent_class)->finalize (object);
}

static void
bonsai_dao_transaction_log_class_init (BonsaiDaoTransactionLogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_transaction_log_finalize;
}

static void
bonsai_dao_transaction_log_init (BonsaiDaoTransactionLog *self)
{
  self->is_empty = TRUE;
  self->entries_by_collection =
    g_hash_table_new_full (g_str_hash,
                           g_str_equal,
                           g_free,
                           (GDestroyNotify) g_array_unref);
}

static void
clear_entry (BonsaiDaoTransactionLogEntry *entry)
{
  g_clear_pointer (&entry->id, g_free);

  switch (entry->kind)
    {
    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE:
      g_clear_pointer (&entry->delete, g_variant_unref);
      break;

    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT:
      g_clear_pointer (&entry->insert, g_variant_unref);
      break;

    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE:
      g_clear_pointer (&entry->update, g_variant_unref);
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
bonsai_dao_transaction_log_append (BonsaiDaoTransactionLog            *self,
                                   const gchar                        *collection,
                                   const BonsaiDaoTransactionLogEntry *entry)
{
  GArray *ar;

  g_assert (BONSAI_IS_DAO_TRANSACTION_LOG (self));
  g_assert (entry != NULL);

  if (!(ar = g_hash_table_lookup (self->entries_by_collection, collection)))
    {
      ar = g_array_new (FALSE, FALSE, sizeof (BonsaiDaoTransactionLogEntry));
      g_array_set_clear_func (ar, (GDestroyNotify)clear_entry);
      g_hash_table_insert (self->entries_by_collection, g_strdup (collection), ar);
    }

  self->is_empty = FALSE;
  g_array_append_vals (ar, entry, 1);
}

void
_bonsai_dao_transaction_log_update (BonsaiDaoTransactionLog *self,
                                    const gchar             *collection,
                                    const gchar             *id,
                                    GVariant                *update)
{
  BonsaiDaoTransactionLogEntry entry = {0};

  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self));
  g_return_if_fail (collection != NULL);
  g_return_if_fail (id != NULL);
  g_return_if_fail (update != NULL);

  entry.kind = BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE;
  entry.id = g_strdup (id);
  entry.update = g_variant_ref (update);

  bonsai_dao_transaction_log_append (self, collection, &entry);
}

void
_bonsai_dao_transaction_log_insert (BonsaiDaoTransactionLog *self,
                                    const gchar             *collection,
                                    const gchar             *id,
                                    GVariant                *insert)
{
  BonsaiDaoTransactionLogEntry entry = {0};

  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self));
  g_return_if_fail (collection != NULL);
  g_return_if_fail (id != NULL);
  g_return_if_fail (insert != NULL);

  entry.kind = BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT;
  entry.id = g_strdup (id);
  entry.insert = g_variant_ref (insert);

  bonsai_dao_transaction_log_append (self, collection, &entry);
}

void
_bonsai_dao_transaction_log_delete (BonsaiDaoTransactionLog *self,
                                    const gchar             *collection,
                                    const gchar             *id,
                                    GVariant                *variant)
{
  BonsaiDaoTransactionLogEntry entry = {0};

  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self));
  g_return_if_fail (collection != NULL);
  g_return_if_fail (id != NULL);
  g_return_if_fail (variant != NULL);

  entry.kind = BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE;
  entry.id = g_strdup (id);
  entry.delete = g_variant_ref (variant);

  bonsai_dao_transaction_log_append (self, collection, &entry);
}

/**
 * bonsai_dao_transaction_log_to_variant:
 * @self: a #BonsaiDaoTransactionLog
 * @identifier: an identifier for the transaction log
 *
 * This creates a serialized form of @self using @identifier as the
 * identifier for the log.
 *
 * Returns: (transfer floating): a #GVariant containing the serialized form
 *   of the log.
 */
GVariant *
bonsai_dao_transaction_log_to_variant (BonsaiDaoTransactionLog *self,
                                       const gchar             *identifier)
{
  GVariantBuilder builder;
  GHashTableIter iter;
  const gchar *name;
  GVariant *ret;
  GArray *entries;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self), NULL);
  g_return_val_if_fail (identifier != NULL, NULL);

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a(sa{sv})"));
  g_variant_builder_open (&builder, G_VARIANT_TYPE ("(sa{sv})"));
  g_variant_builder_add (&builder, "s", identifier);
  g_variant_builder_open (&builder, G_VARIANT_TYPE ("a{sv}"));
  g_hash_table_iter_init (&iter, self->entries_by_collection);
  while (g_hash_table_iter_next (&iter, (gpointer *)&name, (gpointer *)&entries))
    {
      g_variant_builder_open (&builder, G_VARIANT_TYPE ("{sv}"));
      g_variant_builder_add (&builder, "s", name);
      g_variant_builder_open (&builder, G_VARIANT_TYPE ("v"));
      g_variant_builder_open (&builder, G_VARIANT_TYPE ("a(usv)"));
      for (guint i = 0; i < entries->len; i++)
        {
          const BonsaiDaoTransactionLogEntry *entry = &g_array_index (entries, BonsaiDaoTransactionLogEntry, i);

          g_variant_builder_open (&builder, G_VARIANT_TYPE ("(usv)"));
          g_variant_builder_add (&builder, "u", entry->kind);
          g_variant_builder_add (&builder, "s", entry->id);
          g_variant_builder_open (&builder, G_VARIANT_TYPE ("v"));

          switch (entry->kind)
            {
            case BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT:
              g_variant_builder_add_value (&builder, entry->insert);
              break;

            case BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE:
              g_variant_builder_add_value (&builder, entry->delete);
              break;

            case BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE:
              g_variant_builder_add_value (&builder, entry->update);
              break;

            default:
              g_assert_not_reached ();
            }

          g_variant_builder_close (&builder);
          g_variant_builder_close (&builder);
        }
      g_variant_builder_close (&builder);
      g_variant_builder_close (&builder);
      g_variant_builder_close (&builder);
    }
  g_variant_builder_close (&builder);
  g_variant_builder_close (&builder);

  ret = g_variant_builder_end (&builder);

  BONSAI_RETURN (ret);
}

/**
 * bonsai_dao_transaction_log_is_empty:
 * @self: a #BonsaiDaoTransactionLog
 *
 * Checks if the transaction log is empty. This is useful to avoid committing
 * empty transactions logs.
 *
 * Returns: %TRUE if there are no entries in @self
 */
gboolean
bonsai_dao_transaction_log_is_empty (BonsaiDaoTransactionLog *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self), FALSE);

  return self->is_empty;
}

static gchar *
generate_identifier (MDB_txn  *txn,
                     MDB_dbi   slot,
                     GError  **error)
{
  g_autoptr(GDateTime) now = g_date_time_new_now_utc ();
  gint64 now_seconds = g_date_time_to_unix (now);
  gint64 usec = g_date_time_get_microsecond (now);
  gint64 commit_time = now_seconds * 1000L + usec / 1000L;
  guint counter = 0;

  /* The identifier for the transaction log BonsaiDaoTransactionLogEntry is the UNIX time in
   * milliseconds suffixed with period '.' and then a counter that is
   * monotonic if we get multiple write transactions during the same
   * millisecond (unlikely for our current use-case).
   */

  for (;;)
    {
      g_autofree gchar *identifier = NULL;
      MDB_val key, val;
      gint r;

      identifier = g_strdup_printf ("%020"G_GINT64_FORMAT".%u", commit_time, counter);

      key.mv_data = identifier;
      key.mv_size = strlen (identifier) + 1;

      r = mdb_get (txn, slot, &key, &val);

      if (r == MDB_NOTFOUND)
        return g_steal_pointer (&identifier);

      if (r == 0)
        {
          counter++;
          continue;
        }

      g_set_error_literal (error,
                           BONSAI_DAO_REPOSITORY_ERROR,
                           r,
                           mdb_strerror (r));
      return NULL;
    }

  g_assert_not_reached ();
}

gboolean
_bonsai_dao_transaction_log_commit (BonsaiDaoTransactionLog  *self,
                                    MDB_txn                  *txn,
                                    MDB_dbi                   slot,
                                    GError                  **error)
{
  g_autoptr(GVariant) variant = NULL;
  g_autofree gchar *identifier = NULL;
  MDB_val key, val;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self), FALSE);
  g_return_val_if_fail (txn != NULL, FALSE);
  g_return_val_if_fail (slot > 0, FALSE);
  g_return_val_if_fail (self->identifier == NULL, FALSE);

  if (!(identifier = generate_identifier (txn, slot, error)))
    BONSAI_RETURN (FALSE);

  variant = bonsai_dao_transaction_log_to_variant (self, identifier);

  key.mv_data = identifier;
  key.mv_size = strlen (identifier) + 1;

  val.mv_data = (gpointer)g_variant_get_data (variant);
  val.mv_size = g_variant_get_size (variant);

  GOTO_LABEL_IF_NON_ZERO (mdb_put (txn, slot, &key, &val, MDB_APPEND), error, failure);

  self->identifier = g_steal_pointer (&identifier);

  BONSAI_RETURN (TRUE);

failure:
  BONSAI_RETURN (FALSE);
}

/**
 * bonsai_dao_transaction_log_from_variant:
 * @variant: a #GVariant containing a log
 *
 * This creates a new transaction log from a serialized log entry
 * provided in @variant.
 *
 * Returns: (transfer full) (nullable): a #BonsaiDaoTransactionLog or %NULL
 *   if there was a failure to decode.
 */
BonsaiDaoTransactionLog *
bonsai_dao_transaction_log_from_variant (GVariant *variant)
{
  g_autoptr(BonsaiDaoTransactionLog) self = NULL;
  g_autoptr(GVariantIter) collections = NULL;
  g_autofree gchar *identifier = NULL;
  GVariantIter iter;

  BONSAI_ENTRY;

  g_return_val_if_fail (variant != NULL, NULL);
  g_return_val_if_fail (g_variant_is_of_type (variant, G_VARIANT_TYPE ("a(sa{sv})")), NULL);

  self = _bonsai_dao_transaction_log_new ();

  g_variant_iter_init (&iter, variant);

  /* TODO: Remove the outer array from this? */
  if (g_variant_iter_next (&iter, "(sa{sv})", &identifier, &collections))
    {
      g_autoptr(GVariant) entriesv = NULL;
      const gchar *collection_name;

      while (g_variant_iter_next (collections, "{&sv}", &collection_name, &entriesv))
        {
          g_autoptr(GVariant) state = NULL;
          GVariantIter entries_iter;
          const gchar *object_id;
          BonsaiDaoTransactionLogEntryKind kind;

          if (!g_variant_is_of_type (entriesv, G_VARIANT_TYPE ("a(usv)")))
            BONSAI_GOTO (failure);

          g_variant_iter_init (&entries_iter, entriesv);

          while (g_variant_iter_next (&entries_iter, "(u&sv)", &kind, &object_id, &state))
            {
              BonsaiDaoTransactionLogEntry entry;

              switch (kind)
                {
                case BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT:
                  if (!g_variant_is_of_type (state, G_VARIANT_TYPE_VARDICT))
                    BONSAI_GOTO (failure);
                  entry.kind = BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT;
                  entry.id = g_strdup (object_id);
                  entry.insert = g_variant_ref (state);
                  break;

                case BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE:
                  if (!g_variant_is_of_type (state, G_VARIANT_TYPE_VARDICT))
                    BONSAI_GOTO (failure);
                  entry.kind = BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE;
                  entry.id = g_strdup (object_id);
                  entry.delete = g_variant_ref (state);
                  break;

                case BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE:
                  if (!g_variant_is_of_type (state, G_VARIANT_TYPE ("(sa(usv))")))
                    BONSAI_GOTO (failure);
                  entry.kind = BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE;
                  entry.id = g_strdup (object_id);
                  entry.update = g_variant_ref (state);
                  break;

                default:
                  BONSAI_GOTO (failure);
                }

              bonsai_dao_transaction_log_append (self, collection_name, &entry);
            }
        }
    }

  self->identifier = g_steal_pointer (&identifier);

  BONSAI_RETURN (g_steal_pointer (&self));

failure:
  BONSAI_RETURN (NULL);
}

/**
 * bonsai_dao_transaction_log_foreach:
 * @self: a #BonsaiDaoTransactionLog
 * @reverse: if iteration should occur from most-recent to least-recent;
 *   this is useful for reverting changes
 * @foreach_func: (scope call) (closure foreach_func_data): a callback for each log entry
 * @foreach_func_data: closure data for @foreach_func
 *
 * Iterates each entry in the log.
 *
 * If @reverse is %TRUE, then the entries are iterated in reverse order.
 *
 * If @foreach_func returns %FALSE, iteration is stopped immediately. Otherwise
 * it should return %TRUE to continue processing.
 *
 * Returns: %FALSE if iteration was stopped prematurely; otherwise %TRUE.
 */
gboolean
bonsai_dao_transaction_log_foreach (BonsaiDaoTransactionLog        *self,
                                    gboolean                        reverse,
                                    BonsaiDaoTransactionLogForeach  foreach_func,
                                    gpointer                        foreach_func_data)
{
  GHashTableIter iter;
  const gchar *name;
  GArray *entries;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self), FALSE);
  g_return_val_if_fail (foreach_func != NULL, FALSE);

  g_hash_table_iter_init (&iter, self->entries_by_collection);
  while (g_hash_table_iter_next (&iter, (gpointer *)&name, (gpointer *)&entries))
    {
      g_assert (name != NULL);
      g_assert (entries != NULL);

      if (reverse)
        {
          for (guint i = entries->len; i > 0; i--)
            {
              const BonsaiDaoTransactionLogEntry *entry = &g_array_index (entries, BonsaiDaoTransactionLogEntry, i - 1);

              if (!foreach_func (name, entry, foreach_func_data))
                BONSAI_RETURN (FALSE);
            }
        }
      else
        {
          for (guint i = 0; i < entries->len; i++)
            {
              const BonsaiDaoTransactionLogEntry *entry = &g_array_index (entries, BonsaiDaoTransactionLogEntry, i);

              if (!foreach_func (name, entry, foreach_func_data))
                BONSAI_RETURN (FALSE);
            }
        }
    }

  BONSAI_RETURN (TRUE);
}

/**
 * bonsai_dao_transaction_log_get_identifier:
 * @self: a #BonsaiDaoTransactionLog
 *
 * Gets the identifier for the log if one has ben set.
 *
 * Returns: (nullable): a string contianing the identifier or %NULL
 */
const gchar *
bonsai_dao_transaction_log_get_identifier (BonsaiDaoTransactionLog *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self), NULL);

  return self->identifier;
}

/**
 * bonsai_dao_transaction_log_print:
 * @self: a #BonsaiDaoTransactionLog
 *
 * Creates a string representation of a transaction log which can aid
 * in troubleshooting errors.
 *
 * Returns: (transfer full): a string containing a representation of the
 *   transaction log.
 */
gchar *
bonsai_dao_transaction_log_print (BonsaiDaoTransactionLog *self)
{
  GString *str;
  GHashTableIter iter;
  gpointer k,v;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (self), NULL);

  str = g_string_new (NULL);
  g_string_append_printf (str, "Transaction %s (\n", self->identifier ?: "");

  g_hash_table_iter_init (&iter, self->entries_by_collection);
  while (g_hash_table_iter_next (&iter, &k, &v))
    {
      const gchar *collection = k;
      GArray *entries = v;

      g_string_append_printf (str, "  Collection %s (\n", collection);
      for (guint i = 0; i < entries->len; i++)
        {
          BonsaiDaoTransactionLogEntry *entry = &g_array_index (entries, BonsaiDaoTransactionLogEntry, i);
          g_autofree gchar *vstr = NULL;

          switch (entry->kind)
            {
            case BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT:
              vstr = g_variant_print (entry->insert, TRUE);
              g_string_append_printf (str, "    INSERT %s: %s\n", entry->id, vstr);
              break;

            case BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE:
              vstr = g_variant_print (entry->delete, TRUE);
              g_string_append_printf (str, "    DELETE %s: %s\n", entry->id, vstr);
              break;

            case BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE:
              vstr = g_variant_print (entry->update, TRUE);
              g_string_append_printf (str, "    UPDATE %s: %s\n", entry->id, vstr);
              break;

            default:
              g_assert_not_reached ();
            }
        }
      g_string_append (str, "  )\n");
    }

  g_string_append (str, ")");

  return g_string_free (str, FALSE);
}
