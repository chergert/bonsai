/* bonsai-dao-secondary-index.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-secondary-index"

#include "config.h"

#include "bonsai-dao-query-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-secondary-index-private.h"
#include "bonsai-dao-utils.h"

struct _BonsaiDaoSecondaryIndex
{
  BonsaiDaoIndex       parent_instance;
  const GVariantType  *variant_type;
  gchar               *name;
  GParamSpec         **pspecs;
  guint                n_pspecs;
  MDB_dbi              slot;
};

G_DEFINE_TYPE (BonsaiDaoSecondaryIndex, bonsai_dao_secondary_index, BONSAI_TYPE_DAO_INDEX)

static void
add_default_value (GVariantBuilder *builder,
                   GParamSpec      *pspec)
{
  GType type;

  g_assert (builder != NULL);
  g_assert (pspec != NULL);

  type = pspec->value_type;

  if (G_TYPE_IS_ENUM (type))
    type = G_TYPE_ENUM;
  else if (G_TYPE_IS_FLAGS (type))
    type = G_TYPE_FLAGS;

  switch (pspec->value_type)
    {
    case G_TYPE_INT:
      g_variant_builder_add (builder, "i", 0);
      break;
    case G_TYPE_ENUM:
    case G_TYPE_FLAGS:
    case G_TYPE_UINT:
      g_variant_builder_add (builder, "u", 0);
      break;
    case G_TYPE_INT64:
    case G_TYPE_LONG:
      g_variant_builder_add (builder, "x", 0);
      break;
    case G_TYPE_UINT64:
    case G_TYPE_ULONG:
      g_variant_builder_add (builder, "t", 0);
      break;
    case G_TYPE_STRING:
      g_variant_builder_add (builder, "s", "");
      break;
    case G_TYPE_BOOLEAN:
      g_variant_builder_add (builder, "b", FALSE);
      break;
    case G_TYPE_DOUBLE:
    case G_TYPE_FLOAT:
      g_variant_builder_add (builder, "d", 0.0);
      break;

    default:
      if (pspec->value_type == G_TYPE_DATE_TIME)
        {
          g_variant_builder_add (builder, "x", 0);
          break;
        }

      /* Multi-key documents are not yet supported */
      if (pspec->value_type == G_TYPE_STRV) { }

      g_assert_not_reached ();
    }
}

static GVariant *
create_default_value (GParamSpec *pspec)
{
  g_assert (pspec != NULL);

  switch (pspec->value_type)
    {
    case G_TYPE_INT:
      return g_variant_new_int32 (0);
    case G_TYPE_UINT:
      return g_variant_new_uint32 (0);
    case G_TYPE_INT64:
    case G_TYPE_LONG:
      return g_variant_new_int64 (0);
    case G_TYPE_UINT64:
    case G_TYPE_ULONG:
      return g_variant_new_uint64 (0);
    case G_TYPE_STRING:
      return g_variant_new_string ("");
    case G_TYPE_BOOLEAN:
      return g_variant_new_boolean (FALSE);
    case G_TYPE_DOUBLE:
    case G_TYPE_FLOAT:
      return g_variant_new_double (0.0);

    default:
      if (pspec->value_type == G_TYPE_DATE_TIME)
        return g_variant_new_int64 (0);

      /* Multi-key documents are not yet supported */
      if (pspec->value_type == G_TYPE_STRV) { }

      g_assert_not_reached ();
    }
}

static GVariant *
bonsai_dao_secondary_index_build_key (BonsaiDaoSecondaryIndex *self,
                                      const gchar             *id,
                                      GVariant                *variant)
{
  GVariantBuilder builder;

  g_assert (BONSAI_IS_DAO_SECONDARY_INDEX (self));
  g_assert (id != NULL);
  g_assert (variant != NULL);
  g_assert (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARDICT));

  if (self->n_pspecs == 1)
    {
      GParamSpec *pspec = self->pspecs[0];
      GVariant *ret;

      if (!(ret = g_variant_lookup_value (variant, pspec->name, NULL)))
        ret = create_default_value (pspec);

      return g_variant_take_ref (ret);
    }

  g_variant_builder_init (&builder, self->variant_type);

  for (guint i = 0; i < self->n_pspecs; i++)
    {
      GParamSpec *pspec = self->pspecs[i];
      g_autoptr(GVariant) element = NULL;

      if ((element = g_variant_lookup_value (variant, pspec->name, NULL)))
        g_variant_builder_add_value (&builder, element);
      else
        add_default_value (&builder, pspec);
    }

  return g_variant_take_ref (g_variant_builder_end (&builder));
}

static gboolean
bonsai_dao_secondary_index_insert (BonsaiDaoIndex  *index,
                                   MDB_txn         *txn,
                                   const gchar     *id,
                                   GVariant        *variant,
                                   GError         **error)
{
  BonsaiDaoSecondaryIndex *self = (BonsaiDaoSecondaryIndex *)index;
  g_autoptr(GVariant) keydoc = NULL;
  MDB_val key;
  MDB_val val;

  g_assert (BONSAI_IS_DAO_SECONDARY_INDEX (self));
  g_assert (txn != NULL);
  g_assert (id != NULL);
  g_assert (variant != NULL);
  g_assert (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARDICT));

  keydoc = bonsai_dao_secondary_index_build_key (self, id, variant);

  key.mv_data = (gpointer)g_variant_get_data (keydoc);
  key.mv_size = g_variant_get_size (keydoc);

  val.mv_data = (gpointer)id;
  val.mv_size = strlen (id) + 1;

  GOTO_LABEL_IF_NON_ZERO (mdb_put (txn, self->slot, &key, &val, 0), error, failure);

  return TRUE;

failure:
  return FALSE;
}

static gboolean
bonsai_dao_secondary_index_delete (BonsaiDaoIndex  *index,
                                   MDB_txn         *txn,
                                   const gchar     *id,
                                   GVariant        *variant,
                                   GError         **error)
{
  BonsaiDaoSecondaryIndex *self = (BonsaiDaoSecondaryIndex *)index;
  g_autoptr(GVariant) keydoc = NULL;
  MDB_val key;
  MDB_val val;

  g_assert (BONSAI_IS_DAO_SECONDARY_INDEX (self));
  g_assert (txn != NULL);
  g_assert (id != NULL);
  g_assert (variant != NULL);
  g_assert (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARDICT));

  keydoc = bonsai_dao_secondary_index_build_key (self, id, variant);

  key.mv_data = (gpointer)g_variant_get_data (keydoc);
  key.mv_size = g_variant_get_size (keydoc);

  val.mv_data = (gpointer)id;
  val.mv_size = strlen (id) + 1;

  GOTO_LABEL_IF_NON_ZERO (mdb_del (txn, self->slot, &key, &val), error, failure);

  return TRUE;

failure:
  return FALSE;
}

static gboolean
bonsai_dao_secondary_index_create_cursor (BonsaiDaoIndex  *index,
                                          MDB_txn         *txn,
                                          MDB_cursor     **cursor)
{
  BonsaiDaoSecondaryIndex *self = (BonsaiDaoSecondaryIndex *)index;

  g_assert (BONSAI_IS_DAO_INDEX (index));
  g_assert (txn != NULL);
  g_assert (cursor != NULL);

  GOTO_LABEL_IF_NON_ZERO (mdb_cursor_open (txn, self->slot, cursor), NULL, failure);

  return TRUE;

failure:
  return FALSE;
}

static gboolean
bonsai_dao_secondary_index_move_first (BonsaiDaoIndex *index,
                                       BonsaiDaoQuery *query,
                                       MDB_cursor     *cursor,
                                       MDB_val        *key,
                                       MDB_val        *val)
{
  BonsaiDaoSecondaryIndex *self = (BonsaiDaoSecondaryIndex *)index;

  g_assert (BONSAI_IS_DAO_SECONDARY_INDEX (self));
  g_assert (BONSAI_IS_DAO_QUERY (query));
  g_assert (cursor != NULL);
  g_assert (key != NULL);
  g_assert (val != NULL);

  /*
   * If we have a single pspec for the index, then we can try to set
   * the lower bound for the key and move starting from that key.
   */

  if (self->n_pspecs == 1)
    {
      GValue lower = G_VALUE_INIT;
      GValue upper = G_VALUE_INIT;

      /* Try to get the lower bound for this pspec so that we
       * can start at the first item matching it (or above).
       */
      if (!_bonsai_dao_query_get_bounds (query, self->pspecs[0], &lower, &upper))
        return FALSE;

      if (G_IS_VALUE (&lower))
        {
          g_autoptr(GVariant) keyv = bonsai_dao_value_to_variant (&lower);

          key->mv_data = (gpointer)g_variant_get_data (keyv);
          key->mv_size = g_variant_get_size (keyv);

          GOTO_LABEL_IF_NON_ZERO (mdb_cursor_get (cursor, key, val, MDB_SET_RANGE), NULL, failure);

          return TRUE;
        }
    }

  /* TODO: Support complex tuple keys */

  /* We failed to find a lower bound for the cursor, so we have
   * to start at the beginning of the index.
   */
  GOTO_LABEL_IF_NON_ZERO (mdb_cursor_get (cursor, key, val, MDB_FIRST), NULL, failure);

  return TRUE;

failure:
  return FALSE;
}

static gboolean
bonsai_dao_secondary_index_has_pspec (BonsaiDaoIndex *index,
                                      GParamSpec     *pspec)
{
  BonsaiDaoSecondaryIndex *self = (BonsaiDaoSecondaryIndex *)index;

  g_assert (BONSAI_IS_DAO_SECONDARY_INDEX (index));
  g_assert (pspec != NULL);

  for (guint i = 0; i < self->n_pspecs; i++)
    {
      if (self->pspecs[i] == pspec)
        return TRUE;
    }

  return FALSE;
}

static void
bonsai_dao_secondary_index_finalize (GObject *object)
{
  BonsaiDaoSecondaryIndex *self = (BonsaiDaoSecondaryIndex *)object;

  for (guint i = 0; i < self->n_pspecs; i++)
    g_param_spec_unref (self->pspecs[i]);
  g_clear_pointer (&self->pspecs, g_free);
  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (bonsai_dao_secondary_index_parent_class)->finalize (object);
}

static void
bonsai_dao_secondary_index_class_init (BonsaiDaoSecondaryIndexClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  BonsaiDaoIndexClass *index_class = BONSAI_DAO_INDEX_CLASS (klass);

  object_class->finalize = bonsai_dao_secondary_index_finalize;

  index_class->insert = bonsai_dao_secondary_index_insert;
  index_class->delete = bonsai_dao_secondary_index_delete;
  index_class->create_cursor = bonsai_dao_secondary_index_create_cursor;
  index_class->move_first = bonsai_dao_secondary_index_move_first;
  index_class->has_pspec = bonsai_dao_secondary_index_has_pspec;
}

static void
bonsai_dao_secondary_index_init (BonsaiDaoSecondaryIndex *self)
{
}

BonsaiDaoSecondaryIndex *
_bonsai_dao_secondary_index_new (const gchar  *primary_name,
                                 GParamSpec  **pspecs,
                                 guint         n_pspecs)
{
  BonsaiDaoSecondaryIndex *self;
  g_autoptr(GString) str = NULL;
  g_autoptr(GChecksum) checksum = NULL;
  const gchar *digest;

  g_assert (pspecs != NULL);
  g_assert (n_pspecs > 0);

  str = g_string_new (NULL);

  if (n_pspecs > 1)
    g_string_append_c (str, '(');

  for (guint i = 0; i < n_pspecs; i++)
    {
      const gchar *tstr = bonsai_dao_get_variant_type_string (pspecs[i]->value_type);

      if (tstr == NULL)
        return NULL;

      g_string_append (str, tstr);
    }

  if (n_pspecs > 1)
    g_string_append_c (str, ')');

  self = g_object_new (BONSAI_TYPE_DAO_SECONDARY_INDEX, NULL);
  self->pspecs = g_memdup2 (pspecs, n_pspecs * sizeof (GParamSpec *));
  self->n_pspecs = n_pspecs;
  self->variant_type = G_VARIANT_TYPE (str->str);

  for (guint i = 0; i < n_pspecs; i++)
    g_param_spec_ref (pspecs[i]);

  checksum = g_checksum_new (G_CHECKSUM_SHA1);
  g_checksum_update (checksum, (const guchar *)str->str, str->len);
  digest = g_checksum_get_string (checksum);

  self->name = g_strdup_printf ("%s$%s", primary_name, digest);

  return g_steal_pointer (&self);
}

gboolean
_bonsai_dao_secondary_index_is_supported_type (GType type)
{
  switch (type)
    {
    case G_TYPE_INT:
    case G_TYPE_UINT:
    case G_TYPE_INT64:
    case G_TYPE_UINT64:
    case G_TYPE_LONG:
    case G_TYPE_ULONG:
    case G_TYPE_STRING:
    case G_TYPE_BOOLEAN:
    case G_TYPE_FLOAT:
    case G_TYPE_DOUBLE:
      return TRUE;

    default:
#if 0
    /* Not currently supported */
    case G_TYPE_VARIANT:
    case G_TYPE_STRV:
#endif

      return type == G_TYPE_DATE_TIME;
    }
}

#define DEFINE_INT_COMPARE_FUNC(name, type)   \
static gint                                   \
name (const MDB_val *a,                       \
      const MDB_val *b)                       \
{                                             \
  type aval, bval;                            \
  memcpy (&aval, a->mv_data, sizeof (type));  \
  memcpy (&bval, b->mv_data, sizeof (type));  \
  if (aval < bval) return -1;                 \
  if (aval > bval) return 1;                  \
  return 0;                                   \
}

DEFINE_INT_COMPARE_FUNC (compare_i32, gint32)
DEFINE_INT_COMPARE_FUNC (compare_u32, guint32)
DEFINE_INT_COMPARE_FUNC (compare_i64, gint64)
DEFINE_INT_COMPARE_FUNC (compare_u64, guint64)
DEFINE_INT_COMPARE_FUNC (compare_boolean, gboolean)
DEFINE_INT_COMPARE_FUNC (compare_double, gdouble)

static gint
compare_string (const MDB_val *a,
                const MDB_val *b)
{
  /* strings are \0 terminated */
  return strcmp (a->mv_data, b->mv_data);
}

static gint
compare_slow (const MDB_val *a,
              const MDB_val *b)
{
  g_assert (a != NULL);
  g_assert (b != NULL);

  /* TODO: Implement complex compare func. What is difficult here is that
   * we don't have a way to set compare data, which we need to decode the
   * tuple. Unless we find a way to do that, we'll have to use a Vardict
   * so that we have encapsulated types. Unforutnately that adds a lot of
   * size overhead to the indexed objects (and our limit is 511 bytes).
   */
  g_assert_not_reached ();

  return 0;
}

static MDB_cmp_func *
get_compare_func (BonsaiDaoSecondaryIndex *self)
{
  g_assert (BONSAI_IS_DAO_SECONDARY_INDEX (self));

  /* We can use fast-path compare funcs if there is only a single
   * item being indexed by comparing the data directly.
   */
  if (self->n_pspecs == 1)
    {
      GParamSpec *pspec = self->pspecs[0];
      GType type = pspec->value_type;

      if (G_TYPE_IS_ENUM (type))
        type = G_TYPE_ENUM;
      else if (G_TYPE_IS_FLAGS (type))
        type = G_TYPE_FLAGS;
      else if (type == G_TYPE_DATE_TIME)
        type = G_TYPE_INT64;

      switch (type)
        {
        case G_TYPE_INT:
          return compare_i32;

        case G_TYPE_ENUM:
        case G_TYPE_FLAGS:
        case G_TYPE_UINT:
          return compare_u32;

        case G_TYPE_LONG:
        case G_TYPE_INT64:
          return compare_i64;

        case G_TYPE_ULONG:
        case G_TYPE_UINT64:
          return compare_u64;

        case G_TYPE_DOUBLE:
        case G_TYPE_FLOAT:
          return compare_double;

        case G_TYPE_BOOLEAN:
          return compare_boolean;

        case G_TYPE_STRING:
          return compare_string;

        default:
          break;
        }
    }

  return compare_slow;
}

gboolean
_bonsai_dao_secondary_index_open (BonsaiDaoSecondaryIndex  *self,
                                  MDB_txn                  *txn,
                                  GError                  **error)
{
  MDB_cmp_func *compare;

  g_return_val_if_fail (BONSAI_IS_DAO_SECONDARY_INDEX (self), FALSE);
  g_return_val_if_fail (txn != NULL, FALSE);

  compare = get_compare_func (self);

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (txn, self->name, 0, &self->slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_set_compare (txn, self->slot, compare), error, failure);

  return TRUE;

failure:
  return FALSE;
}

gboolean
_bonsai_dao_secondary_index_create (BonsaiDaoSecondaryIndex  *self,
                                    MDB_txn                  *txn,
                                    MDB_dbi                   primary_slot,
                                    GError                  **error)
{
  MDB_cmp_func *compare;

  g_return_val_if_fail (BONSAI_IS_DAO_SECONDARY_INDEX (self), FALSE);
  g_return_val_if_fail (txn != NULL, FALSE);

  compare = get_compare_func (self);

  GOTO_LABEL_IF_NON_ZERO (mdb_dbi_open (txn, self->name, MDB_CREATE|MDB_DUPSORT, &self->slot), error, failure);
  GOTO_LABEL_IF_NON_ZERO (mdb_set_compare (txn, self->slot, compare), error, failure);

  return TRUE;

failure:
  return FALSE;
}
