/* bonsai-dao-transaction-log-private.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <lmdb.h>

#include "bonsai-dao-transaction-log.h"

G_BEGIN_DECLS

BonsaiDaoTransactionLog *_bonsai_dao_transaction_log_new    (void);
gboolean                 _bonsai_dao_transaction_log_commit (BonsaiDaoTransactionLog  *self,
                                                             MDB_txn                  *txn,
                                                             MDB_dbi                   slot,
                                                             GError                  **error);
void                     _bonsai_dao_transaction_log_insert (BonsaiDaoTransactionLog  *self,
                                                             const gchar              *collection,
                                                             const gchar              *id,
                                                             GVariant                 *update);
void                     _bonsai_dao_transaction_log_update (BonsaiDaoTransactionLog  *self,
                                                             const gchar              *collection,
                                                             const gchar              *id,
                                                             GVariant                 *update);
void                     _bonsai_dao_transaction_log_delete (BonsaiDaoTransactionLog  *self,
                                                             const gchar              *collection,
                                                             const gchar              *id,
                                                             GVariant                 *delete);

G_END_DECLS
