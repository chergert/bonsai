/* bonsai-dao-snapshot.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_SNAPSHOT (bonsai_dao_snapshot_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoSnapshot, bonsai_dao_snapshot, BONSAI, DAO_SNAPSHOT, GObject)

BonsaiDaoSnapshot *bonsai_dao_snapshot_new          (void);
gboolean           bonsai_dao_snapshot_push_object  (BonsaiDaoSnapshot *self,
                                                     const gchar       *id);
void               bonsai_dao_snapshot_pop          (BonsaiDaoSnapshot *self);
void               bonsai_dao_snapshot_append_value (BonsaiDaoSnapshot *self,
                                                     GParamSpec        *pspec,
                                                     const GValue      *value);
GPtrArray         *bonsai_dao_snapshot_diff         (BonsaiDaoSnapshot *before,
                                                     BonsaiDaoSnapshot *after);

G_END_DECLS
