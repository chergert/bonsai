/* bonsai-dao-secondary-index.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-index-private.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_SECONDARY_INDEX (bonsai_dao_secondary_index_get_type())

G_DECLARE_FINAL_TYPE (BonsaiDaoSecondaryIndex, bonsai_dao_secondary_index, BONSAI, DAO_SECONDARY_INDEX, BonsaiDaoIndex)

gboolean                 _bonsai_dao_secondary_index_is_supported_type (GType                     type);
BonsaiDaoSecondaryIndex *_bonsai_dao_secondary_index_new               (const gchar              *primary_name,
                                                                        GParamSpec              **pspecs,
                                                                        guint                     n_pspecs);
gboolean                 _bonsai_dao_secondary_index_open              (BonsaiDaoSecondaryIndex  *self,
                                                                        MDB_txn                  *txn,
                                                                        GError                  **error);
gboolean                 _bonsai_dao_secondary_index_create            (BonsaiDaoSecondaryIndex  *self,
                                                                        MDB_txn                  *txn,
                                                                        MDB_dbi                   primary_slot,
                                                                        GError                  **error);

G_END_DECLS
