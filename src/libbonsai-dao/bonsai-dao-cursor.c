/* bonsai-dao-cursor.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-cursor"

#include "config.h"

#include <libbonsai.h>
#include <lmdb.h>

#include "bonsai-dao-collection.h"
#include "bonsai-dao-collection-private.h"
#include "bonsai-dao-cursor.h"
#include "bonsai-dao-error.h"
#include "bonsai-dao-cursor-private.h"
#include "bonsai-dao-index-private.h"
#include "bonsai-dao-query-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-repository-private.h"

struct _BonsaiDaoCursor
{
  GObject              parent_instance;
  BonsaiDaoCollection *collection;
  BonsaiDaoQuery      *query;
  BonsaiDaoIndex      *index;
  MDB_cursor          *cursor;
  GError              *failure;
  MDB_dbi              slot;
  guint                did_first : 1;
};

G_DEFINE_TYPE_WITH_CODE (BonsaiDaoCursor, bonsai_dao_cursor, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, NULL))

enum {
  PROP_0,
  PROP_COLLECTION,
  PROP_INDEX,
  PROP_NATIVE,
  PROP_QUERY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
bonsai_dao_cursor_finalize (GObject *object)
{
  BonsaiDaoCursor *self = (BonsaiDaoCursor *)object;

  g_clear_pointer (&self->cursor, mdb_cursor_close);
  g_clear_object (&self->collection);
  g_clear_object (&self->index);
  g_clear_object (&self->query);
  g_clear_error (&self->failure);

  G_OBJECT_CLASS (bonsai_dao_cursor_parent_class)->finalize (object);
}

static void
bonsai_dao_cursor_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  BonsaiDaoCursor *self = BONSAI_DAO_CURSOR (object);

  switch (prop_id)
    {
    case PROP_COLLECTION:
      g_value_set_object (value, self->collection);
      break;

    case PROP_INDEX:
      g_value_set_object (value, self->index);
      break;

    case PROP_NATIVE:
      g_value_set_pointer (value, self->cursor);
      break;

    case PROP_QUERY:
      g_value_set_object (value, self->query);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_cursor_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  BonsaiDaoCursor *self = BONSAI_DAO_CURSOR (object);

  switch (prop_id)
    {
    case PROP_COLLECTION:
      if ((self->collection = g_value_dup_object (value)))
        self->slot = _bonsai_dao_collection_get_slot (self->collection);
      g_assert (self->slot > 0);
      break;

    case PROP_INDEX:
      self->index = g_value_dup_object (value);
      break;

    case PROP_NATIVE:
      self->cursor = g_value_get_pointer (value);
      break;

    case PROP_QUERY:
      self->query = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_cursor_class_init (BonsaiDaoCursorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_cursor_finalize;
  object_class->get_property = bonsai_dao_cursor_get_property;
  object_class->set_property = bonsai_dao_cursor_set_property;

  /**
   * BonsaiDaoCursor:collection:
   *
   * The :collection property contains the collection to be queried.
   */
  properties [PROP_COLLECTION] =
    g_param_spec_object ("collection",
                         "Collection",
                         "The collection to be queried",
                         BONSAI_TYPE_DAO_COLLECTION,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoCursor:index:
   *
   * The :index that will be iterated by the cursor.
   *
   * Currently, only a single index is supported for cursors.
   */
  properties [PROP_INDEX] =
    g_param_spec_object ("index",
                         "Index",
                         "The index for the cursor",
                         BONSAI_TYPE_DAO_INDEX,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoCursor:native:
   *
   * The :native property is the underlying cursor provided by the
   * storage backend.
   */
  properties [PROP_NATIVE] =
    g_param_spec_pointer ("native",
                          "Native",
                          "The native cursor",
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoCursor:query:
   *
   * The :query to be executed on the :collection.
   */
  properties [PROP_QUERY] =
    g_param_spec_object ("query",
                         "Query",
                         "The query to be executed",
                         BONSAI_TYPE_DAO_QUERY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_dao_cursor_init (BonsaiDaoCursor *self)
{
  self->slot = -1;
}

BonsaiDaoCursor *
_bonsai_dao_cursor_new_failed (const GError *error)
{
  BonsaiDaoCursor *self;

  g_return_val_if_fail (error != NULL, NULL);

  self = g_object_new (BONSAI_TYPE_DAO_CURSOR, NULL);
  self->failure = g_error_copy (error);

  return self;
}

BonsaiDaoCursor *
_bonsai_dao_cursor_new (BonsaiDaoCollection *collection,
                        BonsaiDaoQuery      *query)
{
  g_autoptr(BonsaiDaoIndex) index = NULL;
  g_autoptr(GError) error = NULL;
  BonsaiDaoCursor *self;
  MDB_cursor *cursor = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (collection), NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (query), NULL);

  if (!_bonsai_dao_query_create_cursor (query, &cursor, &index, &error))
    {
      g_warning ("%s", error->message);
      BONSAI_RETURN (NULL);
    }

  self = g_object_new (BONSAI_TYPE_DAO_CURSOR,
                       "collection", collection,
                       "query", query,
                       "index", index,
                       "native", cursor,
                       NULL);

  BONSAI_RETURN (g_steal_pointer (&self));
}

static gboolean
bonsai_dao_cursor_try_next (BonsaiDaoCursor  *self,
                            MDB_val          *key,
                            MDB_val          *value,
                            GError          **error)
{
  gboolean ret;

  BONSAI_ENTRY;

  if (self->failure)
    {
      if (error)
        *error = g_error_copy (self->failure);
      BONSAI_RETURN (FALSE);
    }

  if (self->cursor == NULL)
    BONSAI_RETURN (FALSE);

  if G_UNLIKELY (!self->did_first)
    {
      self->did_first = TRUE;
      ret = _bonsai_dao_index_move_first (self->index, self->query, self->cursor, key, value);
    }
  else
    {
      ret = _bonsai_dao_index_move_next (self->index, self->cursor, key, value);
    }

  BONSAI_RETURN (ret);
}

static gboolean
bonsai_dao_cursor_next_internal (BonsaiDaoCursor     *self,
                                 const GVariantType  *expected_type,
                                 gchar              **out_id,
                                 GVariant           **out_doc,
                                 GCancellable        *cancellable,
                                 GError             **error)
{
  MDB_val key, val;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_CURSOR (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (expected_type == NULL)
    expected_type = G_VARIANT_TYPE_VARDICT;

  while (bonsai_dao_cursor_try_next (self, &key, &val, error))
    {
      gboolean is_primary = G_OBJECT_TYPE (self->index) == BONSAI_TYPE_DAO_INDEX;
      g_autoptr(GBytes) bytes = NULL;
      g_autoptr(GVariant) doc = NULL;
      const gchar *id;
      MDB_val keycopy;
      gboolean matches;

      /* Primary keys store "id" in the key, secondary in the value (referencing
       * the primary key).
       */
      keycopy = is_primary ? key : val;

      /* Make sure we have a proper id to work with */
      if (keycopy.mv_size == 0 || ((gchar *)keycopy.mv_data)[keycopy.mv_size-1] != 0)
        continue;

      id = (const gchar *)keycopy.mv_data;

      /* If this is a secondary, we need to open the document from the
       * primary index. Note that there is an optimization possibility
       * here if the secondary index includes all the keys necessary to
       * satisfy the query we can avoid this expensive fetch.
       */
      if (!is_primary)
        {
          /* If we can't find the referenced document, then we have a secondary
           * index that is out of sync. We should probably find a way to notify
           * the application that a re-indexing is necessary.
           */
          if (mdb_get (mdb_cursor_txn (self->cursor), self->slot, &keycopy, &val) != 0)
            continue;
        }

      /* Create a new document for the record. One problem here is that
       * GVariant requires that the data be aligned. This is problematic
       * since there is likely probability that our value will only be
       * aligned to two-bytes. Instead of special casing, we just take the
       * hit and copy the bytes up-front.
       *
       * What we might want to do, is to determine how likely we are to
       * hit the right alignment (on 64-bit, if we tweak keys, we can
       * probably make this more likely). If so, using a fast path for
       * query matches might make sense.
       */
      bytes = g_bytes_new (val.mv_data, val.mv_size);
      doc = g_variant_take_ref (g_variant_new_from_bytes (expected_type, bytes, TRUE));
      matches = _bonsai_dao_query_matches (self->query, id, doc);

      /* If we matched the query, it's okay to take the hit and copy the data
       * into a new allocation as it could end up living for a bit of time (as
       * we don't know what the user-layers will do with it).
       */
      if (matches)
        {
          if (out_id != NULL)
            *out_id = g_strdup ((const gchar *)keycopy.mv_data);

          if (out_doc)
            *out_doc = g_steal_pointer (&doc);

          BONSAI_RETURN (TRUE);
        }
    }

  BONSAI_RETURN (FALSE);
}

/**
 * bonsai_dao_cursor_next:
 * @self: a #BonsaiDaoCursor
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @error: a location for a #GError or %NULL
 *
 * Advances the cursor to the next result.
 *
 * This function will return %NULL after the result set has been
 * exhausted and @error is left unset.
 *
 * If an error occurred decoding a document, @error will be set and
 * %NULL is returned.
 *
 * Returns: (transfer full) (nullable): a #BonsaiDaoObject if successful;
 *   %NULL if the cursor is exhausted and @error may be set.
 */
BonsaiDaoObject *
bonsai_dao_cursor_next (BonsaiDaoCursor  *self,
                        GCancellable     *cancellable,
                        GError          **error)
{
  g_autoptr(GVariant) doc = NULL;
  g_autofree gchar *id = NULL;
  BonsaiDaoObject *ret = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_CURSOR (self), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  if (bonsai_dao_cursor_next_internal (self, G_VARIANT_TYPE_VARDICT, &id, &doc, cancellable, error))
    ret = _bonsai_dao_collection_inflate (self->collection, id, doc);

  BONSAI_RETURN (ret);
}

/**
 * bonsai_dao_cursor_next_value:
 * @self: a #BonsaiDaoCursor
 * @expected_type: (nullable): a #GVariantType or %NULL for a vardict
 * @cancellable: (nullable): a #GCancellable
 * @id: (out): a location for the primary key
 * @error: a location for a #GError
 *
 * Gets the next value from the cursor without inflating it into a
 * #BonsaiDaoObject.
 *
 * Returns: (transfer full): a #GVariant if successful; otherwise %NULL
 *   and @error is set.
 */
GVariant *
bonsai_dao_cursor_next_value (BonsaiDaoCursor     *self,
                              const GVariantType  *expected_type,
                              GCancellable        *cancellable,
                              gchar              **id,
                              GError             **error)
{
  g_autoptr(GVariant) doc = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_CURSOR (self), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  if (id != NULL)
    *id = NULL;

  if (bonsai_dao_cursor_next_internal (self, expected_type, id, &doc, cancellable, error))
    BONSAI_RETURN (g_steal_pointer (&doc));

  BONSAI_RETURN (NULL);
}

gint64
bonsai_dao_cursor_count (BonsaiDaoCursor  *self,
                         GCancellable     *cancellable,
                         GError          **error)
{
  g_autoptr(GError) local_error = NULL;
  gint64 count = 0;

  g_return_val_if_fail (BONSAI_IS_DAO_CURSOR (self), -1);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), -1);

  while (bonsai_dao_cursor_next_internal (self, NULL, NULL, NULL, cancellable, &local_error))
    count++;

  if (local_error != NULL)
    {
      g_propagate_error (error, g_steal_pointer (&local_error));
      return -1;
    }

  return count;
}

void
bonsai_dao_cursor_reset (BonsaiDaoCursor *self)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CURSOR (self));

  self->did_first = 0;
  g_clear_pointer (&self->failure, g_error_free);

  BONSAI_EXIT;
}

GVariant *
_bonsai_dao_cursor_move_last_value (BonsaiDaoCursor     *self,
                                    const GVariantType  *expected_type,
                                    gchar              **id,
                                    GError             **error)
{
  MDB_val key, val;

  g_return_val_if_fail (BONSAI_IS_DAO_CURSOR (self), NULL);
  g_return_val_if_fail (id != NULL, NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self->index), NULL);
  g_return_val_if_fail (G_OBJECT_TYPE (self->index) == BONSAI_IS_DAO_INDEX (self->index), NULL);

  if (_bonsai_dao_index_move_last (self->index, self->cursor, &key, &val))
    {
      g_autoptr(GBytes) bytes = NULL;
      GVariant *v;

      bytes = g_bytes_new (val.mv_data, val.mv_size);
      v = g_variant_new_from_bytes (expected_type, bytes, TRUE);

      if (v != NULL)
        {
          *id = g_strndup (key.mv_data, MAX (1, key.mv_size) - 1);
          return g_variant_take_ref (v);
        }
    }

  return FALSE;
}

GVariant *
_bonsai_dao_cursor_move_prev_value (BonsaiDaoCursor     *self,
                                    const GVariantType  *expected_type,
                                    gchar              **id,
                                    GError             **error)
{
  MDB_val key, val;

  g_return_val_if_fail (BONSAI_IS_DAO_CURSOR (self), NULL);
  g_return_val_if_fail (id != NULL, NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_INDEX (self->index), NULL);
  g_return_val_if_fail (G_OBJECT_TYPE (self->index) == BONSAI_IS_DAO_INDEX (self->index), NULL);

  if (_bonsai_dao_index_move_prev (self->index, self->cursor, &key, &val))
    {
      g_autoptr(GBytes) bytes = NULL;
      GVariant *v;

      bytes = g_bytes_new (val.mv_data, val.mv_size);
      v = g_variant_new_from_bytes (expected_type, bytes, TRUE);

      if (v != NULL)
        {
          *id = g_strndup (key.mv_data, MAX (1, key.mv_size) - 1);
          return g_variant_take_ref (v);
        }
    }

  return FALSE;
}
