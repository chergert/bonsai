/* bonsai-dao-transaction.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-transaction"

#include "config.h"

#include <libbonsai.h>
#include <lmdb.h>

#include "bonsai-dao-changeset.h"
#include "bonsai-dao-collection.h"
#include "bonsai-dao-collection-private.h"
#include "bonsai-dao-cursor-private.h"
#include "bonsai-dao-object.h"
#include "bonsai-dao-object-private.h"
#include "bonsai-dao-query-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-secondary-index-private.h"
#include "bonsai-dao-snapshot.h"
#include "bonsai-dao-snapshot-private.h"
#include "bonsai-dao-transaction.h"
#include "bonsai-dao-transaction-log-private.h"
#include "bonsai-dao-transaction-private.h"
#include "bonsai-dao-where.h"

/**
 * SECTION:bonsai-dao-transaction
 * @title: BonsaiDaoTransaction
 * @short_description: Transaction support for collections
 *
 * The #BonsaiDaoTransaction object allows for transactions across the
 * collections within the repository.
 *
 * The #BonsaiDaoTransaction may only be used from a single thread and
 * a thread may only have a single transaction at a time.
 */

struct _BonsaiDaoTransaction
{
  GObject                  parent_instance;
  BonsaiDaoTransactionLog *log;
  BonsaiDaoRepository     *repository;
  MDB_txn                 *txn;
  BonsaiDaoSnapshot       *snapshot;
  GError                  *failure;
  GPtrArray               *inserted;
  GPtrArray               *deleted;
  guint                    writeable : 1;
  guint                    released : 1;
};

static void initable_iface_init (GInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (BonsaiDaoTransaction, bonsai_dao_transaction, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, initable_iface_init))

enum {
  PROP_0,
  PROP_REPOSITORY,
  PROP_WRITEABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
bonsai_dao_transaction_release (BonsaiDaoTransaction *self)
{
  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));

  if (self->released)
    return;

  self->released = TRUE;

  if (self->writeable)
    _bonsai_dao_repository_wrunlock (self->repository);

  BONSAI_EXIT;
}

static void
bonsai_dao_transaction_constructed (GObject *object)
{
  BonsaiDaoTransaction *self = (BonsaiDaoTransaction *)object;

  if (self->writeable)
    _bonsai_dao_repository_wrlock (self->repository);

  G_OBJECT_CLASS (bonsai_dao_transaction_parent_class)->constructed (object);
}

static void
bonsai_dao_transaction_finalize (GObject *object)
{
  BonsaiDaoTransaction *self = (BonsaiDaoTransaction *)object;

  g_clear_pointer (&self->inserted, g_ptr_array_unref);
  g_clear_pointer (&self->deleted, g_ptr_array_unref);
  g_clear_object (&self->repository);
  g_clear_object (&self->log);
  g_clear_object (&self->snapshot);
  g_clear_pointer (&self->failure, g_error_free);
  g_clear_pointer (&self->txn, mdb_txn_abort);

  bonsai_dao_transaction_release (self);

  G_OBJECT_CLASS (bonsai_dao_transaction_parent_class)->finalize (object);
}

static void
bonsai_dao_transaction_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  BonsaiDaoTransaction *self = BONSAI_DAO_TRANSACTION (object);

  switch (prop_id)
    {
    case PROP_REPOSITORY:
      g_value_set_object (value, bonsai_dao_transaction_get_repository (self));
      break;

    case PROP_WRITEABLE:
      g_value_set_boolean (value, bonsai_dao_transaction_get_writeable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_transaction_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  BonsaiDaoTransaction *self = BONSAI_DAO_TRANSACTION (object);

  switch (prop_id)
    {
    case PROP_REPOSITORY:
      self->repository = g_value_dup_object (value);
      break;

    case PROP_WRITEABLE:
      self->writeable = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_transaction_class_init (BonsaiDaoTransactionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = bonsai_dao_transaction_constructed;
  object_class->finalize = bonsai_dao_transaction_finalize;
  object_class->get_property = bonsai_dao_transaction_get_property;
  object_class->set_property = bonsai_dao_transaction_set_property;

  /**
   * BonsaiDaoTransaction:repository:
   *
   * The :repository contains the repository that created the transaction.
   */
  properties [PROP_REPOSITORY] =
    g_param_spec_object ("repository",
                         "Repository",
                         "The repository for the transaction",
                         BONSAI_TYPE_DAO_REPOSITORY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoTransaction:writeable:
   *
   * The :writeable property denotes if the transaction is writeable.
   */
  properties [PROP_WRITEABLE] =
    g_param_spec_boolean ("writeable",
                          "Writeable",
                          "If the transaction is writeable",
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_dao_transaction_init (BonsaiDaoTransaction *self)
{
  BONSAI_ENTRY;

  self->log = _bonsai_dao_transaction_log_new ();
  self->inserted = g_ptr_array_new_with_free_func (g_object_unref);
  self->deleted = g_ptr_array_new_with_free_func (g_object_unref);
  self->snapshot = bonsai_dao_snapshot_new ();

  BONSAI_EXIT;
}

BonsaiDaoTransaction *
_bonsai_dao_transaction_new (BonsaiDaoRepository *repository,
                             gboolean             writeable)
{
  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (repository), NULL);

  return g_object_new (BONSAI_TYPE_DAO_TRANSACTION,
                       "repository", repository,
                       "writeable", writeable,
                       NULL);
}

/**
 * bonsai_dao_transaction_get_repository:
 * @self: a #BonsaiDaoTransaction
 *
 * Gets the :repository property which is the repository that created
 * this transaction.
 *
 * Returns: (transfer none): a #BonsaiDaoRepository
 */
BonsaiDaoRepository *
bonsai_dao_transaction_get_repository (BonsaiDaoTransaction *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), NULL);

  return self->repository;
}

void
bonsai_dao_transaction_cancel (BonsaiDaoTransaction  *self)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION (self));

  g_clear_pointer (&self->txn, mdb_txn_abort);
  g_clear_pointer (&self->failure, g_error_free);

  bonsai_dao_transaction_release (self);

  BONSAI_EXIT;
}

gboolean
bonsai_dao_transaction_commit (BonsaiDaoTransaction  *self,
                               GCancellable          *cancellable,
                               GError               **error)
{
  MDB_txn *txn;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  if (!(txn = g_steal_pointer (&self->txn)))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Cannot commit non-existent transaction");
      BONSAI_RETURN (FALSE);
    }

  if (self->failure != NULL)
    {
      g_propagate_error (error, g_error_copy (self->failure));
      g_clear_pointer (&self->txn, mdb_txn_abort);
      return FALSE;
    }

  /* If this is a writeable transaction, we need to append the log to our
   * internal transaction log so that we can replay transactions after pulling
   * new objects from the Bonsai peer. This log will be truncated during sync
   * once the changes have been re-applied on top of the updated objects.
   */
  if (self->writeable && !bonsai_dao_transaction_log_is_empty (self->log))
    {
      MDB_dbi log_dbi;

      /* Add the log of items to the transaction log index so that we can replay
       * the items later if we resync from the peer (or need to push these changes
       * to the peer).
       */

      log_dbi = _bonsai_dao_repository_get_log (self->repository);
      if (!_bonsai_dao_transaction_log_commit (self->log, txn, log_dbi, &self->failure))
        BONSAI_GOTO (failure);
    }

  GOTO_LABEL_IF_NON_ZERO (mdb_txn_commit (txn), error, failure);

  /* Mark current object state has committed. This allows for future
   * diffs to be incremental from the new state. We also clear the
   * snapshot state for deleted objects so they don't get incremental
   * updates further.
   */

  for (guint i = 0; i < self->inserted->len; i++)
    {
      BonsaiDaoObject *object = g_ptr_array_index (self->inserted, i);
      _bonsai_dao_object_stash (object);
    }

  for (guint i = 0; i < self->deleted->len; i++)
    {
      BonsaiDaoObject *object = g_ptr_array_index (self->deleted, i);
      _bonsai_dao_object_set_snapshot (object, NULL);
    }

  bonsai_dao_transaction_release (self);

  BONSAI_RETURN (TRUE);

failure:
  bonsai_dao_transaction_release (self);

  BONSAI_RETURN (FALSE);
}

static gboolean
bonsai_dao_transaction_initable_init (GInitable     *initable,
                                      GCancellable  *cancellable,
                                      GError       **error)
{
  BonsaiDaoTransaction *self = (BonsaiDaoTransaction *)initable;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (self->repository == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_INITIALIZED,
                   "No repository provided for transaction");
      return FALSE;
    }
  else
    {
      self->txn = _bonsai_dao_repository_create_transaction (self->repository,
                                                             self->writeable,
                                                             error);
      return self->txn != NULL;
    }
}

static void
initable_iface_init (GInitableIface *iface)
{
  iface->init = bonsai_dao_transaction_initable_init;
}

static gboolean
check_disposition (BonsaiDaoTransaction *self,
                   gboolean              requires_write)
{
  g_assert (BONSAI_IS_DAO_TRANSACTION (self));

  if (self->failure)
    {
      return FALSE;
    }
  else if (self->txn == NULL)
    {
      self->failure = g_error_new_literal (G_IO_ERROR,
                                           G_IO_ERROR_NOT_INITIALIZED,
                                           "Cannot perform operation, transaction not initialized");
      return FALSE;
    }
  else if (requires_write)
    {
      if (!self->writeable)
        {
          self->failure = g_error_new_literal (G_FILE_ERROR,
                                               G_FILE_ERROR_ACCES,
                                               "Transaction is not writeable");
          return FALSE;
        }
    }

  return TRUE;
}

static GVariant *
bonsai_dao_transaction_lookup_variant (BonsaiDaoTransaction *self,
                                       BonsaiDaoCollection  *collection,
                                       const gchar          *id)
{
  g_autoptr(GBytes) bytes = NULL;
  GVariant *variant;
  MDB_val key, val;
  MDB_dbi slot;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (BONSAI_IS_DAO_COLLECTION (collection));
  g_assert (id != NULL);

  slot = _bonsai_dao_collection_get_slot (collection);

  key.mv_data = (gpointer)id;
  key.mv_size = strlen (id) + 1;

  val.mv_data = NULL;
  val.mv_size = 0;

  if (mdb_get (self->txn, slot, &key, &val) != 0)
    return NULL;

  if (val.mv_size == 0)
    return NULL;

  g_assert (val.mv_data != NULL);
  g_assert (val.mv_size > 0);

  bytes = g_bytes_new (val.mv_data, val.mv_size);
  variant = g_variant_new_from_bytes (G_VARIANT_TYPE_VARDICT, bytes, TRUE);

  return variant ? g_variant_take_ref (variant) : NULL;
}

static gboolean
bonsai_dao_transaction_insert_variant (BonsaiDaoTransaction *self,
                                       BonsaiDaoCollection  *collection,
                                       const gchar          *id,
                                       GVariant             *variant)
{
  BonsaiDaoIndex * const *indexes;
  const gchar *name;
  guint n_indexes;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (BONSAI_IS_DAO_COLLECTION (collection));
  g_assert (id != NULL);
  g_assert (variant != NULL);

  /* Add this entry to the log so we can replay it */
  name = bonsai_dao_collection_get_name (collection);
  _bonsai_dao_transaction_log_insert (self->log, name, id, variant);

  /* Update secondary indexes to point at the object */
  indexes = _bonsai_dao_collection_indexes (collection, &n_indexes);
  for (guint i = 0; i < n_indexes; i++)
    {
      BonsaiDaoIndex *index = indexes[i];

      if (!_bonsai_dao_index_insert (index, self->txn, id, variant, &self->failure))
        BONSAI_RETURN (FALSE);
    }

  BONSAI_RETURN (TRUE);
}

/**
 * bonsai_dao_transaction_insert:
 * @self: a #BonsaiDaoTransaction
 * @collection: a #BonsaiDaoCollection
 * @object: a #BonsaiDaoObject
 *
 * This function inserts a new document as part of the transaction.
 * The contents of @object are inserted using the #BonsaiDaoObject:id
 * property as the primary key for the record.
 *
 * If the collection is configured to have secondary indexes, additional
 * index entries for the object will be inserted into the respective
 * secondary indexes.
 *
 * Use bonsai_dao_transaction_commit() to complete the transaction.
 */
void
bonsai_dao_transaction_insert (BonsaiDaoTransaction *self,
                               BonsaiDaoCollection  *collection,
                               BonsaiDaoObject      *object)
{
  g_autoptr(GVariant) variant = NULL;
  const gchar *id;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION (self));
  g_return_if_fail (BONSAI_IS_DAO_OBJECT (object));
  g_return_if_fail (self->txn != NULL);

  if (!check_disposition (self, TRUE))
    BONSAI_EXIT;

  bonsai_dao_object_snapshot (object, self->snapshot);

  if (!(id = bonsai_dao_object_get_id (object)))
    {
      self->failure = g_error_new (G_IO_ERROR,
                                   G_IO_ERROR_NOT_INITIALIZED,
                                   "Object is missing an id!");
      BONSAI_EXIT;
    }

  if ((variant = _bonsai_dao_snapshot_get_object_as_variant (self->snapshot, id, &self->failure)))
    {
      if (bonsai_dao_transaction_insert_variant (self, collection, id, variant))
        {
          /* Keep the object around so we can persist snapshot state
           * to the underlying object. This allows future diffs of the
           * object to apply to the committed state.
           */
          g_ptr_array_add (self->inserted, g_object_ref (object));
        }
    }

  BONSAI_EXIT;
}

/**
 * bonsai_dao_transaction_update:
 * @self: a #BonsaiDaoTransaction
 * @collection: a #BonsaiDaoCollection
 * @object: a #BonsaiDaoObject to be updated
 *
 * Updates @object in the backing store.
 *
 * This attempts to do a minimal number of transforms to the underlying
 * object in underlying storage. Those changes are also recorded in a
 * transaction log so that updates can be replayed on Bonsai peers through
 * the use of a Bonsai transaction object storage service.
 *
 * The changes are applied during a successful call to
 * bonsai_dao_transaction_commit().
 *
 * It is a programming error to modify @object after calling this function
 * and before a call to bonsai_dao_transaction_commit() or
 * bonsai_dao_transaction_cancel().
 */
void
bonsai_dao_transaction_update (BonsaiDaoTransaction *self,
                               BonsaiDaoCollection  *collection,
                               BonsaiDaoObject      *object)
{
  g_autoptr(GPtrArray) diff = NULL;
  const gchar *id;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION (self));
  g_return_if_fail (BONSAI_IS_DAO_COLLECTION (collection));
  g_return_if_fail (BONSAI_IS_DAO_OBJECT (object));

  if (!check_disposition (self, TRUE))
    BONSAI_EXIT;

  if (!(id = bonsai_dao_object_get_id (object)))
    {
      self->failure = g_error_new (G_IO_ERROR,
                                   G_IO_ERROR_NOT_INITIALIZED,
                                   "Object is missing an id!");
      BONSAI_EXIT;
    }

  diff = _bonsai_dao_object_diff (object);

  for (guint i = 0; i < diff->len; i++)
    {
      BonsaiDaoChangeset *changeset = g_ptr_array_index (diff, i);
      g_autoptr(GVariant) variant = NULL;

      g_assert (BONSAI_IS_DAO_CHANGESET (changeset));

      /* Only update changes to the requested object */
      if (g_strcmp0 (id, bonsai_dao_changeset_get_object_id (changeset)) != 0)
        continue;

      /* Make sure we have an object to update */
      if (!(variant = bonsai_dao_transaction_lookup_variant (self, collection, id)))
        {
          self->failure = g_error_new (G_IO_ERROR,
                                       G_IO_ERROR_NOT_FOUND,
                                       "No such object id '%s' to update",
                                       id);
          BONSAI_EXIT;
        }
      else
        {
          GVariantDict dict = G_VARIANT_DICT_INIT (variant);
          g_autoptr(GVariant) old_variant = NULL;
          g_autoptr(GVariant) changesetv = NULL;
          BonsaiDaoIndex * const *indexes;
          const gchar *name;
          guint n_indexes;

          if (!bonsai_dao_changeset_apply (changeset, &dict, &self->failure))
            BONSAI_EXIT;

          /* Add this entry to the log so we can replay it */
          name = bonsai_dao_collection_get_name (collection);
          changesetv = bonsai_dao_changeset_to_variant (changeset);
          _bonsai_dao_transaction_log_update (self->log, name, id, changesetv);

          g_clear_pointer (&variant, g_variant_unref);
          variant = g_variant_take_ref (g_variant_dict_end (&dict));

          /* To simplify things we do a delete/insert for the document so
           * that we get all of the indexes updated appropriately. To do that
           * we need to get the original document with old values from the
           * secondary indexes.
           */
          if (!(old_variant = _bonsai_dao_object_get_original (object, &self->failure)))
            BONSAI_EXIT;

          indexes = _bonsai_dao_collection_indexes (collection, &n_indexes);
          for (guint j = 0; j < n_indexes; j++)
            {
              BonsaiDaoIndex *index = indexes[j];

              /* Delete the old index entries */
              if (!_bonsai_dao_index_delete (index, self->txn, id, old_variant, &self->failure))
                BONSAI_EXIT;

              /* Insert the new primary/secondary index entries */
              if (!_bonsai_dao_index_insert (index, self->txn, id, variant, &self->failure))
                BONSAI_EXIT;
            }
        }
    }

  BONSAI_EXIT;
}

/**
 * bonsai_dao_transaction_lookup:
 * @self: a #BonsaiDaoTransaction
 * @collection: a #BonsaiDaoCollection
 * @object_id: the object identifier
 *
 * Lookups up and inflates a #BonsaiDaoObject from @collection with the
 * identifier @object_id.
 *
 * Returns: (transfer full) (nullable): a #BonsaiDaoObject if found;
 *   otherwise %NULL.
 */
BonsaiDaoObject *
bonsai_dao_transaction_lookup (BonsaiDaoTransaction *self,
                               BonsaiDaoCollection  *collection,
                               const gchar          *id)
{
  g_autoptr(GVariant) variant = NULL;
  BonsaiDaoObject *ret = NULL;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (collection), NULL);
  g_return_val_if_fail (id != NULL, NULL);

  if (!check_disposition (self, FALSE))
    BONSAI_GOTO (not_found);

  if (!(variant = bonsai_dao_transaction_lookup_variant (self, collection, id)))
    BONSAI_GOTO (not_found);

  ret = _bonsai_dao_collection_inflate (collection, id, variant);

not_found:
  BONSAI_RETURN (ret);
}

static gboolean
bonsai_dao_transaction_delete_variant (BonsaiDaoTransaction *self,
                                       BonsaiDaoCollection  *collection,
                                       const gchar          *id,
                                       GVariant             *variant)
{
  BonsaiDaoIndex * const *indexes;
  const gchar *name;
  guint n_indexes;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (BONSAI_IS_DAO_COLLECTION (collection));
  g_assert (id != NULL);
  g_assert (variant != NULL);

  /* Add this entry to the log so we can replay it */
  name = bonsai_dao_collection_get_name (collection);
  _bonsai_dao_transaction_log_delete (self->log, name, id, variant);

  /* Remove object from primary/secondary indexes */
  indexes = _bonsai_dao_collection_indexes (collection, &n_indexes);
  for (guint i = 0; i < n_indexes; i++)
    {
      BonsaiDaoIndex *index = indexes[i];

      if (_bonsai_dao_index_delete (index, self->txn, id, variant, &self->failure))
        BONSAI_GOTO (failure);
    }

  BONSAI_RETURN (TRUE);

failure:
  BONSAI_RETURN (FALSE);
}

/**
 * bonsai_dao_transaction_delete:
 * @self: a #BonsaiDaoTransaction
 * @collection: a #BonsaiDaoCollection
 * @object: a #BonsaiDaoObject
 *
 * Deletes @object from @collection.
 */
void
bonsai_dao_transaction_delete (BonsaiDaoTransaction *self,
                               BonsaiDaoCollection  *collection,
                               BonsaiDaoObject      *object)
{
  g_autoptr(GVariant) variant = NULL;
  const gchar *id;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (BONSAI_IS_DAO_COLLECTION (collection));
  g_assert (BONSAI_IS_DAO_OBJECT (object));

  if (!check_disposition (self, TRUE))
    BONSAI_EXIT;

  if (!(id = bonsai_dao_object_get_id (object)))
    {
      self->failure = g_error_new (G_IO_ERROR,
                                   G_IO_ERROR_NOT_INITIALIZED,
                                   "Object is missing an id!");
      BONSAI_EXIT;
    }

  /* We need the object snapshot so we can remove values from
   * the secondary indexes (which need to know the values to remove).
   */
  if ((variant = _bonsai_dao_object_get_original (object, &self->failure)))
    {
      if (!bonsai_dao_transaction_delete_variant (self, collection, id, variant))
        {
          /* Save for later to clear snapshot upon success */
          g_ptr_array_add (self->deleted, g_object_ref (object));
        }
    }

  BONSAI_EXIT;
}

/**
 * bonsai_dao_transaction_query:
 * @self: a #BonsaiDaoTransaction
 * @collection: a #BonsaiDaoCollection
 * @first_clause: (transfer full): a #BonsaiDaoQueryClause
 *
 * Queries a collection within the current state of the #BonsaiDaoTransaction.
 *
 * Cursors must be used by the current thread and cannot outlive the
 * life of the transaction.
 *
 * Returns: (transfer full): a #BonsaiDaoCursor
 */
BonsaiDaoCursor *
bonsai_dao_transaction_query (BonsaiDaoTransaction *self,
                              BonsaiDaoCollection  *collection,
                              BonsaiDaoQueryClause *first_clause,
                              ...)
{
  g_autoptr(BonsaiDaoQuery) query = NULL;
  BonsaiDaoQueryClause *accum = first_clause;
  BonsaiDaoQueryClause *iter;
  BonsaiDaoCursor *ret = NULL;
  va_list args;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (collection), NULL);

  va_start (args, first_clause);
  while ((iter = va_arg (args, BonsaiDaoQueryClause *)))
    accum = BONSAI_DAO_WHERE_AND (accum, iter);
  va_end (args);

  query = _bonsai_dao_query_new (self->repository, collection, self, accum);
  ret = _bonsai_dao_cursor_new (collection, query);

  BONSAI_RETURN (ret);
}

MDB_txn *
_bonsai_dao_transaction_get_native (BonsaiDaoTransaction *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), NULL);

  return self->txn;
}

/**
 * bonsai_dao_transaction_get_writeable:
 * @self: a #BonsaiDaoTransaction
 *
 * Gets the :writeable property.
 *
 * Only one writeable transaction can be performed at a time.
 *
 * Returns: %TRUE if the transaction allows writes.
 */
gboolean
bonsai_dao_transaction_get_writeable (BonsaiDaoTransaction *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), FALSE);

  return self->writeable;
}

static BonsaiDaoCollection *
bonsai_dao_transaction_get_collection (BonsaiDaoTransaction *self,
                                       const gchar          *name)
{
  BonsaiDaoCollection *ret;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), NULL);
  g_return_val_if_fail (name != NULL, NULL);

  /* Short-circuit if we already failed */
  if (self->failure != NULL)
    return NULL;

  if (!(ret = _bonsai_dao_repository_get_collection (self->repository, name)))
    {
      self->failure = g_error_new (G_IO_ERROR,
                                   G_IO_ERROR_NOT_FOUND,
                                   "No such collection \"%s\" has been loaded",
                                   name);
      return NULL;
    }

  return ret;
}

static gboolean
bonsai_dao_transaction_apply_changeset (BonsaiDaoTransaction *self,
                                        BonsaiDaoCollection  *collection,
                                        GVariant             *update,
                                        gboolean              do_revert,
                                        const gchar          *id)
{
  BonsaiDaoIndex * const *indexes;
  GType object_type;
  g_autoptr(BonsaiDaoChangeset) changeset = NULL;
  g_autoptr(BonsaiDaoChangeset) revert = NULL;
  g_autoptr(GVariant) old_variant = NULL;
  g_autoptr(GVariant) variant = NULL;
  g_auto(GVariantDict) dict = G_VARIANT_DICT_INIT (NULL);
  BonsaiDaoChangeset *apply;
  guint n_indexes;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (BONSAI_IS_DAO_COLLECTION (collection));
  g_assert (update != NULL);
  g_assert (id != NULL);

  object_type = bonsai_dao_collection_get_object_type (collection);

  if (!(changeset = bonsai_dao_changeset_from_variant (object_type, update)))
    {
      self->failure = g_error_new_literal (G_IO_ERROR,
                                           G_IO_ERROR_INVALID_DATA,
                                           "Failed to decode changelog");
      BONSAI_RETURN (FALSE);
    }

  if (!(old_variant = bonsai_dao_transaction_lookup_variant (self, collection, id)))
    {
      self->failure = g_error_new (G_IO_ERROR,
                                   G_IO_ERROR_NOT_FOUND,
                                   "Failed to locate object \"%s\" in collection \"%s\"",
                                   id,
                                   bonsai_dao_collection_get_name (collection));
      BONSAI_RETURN (FALSE);
    }

  if (do_revert)
    apply = revert = bonsai_dao_changeset_revert (changeset);
  else
    apply = changeset;

  g_variant_dict_init (&dict, old_variant);
  if (!bonsai_dao_changeset_apply (apply, &dict, &self->failure))
    BONSAI_RETURN (FALSE);

  variant = g_variant_dict_end (&dict);

  indexes = _bonsai_dao_collection_indexes (collection, &n_indexes);
  for (guint j = 0; j < n_indexes; j++)
    {
      BonsaiDaoIndex *index = indexes[j];

      /* Delete the old index entries */
      if (!_bonsai_dao_index_delete (index, self->txn, id, old_variant, &self->failure))
        BONSAI_RETURN (FALSE);

      /* Insert the new primary/secondary index entries */
      if (!_bonsai_dao_index_insert (index, self->txn, id, variant, &self->failure))
        BONSAI_RETURN (FALSE);
    }

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_dao_transaction_revert_foreach_cb (const gchar                        *collection,
                                          const BonsaiDaoTransactionLogEntry *entry,
                                          gpointer                            user_data)
{
  g_autoptr(BonsaiDaoCollection) dao_collection = NULL;
  BonsaiDaoTransaction *self = user_data;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_DAO_TRANSACTION (self));
  g_assert (collection != NULL);
  g_assert (entry != NULL);
  g_assert (entry->kind == BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT ||
            entry->kind == BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE ||
            entry->kind == BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE);

  if (self->failure != NULL)
    BONSAI_RETURN (FALSE);

  if (!(dao_collection = bonsai_dao_transaction_get_collection (self, collection)))
    BONSAI_RETURN (FALSE);

  switch (entry->kind)
    {
    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT:
      bonsai_dao_transaction_delete_variant (self, dao_collection, entry->id, entry->insert);
      break;

    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE:
      /* We shouldn't really fail to revert *local* changes, but just in case */
      if (!bonsai_dao_transaction_apply_changeset (self, dao_collection, entry->update, TRUE, entry->id))
        BONSAI_RETURN (FALSE);
      break;

    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE:
      bonsai_dao_transaction_insert_variant (self, dao_collection, entry->id, entry->delete);
      break;

    default:
      g_assert_not_reached ();
    }

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsai_dao_transaction_apply_foreach_cb (const gchar                        *collection,
                                         const BonsaiDaoTransactionLogEntry *entry,
                                         gpointer                            user_data)
{
  g_autoptr(BonsaiDaoCollection) dao_collection = NULL;
  struct {
    BonsaiDaoTransaction *self;
    gboolean dup_on_conflict;
  } *state = user_data;
  BonsaiDaoTransaction *self;

  BONSAI_ENTRY;

  g_assert (state != NULL);
  g_assert (BONSAI_IS_DAO_TRANSACTION (state->self));
  g_assert (collection != NULL);
  g_assert (entry != NULL);
  g_assert (entry->kind == BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT ||
            entry->kind == BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE ||
            entry->kind == BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE);

  self = state->self;

  if (self->failure != NULL)
    BONSAI_RETURN (FALSE);

  if (!(dao_collection = bonsai_dao_transaction_get_collection (self, collection)))
    BONSAI_RETURN (FALSE);

  switch (entry->kind)
    {
    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_INSERT:
      bonsai_dao_transaction_insert_variant (self, dao_collection, entry->id, entry->insert);
      break;

    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_UPDATE:
      /* We shouldn't be failing to apply changes we got from the peer, but
       * we might fail when re-applying local changes. If dup_on_conflict is set,
       * then we can apply the old version of the object as a duplicate.
       */
      if (!bonsai_dao_transaction_apply_changeset (self, dao_collection, entry->update, FALSE, entry->id))
        {
          if (state->dup_on_conflict)
            {
              /* TODO: Get the stashed version of the object and insert it as a dup */
            }
          BONSAI_RETURN (FALSE);
        }
      break;

    case BONSAI_DAO_TRANSACTION_LOG_ENTRY_DELETE:
      bonsai_dao_transaction_delete_variant (self, dao_collection, entry->id, entry->delete);
      break;

    default:
      g_assert_not_reached ();
    }

  BONSAI_RETURN (TRUE);
}

gboolean
_bonsai_dao_transaction_revert_log (BonsaiDaoTransaction    *self,
                                    BonsaiDaoTransactionLog *log)
{
  gboolean ret;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), FALSE);
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (log), FALSE);
  g_return_val_if_fail (log != self->log, FALSE);

#if 0
    {
      g_autofree gchar *str = _bonsai_dao_transaction_log_print (log);
      g_print ("REVERT: %s\n", str);
    }
#endif

  ret = bonsai_dao_transaction_log_foreach (log,
                                            TRUE,
                                            bonsai_dao_transaction_revert_foreach_cb,
                                            self);

  BONSAI_RETURN (ret);
}

/**
 * bonsai_dao_transaction_apply_log:
 * @self: a #BonsaiDaoTransaction
 * @log: a #BonsaiDaoTransactionLog
 * @dup_on_conflict: if objects should be duplicated (forked) if there is a
 *   conflict instead of failing the transaction.
 *
 * Applies a previous transaction log to a transaction.
 *
 * Note that the transaction is not committed as part of this operation. You
 * must call bonsai_dao_transaction_commit() to commit the changes to storage.
 *
 * Returns: %TRUE if @log was applied; %FALSE on failure.
 *
 * Since: 0.2
 */
gboolean
bonsai_dao_transaction_apply_log (BonsaiDaoTransaction    *self,
                                  BonsaiDaoTransactionLog *log,
                                  gboolean                 dup_on_conflict)
{
  struct {
    BonsaiDaoTransaction *self;
    gboolean dup_on_conflict;
  } state = {self, dup_on_conflict };
  gboolean ret;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), FALSE);
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION_LOG (log), FALSE);
  g_return_val_if_fail (log != self->log, FALSE);

#if 0
    {
      g_autofree gchar *str = _bonsai_dao_transaction_log_print (log);
      g_print ("APPLY: %s\n", str);
    }
#endif

  ret = bonsai_dao_transaction_log_foreach (log,
                                            FALSE,
                                            bonsai_dao_transaction_apply_foreach_cb,
                                            &state);

  BONSAI_RETURN (ret);
}

void
_bonsai_dao_transaction_reset_log (BonsaiDaoTransaction *self)
{
  g_return_if_fail (BONSAI_IS_DAO_TRANSACTION (self));

  g_clear_object (&self->log);
  self->log = _bonsai_dao_transaction_log_new ();
}

/**
 * bonsai_dao_transaction_get_log:
 * @self: a #BonsaiDaoTransaction
 *
 * Gets the in-progress #BonsaiDaoTransactionLog for the transaction. This is
 * mostly just useful during repository synchronization and should rarely if
 * ever be used by applications directly.
 *
 * Returns: (transfer none): a #BonsaiDaoTransactionLog
 */
BonsaiDaoTransactionLog *
bonsai_dao_transaction_get_log (BonsaiDaoTransaction *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), NULL);

  return self->log;
}

/**
 * bonsai_dao_transaction_has_error:
 *
 * Returns: %TRUE if the transaction has an error detected before it
 *   has attempted to be committed.
 */
gboolean
bonsai_dao_transaction_has_error (BonsaiDaoTransaction *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), FALSE);

  return self->failure != NULL;
}

/**
 * bonsai_dao_transaction_get_error:
 * @self: a #BonsaiDaoTransaction
 * @error: a location for a #GError
 *
 * Checks if an error has been registered during the transaction. This can
 * be used to avoid doing costly work if a transaction is already known to
 * fail before making further progress.
 *
 * Returns: %TRUE if the transaction is known to fail if commit were
 *   to be called and @error is set. Otherwise %FALSE.
 */
gboolean
bonsai_dao_transaction_get_error (BonsaiDaoTransaction  *self,
                                  GError               **error)
{
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (self), FALSE);

  if (self->failure)
    {
      g_propagate_error (error, g_error_copy (self->failure));
      return TRUE;
    }

  return FALSE;
}
