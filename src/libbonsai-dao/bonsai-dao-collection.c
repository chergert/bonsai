/* bonsai-dao-collection.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-collection"

#include "config.h"

#include "bonsai-dao-collection.h"
#include "bonsai-dao-collection-private.h"
#include "bonsai-dao-index-private.h"
#include "bonsai-dao-object.h"
#include "bonsai-dao-object-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-secondary-index-private.h"
#include "bonsai-dao-transaction.h"
#include "bonsai-dao-transaction-private.h"

struct _BonsaiDaoCollection
{
  GObject         parent_instance;
  GWeakRef        repository_ref;
  gchar          *name;
  GPtrArray      *indexes;
  GType           object_type;
  MDB_dbi         slot;
  BonsaiDaoIndex *primary;
};

G_DEFINE_TYPE (BonsaiDaoCollection, bonsai_dao_collection, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_OBJECT_TYPE,
  PROP_REPOSITORY,
  N_PROPS
};

enum {
  REQUEST_OBJECT_TYPE,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static GType
bonsai_dao_collection_real_request_object_type (BonsaiDaoCollection *self,
                                                const gchar         *id,
                                                GVariant            *variant)
{
  return self->object_type;
}

static void
bonsai_dao_collection_finalize (GObject *object)
{
  BonsaiDaoCollection *self = (BonsaiDaoCollection *)object;

  g_clear_pointer (&self->indexes, g_ptr_array_unref);
  g_clear_pointer (&self->name, g_free);
  g_weak_ref_clear (&self->repository_ref);
  g_clear_object (&self->primary);

  G_OBJECT_CLASS (bonsai_dao_collection_parent_class)->finalize (object);
}

static void
bonsai_dao_collection_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  BonsaiDaoCollection *self = BONSAI_DAO_COLLECTION (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, bonsai_dao_collection_get_name (self));
      break;

    case PROP_OBJECT_TYPE:
      g_value_set_gtype (value, bonsai_dao_collection_get_object_type (self));
      break;

    case PROP_REPOSITORY:
      g_value_take_object (value, g_weak_ref_get (&self->repository_ref));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_collection_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  BonsaiDaoCollection *self = BONSAI_DAO_COLLECTION (object);

  switch (prop_id)
    {
    case PROP_NAME:
      self->name = g_value_dup_string (value);
      break;

    case PROP_OBJECT_TYPE:
      if (g_type_is_a (g_value_get_gtype (value), BONSAI_TYPE_DAO_OBJECT) &&
          g_value_get_gtype (value) != BONSAI_TYPE_DAO_OBJECT)
        self->object_type = g_value_get_gtype (value);
      break;

    case PROP_REPOSITORY:
      g_weak_ref_set (&self->repository_ref, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_collection_class_init (BonsaiDaoCollectionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_collection_finalize;
  object_class->get_property = bonsai_dao_collection_get_property;
  object_class->set_property = bonsai_dao_collection_set_property;

  /**
   * BonsaiDaoCollection:name:
   *
   * The :name property contains the name of the collection.
   *
   * The name of the collection must be UTF-8 and less than
   * 64 characters in length.
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the collection",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoCollection:object-type:
   *
   * The :object-type property is a #GType derived from #BonsaiDaoObject
   * that is intended to be stored within the collection. It is expected
   * that all objects inserted or retrieved from this collection will
   * match at least this type.
   *
   * This type is also used to reconstruct objects when they are read back
   * from the underlying storage. If your objects involve subclasses of this
   * #GType, you can use the #BonsaiDaoCollection::request-object-type signal
   * to determine the type.
   */
  properties [PROP_OBJECT_TYPE] =
    g_param_spec_gtype ("object-type",
                        "Object Type",
                        "The GType of objects to be stored within the collection",
                        BONSAI_TYPE_DAO_OBJECT,
                        (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BonsaiDaoCollection:repository:
   *
   * The :repository property contains the #BonsaiDaoRepository
   * that created the collection.
   */
  properties [PROP_REPOSITORY] =
    g_param_spec_object ("repository",
                         "Repository",
                         "The repository the collection is owned by",
                         BONSAI_TYPE_DAO_REPOSITORY,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * BonsaiDaoObject::request-object-type:
   * @self: a #BonsaiDaoCollection
   * @id: the identifier for the object
   * @variant: the serialized document from storage
   *
   * The ::request-object-type signal is used to sniff the #GType that should
   * be used to inflate the object when reading back from storage.
   *
   * Returns: a #GType derrived from #BonsaiDaoObject
   */
  signals [REQUEST_OBJECT_TYPE] =
    g_signal_new_class_handler ("request-object-type",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_CALLBACK (bonsai_dao_collection_real_request_object_type),
                                g_signal_accumulator_first_wins,
                                NULL,
                                NULL,
                                G_TYPE_GTYPE,
                                2,
                                G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                                G_TYPE_VARIANT);
}

static void
bonsai_dao_collection_init (BonsaiDaoCollection *self)
{
  self->indexes = g_ptr_array_new_with_free_func (g_object_unref);
}

BonsaiDaoCollection *
_bonsai_dao_collection_new (BonsaiDaoRepository *repository,
                            const gchar         *name,
                            GType                object_type,
                            MDB_dbi              slot)
{
  BonsaiDaoCollection *ret;

  g_return_val_if_fail (BONSAI_IS_DAO_REPOSITORY (repository), NULL);
  g_return_val_if_fail (name != NULL, NULL);
  g_return_val_if_fail (g_type_is_a (object_type, BONSAI_TYPE_DAO_OBJECT), NULL);
  g_return_val_if_fail (strlen (name) <= 64, NULL);

  ret = g_object_new (BONSAI_TYPE_DAO_COLLECTION,
                      "name", name,
                      "object-type", object_type,
                      "repository", repository,
                      NULL);

  if (ret != NULL)
    {
      ret->slot = slot;
      ret->primary = _bonsai_dao_index_new_for_id (repository, slot);
      g_ptr_array_add (ret->indexes, g_object_ref (ret->primary));
    }

  return g_steal_pointer (&ret);
}

/**
 * bonsai_dao_collection_get_name:
 * @self: a #BonsaiDaoCollection
 *
 * Gets the :name property.
 *
 * Returns: the name of the collection
 */
const gchar *
bonsai_dao_collection_get_name (BonsaiDaoCollection *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), NULL);

  return self->name;
}

/**
 * bonsai_dao_collection_ref_repository:
 * @self: a #BonsaiDaoCollection
 *
 * Gets a new reference to the repository that owns the collection.
 *
 * Returns: (nullable) (transfer full): a #BonsaiDaoRepository or %NULL
 */
BonsaiDaoRepository *
bonsai_dao_collection_ref_repository (BonsaiDaoCollection *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), NULL);

  return g_weak_ref_get (&self->repository_ref);
}

MDB_dbi
_bonsai_dao_collection_get_slot (BonsaiDaoCollection *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), 0);

  return self->slot;
}

BonsaiDaoObject *
_bonsai_dao_collection_inflate (BonsaiDaoCollection *self,
                                const gchar         *id,
                                GVariant            *variant)
{
  GType object_type = G_TYPE_INVALID;
  BonsaiDaoObject *ret;

  g_assert (BONSAI_IS_DAO_COLLECTION (self));
  g_assert (id != NULL);
  g_assert (variant != NULL);

  g_signal_emit (self, signals [REQUEST_OBJECT_TYPE], 0, id, variant, &object_type);

  if (object_type == G_TYPE_INVALID ||
      !g_type_is_a (object_type, BONSAI_TYPE_DAO_OBJECT) ||
      object_type == BONSAI_TYPE_DAO_OBJECT)
    return NULL;

  if ((ret = bonsai_dao_object_new_from_variant (object_type, id, variant)))
    _bonsai_dao_object_stash (ret);

  return g_steal_pointer (&ret);
}

/**
 * bonsai_dao_collection_get_object_type:
 * @self: a #BonsaiDaoCollection
 *
 * Gets the :object-type property.
 *
 * This property denotes the #GType of a #BonsaiDaoObject based subclass
 * that is to be stored or retrieved from the collection.
 *
 * Returns: a #GType of a #BonsaiDaoObject derived class.
 */
GType
bonsai_dao_collection_get_object_type (BonsaiDaoCollection *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), G_TYPE_INVALID);

  return self->object_type;
}

/*
 * _bonsai_dao_collection_get_primary:
 *
 * Gets the primary index for the collection. This is the key for
 * the table that stores the data. That is currently in all cases
 * the "id" field with a trailing \0 byte.
 */
BonsaiDaoIndex *
_bonsai_dao_collection_get_primary (BonsaiDaoCollection *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), NULL);

  return self->primary;
}

static gboolean
bonsai_dao_collection_ensure_indexv (BonsaiDaoCollection  *self,
                                     BonsaiDaoTransaction *transaction,
                                     GCancellable         *cancellable,
                                     GError              **error,
                                     const gchar * const  *prop_names)
{
  g_autoptr(BonsaiDaoSecondaryIndex) index = NULL;
  g_autoptr(GTypeClass) oclass = NULL;
  g_autoptr(GPtrArray) pspecs = NULL;
  MDB_txn *txn;

  g_assert (BONSAI_IS_DAO_COLLECTION (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (prop_names != NULL);

  oclass = g_type_class_ref (self->object_type);
  pspecs = g_ptr_array_new ();

  for (guint i = 0; prop_names[i]; i++)
    {
      GParamSpec *pspec;

      if (!(pspec = g_object_class_find_property (G_OBJECT_CLASS (oclass), prop_names[i])))
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_NOT_FOUND,
                       "No property \"%s\" was found for type %s",
                       prop_names[i], g_type_name (self->object_type));
          return FALSE;
        }

      if (!_bonsai_dao_secondary_index_is_supported_type (pspec->value_type))
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_NOT_SUPPORTED,
                       "Indexing properties of type %s is not supported",
                       g_type_name (pspec->value_type));
          return FALSE;
        }

      g_ptr_array_add (pspecs, pspec);
    }

  index = _bonsai_dao_secondary_index_new (self->name, (GParamSpec **)(gpointer)pspecs->pdata, pspecs->len);

  if (index == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Failed to initialize secondary index");
      return FALSE;
    }

  txn = _bonsai_dao_transaction_get_native (transaction);

  if (!_bonsai_dao_secondary_index_open (index, txn, NULL))
    {
      if (!_bonsai_dao_secondary_index_create (index, txn, self->slot, error))
        return FALSE;
    }

  g_ptr_array_add (self->indexes, g_steal_pointer (&index));

  return TRUE;
}

gboolean
bonsai_dao_collection_ensure_index (BonsaiDaoCollection  *self,
                                    BonsaiDaoTransaction *transaction,
                                    GCancellable         *cancellable,
                                    GError              **error,
                                    const gchar          *first_property,
                                    ...)
{
  g_autoptr(GPtrArray) ar = NULL;
  va_list args;

  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), FALSE);
  g_return_val_if_fail (BONSAI_IS_DAO_TRANSACTION (transaction), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);
  g_return_val_if_fail (first_property != NULL, FALSE);

  ar = g_ptr_array_new ();
  va_start (args, first_property);
  do
    g_ptr_array_add (ar, (gchar *)first_property);
  while ((first_property = va_arg (args, const gchar *)));
  va_end (args);
  g_ptr_array_add (ar, NULL);

  return bonsai_dao_collection_ensure_indexv (self,
                                              transaction,
                                              cancellable,
                                              error,
                                              (const gchar * const *)ar->pdata);
}

BonsaiDaoIndex * const *
_bonsai_dao_collection_indexes (BonsaiDaoCollection *self,
                                guint               *n_indexes)
{
  g_return_val_if_fail (BONSAI_IS_DAO_COLLECTION (self), NULL);
  g_return_val_if_fail (n_indexes != NULL, NULL);

  *n_indexes = self->indexes->len;
  return (BonsaiDaoIndex * const *)(gconstpointer)self->indexes->pdata;
}
