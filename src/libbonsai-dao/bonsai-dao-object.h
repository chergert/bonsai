/* bonsai-dao-object.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

typedef enum
{
  BONSAI_DAO_OBJECT_PROPERTY_NONE        = 0,
  BONSAI_DAO_OBJECT_PROPERTY_OVERWRITE   = 1,
  BONSAI_DAO_OBJECT_PROPERTY_STRING_DIFF = 2,
  BONSAI_DAO_OBJECT_PROPERTY_INCREMENT   = 3,
  BONSAI_DAO_OBJECT_PROPERTY_PUSH_POP    = 4,
} BonsaiDaoObjectPropertyFlags;

typedef struct _BonsaiDaoObjectAutoProperty BonsaiDaoObjectAutoProperty;

struct _BonsaiDaoObjectAutoProperty
{
  BonsaiDaoObjectAutoProperty  *next;
  GParamSpec                   *pspec;
  guint                         prop_id;
  BonsaiDaoObjectPropertyFlags  flags;
};

#define BONSAI_TYPE_DAO_OBJECT (bonsai_dao_object_get_type())

G_DECLARE_DERIVABLE_TYPE (BonsaiDaoObject, bonsai_dao_object, BONSAI, DAO_OBJECT, GObject)

struct _BonsaiDaoObjectClass
{
  GObjectClass parent_class;

  BonsaiDaoObjectAutoProperty *auto_properties;

  BonsaiDaoObject *(*inflate)          (BonsaiDaoObjectClass  *klass,
                                        const gchar           *id,
                                        GVariant              *variant);
  gboolean         (*inflate_property) (BonsaiDaoObjectClass  *klass,
                                        const gchar           *key,
                                        GVariant              *variant,
                                        GParamSpec           **pspec,
                                        GValue                *value);
  void             (*snapshot)         (BonsaiDaoObject       *self,
                                        BonsaiDaoSnapshot     *snapshot);

  /*< private >*/
  gpointer _reserved[32];
};

void              bonsai_dao_object_class_install_auto_property (BonsaiDaoObjectClass         *klass,
                                                                 guint                         prop_id,
                                                                 GParamSpec                   *pspec,
                                                                 BonsaiDaoObjectPropertyFlags  flags);
gpointer          bonsai_dao_object_new_from_snapshot           (GType                         object_type,
                                                                 const gchar                  *id,
                                                                 BonsaiDaoSnapshot            *snapshot);
gpointer          bonsai_dao_object_new_from_variant            (GType                         object_type,
                                                                 const gchar                  *id,
                                                                 GVariant                     *variant);
const gchar      *bonsai_dao_object_get_id                      (BonsaiDaoObject              *self);
void              bonsai_dao_object_snapshot                    (BonsaiDaoObject              *self,
                                                                 BonsaiDaoSnapshot            *snapshot);
BonsaiDaoVersion  bonsai_dao_object_get_upstream_version        (BonsaiDaoObject              *self);

G_END_DECLS
