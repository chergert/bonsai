/* bonsai-dao-repository.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "bonsai-dao-types.h"

G_BEGIN_DECLS

#define BONSAI_TYPE_DAO_REPOSITORY  (bonsai_dao_repository_get_type())
#define BONSAI_DAO_REPOSITORY_ERROR (bonsai_dao_repository_error_quark())

G_DECLARE_DERIVABLE_TYPE (BonsaiDaoRepository, bonsai_dao_repository, BONSAI, DAO_REPOSITORY, GObject)

struct _BonsaiDaoRepositoryClass
{
  GObjectClass parent_class;

  /*< private >*/
  gpointer _reserved[32];
};

GQuark                bonsai_dao_repository_error_quark       (void);
BonsaiDaoRepository  *bonsai_dao_repository_new_for_path      (const gchar           *identifier,
                                                               const gchar           *path,
                                                               GCancellable          *cancellable,
                                                               GError               **error);
const gchar          *bonsai_dao_repository_get_identifier    (BonsaiDaoRepository   *self);
const gchar          *bonsai_dao_repository_get_path          (BonsaiDaoRepository   *self);
BonsaiDaoCollection  *bonsai_dao_repository_load_collection   (BonsaiDaoRepository   *self,
                                                               const gchar           *name,
                                                               GType                  object_type,
                                                               GCancellable          *cancellable,
                                                               GError               **error);
BonsaiDaoTransaction *bonsai_dao_repository_begin_readwrite   (BonsaiDaoRepository   *self,
                                                               GError               **error);
BonsaiDaoTransaction *bonsai_dao_repository_begin_read        (BonsaiDaoRepository   *self,
                                                               GError               **error);
gboolean              bonsai_dao_repository_synchronize       (BonsaiDaoRepository   *self,
                                                               GDBusConnection       *connection,
                                                               const gchar           *bus_name,
                                                               gchar                **sync_revision,
                                                               GCancellable          *cancellable,
                                                               GError               **error);
gchar                *bonsai_dao_repository_get_last_revision (BonsaiDaoRepository   *self,
                                                               BonsaiDaoTransaction  *txn);

G_END_DECLS
