/* bonsai-dao-snapshot.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-snapshot"

#include "config.h"

#include <libbonsai.h>
#include <lmdb.h>
#include <string.h>

#include "bonsai-dao-changeset.h"
#include "bonsai-dao-object.h"
#include "bonsai-dao-object-private.h"
#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-snapshot.h"
#include "bonsai-dao-snapshot-private.h"
#include "bonsai-dao-utils.h"

typedef struct
{
  GParamSpec *pspec;
  GValue      value;
} ObjectProperty;

typedef struct
{
  gchar  *id;
  GArray *properties;
} ObjectSnapshot;

struct _BonsaiDaoSnapshot
{
  GObject     parent_instance;
  GHashTable *seen;
  GQueue      stack;
  guint       sealed : 1;
};

G_DEFINE_TYPE (BonsaiDaoSnapshot, bonsai_dao_snapshot, G_TYPE_OBJECT)

static void
clear_property (ObjectProperty *property)
{
  g_clear_pointer (&property->pspec, g_param_spec_unref);
  g_value_unset (&property->value);
}

static void
clear_snapshot (ObjectSnapshot *snapshot)
{
  g_clear_pointer (&snapshot->id, g_free);
  g_clear_pointer (&snapshot->properties, g_array_unref);
}

static void
object_snapshot_destroy (ObjectSnapshot *snapshot)
{
  g_atomic_rc_box_release_full (snapshot, (GDestroyNotify)clear_snapshot);
}

/**
 * bonsai_dao_snapshot_new:
 *
 * Create a new #BonsaiDaoSnapshot.
 *
 * Returns: (transfer full): a newly created #BonsaiDaoSnapshot
 */
BonsaiDaoSnapshot *
bonsai_dao_snapshot_new (void)
{
  return g_object_new (BONSAI_TYPE_DAO_SNAPSHOT, NULL);
}

static void
bonsai_dao_snapshot_finalize (GObject *object)
{
  BonsaiDaoSnapshot *self = (BonsaiDaoSnapshot *)object;

  BONSAI_ENTRY;

  g_clear_pointer (&self->seen, g_hash_table_unref);
  g_queue_clear_full (&self->stack, (GDestroyNotify)object_snapshot_destroy);

  G_OBJECT_CLASS (bonsai_dao_snapshot_parent_class)->finalize (object);

  BONSAI_EXIT;
}

static void
bonsai_dao_snapshot_class_init (BonsaiDaoSnapshotClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_snapshot_finalize;
}

static void
bonsai_dao_snapshot_init (BonsaiDaoSnapshot *self)
{
  BONSAI_ENTRY;

  self->seen = g_hash_table_new_full (g_str_hash,
                                      g_str_equal,
                                      NULL,
                                      (GDestroyNotify)object_snapshot_destroy);

  BONSAI_EXIT;
}

/**
 * bonsai_dao_snapshot_push_object:
 * @self: a #BonsaiDaoSnapshot
 * @id: the identifier for the object
 *
 * Pushes a new object layer onto the snapshot's internal stack. This allows
 * snapshoting multiple objects together as a group which might be necessary
 * when including relative objects in a transaction.
 *
 * If %FALSE is returned, then @snapshot has already seen this object and the
 * caller should ignore snapshoting it further. This can be used to avoid
 * circular changes in an object graph.
 *
 * You should add the object's state to the snapshot after calling this function
 * if %TRUE is returned. Afterwards, call bonsai_dao_snapshot_pop().
 *
 * Returns: %TRUE if @id was placed on the internal stack and caller should
 *   add object state. Otherwise %FALSE and caller should not call
 *   bonsai_dao_snapshot_pop().
 */
gboolean
bonsai_dao_snapshot_push_object (BonsaiDaoSnapshot *self,
                                 const gchar       *id)
{
  ObjectSnapshot *snap;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_SNAPSHOT (self), FALSE);
  g_return_val_if_fail (id != NULL, FALSE);
  g_return_val_if_fail (self->sealed == FALSE, FALSE);

  /* If we've seen this object, reject this and notify the caller that
   * they can cull this portion of the graph (as we've already seen it
   * or are currently within it lower in our stack.
   */
  if (g_hash_table_contains (self->seen, id))
    BONSAI_RETURN (FALSE);

  snap = g_atomic_rc_box_new0 (ObjectSnapshot);
  snap->id = g_strdup (id);
  snap->properties = g_array_new (FALSE, TRUE, sizeof (ObjectProperty));
  g_array_set_clear_func (snap->properties, (GDestroyNotify)clear_property);

  g_queue_push_tail (&self->stack, snap);
  g_hash_table_insert (self->seen, snap->id, snap);

  BONSAI_RETURN (TRUE);
}

static gint
sort_props (gconstpointer a,
            gconstpointer b)
{
  const ObjectProperty *a_prop = a;
  const ObjectProperty *b_prop = b;

  return strcmp (a_prop->pspec->name, b_prop->pspec->name);
}

/**
 * bonsai_dao_snapshot_pop:
 * @self: a #BonsaiDaoSnapshot
 *
 * Pops the currently snapshoting object from the stack. This should be called
 * after a successful call to bonsai_dao_snapshot_push_object() (which returns
 * %TRUE) once the object's state has been added to the snapshot.
 */
void
bonsai_dao_snapshot_pop (BonsaiDaoSnapshot *self)
{
  ObjectSnapshot *snap;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_SNAPSHOT (self));
  g_return_if_fail (self->sealed == FALSE);

  if (!(snap = g_queue_pop_tail (&self->stack)))
    g_critical ("Mismatched bonsai_dao_snapshot_pop()");
  else
    g_array_sort (snap->properties, sort_props);

  BONSAI_EXIT;
}

/**
 * bonsai_dao_snapshot_append_value:
 * @self: a #BonsaiDaoSnapshot
 * @pspec: a #GParamSpec
 * @value: a #GValue containing the property value
 *
 * Adds a new value to the snapshot for the current object.
 */
void
bonsai_dao_snapshot_append_value (BonsaiDaoSnapshot *self,
                                  GParamSpec        *pspec,
                                  const GValue      *value)
{
  ObjectSnapshot *os;
  ObjectProperty op = {0};

  g_return_if_fail (BONSAI_IS_DAO_SNAPSHOT (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (G_IS_VALUE (value));
  g_return_if_fail (self->stack.tail != NULL);
  g_return_if_fail (self->stack.tail->data != NULL);

  op.pspec = g_param_spec_ref (pspec);
  g_value_init (&op.value, G_VALUE_TYPE (value));
  g_value_copy (value, &op.value);

  os = g_queue_peek_tail (&self->stack);
  g_array_append_val (os->properties, op);
}

void
_bonsai_dao_snapshot_append_take_value (BonsaiDaoSnapshot *self,
                                        GParamSpec        *pspec,
                                        GValue            *value)
{
  ObjectSnapshot *os;
  ObjectProperty op = {0};

  g_return_if_fail (BONSAI_IS_DAO_SNAPSHOT (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (G_IS_VALUE (value));
  g_return_if_fail (self->stack.tail != NULL);
  g_return_if_fail (self->stack.tail->data != NULL);

  os = g_queue_peek_tail (&self->stack);
  op.pspec = g_param_spec_ref (pspec);
  op.value = *value;

  g_array_append_val (os->properties, op);
}

static void
_bonsai_dao_snapshot_seal (BonsaiDaoSnapshot *self)
{
  g_assert (BONSAI_IS_DAO_SNAPSHOT (self));

  self->sealed = TRUE;
}

static void
object_property_string_compare (const gchar        *before,
                                const gchar        *after,
                                GParamSpec         *pspec,
                                BonsaiDaoChangeset *changeset)
{
  g_assert (BONSAI_IS_DAO_CHANGESET (changeset));

  if (g_strcmp0 (before, after) != 0)
    bonsai_dao_changeset_append_string_diff (changeset, pspec, before, after);
}

static void
object_property_compare (ObjectProperty     *before,
                         ObjectProperty     *after,
                         GParamSpec         *pspec,
                         BonsaiDaoChangeset *changeset)
{
  const GValue *av;
  const GValue *bv;

  g_assert (before != NULL);
  g_assert (after != NULL);
  g_assert (BONSAI_IS_DAO_CHANGESET (changeset));
  g_assert (G_VALUE_HOLDS (&before->value, G_VALUE_TYPE (&after->value)));

  av = &after->value;
  bv = &before->value;

  /* TODO: We need to look at the recorded property kind so that we
   *       can determine what type of differences to do.
   */

  if (G_VALUE_HOLDS_STRING (bv))
    {
      object_property_string_compare (g_value_get_string (bv),
                                      g_value_get_string (av),
                                      pspec,
                                      changeset);
    }
  else if (G_VALUE_HOLDS_INT (bv))
    {
      if (g_value_get_int (bv) != g_value_get_int (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_UINT (bv))
    {
      if (g_value_get_uint (bv) != g_value_get_uint (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_INT64 (bv))
    {
      if (g_value_get_int64 (bv) != g_value_get_int64 (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_UINT64 (bv))
    {
      if (g_value_get_uint64 (bv) != g_value_get_uint64 (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_LONG (bv))
    {
      if (g_value_get_long (bv) != g_value_get_long (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_ULONG (bv))
    {
      if (g_value_get_ulong (bv) != g_value_get_ulong (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_BOOLEAN (bv))
    {
      if (g_value_get_boolean (bv) != g_value_get_boolean (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_DOUBLE (bv))
    {
      if (g_value_get_double (bv) != g_value_get_double (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS_FLOAT (bv))
    {
      if (g_value_get_float (bv) != g_value_get_float (av))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else if (G_VALUE_HOLDS (bv, G_TYPE_DATE_TIME))
    {
      GDateTime *dt1 = g_value_get_boxed (bv);
      GDateTime *dt2 = g_value_get_boxed (av);

      if (dt1 != dt2)
        {
          if (dt1 == NULL ||
              dt2 == NULL ||
              g_date_time_to_unix (dt1) != g_date_time_to_unix (dt2))
            bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
        }
    }
  else if (G_VALUE_HOLDS (bv, G_TYPE_STRV))
    {
      const gchar * const *before_strv = g_value_get_boxed (bv);
      const gchar * const *after_strv = g_value_get_boxed (av);

      if (before_strv != after_strv && !g_strv_equal (before_strv, after_strv))
        bonsai_dao_changeset_append_overwrite_value (changeset, pspec->name, bv, av);
    }
  else
    {
      g_warning ("Unknown comparison for type %s", G_VALUE_TYPE_NAME (bv));
    }
}

static BonsaiDaoChangeset *
object_snapshot_diff (ObjectSnapshot *before,
                      ObjectSnapshot *after)
{
  g_autoptr(BonsaiDaoChangeset) changeset = NULL;
  guint a = 0;
  guint b = 0;

  g_assert (before != NULL);
  g_assert (after != NULL);
  g_assert (g_strcmp0 (before->id, after->id) == 0);

  changeset = bonsai_dao_changeset_new (before->id);

  while (b < before->properties->len && a < after->properties->len)
    {
      ObjectProperty *ap = &g_array_index (after->properties, ObjectProperty, a);
      ObjectProperty *bp = &g_array_index (before->properties, ObjectProperty, b);
      gint r = strcmp (bp->pspec->name, ap->pspec->name);

      if (r == 0)
        {
          object_property_compare (bp, ap, ap->pspec, changeset);
          b++, a++;
        }
      else if (r < 0)
        {
          bonsai_dao_changeset_append_unset (changeset, bp->pspec->name, &bp->value);
          b++;
        }
      else
        {
          bonsai_dao_changeset_append_overwrite_value (changeset, ap->pspec->name, &bp->value, &ap->value);
          a++;
        }
    }

  while (b < before->properties->len)
    {
      ObjectProperty *bp = &g_array_index (before->properties, ObjectProperty, b);
      bonsai_dao_changeset_append_unset (changeset, bp->pspec->name, &bp->value);
      b++;
    }

  while (a < after->properties->len)
    {
      ObjectProperty *ap = &g_array_index (after->properties, ObjectProperty, a);
      bonsai_dao_changeset_append_overwrite_value (changeset, ap->pspec->name, NULL, &ap->value);
      a++;
    }

  if (bonsai_dao_changeset_is_empty (changeset))
    return NULL;

  return g_steal_pointer (&changeset);
}

static BonsaiDaoChangeset *
object_snapshot_to_changeset (ObjectSnapshot *snapshot)
{
  BonsaiDaoChangeset *changeset;

  g_assert (snapshot != NULL);
  g_assert (snapshot->id != NULL);
  g_assert (snapshot->properties != NULL);

  changeset = bonsai_dao_changeset_new (snapshot->id);

  for (guint i = 0; i < snapshot->properties->len; i++)
    {
      ObjectProperty *op = &g_array_index (snapshot->properties, ObjectProperty, i);
      bonsai_dao_changeset_append_overwrite_value (changeset, op->pspec->name, NULL, &op->value);
    }

  return g_steal_pointer (&changeset);
}

/**
 * bonsai_dao_snapshot_diff:
 * @before: a #BonsaiDaoSnapshot
 * @after: a #BonsaiDaoSnapshot
 *
 * Differences two snapshots of a series of objects to create a changeset
 * that can be applied to underlying storage.
 *
 * Returns: (transfer full) (element-type BonsaiDaoChangeset): a #GPtrArray
 *   containing #BonsaiDaoChangeset for the objects contained in @before
 *   and @after.
 */
GPtrArray *
bonsai_dao_snapshot_diff (BonsaiDaoSnapshot *before,
                          BonsaiDaoSnapshot *after)
{
  g_autoptr(GPtrArray) ar = NULL;
  ObjectSnapshot *after_obj;
  ObjectSnapshot *before_obj;
  GHashTableIter iter;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_SNAPSHOT (before), NULL);
  g_return_val_if_fail (BONSAI_IS_DAO_SNAPSHOT (after), NULL);
  g_return_val_if_fail (before->stack.length == 0, NULL);
  g_return_val_if_fail (after->stack.length == 0, NULL);

  _bonsai_dao_snapshot_seal (before);
  _bonsai_dao_snapshot_seal (after);

  ar = g_ptr_array_new ();

  g_hash_table_iter_init (&iter, after->seen);
  while (g_hash_table_iter_next (&iter, NULL, (gpointer *)&after_obj))
    {
      BonsaiDaoChangeset *changeset;

      if ((before_obj = g_hash_table_lookup (before->seen, after_obj->id)))
        {
          if ((changeset = object_snapshot_diff (before_obj, after_obj)))
            g_ptr_array_add (ar, g_steal_pointer (&changeset));
        }
      else
        {
          changeset = object_snapshot_to_changeset (after_obj);
          g_ptr_array_add (ar, g_steal_pointer (&changeset));
        }
    }

  BONSAI_RETURN (g_steal_pointer (&ar));
}

static void
clear_value (gpointer data)
{
  GValue *value = data;
  g_value_unset (value);
}

BonsaiDaoObject *
_bonsai_dao_snapshot_create_object (BonsaiDaoSnapshot *self,
                                    const gchar       *id,
                                    GType              object_type)
{
  g_autoptr(GPtrArray) names = NULL;
  g_autoptr(GArray) values = NULL;
  ObjectSnapshot *os;
  GObject *ret;
  GValue value = G_VALUE_INIT;

  g_return_val_if_fail (g_type_is_a (object_type, BONSAI_TYPE_DAO_OBJECT), NULL);
  g_return_val_if_fail (object_type != BONSAI_TYPE_DAO_OBJECT, NULL);

  if (!(os = g_hash_table_lookup (self->seen, id)))
    g_return_val_if_reached (NULL);

  names = g_ptr_array_new ();
  values = g_array_new (FALSE, FALSE, sizeof (GValue));
  g_array_set_clear_func (values, clear_value);

  g_ptr_array_add (names, (gchar *)"id");
  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, id);
  g_array_append_val (values, value);

  for (guint i = 0; i < os->properties->len; i++)
    {
      ObjectProperty *prop = &g_array_index (os->properties, ObjectProperty, i);
      GValue copy = G_VALUE_INIT;

      g_value_init (&copy, G_VALUE_TYPE (&prop->value));
      g_value_copy (&prop->value, &copy);
      g_ptr_array_add (names, (gchar *)prop->pspec->name);
      g_array_append_val (values, copy);
    }

  ret = g_object_new_with_properties (object_type,
                                      names->len,
                                      (const gchar **)names->pdata,
                                      &g_array_index (values, GValue, 0));

  if (ret != NULL)
    _bonsai_dao_object_set_snapshot (BONSAI_DAO_OBJECT (ret), self);

  return BONSAI_DAO_OBJECT (ret);
}

GVariant *
_bonsai_dao_snapshot_get_object_as_variant (BonsaiDaoSnapshot  *self,
                                            const gchar        *id,
                                            GError            **error)
{
  ObjectSnapshot *os;
  GVariantBuilder builder;

  g_return_val_if_fail (BONSAI_IS_DAO_SNAPSHOT (self), FALSE);
  g_return_val_if_fail (id != NULL, FALSE);

  if (!(os = g_hash_table_lookup (self->seen, id)))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_FOUND,
                   "No such object-id '%s' found in snapshot",
                   id);
      return FALSE;
    }

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));

  for (guint i = 0; i < os->properties->len; i++)
    {
      const ObjectProperty *op = &g_array_index (os->properties, ObjectProperty, i);

      g_variant_builder_open (&builder, G_VARIANT_TYPE ("{sv}"));
      g_variant_builder_add (&builder, "s", op->pspec->name);
      bonsai_dao_variant_builder_append_value (&builder, &op->value);
      g_variant_builder_close (&builder);
    }

  return g_variant_take_ref (g_variant_builder_end (&builder));
}

BonsaiDaoSnapshot *
_bonsai_dao_snapshot_dup_for_id (BonsaiDaoSnapshot *self,
                                 const gchar       *id)
{
  BonsaiDaoSnapshot *copy;
  ObjectSnapshot *os;

  g_return_val_if_fail (BONSAI_IS_DAO_SNAPSHOT (self), NULL);

  copy = bonsai_dao_snapshot_new ();

  if ((os = g_hash_table_lookup (self->seen, id)))
    {
      bonsai_dao_snapshot_push_object (copy, id);

      for (guint i = 0; i < os->properties->len; i++)
        {
          const ObjectProperty *op = &g_array_index (os->properties, ObjectProperty, i);
          bonsai_dao_snapshot_append_value (copy, op->pspec, &op->value);
        }

      bonsai_dao_snapshot_pop (copy);
    }

  return g_steal_pointer (&copy);
}
