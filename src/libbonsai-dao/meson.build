libbonsai_dao_header_dir = join_paths(include_dir, 'libbonsai-dao')
libbonsai_dao_header_subdir = join_paths('libbonsai-@0@'.format(libbonsai_api_version), 'libbonsai-dao')

libbonsai_dao_generated_sources = []

libbonsai_dao_public_headers = [
  'bonsai-dao-changeset.h',
  'bonsai-dao-collection.h',
  'bonsai-dao-cursor.h',
  'bonsai-dao-error.h',
  'bonsai-dao-object.h',
  'bonsai-dao-repository.h',
  'bonsai-dao-snapshot.h',
  'bonsai-dao-transaction.h',
  'bonsai-dao-transaction-log.h',
  'bonsai-dao-types.h',
  'bonsai-dao-utils.h',
  'libbonsai-dao.h',
]

libbonsai_dao_public_sources = [
  'bonsai-dao-changeset.c',
  'bonsai-dao-collection.c',
  'bonsai-dao-cursor.c',
  'bonsai-dao-error.c',
  'bonsai-dao-object.c',
  'bonsai-dao-repository.c',
  'bonsai-dao-snapshot.c',
  'bonsai-dao-transaction.c',
  'bonsai-dao-transaction-log.c',
  'bonsai-dao-utils.c',
  'bonsai-dao-where.c',
]

libbonsai_dao_private_sources = [
  'bonsai-dao-index.c',
  'bonsai-dao-query.c',
  'bonsai-dao-repository-sync.c',
  'bonsai-dao-secondary-index.c',
]

libbonsai_dao_deps = [
  libbonsai_dep,
  lmdb_dep,
]

ipc_dao_sync = gnome.gdbus_codegen('ipc-dao-sync',
           sources: 'org.gnome.Bonsai.Dao.Sync.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Ipc',
    install_header: false,
       autocleanup: 'all',
)

ipc_dao_log = gnome.gdbus_codegen('ipc-dao-log',
           sources: 'org.gnome.Bonsai.Dao.Log.xml',
  interface_prefix: 'org.gnome.Bonsai.',
         namespace: 'Ipc',
    install_header: false,
       autocleanup: 'all',
)

libbonsai_dao_sources = libbonsai_dao_public_sources \
                      + libbonsai_dao_private_sources \
                      + libbonsai_dao_generated_sources \
                      + ipc_dao_sync \
                      + ipc_dao_log

libbonsai_dao = library('bonsai-dao-@0@'.format(libbonsai_api_version), libbonsai_dao_sources,
           dependencies: libbonsai_dao_deps,
            install_dir: get_option('libdir'),
                install: true,
)

libbonsai_dao_dep = declare_dependency(
              sources: libbonsai_generated_sources,
            link_with: [libbonsai_dao],
         dependencies: libbonsai_dao_deps,
  include_directories: [include_directories('.')],
)

install_headers(libbonsai_dao_public_headers, install_dir: libbonsai_dao_header_dir)

pkgg.generate(libbonsai_dao,
      subdirs: ['libbonsai-dao'],
      version: meson.project_version(),
         name: 'Bonsai',
     filebase: 'libbonsai-dao-@0@'.format(libbonsai_api_version),
  description: 'Libbonsai contains a client and server library for working with Bonsai devices.',
     requires: [ 'gio-2.0' ],
)

libbonsai_dao_gir = gnome.generate_gir(libbonsai_dao,
              sources: libbonsai_dao_generated_sources + \
                       libbonsai_dao_public_headers + \
                       libbonsai_dao_public_sources,
            nsversion: libbonsai_api_version,
            namespace: 'BonsaiDao',
        symbol_prefix: 'bonsai',
    identifier_prefix: 'Bonsai',
             includes: [ 'Gio-2.0' ],
              install: true,
           extra_args: [ '--c-include=libbonsai-dao.h',
                         '--c-include=config.h',
                         '--pkg-export=libbonsai-dao-@0@'.format(libbonsai_api_version) ]
)

subdir('tests')
