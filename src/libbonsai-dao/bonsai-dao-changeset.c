/* bonsai-dao-changeset.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-changeset"

#include "config.h"

#include <gobject/gvaluecollector.h>
#include <libbonsai.h>

#include "bonsai-dao-changeset.h"
#include "bonsai-dao-object.h"
#include "bonsai-dao-repository-private.h"
#include "bonsai-dao-utils.h"

typedef enum
{
  CHANGE_KIND_OVERWRITE = 1,
  CHANGE_KIND_INCREMENT,
  CHANGE_KIND_POP,
  CHANGE_KIND_PUSH,
} ChangeKind;

typedef struct
{
  ChangeKind   kind;
  const gchar *pname;
  union {
    struct {
      GValue from;
      GValue to;
    } overwrite;
    gint64    increment;
    GValue    pop;
    GValue    push;
  };
} Change;

struct _BonsaiDaoChangeset
{
  GObject  parent_instance;
  gchar   *object_id;
  GArray  *changes;
  guint    sealed : 1;
};

G_DEFINE_TYPE (BonsaiDaoChangeset, bonsai_dao_changeset, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_OBJECT_ID,
  PROP_SEALED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
clear_change (Change *change)
{
  switch (change->kind)
    {
    case CHANGE_KIND_OVERWRITE:
      g_value_unset (&change->overwrite.from);
      g_value_unset (&change->overwrite.to);
      break;

    case CHANGE_KIND_INCREMENT:
      break;

    case CHANGE_KIND_POP:
      g_value_unset (&change->pop);
      break;

    case CHANGE_KIND_PUSH:
      g_value_unset (&change->pop);
      break;

    default:
      g_assert_not_reached ();
    }
}

/**
 * bonsai_dao_changeset_new:
 * @object_id: object id of the object being diffed
 *
 * Create a new #BonsaiDaoChangeset.
 *
 * Returns: (transfer full): a newly created #BonsaiDaoChangeset
 */
BonsaiDaoChangeset *
bonsai_dao_changeset_new (const gchar *object_id)
{
  BonsaiDaoChangeset *self;

  BONSAI_ENTRY;

  g_return_val_if_fail (object_id != NULL, NULL);

  self = g_object_new (BONSAI_TYPE_DAO_CHANGESET,
                       "object-id", object_id,
                       NULL);

  BONSAI_RETURN (self);
}

static void
bonsai_dao_changeset_finalize (GObject *object)
{
  BonsaiDaoChangeset *self = (BonsaiDaoChangeset *)object;

  BONSAI_ENTRY;

  g_clear_pointer (&self->changes, g_array_unref);
  g_clear_pointer (&self->object_id, g_free);

  G_OBJECT_CLASS (bonsai_dao_changeset_parent_class)->finalize (object);

  BONSAI_EXIT;
}

static void
bonsai_dao_changeset_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  BonsaiDaoChangeset *self = BONSAI_DAO_CHANGESET (object);

  switch (prop_id)
    {
    case PROP_OBJECT_ID:
      g_value_set_string (value, bonsai_dao_changeset_get_object_id (self));
      break;

    case PROP_SEALED:
      g_value_set_boolean (value, bonsai_dao_changeset_is_sealed (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_changeset_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  BonsaiDaoChangeset *self = BONSAI_DAO_CHANGESET (object);

  switch (prop_id)
    {
    case PROP_OBJECT_ID:
      self->object_id = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsai_dao_changeset_class_init (BonsaiDaoChangesetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_changeset_finalize;
  object_class->get_property = bonsai_dao_changeset_get_property;
  object_class->set_property = bonsai_dao_changeset_set_property;

  properties [PROP_OBJECT_ID] =
    g_param_spec_string ("object-id",
                         "Object Id",
                         "Object Id",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SEALED] =
    g_param_spec_boolean ("sealed",
                          "Sealed",
                          "If the snapshot is sealed, meaning no more changes.",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsai_dao_changeset_init (BonsaiDaoChangeset *self)
{
  self->changes = g_array_new (FALSE, TRUE, sizeof (Change));
  g_array_set_clear_func (self->changes, (GDestroyNotify)clear_change);
}

void
bonsai_dao_changeset_seal (BonsaiDaoChangeset *self)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));

  if (self->sealed == FALSE)
    {
      self->sealed = TRUE;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SEALED]);
    }

  BONSAI_EXIT;
}

gboolean
bonsai_dao_changeset_is_sealed (BonsaiDaoChangeset *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_CHANGESET (self), FALSE);

  return self->sealed;
}

static Change *
bonsai_dao_changeset_append (BonsaiDaoChangeset *self)
{
  g_assert (BONSAI_IS_DAO_CHANGESET (self));

  /* Grow array by one and return pointer to that element
   * to help avoid extra memcpy from stack to array.
   */
  g_array_set_size (self->changes, self->changes->len + 1);
  return &g_array_index (self->changes, Change, self->changes->len - 1);
}

/**
 * bonsai_dao_changeset_append_overwrite:
 * @self: a #BonsaiDaoChangeset
 * @pspec: the property to be appended
 * @...: the from value followed by the to value
 *
 * a variadic form of bonsai_dao_changeset_append_overwrite_value().
 *
 * @pspec should be followed by the from value and then the to value
 * and must match the promotion of types from @pspec's value_type.
 */
void
bonsai_dao_changeset_append_overwrite (BonsaiDaoChangeset *self,
                                       GParamSpec         *pspec,
                                       ...)
{
  g_autofree gchar *errmsg = NULL;
  Change *change;
  va_list args;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (pspec != NULL);
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (self->changes->len < G_MAXUINT);

  change = bonsai_dao_changeset_append (self);
  change->kind = CHANGE_KIND_OVERWRITE;
  change->pname = pspec->name;

  va_start (args, pspec);
  G_VALUE_COLLECT_INIT (&change->overwrite.from, pspec->value_type, args, 0, &errmsg);
  if (errmsg != NULL)
    g_critical ("%s", errmsg);
  else
    {
      G_VALUE_COLLECT_INIT (&change->overwrite.to, pspec->value_type, args, 0, &errmsg);
      if (errmsg != NULL)
        g_critical ("%s", errmsg);
    }
  va_end (args);

  if (errmsg != NULL)
    g_array_set_size (self->changes, self->changes->len - 1);

  BONSAI_EXIT;
}

void
bonsai_dao_changeset_append_overwrite_value (BonsaiDaoChangeset *self,
                                             const gchar        *property_name,
                                             const GValue       *from,
                                             const GValue       *to)
{
  Change *change;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (self->changes->len < G_MAXUINT);
  g_return_if_fail (!from || G_IS_VALUE (from));
  g_return_if_fail (G_IS_VALUE (to));

  change = bonsai_dao_changeset_append (self);
  change->kind = CHANGE_KIND_OVERWRITE;
  change->pname = g_intern_string (property_name);
  if (from != NULL)
    {
      g_value_init (&change->overwrite.from, G_VALUE_TYPE (from));
      g_value_copy (from, &change->overwrite.from);
    }
  g_value_init (&change->overwrite.to, G_VALUE_TYPE (to));
  g_value_copy (to, &change->overwrite.to);

  BONSAI_EXIT;
}

void
bonsai_dao_changeset_append_increment (BonsaiDaoChangeset *self,
                                       const gchar        *property_name,
                                       gint64              value)
{
  Change *change;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (self->changes->len < G_MAXUINT);

  change = bonsai_dao_changeset_append (self);
  change->kind = CHANGE_KIND_INCREMENT;
  change->pname = g_intern_string (property_name);
  change->increment = value;

  BONSAI_EXIT;
}

void
bonsai_dao_changeset_append_pop (BonsaiDaoChangeset *self,
                                 const gchar        *property_name,
                                 const GValue       *value)
{
  Change *change;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (self->changes->len < G_MAXUINT);
  g_return_if_fail (G_IS_VALUE (value));

  change = bonsai_dao_changeset_append (self);
  change->kind = CHANGE_KIND_POP;
  change->pname = g_intern_string (property_name);
  g_value_init (&change->pop, G_VALUE_TYPE (value));
  g_value_copy (value, &change->pop);

  BONSAI_EXIT;
}

void
bonsai_dao_changeset_append_push (BonsaiDaoChangeset *self,
                                  const gchar        *property_name,
                                  const GValue       *value)
{
  Change *change;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (self->changes->len < G_MAXUINT);
  g_return_if_fail (G_IS_VALUE (value));

  change = bonsai_dao_changeset_append (self);
  change->kind = CHANGE_KIND_PUSH;
  change->pname = g_intern_string (property_name);
  g_value_init (&change->push, G_VALUE_TYPE (value));
  g_value_copy (value, &change->push);

  BONSAI_EXIT;
}

const gchar *
bonsai_dao_changeset_get_object_id (BonsaiDaoChangeset *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_CHANGESET (self), NULL);

  return self->object_id;
}

gboolean
bonsai_dao_changeset_is_empty (BonsaiDaoChangeset *self)
{
  g_return_val_if_fail (BONSAI_IS_DAO_CHANGESET (self), FALSE);

  return self->changes->len == 0;
}

void
bonsai_dao_changeset_append_unset (BonsaiDaoChangeset *self,
                                   const gchar        *property_name,
                                   const GValue       *from)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (self->sealed == FALSE);
  g_return_if_fail (property_name != NULL);
  g_return_if_fail (from != NULL);
  g_return_if_fail (self->changes->len < G_MAXUINT);

  bonsai_dao_changeset_append_overwrite_value (self, property_name, from, NULL);

  BONSAI_EXIT;
}

void
bonsai_dao_changeset_append_string_diff (BonsaiDaoChangeset *self,
                                         GParamSpec         *pspec,
                                         const gchar        *before,
                                         const gchar        *after)
{
  g_return_if_fail (BONSAI_IS_DAO_CHANGESET (self));
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (pspec->value_type == G_TYPE_STRING);

  /* TODO: Actually perform a line-diff with diff/etc so that these
   * can be incrementally updated instead of colliding to fail the
   * transaction.
   */

  bonsai_dao_changeset_append_overwrite (self, pspec, before, after);
}

/**
 * bonsai_dao_changeset_to_variant:
 * @self: a #BonsaiDaoChangeset
 *
 * Gets the contents of the changeset as a #GVariant which can be
 * used to share the changeset with external systems.
 *
 * Returns: (transfer full): a #GVariant
 */
GVariant *
bonsai_dao_changeset_to_variant (BonsaiDaoChangeset *self)
{
  GVariantBuilder builder;

  g_return_val_if_fail (BONSAI_IS_DAO_CHANGESET (self), NULL);

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("(sa(usv))"));
  g_variant_builder_add (&builder, "s", self->object_id ?: "");
  g_variant_builder_open (&builder, G_VARIANT_TYPE ("a(usv)"));

  for (guint i = 0; i < self->changes->len; i++)
    {
      const Change *change = &g_array_index (self->changes, Change, i);

      g_variant_builder_open (&builder, G_VARIANT_TYPE ("(usv)"));
      g_variant_builder_add (&builder, "u", change->kind);
      g_variant_builder_add (&builder, "s", change->pname);

      switch (change->kind)
        {
        case CHANGE_KIND_POP:
          bonsai_dao_variant_builder_append_value (&builder, &change->pop);
          break;

        case CHANGE_KIND_PUSH:
          bonsai_dao_variant_builder_append_value (&builder, &change->push);
          break;

        case CHANGE_KIND_OVERWRITE:
          /* We cannot serialize "av" over D-Bus, so instead we use an array
           * of variant where an empty array indicates unset. Otherwise it is
           * an array of 1 with the actual value.
           */

          g_variant_builder_open (&builder, G_VARIANT_TYPE ("v"));
          g_variant_builder_open (&builder, G_VARIANT_TYPE ("(avav)"));

          g_variant_builder_open (&builder, G_VARIANT_TYPE ("av"));
          if (G_IS_VALUE (&change->overwrite.from))
            bonsai_dao_variant_builder_append_value (&builder, &change->overwrite.from);
          g_variant_builder_close (&builder);

          g_variant_builder_open (&builder, G_VARIANT_TYPE ("av"));
          if (G_IS_VALUE (&change->overwrite.to))
            bonsai_dao_variant_builder_append_value (&builder, &change->overwrite.to);
          g_variant_builder_close (&builder);

          g_variant_builder_close (&builder);
          g_variant_builder_close (&builder);
          break;

        case CHANGE_KIND_INCREMENT:
          g_variant_builder_add (&builder, "x", change->increment);
          break;

        default:
          g_assert_not_reached ();
        }

      g_variant_builder_close (&builder);
    }

  g_variant_builder_close (&builder);

  return g_variant_take_ref (g_variant_builder_end (&builder));
}

gboolean
bonsai_dao_changeset_apply (BonsaiDaoChangeset  *self,
                            GVariantDict        *dict,
                            GError             **error)
{
  GVariant *variant;
  GVariant *replace;

  g_return_val_if_fail (BONSAI_IS_DAO_CHANGESET (self), FALSE);
  g_return_val_if_fail (dict != NULL, FALSE);

  for (guint i = 0; i < self->changes->len; i++)
    {
      const Change *change = &g_array_index (self->changes, Change, i);

      switch (change->kind)
        {
        case CHANGE_KIND_OVERWRITE:
          /* If the to value has no GType, then we're really just unsetting
           * the value so we'll get the default property value.
           */
          if (!G_IS_VALUE (&change->overwrite.to))
            {
              g_variant_dict_remove (dict, change->pname);
              break;
            }

          /* TODO: Check that @from matches to see if we are overwriting
           * unexpected data. If so, we need to fail the transaction unless
           * a flag is set to ignore the old value.
           *
           * We might also just change the meaning by adding a new application
           * type for overwrite vs set.
           */
          if (!(variant = bonsai_dao_value_to_variant (&change->overwrite.to)))
            {
              g_set_error (error,
                           G_IO_ERROR,
                           G_IO_ERROR_INVALID_DATA,
                           "Cannot serialize value of type %s",
                           G_VALUE_TYPE_NAME (&change->overwrite));
              return FALSE;
            }

          g_variant_dict_insert_value (dict,
                                       change->pname,
                                       g_steal_pointer (&variant));

          break;

        case CHANGE_KIND_INCREMENT:
          variant = g_variant_dict_lookup_value (dict, change->pname, NULL);

          if (!(replace = bonsai_dao_increment (variant, change->increment)))
            {
              g_set_error (error,
                           G_IO_ERROR,
                           G_IO_ERROR_INVALID_DATA,
                           "Cannot increment variant of type %s",
                           variant ? g_variant_get_type_string (variant) : "(null)");
              g_clear_pointer (&variant, g_variant_unref);
              return FALSE;
            }

          g_variant_dict_insert_value (dict,
                                       change->pname,
                                       g_steal_pointer (&replace));
          g_clear_pointer (&variant, g_variant_unref);

          break;

        case CHANGE_KIND_POP:
        case CHANGE_KIND_PUSH:
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_NOT_SUPPORTED,
                       "Push and Pop are not yet supported");
          return FALSE;

        default:
          break;
        }
    }

  return TRUE;
}

/**
 * bonsai_dao_changeset_revert:
 * @self: a #BonsaiDaoChangeset
 *
 * Creates a new changeset that will revert the changes of @self.
 *
 * This is useful when you want to rollback local changes so that
 * you can replay them on top of changes pulled from a remote.
 *
 * Returns: (transfer full): a #BonsaiDaoChangeset
 */
BonsaiDaoChangeset *
bonsai_dao_changeset_revert (BonsaiDaoChangeset *self)
{
  BonsaiDaoChangeset *ret;

  g_return_val_if_fail (BONSAI_IS_DAO_CHANGESET (self), NULL);

  ret = bonsai_dao_changeset_new (self->object_id);

  for (guint i = 0; i < self->changes->len; i++)
    {
      const Change *change = &g_array_index (self->changes, Change, i);

      switch (change->kind)
        {
        case CHANGE_KIND_OVERWRITE:
          bonsai_dao_changeset_append_overwrite_value (ret, change->pname, &change->overwrite.to, &change->overwrite.from);
          break;

        case CHANGE_KIND_INCREMENT:
          bonsai_dao_changeset_append_increment (ret, change->pname, -change->increment);
          break;

        case CHANGE_KIND_POP:
          bonsai_dao_changeset_append_push (ret, change->pname, &change->pop);
          break;

        case CHANGE_KIND_PUSH:
          bonsai_dao_changeset_append_pop (ret, change->pname, &change->push);
          break;

        default:
          break;
        }
    }

  return g_steal_pointer (&ret);
}

/**
 * bonsai_dao_changeset_from_variant:
 * @object_type: a #GType of the object to resolve properties
 * @variant: a #GVariant
 *
 * Creates a #BonsaiDaoChangeset from a previously serialized #GVariant
 * created with bonsai_dao_changeset_to_variant().
 *
 * Returns: (transfer full) (nullable): a #BonsaiDaoChangeset if successful;
 *   otherwise %NULL if an error was encountered.
 */
BonsaiDaoChangeset *
bonsai_dao_changeset_from_variant (GType     object_type,
                                   GVariant *variant)
{
  g_autoptr(BonsaiDaoChangeset) ret = NULL;
  g_autoptr(GVariantIter) iter = NULL;
  g_autoptr(GTypeClass) type_class = NULL;
  const gchar *object_id = NULL;
  const gchar *property_name;
  gpointer vptr;
  guint32 kind;

  g_return_val_if_fail (g_type_is_a (object_type, BONSAI_TYPE_DAO_OBJECT), NULL);
  g_return_val_if_fail (variant != NULL, NULL);
  g_return_val_if_fail (object_type != G_TYPE_INVALID, NULL);
  g_return_val_if_fail (g_variant_is_of_type (variant, G_VARIANT_TYPE ("(sa(usv))")), NULL);

  if (!(type_class = g_type_class_ref (object_type)))
    return NULL;

  g_variant_get (variant, "(&sa(usv))", &object_id, &iter);

  if (object_id == NULL || object_id[0] == 0)
    return NULL;

  ret = bonsai_dao_changeset_new (object_id);

  while (g_variant_iter_next (iter, "(u&sv)", &kind, &property_name, &vptr))
    {
      g_autoptr(GVariant) v = vptr;
      GParamSpec *pspec;

      /* TODO: some effort will need to be done to round-trip objects with
       *       new properties along with old versions of apps which do not
       *       support those object changes.
       *
       *       Another option is to just fail updates with old versions of
       *       the app which has missing properties.
       */
      if (!(pspec = g_object_class_find_property (G_OBJECT_CLASS (type_class), property_name)))
        continue;

      switch ((ChangeKind)kind)
        {
        case CHANGE_KIND_POP:
          g_critical ("pop not yet implemented");
          break;

        case CHANGE_KIND_PUSH:
          g_critical ("push not yet implemented");
          break;

        case CHANGE_KIND_INCREMENT:
          if (!g_variant_is_of_type (v, G_VARIANT_TYPE_INT64))
            return NULL;
          bonsai_dao_changeset_append_increment (ret,
                                                 property_name,
                                                 g_variant_get_int64 (v));
          break;

        case CHANGE_KIND_OVERWRITE: {
          g_autoptr(GVariant) from = NULL;
          g_autoptr(GVariant) to = NULL;
          g_auto(GValue) fromv = G_VALUE_INIT;
          g_auto(GValue) tov = G_VALUE_INIT;
          const GValue *default_value;

          /* We use "av" with an empty array to indicate NULL which
           * allows us to have something like "mv" that serializes
           * to D-Bus just fine.
           */
          if (!g_variant_is_of_type (v, G_VARIANT_TYPE ("(avav)")))
            return NULL;

          g_variant_get (v, "(@av@av)", &from, &to);
          if (from == NULL && to == NULL)
            break;

          g_value_init (&fromv, pspec->value_type);
          g_value_init (&tov, pspec->value_type);

          if ((default_value = g_param_spec_get_default_value (pspec)))
            g_value_copy (default_value, &tov);

          if (from != NULL)
            {
              GVariant *tmp = NULL;

              g_assert (g_variant_is_of_type (from, G_VARIANT_TYPE ("av")));

              if (g_variant_n_children (from) == 1)
                tmp = g_variant_get_child_value (from, 0);

              g_clear_pointer (&from, g_variant_unref);
              from = tmp;
            }

          if (to != NULL)
            {
              GVariant *tmp = NULL;

              g_assert (g_variant_is_of_type (to, G_VARIANT_TYPE ("av")));

              if (g_variant_n_children (to) == 1)
                tmp = g_variant_get_child_value (to, 0);

              g_clear_pointer (&to, g_variant_unref);
              to = tmp;
            }

          if (from && !bonsai_dao_variant_to_value (from, &fromv))
            return NULL;

          if (to && !bonsai_dao_variant_to_value (to, &tov))
            return NULL;

          bonsai_dao_changeset_append_overwrite_value (ret, property_name, &fromv, &tov);

          break;
        }

        default:
          return NULL;
        }

    }

  return g_steal_pointer (&ret);
}
