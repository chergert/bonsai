/* bonsai-dao-query.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-dao-query"

#include "config.h"

#include <lmdb.h>
#include <libbonsai.h>

#include "bonsai-dao-collection.h"
#include "bonsai-dao-collection-private.h"
#include "bonsai-dao-index-private.h"
#include "bonsai-dao-query-private.h"
#include "bonsai-dao-repository.h"
#include "bonsai-dao-transaction-private.h"
#include "bonsai-dao-utils.h"
#include "bonsai-dao-where.h"

struct _BonsaiDaoQuery
{
  GObject               parent_instance;
  BonsaiDaoRepository  *repository;
  BonsaiDaoCollection  *collection;
  BonsaiDaoTransaction *transaction;
  BonsaiDaoQueryClause *where;
};

G_DEFINE_TYPE (BonsaiDaoQuery, bonsai_dao_query, G_TYPE_OBJECT)

static gint
date_time_compare (GDateTime *dt1,
                   GDateTime *dt2)
{
  gint64 i1 = 0;
  gint64 i2 = 0;

  if (dt1)
    i1 = g_date_time_to_unix (dt1);

  if (dt2)
    i2 = g_date_time_to_unix (dt2);

  if (i1 < i2)
    return -1;

  if (i1 > i2)
    return 1;

  return 0;
}

static void
bonsai_dao_query_clause_to_string (const BonsaiDaoQueryClause *clause,
                                   GString                    *str)
{
  if (clause == NULL)
    return;

  if (str->len)
    g_string_append_c (str, '\n');

  switch (clause->oper)
    {
    case BONSAI_DAO_OPERATOR_OR:
      g_string_append (str, "\n(");
      bonsai_dao_query_clause_to_string (clause->boolean.lhs, str);
      g_string_append (str, "\n  OR");
      bonsai_dao_query_clause_to_string (clause->boolean.rhs, str);
      g_string_append (str, "\n)");
      break;

    case BONSAI_DAO_OPERATOR_AND:
      g_string_append (str, "\n(");
      bonsai_dao_query_clause_to_string (clause->boolean.lhs, str);
      g_string_append (str, "\n  AND");
      bonsai_dao_query_clause_to_string (clause->boolean.rhs, str);
      g_string_append (str, "\n)");
      break;

    case BONSAI_DAO_OPERATOR_NOT:
      g_string_append (str, "\nNOT (");
      bonsai_dao_query_clause_to_string (clause->boolean.lhs, str);
      g_string_append (str, "\n)");
      break;

    case BONSAI_DAO_OPERATOR_IN:
    case BONSAI_DAO_OPERATOR_LESS_THAN:
    case BONSAI_DAO_OPERATOR_GREATER_THAN:
    case BONSAI_DAO_OPERATOR_EQUAL: {
      const gchar *op;
      GValue value = G_VALUE_INIT;

      if (clause->oper == BONSAI_DAO_OPERATOR_IN)
        op = "IN";
      else if (clause->oper == BONSAI_DAO_OPERATOR_LESS_THAN)
        op =  "LESS THAN";
      else if (clause->oper == BONSAI_DAO_OPERATOR_GREATER_THAN)
        op =  "GREATER THAN";
      else if (clause->oper == BONSAI_DAO_OPERATOR_EQUAL)
        op =  "EQUAL";
      else
        g_assert_not_reached ();

      g_value_init (&value, G_TYPE_STRING);
      g_value_transform (&clause->predicate.rhs, &value);
      g_string_append_printf (str,
                              "\n%s %s %s",
                              clause->predicate.lhs->name,
                              op,
                              g_value_get_string (&value));
      g_value_unset (&value);
      break;
    }

    default:
      g_assert_not_reached ();
    }
}

gchar *
_bonsai_dao_query_to_string (BonsaiDaoQuery *self)
{
  GString *str = g_string_new (NULL);
  g_string_append_printf (str, "  Repository: %s\n", bonsai_dao_repository_get_identifier (self->repository));
  g_string_append_printf (str, "  Collection: %s\n", bonsai_dao_collection_get_name (self->collection));
  g_string_append_printf (str, "        Type: %s\n", g_type_name (bonsai_dao_collection_get_object_type (self->collection)));
  g_string_append_printf (str, "       Where: %s", self->where == NULL ? "(null)\n" : "");

  if (self->where)
    bonsai_dao_query_clause_to_string (self->where, str);

  return g_string_free (str, FALSE);
}

static void
bonsai_dao_query_finalize (GObject *object)
{
  BonsaiDaoQuery *self = (BonsaiDaoQuery *)object;

  g_clear_object (&self->repository);
  g_clear_object (&self->collection);
  g_clear_object (&self->transaction);
  g_clear_pointer (&self->where, bonsai_dao_query_clause_free);

  G_OBJECT_CLASS (bonsai_dao_query_parent_class)->finalize (object);
}

static void
bonsai_dao_query_class_init (BonsaiDaoQueryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsai_dao_query_finalize;
}

static void
bonsai_dao_query_init (BonsaiDaoQuery *self)
{
}

BonsaiDaoQuery *
_bonsai_dao_query_new (BonsaiDaoRepository  *repository,
                       BonsaiDaoCollection  *collection,
                       BonsaiDaoTransaction *transaction,
                       BonsaiDaoQueryClause *where)
{
  BonsaiDaoQuery *self;

  self = g_object_new (BONSAI_TYPE_DAO_QUERY, NULL);
  self->repository = g_object_ref (repository);
  self->collection = g_object_ref (collection);
  self->transaction = g_object_ref (transaction);
  self->where = where;

  return g_steal_pointer (&self);
}

static BonsaiDaoIndex *
bonsai_dao_query_choose_best_index (BonsaiDaoQuery *self)
{
  g_autoptr(BonsaiDaoIndex) ret = NULL;
  g_autofree GParamSpec **pspecs = NULL;
  BonsaiDaoIndex * const *indexes;
  guint n_indexes = 0;
  guint n_pspecs = 0;
  gint score = -1;

  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (self), NULL);

  /* In worse case, we can full-table scan using the primary index */
  g_set_object (&ret, _bonsai_dao_collection_get_primary (self->collection));

  pspecs = _bonsai_dao_query_pspecs (self, &n_pspecs);
  indexes = _bonsai_dao_collection_indexes (self->collection, &n_indexes);

  for (guint i = 0; i < n_indexes; i++)
    {
      gint our_score = 0;

      for (guint j = 0; j < n_pspecs; j++)
        our_score += _bonsai_dao_index_has_pspec (indexes[i], pspecs[j]);

      if (our_score > score)
        {
          score = our_score;
          g_set_object (&ret, indexes[i]);
        }
    }

  return g_steal_pointer (&ret);
}

gboolean
_bonsai_dao_query_create_cursor (BonsaiDaoQuery  *self,
                                 MDB_cursor     **cursor,
                                 BonsaiDaoIndex **index,
                                 GError         **error)
{
  g_autoptr(BonsaiDaoIndex) local = NULL;
  MDB_txn *txn;

  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (self), FALSE);
  g_return_val_if_fail (cursor != NULL, FALSE);
  g_return_val_if_fail (index != NULL, FALSE);

  local = bonsai_dao_query_choose_best_index (self);
  txn = _bonsai_dao_transaction_get_native (self->transaction);

  if (!_bonsai_dao_index_create_cursor (local, txn, cursor))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Failed to create cursor for index");
      return FALSE;
    }

  *index = g_steal_pointer (&local);

  return TRUE;
}

static gboolean
bonsai_dao_query_clause_equal (const BonsaiDaoQueryClause *clause,
                               GVariantDict               *dict)
{
  g_autoptr(GVariant) v = NULL;
  GValue value = G_VALUE_INIT;

  /* Try to get the property field */
  if (!(v = g_variant_dict_lookup_value (dict, clause->predicate.lhs->name, NULL)))
    return FALSE;

  /* Only compare it if we can convert to proper type */
  g_value_init (&value, G_VALUE_TYPE (&clause->predicate.rhs));
  if (!bonsai_dao_variant_to_value (v, &value))
    return FALSE;

  switch (G_VALUE_TYPE (&value))
    {
    case G_TYPE_INT:
      return g_value_get_int (&value) == g_value_get_int (&clause->predicate.rhs);

    case G_TYPE_UINT:
      return g_value_get_uint (&value) == g_value_get_uint (&clause->predicate.rhs);

    case G_TYPE_INT64:
      return g_value_get_int64 (&value) == g_value_get_int64 (&clause->predicate.rhs);

    case G_TYPE_UINT64:
      return g_value_get_uint64 (&value) == g_value_get_uint64 (&clause->predicate.rhs);

    case G_TYPE_LONG:
      return g_value_get_long (&value) == g_value_get_long (&clause->predicate.rhs);

    case G_TYPE_ULONG:
      return g_value_get_ulong (&value) == g_value_get_ulong (&clause->predicate.rhs);

    case G_TYPE_DOUBLE:
      return g_value_get_double (&value) == g_value_get_double (&clause->predicate.rhs);

    case G_TYPE_FLOAT:
      return g_value_get_float (&value) == g_value_get_float (&clause->predicate.rhs);

    case G_TYPE_BOOLEAN:
      return g_value_get_boolean (&value) == g_value_get_boolean (&clause->predicate.rhs);

    case G_TYPE_STRING:
      return g_strcmp0 (g_value_get_string (&value), g_value_get_string (&clause->predicate.rhs)) == 0;

    default:
      if (G_VALUE_HOLDS (&value, G_TYPE_DATE_TIME))
        return date_time_compare (g_value_get_boxed (&value), g_value_get_boxed (&clause->predicate.rhs)) == 0;

      return FALSE;
    }
}

static gboolean
bonsai_dao_query_clause_lt (const BonsaiDaoQueryClause *clause,
                            GVariantDict               *dict)
{
  g_autoptr(GVariant) v = NULL;
  GValue value = G_VALUE_INIT;

  /* Try to get the property field */
  if (!(v = g_variant_dict_lookup_value (dict, clause->predicate.lhs->name, NULL)))
    return FALSE;

  /* Only compare it if we can convert to proper type */
  g_value_init (&value, G_VALUE_TYPE (&clause->predicate.rhs));
  if (!bonsai_dao_variant_to_value (v, &value))
    return FALSE;

  switch (G_VALUE_TYPE (&value))
    {
    case G_TYPE_INT:
      return g_value_get_int (&value) < g_value_get_int (&clause->predicate.rhs);

    case G_TYPE_UINT:
      return g_value_get_uint (&value) < g_value_get_uint (&clause->predicate.rhs);

    case G_TYPE_INT64:
      return g_value_get_int64 (&value) < g_value_get_int64 (&clause->predicate.rhs);

    case G_TYPE_UINT64:
      return g_value_get_uint64 (&value) < g_value_get_uint64 (&clause->predicate.rhs);

    case G_TYPE_LONG:
      return g_value_get_long (&value) < g_value_get_long (&clause->predicate.rhs);

    case G_TYPE_ULONG:
      return g_value_get_ulong (&value) < g_value_get_ulong (&clause->predicate.rhs);

    case G_TYPE_DOUBLE:
      return g_value_get_double (&value) < g_value_get_double (&clause->predicate.rhs);

    case G_TYPE_FLOAT:
      return g_value_get_float (&value) < g_value_get_float (&clause->predicate.rhs);

    case G_TYPE_BOOLEAN:
      return g_value_get_boolean (&value) < g_value_get_boolean (&clause->predicate.rhs);

    case G_TYPE_STRING:
      return g_strcmp0 (g_value_get_string (&value), g_value_get_string (&clause->predicate.rhs)) < 0;

    default:
      if (G_VALUE_HOLDS (&value, G_TYPE_DATE_TIME))
        return date_time_compare (g_value_get_boxed (&value), g_value_get_boxed (&clause->predicate.rhs)) < 0;

      return FALSE;
    }
}

static gboolean
bonsai_dao_query_clause_gt (const BonsaiDaoQueryClause *clause,
                            GVariantDict               *dict)
{
  g_autoptr(GVariant) v = NULL;
  GValue value = G_VALUE_INIT;

  /* Try to get the property field */
  if (!(v = g_variant_dict_lookup_value (dict, clause->predicate.lhs->name, NULL)))
    return FALSE;

  /* Only compare it if we can convert to proper type */
  g_value_init (&value, G_VALUE_TYPE (&clause->predicate.rhs));
  if (!bonsai_dao_variant_to_value (v, &value))
    return FALSE;

  switch (G_VALUE_TYPE (&value))
    {
    case G_TYPE_INT:
      return g_value_get_int (&value) > g_value_get_int (&clause->predicate.rhs);

    case G_TYPE_UINT:
      return g_value_get_uint (&value) > g_value_get_uint (&clause->predicate.rhs);

    case G_TYPE_INT64:
      return g_value_get_int64 (&value) > g_value_get_int64 (&clause->predicate.rhs);

    case G_TYPE_UINT64:
      return g_value_get_uint64 (&value) > g_value_get_uint64 (&clause->predicate.rhs);

    case G_TYPE_LONG:
      return g_value_get_long (&value) > g_value_get_long (&clause->predicate.rhs);

    case G_TYPE_ULONG:
      return g_value_get_ulong (&value) > g_value_get_ulong (&clause->predicate.rhs);

    case G_TYPE_DOUBLE:
      return g_value_get_double (&value) > g_value_get_double (&clause->predicate.rhs);

    case G_TYPE_FLOAT:
      return g_value_get_float (&value) > g_value_get_float (&clause->predicate.rhs);

    case G_TYPE_BOOLEAN:
      return g_value_get_boolean (&value) > g_value_get_boolean (&clause->predicate.rhs);

    case G_TYPE_STRING:
      return g_strcmp0 (g_value_get_string (&value), g_value_get_string (&clause->predicate.rhs)) > 0;

    default:
      if (G_VALUE_HOLDS (&value, G_TYPE_DATE_TIME))
        return date_time_compare (g_value_get_boxed (&value), g_value_get_boxed (&clause->predicate.rhs)) > 0;

      return FALSE;
    }
}

static gboolean
bonsai_dao_query_clause_in (const BonsaiDaoQueryClause *clause,
                            GVariantDict               *dict)
{
  g_autoptr(GVariant) v = NULL;
  const GValue *lookup = &clause->predicate.rhs;

  /* Try to get the property field */
  if (!(v = g_variant_dict_lookup_value (dict, clause->predicate.lhs->name, NULL)))
    return FALSE;

  if (G_VALUE_HOLDS_STRING (lookup))
    {
      if (g_variant_is_of_type (v, G_VARIANT_TYPE_STRING_ARRAY))
        {
          const gchar *lookupstr = g_value_get_string (lookup);
          const gchar *str;
          GVariantIter iter;

          g_variant_iter_init (&iter, v);
          while (g_variant_iter_next (&iter, "&s", &str))
            {
              if (g_strcmp0 (lookupstr, str) == 0)
                return TRUE;
            }
        }

      return FALSE;
    }

  /* Special case GDateTime */
  if (G_VALUE_HOLDS (lookup, G_TYPE_DATE_TIME) &&
      g_variant_is_of_type (v, G_VARIANT_TYPE ("ax")))
    {
      gsize n_elements = 0;
      const gint64 *ar = g_variant_get_fixed_array (v, &n_elements, sizeof (gint64));
      GDateTime *dt = g_value_get_boxed (lookup);
      gint64 intval;

      if (dt == NULL)
        return FALSE;

      intval = g_date_time_to_unix (dt);

      for (gsize i = 0; i < n_elements; i++)
        {
          if (intval == ar[i])
            return TRUE;
        }
    }

  if (g_variant_is_of_type (v, G_VARIANT_TYPE ("ai")))
    {
      GValue as = G_VALUE_INIT;
      gsize n_elements = 0;
      const gint *ar = g_variant_get_fixed_array (v, &n_elements, sizeof (gint));
      gint intval;

      g_value_init (&as, G_TYPE_INT);
      g_value_transform (lookup, &as);
      intval = g_value_get_int (&as);

      for (gsize i = 0; i < n_elements; i++)
        {
          if (intval == ar[i])
            return TRUE;
        }
    }

  if (g_variant_is_of_type (v, G_VARIANT_TYPE ("au")))
    {
      GValue as = G_VALUE_INIT;
      gsize n_elements = 0;
      const guint *ar = g_variant_get_fixed_array (v, &n_elements, sizeof (guint));
      guint intval;

      g_value_init (&as, G_TYPE_UINT);
      g_value_transform (lookup, &as);
      intval = g_value_get_uint (&as);

      for (gsize i = 0; i < n_elements; i++)
        {
          if (intval == ar[i])
            return TRUE;
        }
    }

  if (g_variant_is_of_type (v, G_VARIANT_TYPE ("ax")))
    {
      GValue as = G_VALUE_INIT;
      gsize n_elements = 0;
      const gint64 *ar = g_variant_get_fixed_array (v, &n_elements, sizeof (gint64));
      gint64 intval;

      g_value_init (&as, G_TYPE_INT64);
      g_value_transform (lookup, &as);
      intval = g_value_get_int64 (&as);

      for (gsize i = 0; i < n_elements; i++)
        {
          if (intval == ar[i])
            return TRUE;
        }
    }

  if (g_variant_is_of_type (v, G_VARIANT_TYPE ("at")))
    {
      GValue as = G_VALUE_INIT;
      gsize n_elements = 0;
      const guint64 *ar = g_variant_get_fixed_array (v, &n_elements, sizeof (guint64));
      guint64 intval;

      g_value_init (&as, G_TYPE_UINT64);
      g_value_transform (lookup, &as);
      intval = g_value_get_uint64 (&as);

      for (gsize i = 0; i < n_elements; i++)
        {
          if (intval == ar[i])
            return TRUE;
        }
    }

  if (g_variant_is_of_type (v, G_VARIANT_TYPE ("ad")))
    {
      GValue as = G_VALUE_INIT;
      gsize n_elements = 0;
      const gdouble *ar = g_variant_get_fixed_array (v, &n_elements, sizeof (gdouble));
      gdouble intval;

      g_value_init (&as, G_TYPE_DOUBLE);
      g_value_transform (lookup, &as);
      intval = g_value_get_double (&as);

      for (gsize i = 0; i < n_elements; i++)
        {
          if (intval == ar[i])
            return TRUE;
        }
    }

  return FALSE;
}

static gboolean
bonsai_dao_query_clause_match (const BonsaiDaoQueryClause *clause,
                               GVariantDict               *dict)
{
  if (clause == NULL)
    return TRUE;

  switch (clause->oper)
    {
    case BONSAI_DAO_OPERATOR_AND:
      return bonsai_dao_query_clause_match (clause->boolean.lhs, dict) &&
             bonsai_dao_query_clause_match (clause->boolean.rhs, dict);

    case BONSAI_DAO_OPERATOR_OR:
      return bonsai_dao_query_clause_match (clause->boolean.lhs, dict) ||
             bonsai_dao_query_clause_match (clause->boolean.rhs, dict);

    case BONSAI_DAO_OPERATOR_EQUAL:
      return bonsai_dao_query_clause_equal (clause, dict);

    case BONSAI_DAO_OPERATOR_NOT:
      return !bonsai_dao_query_clause_match (clause->boolean.lhs, dict);

    case BONSAI_DAO_OPERATOR_LESS_THAN:
      return bonsai_dao_query_clause_lt (clause, dict);

    case BONSAI_DAO_OPERATOR_GREATER_THAN:
      return bonsai_dao_query_clause_gt (clause, dict);

    case BONSAI_DAO_OPERATOR_IN:
      return bonsai_dao_query_clause_in (clause, dict);

    default:
      return FALSE;
    }
}

gboolean
_bonsai_dao_query_matches (BonsaiDaoQuery *self,
                           const gchar    *id,
                           GVariant       *variant)
{
  gboolean ret;
  GVariantDict dict;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (self), FALSE);

  if (self->where == NULL)
    return TRUE;

  /* If the variant is a dict, we can use that, otherwise we can create
   * a dict with just the "id" in it as some queries may be simple enough to
   * match that alone.
   */
  if (g_variant_is_of_type (variant, G_VARIANT_TYPE_VARDICT))
    g_variant_dict_init (&dict, variant);
  else
    g_variant_dict_init (&dict, NULL);

  /* Make sure we have an "id" to work with */
  g_variant_dict_insert (&dict, "id", "s", id);

  ret = bonsai_dao_query_clause_match (self->where, &dict);

  g_variant_dict_clear (&dict);

  BONSAI_RETURN (ret);
}

gboolean
_bonsai_dao_query_get_bounds (BonsaiDaoQuery *self,
                              GParamSpec     *pspec,
                              GValue         *lower,
                              GValue         *upper)
{
  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (self), FALSE);
  g_return_val_if_fail (G_IS_PARAM_SPEC (pspec), FALSE);
  g_return_val_if_fail (G_VALUE_TYPE (lower) == G_TYPE_INVALID, FALSE);
  g_return_val_if_fail (G_VALUE_TYPE (upper) == G_TYPE_INVALID, FALSE);

  return _bonsai_dao_query_clause_get_bounds (self->where, pspec, lower, upper);
}

static void
_bonsai_dao_query_clause_pspecs (const BonsaiDaoQueryClause *clause,
                                 GPtrArray                  *ar)
{
  if (clause == NULL)
    return;

  switch (clause->oper)
    {
    case BONSAI_DAO_OPERATOR_OR:
    case BONSAI_DAO_OPERATOR_AND:
    case BONSAI_DAO_OPERATOR_NOT:
      _bonsai_dao_query_clause_pspecs (clause->boolean.rhs, ar);
      _bonsai_dao_query_clause_pspecs (clause->boolean.lhs, ar);
      break;

    case BONSAI_DAO_OPERATOR_IN:
    case BONSAI_DAO_OPERATOR_EQUAL:
    case BONSAI_DAO_OPERATOR_LESS_THAN:
    case BONSAI_DAO_OPERATOR_GREATER_THAN:
      g_ptr_array_add (ar, clause->predicate.lhs);
      break;

    default:
      g_assert_not_reached ();
    }
}

GParamSpec **
_bonsai_dao_query_pspecs (BonsaiDaoQuery *self,
                          guint          *n_pspecs)
{
  GPtrArray *ar = NULL;

  g_return_val_if_fail (BONSAI_IS_DAO_QUERY (self), NULL);
  g_return_val_if_fail (n_pspecs != NULL, NULL);

  ar = g_ptr_array_new ();
  _bonsai_dao_query_clause_pspecs (self->where, ar);
  *n_pspecs = ar->len;
  return (GParamSpec **)g_ptr_array_free (ar, FALSE);
}

static void
take_if_lower (GValue       *dest,
               const GValue *value)
{
  if (!G_IS_VALUE (value))
    return;

  if (!G_IS_VALUE (dest))
    {
      g_value_init (dest, G_VALUE_TYPE (value));
      g_value_copy (value, dest);
      return;
    }

#define CASE_COMPARE(TYPE, type) \
    case G_TYPE_##TYPE: \
      if (g_value_get_##type (value) < g_value_get_##type (dest)) \
        g_value_set_##type (dest, g_value_get_##type (dest)); \
      break

  switch (G_VALUE_TYPE (dest))
    {
    CASE_COMPARE (INT, int);
    CASE_COMPARE (UINT, uint);
    CASE_COMPARE (INT64, int64);
    CASE_COMPARE (UINT64, uint64);
    CASE_COMPARE (LONG, long);
    CASE_COMPARE (ULONG, ulong);
    CASE_COMPARE (FLOAT, float);
    CASE_COMPARE (DOUBLE, double);
    CASE_COMPARE (BOOLEAN, boolean);

    case G_TYPE_STRING:
      if (g_value_get_string (dest) == NULL ||
          g_strcmp0 (g_value_get_string (value), g_value_get_string (dest)) < 0)
        g_value_set_string (dest, g_value_get_string (value));
      break;

    default:
      if (G_VALUE_HOLDS (value, G_TYPE_DATE_TIME))
        {
          if (date_time_compare (g_value_get_boxed (value), g_value_get_boxed (dest)) < 0)
            g_value_set_boxed (dest, g_value_get_boxed (value));
        }
      break;
    }

#undef CASE_COMPARE
}

static void
take_if_upper (GValue       *dest,
               const GValue *value)
{
  if (!G_IS_VALUE (value))
    return;

  if (!G_IS_VALUE (dest))
    {
      g_value_init (dest, G_VALUE_TYPE (value));
      g_value_copy (value, dest);
      return;
    }

#define CASE_COMPARE(TYPE, type) \
    case G_TYPE_##TYPE: \
      if (g_value_get_##type (value) > g_value_get_##type (dest)) \
        g_value_set_##type (dest, g_value_get_##type (dest)); \
      break

  switch (G_VALUE_TYPE (dest))
    {
    CASE_COMPARE (INT, int);
    CASE_COMPARE (UINT, uint);
    CASE_COMPARE (INT64, int64);
    CASE_COMPARE (UINT64, uint64);
    CASE_COMPARE (LONG, long);
    CASE_COMPARE (ULONG, ulong);
    CASE_COMPARE (FLOAT, float);
    CASE_COMPARE (DOUBLE, double);
    CASE_COMPARE (BOOLEAN, boolean);

    case G_TYPE_STRING:
      if (g_value_get_string (dest) == NULL ||
          g_strcmp0 (g_value_get_string (value), g_value_get_string (dest)) > 0)
        g_value_set_string (dest, g_value_get_string (value));
      break;

    default:
      if (G_VALUE_HOLDS (value, G_TYPE_DATE_TIME))
        {
          if (date_time_compare (g_value_get_boxed (value), g_value_get_boxed (dest)) > 0)
            g_value_set_boxed (dest, g_value_get_boxed (value));
        }
      break;
    }

#undef CASE_COMPARE
}

gboolean
_bonsai_dao_query_clause_get_bounds (BonsaiDaoQueryClause *clause,
                                     GParamSpec           *pspec,
                                     GValue               *lower,
                                     GValue               *upper)
{
  if (clause == NULL)
    return FALSE;

  g_return_val_if_fail (G_IS_PARAM_SPEC (pspec), FALSE);
  g_return_val_if_fail (lower != NULL, FALSE);
  g_return_val_if_fail (upper != NULL, FALSE);

  switch (clause->oper)
    {
    case BONSAI_DAO_OPERATOR_OR: {
      g_auto(GValue) lhs_lower = G_VALUE_INIT;
      g_auto(GValue) lhs_upper = G_VALUE_INIT;
      g_auto(GValue) rhs_lower = G_VALUE_INIT;
      g_auto(GValue) rhs_upper = G_VALUE_INIT;

      /* We don't handle NOT well, so we have to abort if we get one of those */
      if (!_bonsai_dao_query_clause_get_bounds (clause->boolean.lhs, pspec, &lhs_lower, &lhs_upper) ||
          !_bonsai_dao_query_clause_get_bounds (clause->boolean.rhs, pspec, &rhs_lower, &rhs_upper))
        return FALSE;

      /* Take the higher of the two lowers, and lower of two uppers */
      take_if_upper (&lhs_lower, &rhs_lower);
      take_if_lower (&rhs_lower, &lhs_lower);

      break;
    }

    case BONSAI_DAO_OPERATOR_AND: {
      g_auto(GValue) lhs_lower = G_VALUE_INIT;
      g_auto(GValue) lhs_upper = G_VALUE_INIT;
      g_auto(GValue) rhs_lower = G_VALUE_INIT;
      g_auto(GValue) rhs_upper = G_VALUE_INIT;

      /* We don't handle NOT well, so we have to abort if we get one of those */
      if (!_bonsai_dao_query_clause_get_bounds (clause->boolean.lhs, pspec, &lhs_lower, &lhs_upper) ||
          !_bonsai_dao_query_clause_get_bounds (clause->boolean.rhs, pspec, &rhs_lower, &rhs_upper))
        return FALSE;

      /* Take the lower of the two uppers, and upper of two lowers */
      take_if_lower (&lhs_lower, &rhs_lower);
      take_if_upper (&rhs_lower, &lhs_lower);

      break;
    }

    case BONSAI_DAO_OPERATOR_NOT:
      /* Most issues here could be fixed by altering the clauses when
       * creating a new NOT clause. For exmaple, a NOT (< 5) can be turned
       * into >= 5. But for now we'll ignore it and move on.
       */
      return FALSE;

    case BONSAI_DAO_OPERATOR_EQUAL:
    case BONSAI_DAO_OPERATOR_IN:
      if (pspec == clause->predicate.lhs)
        {
          if (!G_IS_VALUE (lower))
            g_value_init (lower, clause->predicate.lhs->value_type);
          if (!G_IS_VALUE (upper))
            g_value_init (upper, clause->predicate.lhs->value_type);
          g_value_copy (&clause->predicate.rhs, lower);
          g_value_copy (&clause->predicate.rhs, upper);
        }
      break;

    case BONSAI_DAO_OPERATOR_LESS_THAN:
      if (pspec == clause->predicate.lhs)
        {
          if (!G_IS_VALUE (upper))
            g_value_init (upper, clause->predicate.lhs->value_type);
          take_if_lower (upper, &clause->predicate.rhs);
        }
      break;

    case BONSAI_DAO_OPERATOR_GREATER_THAN:
      if (pspec == clause->predicate.lhs)
        {
          if (!G_IS_VALUE (lower))
            g_value_init (lower, clause->predicate.lhs->value_type);
          take_if_upper (lower, &clause->predicate.rhs);
        }
      break;

    default:
      g_warn_if_reached ();
      return FALSE;
    }

  return TRUE;
}
