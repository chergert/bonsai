/* bonsai-agent.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsai-agent"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-unix.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <gio/gunixfdlist.h>
#include <libbonsai.h>

#include "ipc-agent.h"
#include "ipc-isolation.h"

#include "xdp-utils.h"

typedef struct
{
  BonsaiClient          *client;
  GDBusMethodInvocation *invocation;
  GIOStream             *io_stream;
} Open;

static GMainLoop *g_main_loop;
static IpcAgent *g_agent;
static GSettings *g_settings;

static void
open_clear (Open *state)
{
  g_clear_object (&state->client);
  g_clear_object (&state->invocation);
  g_clear_object (&state->io_stream);
}

static void
open_unref (Open *state)
{
  g_atomic_rc_box_release_full (state, (GDestroyNotify) open_clear);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Open, open_unref)

static void
on_new_connection_cb (GObject      *object,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  g_autoptr(Open) state = user_data;
  g_autoptr(GDBusConnection) connection = NULL;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (state != NULL);
  g_assert (G_IS_IO_STREAM (state->io_stream));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (state->invocation));

  if (!(connection = g_dbus_connection_new_finish (result, &error)))
    BONSAI_GOTO (handle_error);



  BONSAI_EXIT;

handle_error:
  g_dbus_method_invocation_return_gerror (g_steal_pointer (&state->invocation), error);

  BONSAI_EXIT;
}

static void
on_isolate_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  IpcIsolation *isolation = (IpcIsolation *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(Open) state = user_data;

  BONSAI_ENTRY;

  g_assert (IPC_IS_ISOLATION (isolation));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (state != NULL);
  g_assert (BONSAI_IS_CLIENT (state->client));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (state->invocation));

  if (ipc_isolation_call_isolate_finish (isolation, result, &error))
    BONSAI_GOTO (handle_error);

  g_dbus_connection_new (state->io_stream,
                         NULL,
                         G_DBUS_CONNECTION_FLAGS_DELAY_MESSAGE_PROCESSING,
                         NULL,
                         NULL,
                         on_new_connection_cb,
                         g_steal_pointer (&state));

  BONSAI_EXIT;

handle_error:
  g_dbus_method_invocation_return_gerror (g_steal_pointer (&state->invocation), error);

  BONSAI_EXIT;
}

static void
on_connect_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  BonsaiClient *client = (BonsaiClient *)object;
  g_autoptr(IpcIsolation) isolation = NULL;
  g_autoptr(XdpAppInfo) app_info = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(Open) state = user_data;
  GDBusConnection *connection;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (state != NULL);
  g_assert (BONSAI_IS_CLIENT (state->client));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (state->invocation));

  if (!bonsai_client_connect_finish (client, result, &error))
    BONSAI_GOTO (handle_error);

  if (!(app_info = xdp_invocation_lookup_app_info_sync (state->invocation, NULL, &error)))
    BONSAI_GOTO (handle_error);

  if (!(connection = bonsai_client_get_connection (client)))
    {
      error = g_error_new (G_IO_ERROR,
                           G_IO_ERROR_CONNECTION_CLOSED,
                           "Connection lost");
      BONSAI_GOTO (handle_error);
    }

  if (!(isolation = ipc_isolation_proxy_new_sync (connection,
                                                  (G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
                                                   G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS),
                                                  NULL,
                                                  "/org/gnome/Bonsai/Isolation",
                                                  NULL,
                                                  &error)))
    BONSAI_GOTO (handle_error);

  ipc_isolation_call_isolate (isolation,
                              xdp_app_info_get_id (app_info) ?: "",
                              NULL,
                              on_isolate_cb,
                              g_steal_pointer (&state));

  BONSAI_EXIT;

handle_error:
  g_dbus_method_invocation_return_gerror (g_steal_pointer (&state->invocation), error);

  BONSAI_EXIT;
}

static gboolean
on_handle_open_cb (IpcAgent              *agent,
                   GDBusMethodInvocation *invocation,
                   GUnixFDList           *in_fd_list)
{
  g_autoptr(GSocketConnectable) address = NULL;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GTlsCertificate) peer_cert = NULL;
  g_autoptr(BonsaiIdentity) identity = NULL;
  g_autoptr(BonsaiIdentity) peer_identity = NULL;
  g_autoptr(BonsaiEndPoint) end_point = NULL;
  g_autoptr(BonsaiClient) client = NULL;
  g_autoptr(GIOStream) io_stream = NULL;
  g_autoptr(GInputStream) input_stream = NULL;
  g_autoptr(GOutputStream) output_stream = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *address_str = NULL;
  g_autofree gchar *peer_identity_pem = NULL;
  Open *state;
  int fd, fd2;

  BONSAI_ENTRY;

  g_assert (IPC_IS_AGENT (agent));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (!in_fd_list || G_IS_UNIX_FD_LIST (in_fd_list));

  if (!(certificate = bonsai_tls_certificate_new_for_user (NULL, &error)))
    BONSAI_GOTO (handle_error);

  if (g_unix_fd_list_get_length (in_fd_list) != 1)
    {
      error = g_error_new (G_DBUS_ERROR,
                           G_DBUS_ERROR_INVALID_ARGS,
                           "Method invocation requires 1 fd to be provided");
      BONSAI_GOTO (handle_error);
    }

  if ((fd = g_unix_fd_list_get (in_fd_list, 0, &error)) == -1)
    BONSAI_GOTO (handle_error);

  if (!g_unix_set_fd_nonblocking (fd, TRUE, &error))
    {
      close (fd);
      BONSAI_GOTO (handle_error);
    }

  if (-1 == (fd2 = dup (fd)))
    {
      int errsv = errno;
      error = g_error_new_literal (G_IO_ERROR,
                                   g_io_error_from_errno (errsv),
                                   g_strerror (errsv));
      close (fd);
      BONSAI_GOTO (handle_error);
    }

  input_stream = g_unix_input_stream_new (fd, TRUE);
  output_stream = g_unix_output_stream_new (fd2, TRUE);
  io_stream = g_simple_io_stream_new (input_stream, output_stream);

  close (fd);
  close (fd2);

  identity = bonsai_tls_identity_new (certificate);
  client = bonsai_client_new (identity);
  address_str = g_settings_get_string (g_settings, "end-point");

  if (!(address = g_network_address_parse (address_str, BONSAI_DEFAULT_PORT, &error)))
    BONSAI_GOTO (handle_error);

  end_point = bonsai_socket_end_point_new (address);
  peer_identity_pem = g_settings_get_string (g_settings, "peer-identity");

  if (!(peer_cert = g_tls_certificate_new_from_pem (peer_identity_pem, -1, &error)))
    BONSAI_GOTO (handle_error);

  peer_identity = bonsai_tls_identity_new (peer_cert);

  state = g_atomic_rc_box_new0 (Open);
  state->client = g_object_ref (client);
  state->invocation = g_steal_pointer (&invocation);
  state->io_stream = g_steal_pointer (&io_stream);

  bonsai_client_connect_async (client,
                               end_point,
                               peer_identity,
                               NULL,
                               on_connect_cb,
                               g_steal_pointer (&state));

handle_error:
  g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  BONSAI_RETURN (TRUE);
}

static void
bus_acquired_handler (GDBusConnection *connection,
                      const gchar     *name,
                      gpointer         user_data)
{
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_DBUS_CONNECTION (connection));

  g_agent = ipc_agent_skeleton_new ();
  g_signal_connect (g_agent,
                    "handle-open",
                    G_CALLBACK (on_handle_open_cb),
                    NULL);
  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (g_agent),
                                    connection,
                                    "/org/gnome/Bonsai/Agent",
                                    &error);
  if (error != NULL)
    g_error ("%s", error->message);

  BONSAI_EXIT;
}

static void
name_acquired_handler (GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data)
{
  BONSAI_ENTRY;

  g_assert (G_IS_DBUS_CONNECTION (connection));

  g_message ("Bus name %s acquired", name);

  BONSAI_EXIT;
}

static void
name_lost_handler (GDBusConnection *connection,
                   const gchar     *name,
                   gpointer         user_data)
{
  g_message ("Bus name %s lost", name);
  g_main_loop_quit (g_main_loop);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GDBusConnection) bus = NULL;
  g_autoptr(GError) error = NULL;

  context = g_option_context_new (_("- Local agent providing access to Bonsai"));

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  g_main_loop = g_main_loop_new (NULL, FALSE);

  g_settings = g_settings_new ("org.gnome.bonsai.agent");

  if (!(bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error)))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  g_bus_own_name (G_BUS_TYPE_SESSION,
                  "org.gnome.Bonsai.Agent",
                  G_BUS_NAME_OWNER_FLAGS_NONE,
                  bus_acquired_handler,
                  name_acquired_handler,
                  name_lost_handler,
                  NULL, NULL);

  g_main_loop_run (g_main_loop);

  return EXIT_SUCCESS;
}
