/* bonsaid-utils.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-utils"

#include "config.h"

#include "bonsaid-utils.h"

static gboolean
on_g_authorize_method_cb (GDBusInterfaceSkeleton  *skeleton,
                          GDBusMethodInvocation   *invocation,
                          const BonsaidMethodAuth *auth)
{
  const gchar *interface_name;
  const gchar *method_name;

  BONSAI_ENTRY;

  g_assert (G_IS_DBUS_INTERFACE_SKELETON (skeleton));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (auth != NULL);

  method_name = g_dbus_method_invocation_get_method_name (invocation);

  for (guint i = 0; auth[i].method; i++)
    {
      if (g_str_equal (auth[i].method, method_name))
        {
          GDBusConnection *connection = g_dbus_method_invocation_get_connection (invocation);
          BonsaiClient *client = bonsai_client_from_connection (connection);
          BonsaiRealm realm = 0;

          if (client != NULL)
            realm = bonsai_client_get_realm (client);

          /* If we require same user and this is not a TLS connection,
           * then we need to check credentials.
           */
          if (realm == 0 && auth[i].allow_bus)
            {
              GDBusConnectionFlags flags = g_dbus_connection_get_flags (connection);

              if (!(flags & G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION))
                BONSAI_GOTO (not_authorized);

              BONSAI_RETURN (TRUE);
            }

          if (!(realm & auth[i].realm))
            BONSAI_GOTO (not_authorized);

          BONSAI_RETURN (TRUE);
        }
    }

not_authorized:
  interface_name = g_dbus_method_invocation_get_interface_name (invocation);
  g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                         G_DBUS_ERROR,
                                         G_DBUS_ERROR_ACCESS_DENIED,
                                         "Access denied to method %s.%s",
                                         interface_name,
                                         method_name);

  BONSAI_RETURN (FALSE);
}

void
bonsaid_set_method_auth (GDBusInterfaceSkeleton  *skeleton,
                         const BonsaidMethodAuth *auth)
{
  BONSAI_ENTRY;

  g_return_if_fail (G_IS_DBUS_INTERFACE_SKELETON (skeleton));
  g_return_if_fail (auth != NULL);

  g_signal_connect (skeleton,
                    "g-authorize-method",
                    G_CALLBACK (on_g_authorize_method_cb),
                    (gpointer) auth);

  BONSAI_EXIT;
}
