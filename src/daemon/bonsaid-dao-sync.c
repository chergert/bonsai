/* bonsaid-dao-sync.c
 *
 * Copyright 2019-2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-dao-sync"

#include "config.h"

#include <libbonsai-dao.h>

#include "bonsaid-dao-log.h"
#include "bonsaid-dao-repositories.h"
#include "bonsaid-dao-sync.h"

struct _BonsaidDaoSync
{
  IpcDaoSyncSkeleton   parent;
  GMutex               mutex;
  GPtrArray           *logs;
};

static void
on_log_closed_cb (BonsaidDaoSync *self,
                  BonsaidDaoLog  *log)
{
  BONSAI_ENTRY;

  g_assert (BONSAID_IS_DAO_SYNC (self));
  g_assert (BONSAID_IS_DAO_LOG (log));

  g_mutex_lock (&self->mutex);
  g_ptr_array_remove (self->logs, log);
  g_mutex_unlock (&self->mutex);

  BONSAI_EXIT;
}

static gboolean
bonsaid_dao_sync_handle_begin (IpcDaoSync            *service,
                               GDBusMethodInvocation *invocation,
                               const gchar           *repository_id,
                               const gchar           *token)
{
  BonsaidDaoSync *self = (BonsaidDaoSync *)service;
  BonsaidDaoRepositories *repositories;
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaidDaoLog) log = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *path = NULL;
  GDBusConnection *connection;

  g_assert (BONSAID_IS_DAO_SYNC (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (repository_id != NULL);
  g_assert (token != NULL);

  repositories = bonsaid_dao_repositories_get_default ();

  if (!(repository = bonsaid_dao_repositories_get_repository (repositories, repository_id)))
    {

      g_dbus_method_invocation_return_error (invocation,
                                             G_IO_ERROR,
                                             G_IO_ERROR_NOT_FOUND,
                                             "No such repository '%s'",
                                             repository_id);
      return TRUE;
    }

  if (!(log = bonsaid_dao_log_new (repository, token, &error)))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return TRUE;
    }

  path = bonsai_object_path_random ("/org/gnome/Bonsai/Dao/Log/");
  connection = g_dbus_method_invocation_get_connection (invocation);

  g_signal_connect_object (log,
                           "closed",
                           G_CALLBACK (on_log_closed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (log), connection, path, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return TRUE;
    }

  g_mutex_lock (&self->mutex);
  g_ptr_array_add (self->logs, g_object_ref (log));
  g_mutex_unlock (&self->mutex);

  ipc_dao_sync_complete_begin (NULL, g_steal_pointer (&invocation), path);

  return TRUE;
}

static gboolean
bonsaid_dao_sync_handle_push (IpcDaoSync            *service,
                              GDBusMethodInvocation *invocation,
                              const gchar           *repository_id,
                              const gchar           *revision,
                              GVariant              *logv)
{
  BonsaidDaoRepositories *repositories;
  g_autoptr(BonsaiDaoRepository) repository = NULL;
  g_autoptr(BonsaiDaoTransaction) txn = NULL;
  g_autoptr(BonsaiDaoTransactionLog) log = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *current = NULL;
  BonsaiDaoTransactionLog *committed;
  const gchar *new_revision;

  g_assert (BONSAID_IS_DAO_SYNC (service));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (revision != NULL);
  g_assert (logv != NULL);
  g_assert (g_variant_is_of_type (logv, G_VARIANT_TYPE ("a(sa{sv})")));

  repositories = bonsaid_dao_repositories_get_default ();

  if (!(repository = bonsaid_dao_repositories_get_repository (repositories, repository_id)))
    {

      g_dbus_method_invocation_return_error (invocation,
                                             G_IO_ERROR,
                                             G_IO_ERROR_NOT_FOUND,
                                             "No such repository '%s'",
                                             repository_id);
      return TRUE;
    }

  if (!(txn = bonsai_dao_repository_begin_readwrite (repository, &error)))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return TRUE;
    }

  current = bonsai_dao_repository_get_last_revision (repository, txn);

  if (g_strcmp0 (current, revision) != 0)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_IO_ERROR,
                                             G_IO_ERROR_INVALID_ARGUMENT,
                                             "Got revision \"%s\" expected \"%s\"",
                                             revision, current);
      return TRUE;
    }

  if (!(log = bonsai_dao_transaction_log_from_variant (logv)))
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_IO_ERROR,
                                             G_IO_ERROR_INVALID_ARGUMENT,
                                             "Failed to decode transaction log");
      return TRUE;
    }

  bonsai_dao_transaction_apply_log (txn, log, FALSE);

  if (!bonsai_dao_transaction_commit (txn, NULL, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return TRUE;
    }

  committed = bonsai_dao_transaction_get_log (txn);
  new_revision = bonsai_dao_transaction_log_get_identifier (committed);

  ipc_dao_sync_complete_push (service, invocation, new_revision);

  return TRUE;
}

static void
sync_init (IpcDaoSyncIface *iface)
{
  iface->handle_begin = bonsaid_dao_sync_handle_begin;
  iface->handle_push = bonsaid_dao_sync_handle_push;
}

G_DEFINE_TYPE_WITH_CODE (BonsaidDaoSync, bonsaid_dao_sync, IPC_TYPE_DAO_SYNC_SKELETON,
                         G_IMPLEMENT_INTERFACE (IPC_TYPE_DAO_SYNC, sync_init))

static void
bonsaid_dao_sync_finalize (GObject *object)
{
  BonsaidDaoSync *self = (BonsaidDaoSync *)object;

  g_mutex_clear (&self->mutex);
  g_clear_pointer (&self->logs, g_ptr_array_unref);

  G_OBJECT_CLASS (bonsaid_dao_sync_parent_class)->finalize (object);
}

static void
bonsaid_dao_sync_class_init (BonsaidDaoSyncClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsaid_dao_sync_finalize;
}

static void
bonsaid_dao_sync_init (BonsaidDaoSync *self)
{
  g_mutex_init (&self->mutex);
  self->logs = g_ptr_array_new_with_free_func (g_object_unref);
}

BonsaidDaoSync *
bonsaid_dao_sync_new (void)
{
  return g_object_new (BONSAID_TYPE_DAO_SYNC, NULL);
}
