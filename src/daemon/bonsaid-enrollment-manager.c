/* bonsaid-enrollment-manager.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-enrollment-manager.h"

#include "config.h"

#include <string.h>

#include "bonsaid-enrollment-manager.h"
#include "bonsaid-pairing-manager.h"

struct _BonsaidEnrollmentManager
{
  GObject     parent_instance;
  GSettings  *settings;
  gchar     **enrolled;
};

G_DEFINE_TYPE (BonsaidEnrollmentManager, bonsaid_enrollment_manager, G_TYPE_OBJECT)

enum {
  PAIRING_REQUESTED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
on_settings_changed_enrolled_cb (BonsaidEnrollmentManager *self,
                                 const gchar              *key,
                                 GSettings                *settings)
{
  g_auto(GStrv) strv = NULL;
  GPtrArray *ar;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_ENROLLMENT_MANAGER (self));
  g_assert (G_IS_SETTINGS (settings));

  strv = g_settings_get_strv (settings, "enrolled");

  ar = g_ptr_array_new ();
  for (guint i = 0; strv[i]; i++)
    g_ptr_array_add (ar, g_utf8_strdown (strv[i], -1));
  g_ptr_array_add (ar, NULL);

  g_strfreev (self->enrolled);
  self->enrolled = (gchar **)g_ptr_array_free (ar, FALSE);

  BONSAI_EXIT;
}

static void
bonsaid_enrollment_manager_finalize (GObject *object)
{
  BonsaidEnrollmentManager *self = (BonsaidEnrollmentManager *)object;

  g_clear_pointer (&self->enrolled, g_strfreev);

  G_OBJECT_CLASS (bonsaid_enrollment_manager_parent_class)->finalize (object);
}

static void
bonsaid_enrollment_manager_class_init (BonsaidEnrollmentManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsaid_enrollment_manager_finalize;

  signals [PAIRING_REQUESTED] =
    g_signal_new ("pairing-requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE,
                  3,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                  G_TYPE_VARIANT,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE);
}

static void
bonsaid_enrollment_manager_init (BonsaidEnrollmentManager *self)
{
  self->settings = g_settings_new ("org.gnome.bonsai.daemon");

  g_signal_connect_object (self->settings,
                           "changed::enrolled",
                           G_CALLBACK (on_settings_changed_enrolled_cb),
                           self,
                           G_CONNECT_SWAPPED);

  on_settings_changed_enrolled_cb (self, NULL, self->settings);
}

void
bonsaid_enrollment_manager_enroll (BonsaidEnrollmentManager *self,
                                   GTlsCertificate          *certificate)
{
  g_autofree gchar *hash = NULL;
  g_autoptr(GPtrArray) ar = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAID_IS_ENROLLMENT_MANAGER (self));
  g_return_if_fail (G_IS_TLS_CERTIFICATE (certificate));

  hash = bonsai_tls_certificate_get_hash (certificate);

  if (g_strv_contains ((const gchar * const *)self->enrolled, hash))
    BONSAI_EXIT;

  ar = g_ptr_array_new ();
  for (guint i = 0; self->enrolled[i]; i++)
    g_ptr_array_add (ar, self->enrolled[i]);
  g_ptr_array_add (ar, hash);
  g_ptr_array_add (ar, NULL);

  g_settings_set_strv (self->settings,
                       "enrolled",
                       (const gchar * const *)ar->pdata);

  bonsaid_pairing_manager_hash_paired (bonsaid_pairing_manager_get_default (), hash);

  BONSAI_EXIT;
}

void
bonsaid_enrollment_manager_unenroll (BonsaidEnrollmentManager *self,
                                     GTlsCertificate          *certificate)
{
  g_autofree gchar *hash = NULL;
  g_autoptr(GPtrArray) ar = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAID_IS_ENROLLMENT_MANAGER (self));
  g_return_if_fail (G_IS_TLS_CERTIFICATE (certificate));

  hash = bonsai_tls_certificate_get_hash (certificate);

  if (!g_strv_contains ((const gchar * const *)self->enrolled, hash))
    BONSAI_EXIT;

  ar = g_ptr_array_new ();
  for (guint i = 0; self->enrolled[i]; i++)
    {
      if (!(strcasecmp (self->enrolled[i], hash) == 0))
        g_ptr_array_add (ar, self->enrolled[i]);
    }
  g_ptr_array_add (ar, NULL);

  g_settings_set_strv (self->settings,
                       "enrolled",
                       (const gchar * const *)ar->pdata);

  BONSAI_EXIT;
}

BonsaiAuthorization
bonsaid_enrollment_manager_authorize_identity (BonsaidEnrollmentManager *self,
                                               BonsaiIdentity           *identity)
{
  const gchar *hash;
  gboolean ret;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAID_IS_ENROLLMENT_MANAGER (self), BONSAI_AUTHORIZATION_NOT_AUTHORIZED);
  g_return_val_if_fail (BONSAI_IS_IDENTITY (identity), BONSAI_AUTHORIZATION_NOT_AUTHORIZED);

  if (!BONSAI_IS_TLS_IDENTITY (identity))
    BONSAI_RETURN (BONSAI_AUTHORIZATION_NOT_AUTHORIZED);

  hash = bonsai_tls_identity_get_hash (BONSAI_TLS_IDENTITY (identity));
  ret = g_strv_contains ((const gchar * const *)self->enrolled, hash);

  BONSAI_RETURN (ret);
}

BonsaidEnrollmentManager *
bonsaid_enrollment_manager_get_default (void)
{
  static BonsaidEnrollmentManager *instance;

  if (instance == NULL)
    {
      instance = g_object_new (BONSAID_TYPE_ENROLLMENT_MANAGER, NULL);
      g_object_add_weak_pointer (G_OBJECT (instance), (gpointer *)&instance);
    }

  return instance;
}

void
bonsaid_enrollment_manager_request_pairing (BonsaidEnrollmentManager *self,
                                            GTlsCertificate          *certificate,
                                            GVariant                 *info,
                                            const gchar              *token)
{
  g_autofree gchar *pem = NULL;

  BONSAI_ENTRY;

  g_return_if_fail (BONSAID_IS_ENROLLMENT_MANAGER (self));
  g_return_if_fail (G_IS_TLS_CERTIFICATE (certificate));
  g_return_if_fail (info != NULL);
  g_return_if_fail (g_variant_is_of_type (info, G_VARIANT_TYPE_VARDICT));
  g_return_if_fail (token != NULL);

  g_object_get (certificate,
                "certificate-pem", &pem,
                NULL);

  g_signal_emit (self, signals [PAIRING_REQUESTED], 0, pem, info, token);

  BONSAI_EXIT;
}
