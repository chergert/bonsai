/* bonsaid-isolation.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-isolation"

#include "config.h"

#include "bonsaid-isolation.h"

struct _BonsaidIsolation
{
  IpcIsolationSkeleton parent_instance;
};

static BonsaiClient *
get_client_from_invocation (GDBusMethodInvocation  *invocation,
                            GError                **error)
{
  GDBusConnection *conn = g_dbus_method_invocation_get_connection (invocation);
  BonsaiClient *client = bonsai_client_from_connection (conn);

  if (!client)
    g_set_error (error,
                 G_DBUS_ERROR,
                 G_DBUS_ERROR_FAILED,
                 "No client was found.");

  return client;
}

static gboolean
bonsaid_isolation_handle_isolate (IpcIsolation          *service,
                                  GDBusMethodInvocation *invocation,
                                  const gchar           *app_id)
{
  g_autoptr(BonsaiClient) client = NULL;
  g_autoptr(GError) error = NULL;
  BonsaiIdentity *identity;

  BONSAI_ENTRY;

  g_assert (IPC_IS_ISOLATION (service));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (app_id != NULL);

  if (!(client = get_client_from_invocation (invocation, &error)))
    {
      /* This must be a regular D-Bus session connection, otherwise we would have
       * a real client here. Just warn the caller about the error.
       */
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_RETURN (TRUE);
    }

  identity = bonsai_client_get_identity (client);

  /* Don't allow calling Isolate() after it has been set */
  if (bonsai_identity_get_app_id (identity) != NULL)
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "Not authorized to access Isolate");
      BONSAI_RETURN (TRUE);
    }

  /* Ensure that we have a valid application identifier */
  if (!g_application_id_is_valid (app_id))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "\"%s\" is not an application-id",
                                             app_id);
      /* If a client isolate call fails from invalid information, we want
       * to drop the connection so that we don't have a situation where a
       * proxy handler thinks it's isolated and then gives full access to
       * the application.
       */
      g_dbus_connection_close (g_dbus_method_invocation_get_connection (invocation), NULL, NULL, NULL);
      BONSAI_RETURN (TRUE);
    }

  bonsai_identity_set_app_id (identity, app_id);
  ipc_isolation_complete_isolate (service, g_steal_pointer (&invocation));
  BONSAI_RETURN (TRUE);
}

static void
isolation_iface_init (IpcIsolationIface *iface)
{
  iface->handle_isolate = bonsaid_isolation_handle_isolate;
}

G_DEFINE_TYPE_WITH_CODE (BonsaidIsolation, bonsaid_isolation, IPC_TYPE_ISOLATION_SKELETON,
                         G_IMPLEMENT_INTERFACE (IPC_TYPE_ISOLATION, isolation_iface_init))

BonsaidIsolation *
bonsaid_isolation_new (void)
{
  return g_object_new (BONSAID_TYPE_ISOLATION, NULL);
}

static void
bonsaid_isolation_class_init (BonsaidIsolationClass *klass)
{
}

static void
bonsaid_isolation_init (BonsaidIsolation *self)
{
}
