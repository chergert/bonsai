/* test-storage-create.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <libbonsai-storage.h>

#include "../daemon/bonsaid-storage.h"

typedef struct
{
  GTlsCertificate *server_certificate;
  BonsaiServer    *server;
  BonsaiIdentity  *server_identity;
  BonsaiClient    *client;
  GTlsCertificate *client_certificate;
  BonsaiIdentity  *client_identity;
  BonsaiStorage   *storage;
  BonsaiStorage   *proxy;
  GFile           *storage_root;
  GMainLoop       *main_loop;
  GError          *error;
  guint16          port;
  gchar            readbuf[4096];
} TestStorage;

#define test_storage_error(state) \
  G_STMT_START { \
    g_assert_nonnull (state); \
    g_assert_no_error (state->error); \
    g_main_loop_quit (state->main_loop); \
    g_assert_not_reached (); \
  } G_STMT_END

static void
rm_r (const gchar *dir)
{
  const gchar * const argv[] = { "rm", "-r", dir, NULL };
  g_autoptr(GError) error = NULL;

  g_assert_true (g_file_test (dir, G_FILE_TEST_IS_DIR));
  g_spawn_sync (".", (gchar **)argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, NULL, NULL, &error);
  g_assert_no_error (error);
}

static void
delete_cb (GObject      *object,
           GAsyncResult *result,
           gpointer      user_data)
{
  BonsaiStorage *storage = (BonsaiStorage *)object;
  TestStorage *state = user_data;
  gboolean r;

  r = bonsai_storage_call_delete_finish (storage, result, &state->error);
  g_assert_true (BONSAI_IS_STORAGE_PROXY (storage));
  g_assert_true (r);

  g_main_loop_quit (state->main_loop);
}

static void
copy_cb (GObject      *object,
         GAsyncResult *result,
         gpointer      user_data)
{
  BonsaiStorage *storage = (BonsaiStorage *)object;
  TestStorage *state = user_data;
  gboolean r;

  g_assert_true (BONSAI_IS_STORAGE (storage));
  g_assert_nonnull (state);

  r = bonsai_storage_call_copy_finish (storage, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  bonsai_storage_call_delete (state->proxy,
                              "/copied.txt",
                              NULL,
                              delete_cb,
                              state);
}

static void
move_cb (GObject      *object,
         GAsyncResult *result,
         gpointer      user_data)
{
  BonsaiStorage *storage = (BonsaiStorage *)object;
  TestStorage *state = user_data;
  g_autoptr(BonsaiProgress) progress = NULL;
  gboolean r;

  g_assert_true (BONSAI_IS_STORAGE (storage));
  g_assert_nonnull (state);

  r = bonsai_storage_call_move_finish (storage, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  progress = bonsai_client_create_progress (state->client, NULL, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (BONSAI_IS_PROGRESS (progress));

  bonsai_storage_call_copy (state->proxy,
                            "/moved.txt",
                            "/copied.txt",
                            G_FILE_COPY_NONE,
                            g_dbus_interface_skeleton_get_object_path (G_DBUS_INTERFACE_SKELETON (progress)),
                            NULL,
                            copy_cb,
                            state);
}

static void
next_files_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  TestStorage *state = user_data;
  g_autolist(GFileInfo) files = NULL;
  g_autoptr(BonsaiProgress) progress = NULL;

  g_assert_true (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert_nonnull (state);

  files = g_file_enumerator_next_files_finish (enumerator, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_nonnull (files);

  for (const GList *iter = files; iter; iter = iter->next)
    {
      GFileInfo *info = iter->data;

      g_print ("FOUND: %s", g_file_info_get_name (info));
    }

  progress = bonsai_client_create_progress (state->client, NULL, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (BONSAI_IS_PROGRESS (progress));

  bonsai_storage_call_move (state->proxy,
                            "/test.txt",
                            "/moved.txt",
                            G_FILE_COPY_NO_FALLBACK_FOR_MOVE,
                            g_dbus_interface_skeleton_get_object_path (G_DBUS_INTERFACE_SKELETON (progress)),
                            NULL,
                            move_cb,
                            state);
}

static void
enumerate_children_cb (GObject      *object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  BonsaiStorage *storage = (BonsaiStorage *)object;
  TestStorage *state = user_data;
  g_autofree gchar *object_path = NULL;
  g_autoptr(GFileEnumerator) enumerator = NULL;
  GDBusConnection *connection;

  g_assert_true (BONSAI_IS_STORAGE_PROXY (storage));
  g_assert_nonnull (state);

  bonsai_storage_call_enumerate_children_finish (storage, &object_path, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_nonnull (object_path);

  connection = g_dbus_proxy_get_connection (G_DBUS_PROXY (storage));
  enumerator = bonsai_client_file_enumerator_new (connection, object_path, NULL, &state->error);
  g_assert_no_error (state->error);
  g_assert_nonnull (enumerator);

  g_file_enumerator_next_files_async (enumerator,
                                      100,
                                      G_PRIORITY_DEFAULT,
                                      NULL,
                                      next_files_cb,
                                      state);
}

static void
close_input_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
  GInputStream *stream = (GInputStream *)object;
  TestStorage *state = user_data;
  gboolean r;

  g_assert_true (G_IS_INPUT_STREAM (stream));
  g_assert_nonnull (state);

  r = g_input_stream_close_finish (stream, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  bonsai_storage_call_enumerate_children (state->proxy,
                                          "/",
                                          G_FILE_ATTRIBUTE_STANDARD_NAME,
                                          G_FILE_QUERY_INFO_NONE,
                                          NULL,
                                          enumerate_children_cb,
                                          state);
}

static void
read_all_cb (GObject      *object,
             GAsyncResult *result,
             gpointer      user_data)
{
  GInputStream *stream = (GInputStream *)object;
  TestStorage *state = user_data;
  gboolean r;
  gsize n_read = 0;

  g_assert_true (BONSAI_IS_CLIENT_INPUT_STREAM (stream));
  g_assert_nonnull (state);

  r = g_input_stream_read_all_finish (stream, result, &n_read, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);
  g_assert_cmpint (n_read, >, 0);

  g_input_stream_close_async (stream, G_PRIORITY_DEFAULT, NULL, close_input_cb, state);
}

static void
open_read_cb (GObject      *object,
              GAsyncResult *result,
              gpointer      user_data)
{
  BonsaiStorage *storage = (BonsaiStorage *)object;
  g_autofree gchar *object_path = NULL;
  g_autoptr(GInputStream) stream = NULL;
  TestStorage *state = user_data;
  GDBusConnection *connection;
  gboolean r;

  g_assert_true (BONSAI_IS_STORAGE (storage));
  g_assert_nonnull (state);

  r = bonsai_storage_call_read_finish (storage, &object_path, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  connection = g_dbus_proxy_get_connection (G_DBUS_PROXY (storage));
  stream = bonsai_client_input_stream_new (connection, object_path);

  g_input_stream_read_all_async (stream,
                                 state->readbuf,
                                 sizeof state->readbuf,
                                 G_PRIORITY_DEFAULT,
                                 NULL,
                                 read_all_cb,
                                 state);
}

static void
close_cb (GObject      *object,
          GAsyncResult *result,
          gpointer      user_data)
{
  GOutputStream *stream = (GOutputStream *)object;
  TestStorage *state = user_data;
  gboolean r;

  g_assert_true (G_IS_OUTPUT_STREAM (stream));
  g_assert_nonnull (state);

  r = g_output_stream_close_finish (stream, result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  bonsai_storage_call_read (state->proxy,
                            "/test.txt",
                            NULL,
                            open_read_cb,
                            state);
}

static void
write_cb (GObject      *object,
          GAsyncResult *result,
          gpointer      user_data)
{
  static guint count;
  GOutputStream *stream = (GOutputStream *)object;
  TestStorage *state = user_data;
  gsize n_written;
  gboolean r;

  g_assert_true (G_IS_OUTPUT_STREAM (stream));
  g_assert_nonnull (state);

  count++;

  r = g_output_stream_write_all_finish (stream, result, &n_written, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (r);

  if (count == 1000)
    {
      g_output_stream_close_async (stream, G_PRIORITY_DEFAULT, NULL, close_cb, state);
      return;
    }

  g_output_stream_write_all_async (stream, "Next\n", 5, G_PRIORITY_DEFAULT,
                                   NULL, write_cb, state);
}

static void
create_file_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
  BonsaiStorage *service = BONSAI_STORAGE (object);
  g_autoptr(GOutputStream) stream = NULL;
  g_autofree gchar *object_path = NULL;
  GDBusConnection *connection;
  TestStorage *state = user_data;

  g_assert (BONSAI_IS_STORAGE (service));
  g_assert (state != NULL);

  if (!bonsai_storage_call_create_finish (service, &object_path, result, &state->error))
    test_storage_error (state);

  connection = g_dbus_proxy_get_connection (G_DBUS_PROXY (service));
  stream = bonsai_client_output_stream_new (connection, object_path);

  g_output_stream_write_all_async (stream,
                                   "Head\n",
                                   5,
                                   G_PRIORITY_DEFAULT,
                                   NULL,
                                   write_cb,
                                   state);
}

static BonsaiAuthorization
on_authorize_identity_cb (BonsaiServer   *server,
                          BonsaiIdentity *identity,
                          TestStorage    *state)
{
  GTlsCertificate *certificate;

  g_assert (BONSAI_IS_SERVER (server));
  g_assert (BONSAI_IS_TLS_IDENTITY (identity));
  g_assert (state != NULL);

  certificate = bonsai_tls_identity_get_certificate (BONSAI_TLS_IDENTITY (identity));
  g_assert_true (g_tls_certificate_is_same (certificate, state->client_certificate));

  return BONSAI_AUTHORIZATION_AUTHORIZED;
}

static void
test_storage_create_proxy_cb (GObject      *object,
                              GAsyncResult *result,
                              gpointer      user_data)
{
  BonsaiStorage *proxy;
  TestStorage *state = user_data;

  proxy = bonsai_storage_proxy_new_finish (result, &state->error);
  g_assert_no_error (state->error);
  g_assert_true (BONSAI_IS_STORAGE_PROXY (proxy));

  state->proxy = g_object_ref (proxy);

  bonsai_storage_call_create (proxy,
                              "/test.txt",
                              G_FILE_CREATE_NONE,
                              NULL,
                              create_file_cb,
                              state);
}

static void
on_connect_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  BonsaiClient *client = (BonsaiClient *)object;
  GDBusConnection *connection;
  TestStorage *state = user_data;

  g_assert (BONSAI_IS_CLIENT (client));
  g_assert (state != NULL);

  if (!bonsai_client_connect_finish (client, result, &state->error))
    test_storage_error (state);

  connection = bonsai_client_get_connection (client);
  bonsai_storage_proxy_new (connection,
                            G_DBUS_PROXY_FLAGS_NONE,
                            NULL,
                            "/org/gnome/Bonsai/Storage",
                            NULL,
                            test_storage_create_proxy_cb,
                            state);
}

static void
test_storage (void)
{
  g_autofree gchar *server_pub = NULL;
  g_autofree gchar *server_priv = NULL;
  g_autofree gchar *client_pub = NULL;
  g_autofree gchar *client_priv = NULL;
  g_autofree gchar *storage_path = NULL;
  g_autoptr(GSocketConnectable) connectable = NULL;
  g_autoptr(BonsaiEndPoint) end_point = NULL;
  TestStorage state = {0};
  gchar dirtmpl[] = "test-storage-XXXXXX";

  g_mkdtemp (dirtmpl);

  server_pub = g_build_filename (dirtmpl, "server.public.key", NULL);
  server_priv = g_build_filename (dirtmpl, "server.private.key", NULL);
  client_pub = g_build_filename (dirtmpl, "client.public.key", NULL);
  client_priv = g_build_filename (dirtmpl, "client.private.key", NULL);
  storage_path = g_build_filename (dirtmpl, "storage", NULL);

  g_assert_cmpint (g_mkdir_with_parents (storage_path, 0750), ==, 0);

  state.server_certificate =
    bonsai_tls_certificate_new_from_files_or_generate (server_pub, server_priv,
                                                       "US",
                                                       "GNOME:Testing",
                                                       NULL,
                                                       &state.error);
  g_assert_no_error (state.error);
  g_assert (G_IS_TLS_CERTIFICATE (state.server_certificate));

  state.client_certificate =
    bonsai_tls_certificate_new_from_files_or_generate (client_pub, client_priv,
                                                       "US",
                                                       "GNOME:Testing",
                                                       NULL,
                                                       &state.error);
  g_assert_no_error (state.error);
  g_assert (G_IS_TLS_CERTIFICATE (state.client_certificate));

  state.client_identity = bonsai_tls_identity_new (state.client_certificate);
  g_assert_true (BONSAI_IS_IDENTITY (state.client_identity));

  state.server_identity = bonsai_tls_identity_new (state.server_certificate);
  g_assert_true (BONSAI_IS_IDENTITY (state.server_identity));

  state.main_loop = g_main_loop_new (NULL, FALSE);
  g_main_context_push_thread_default (g_main_loop_get_context (state.main_loop));

  state.server = bonsai_server_new (state.server_certificate);
  g_assert_true (BONSAI_IS_SERVER (state.server));

  state.client = bonsai_client_new (state.client_identity);
  g_assert_true (BONSAI_IS_CLIENT (state.client));

  state.storage_root = g_file_new_for_path (storage_path);
  g_assert_true (G_IS_FILE (state.storage_root));

  state.storage = BONSAI_STORAGE (bonsaid_storage_new (state.storage_root));
  g_assert_true (BONSAID_IS_STORAGE (state.storage));
  g_assert_true (g_file_equal (state.storage_root, bonsaid_storage_get_root (BONSAID_STORAGE (state.storage))));

  bonsai_server_export (state.server,
                        BONSAI_REALM_AUTHORIZED,
                        G_DBUS_INTERFACE_SKELETON (state.storage),
                        "/org/gnome/Bonsai/Storage");

  state.port = g_socket_listener_add_any_inet_port (G_SOCKET_LISTENER (state.server), NULL, &state.error);
  g_assert_no_error (state.error);
  g_assert_cmpint (state.port, >, 0);

  connectable = g_network_address_parse ("localhost", state.port, &state.error);
  g_assert_nonnull (connectable);
  g_assert_true (G_IS_SOCKET_CONNECTABLE (connectable));

  end_point = bonsai_socket_end_point_new (connectable);
  g_assert_true (BONSAI_IS_SOCKET_END_POINT (end_point));

  g_signal_connect (state.server,
                    "authorize-identity",
                    G_CALLBACK (on_authorize_identity_cb),
                    &state);

  g_socket_service_start (G_SOCKET_SERVICE (state.server));

  bonsai_client_connect_async (state.client,
                               end_point,
                               state.server_identity,
                               NULL,
                               on_connect_cb,
                               &state);

  g_main_loop_run (state.main_loop);
  g_assert_no_error (state.error);

  g_clear_object (&state.client_identity);
  g_clear_object (&state.client_certificate);
  g_clear_object (&state.server);
  g_clear_object (&state.server_certificate);
  g_clear_object (&state.server_identity);
  g_clear_object (&state.storage);
  g_clear_object (&state.proxy);
  g_clear_pointer (&state.main_loop, g_main_loop_unref);
  g_clear_pointer (&state.error, g_error_free);

  rm_r (dirtmpl);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Bonsai/Storage", test_storage);
  return g_test_run ();
}
