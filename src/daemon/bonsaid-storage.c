/* bonsaid-storage.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-storage"

#include "config.h"

#include "bonsaid-storage.h"
#include "bonsaid-utils.h"

typedef struct
{
  GDBusMethodInvocation *invocation;
  GFile *source;
  GFile *dest;
  BonsaiProgress *progress;
  GFileCopyFlags flags;
} CopyMove;

struct _BonsaidStorage
{
  BonsaiStorageSkeleton parent_instance;
  GSettings *settings;
  GFile *root;
};

enum {
  PROP_0,
  PROP_ROOT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

#define return_val_with_error(invocation, error, val)           \
  G_STMT_START {                                                \
    g_dbus_method_invocation_return_gerror (invocation, error); \
    invocation = NULL;                                          \
    BONSAI_RETURN (val);                                        \
  } G_STMT_END

#define return_with_error(invocation, error)                    \
  G_STMT_START {                                                \
    g_dbus_method_invocation_return_gerror (invocation, error); \
    invocation = NULL;                                          \
    BONSAI_EXIT;                                                \
  } G_STMT_END

static void
copy_move_free (CopyMove *move)
{
  g_clear_object (&move->invocation);
  g_clear_object (&move->source);
  g_clear_object (&move->dest);
  g_clear_object (&move->progress);
  g_slice_free (CopyMove, move);
}

static void
progress_callback (goffset  current_num_bytes,
                   goffset  total_num_bytes,
                   gpointer user_data)
{
  BonsaiProgress *progress = user_data;

  /* We might want to rate-limit here? */
  bonsai_progress_set_current (progress, current_num_bytes);
  bonsai_progress_set_total (progress, total_num_bytes);
}

static GCancellable *
get_cancellable_from_invocation (GDBusMethodInvocation *invocation)
{
  GDBusConnection *conn = g_dbus_method_invocation_get_connection (invocation);
  BonsaiClient *client = bonsai_client_from_connection (conn);

  return client ? bonsai_client_get_cancellable (client) : NULL;
}

static BonsaiClient *
get_client_from_invocation (GDBusMethodInvocation  *invocation,
                            GError                **error)
{
  GDBusConnection *conn = g_dbus_method_invocation_get_connection (invocation);
  BonsaiClient *client = bonsai_client_from_connection (conn);

  if (!client)
    g_set_error (error,
                 G_DBUS_ERROR,
                 G_DBUS_ERROR_FAILED,
                 "No client was found.");

  return client;
}

static GFile *
bonsaid_storage_get_file (BonsaidStorage  *self,
                          const gchar     *path,
                          GError         **error)
{
  g_autofree gchar *canonical = NULL;
  g_autoptr(GFile) child = NULL;

  g_assert (BONSAID_IS_STORAGE (self));
  g_assert (path != NULL);

  path = canonical = g_canonicalize_filename (path, "/");
  while (*path == '/')
    path++;

  child = g_file_get_child (self->root, path);
  if (g_file_has_prefix (child, self->root) || g_file_equal (child, self->root))
    return g_steal_pointer (&child);

  g_set_error (error,
               G_IO_ERROR,
               G_IO_ERROR_INVALID_FILENAME,
               "The filename \"%s\" is invalid",
               g_file_peek_path (child));

  return NULL;
}

static void
bonsaid_storage_create_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(BonsaiServerOutputStream) wrapper = NULL;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *object_path = NULL;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(stream = g_file_create_finish (file, result, &error)))
    return_with_error (invocation, error);

  if (!(client = get_client_from_invocation (invocation, &error)))
    return_with_error (invocation, error);

  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Storage/Streams/");
  wrapper = bonsai_server_output_stream_new (G_OUTPUT_STREAM (stream));

  if (!bonsai_client_export (client,
                             G_DBUS_INTERFACE_SKELETON (wrapper),
                             object_path,
                             &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_create (NULL,
                                  g_steal_pointer (&invocation),
                                  object_path);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_create (BonsaiStorage         *storage,
                               GDBusMethodInvocation *invocation,
                               const gchar           *path,
                               guint                  flags)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_STORAGE (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  cancellable = get_cancellable_from_invocation (invocation);
  flags &= (G_FILE_CREATE_PRIVATE | G_FILE_CREATE_REPLACE_DESTINATION);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  g_file_create_async (file,
                       flags,
                       G_PRIORITY_DEFAULT,
                       cancellable,
                       bonsaid_storage_create_cb,
                       g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_read_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(BonsaiInputStream) wrapper = NULL;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *object_path = NULL;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(stream = g_file_create_finish (file, result, &error)))
    return_with_error (invocation, error);

  if (!(client = get_client_from_invocation (invocation, &error)))
    return_with_error (invocation, error);

  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Storage/Streams/");
  wrapper = bonsai_server_input_stream_new (G_INPUT_STREAM (stream));

  if (!bonsai_client_export (client,
                             G_DBUS_INTERFACE_SKELETON (wrapper),
                             object_path,
                             &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_read (NULL,
                                g_steal_pointer (&invocation),
                                object_path);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_read (BonsaiStorage         *storage,
                             GDBusMethodInvocation *invocation,
                             const gchar           *path)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAI_IS_STORAGE (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  g_file_read_async (file,
                     G_PRIORITY_DEFAULT,
                     cancellable,
                     bonsaid_storage_read_cb,
                     g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_handle_delete_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_file_delete_finish (file, result, &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_delete (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_delete (BonsaiStorage         *storage,
                               GDBusMethodInvocation *invocation,
                               const gchar           *path)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  if (g_file_equal (self->root, file))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "Cannot remove storage root");
      BONSAI_RETURN (TRUE);
    }

  g_file_delete_async (file,
                       G_PRIORITY_DEFAULT,
                       cancellable,
                       bonsaid_storage_handle_delete_cb,
                       g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_handle_trash_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!g_file_trash_finish (file, result, &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_trash (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_trash (BonsaiStorage         *storage,
                              GDBusMethodInvocation *invocation,
                              const gchar           *path)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  if (g_file_equal (self->root, file))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "Cannot remove storage root");
      BONSAI_RETURN (TRUE);
    }

  g_file_trash_async (file,
                      G_PRIORITY_DEFAULT,
                      cancellable,
                      bonsaid_storage_handle_trash_cb,
                      g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_handle_replace_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autofree gchar *object_path = NULL;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(BonsaiServerOutputStream) wrapper = NULL;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GError) error = NULL;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(stream = g_file_replace_finish (file, result, &error)))
    return_with_error (invocation, error);

  if (!(client = get_client_from_invocation (invocation, &error)))
    return_with_error (invocation, error);

  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Storage/Streams/");
  wrapper = bonsai_server_output_stream_new (G_OUTPUT_STREAM (stream));

  if (!bonsai_client_export (client,
                             G_DBUS_INTERFACE_SKELETON (wrapper),
                             object_path,
                             &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_replace (NULL,
                                   g_steal_pointer (&invocation),
                                   object_path);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_replace (BonsaiStorage         *storage,
                                GDBusMethodInvocation *invocation,
                                const gchar           *path,
                                const gchar           *etag,
                                gboolean               make_backup,
                                guint                  flags)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  if (!etag || !etag[0])
    etag = NULL;

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  g_file_replace_async (file,
                        etag,
                        make_backup,
                        flags,
                        G_PRIORITY_DEFAULT,
                        cancellable,
                        bonsaid_storage_handle_replace_cb,
                        g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_handle_append_to_cb (GObject      *object,
                                     GAsyncResult *result,
                                     gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autofree gchar *object_path = NULL;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(BonsaiServerOutputStream) wrapper = NULL;
  g_autoptr(GFileOutputStream) stream = NULL;
  g_autoptr(GError) error = NULL;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(stream = g_file_append_to_finish (file, result, &error)))
    return_with_error (invocation, error);

  if (!(client = get_client_from_invocation (invocation, &error)))
    return_with_error (invocation, error);

  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Storage/Streams/");
  wrapper = bonsai_server_output_stream_new (G_OUTPUT_STREAM (stream));

  if (!bonsai_client_export (client,
                             G_DBUS_INTERFACE_SKELETON (wrapper),
                             object_path,
                             &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_append_to (NULL,
                                     g_steal_pointer (&invocation),
                                     object_path);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_append_to (BonsaiStorage         *storage,
                                  GDBusMethodInvocation *invocation,
                                  const gchar           *path,
                                  guint                  flags)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (storage));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  flags &= (G_FILE_CREATE_PRIVATE | G_FILE_CREATE_REPLACE_DESTINATION);

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  g_file_append_to_async (file,
                          flags,
                          G_PRIORITY_DEFAULT,
                          cancellable,
                          bonsaid_storage_handle_append_to_cb,
                          g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_handle_query_info_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GFileInfo) info = NULL;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(info = g_file_query_info_finish (file, result, &error)))
    return_with_error (invocation, error);

  bonsai_storage_complete_query_info (NULL,
                                      g_steal_pointer (&invocation),
                                      bonsai_file_info_serialize (info));

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_query_info (BonsaiStorage         *storage,
                                   GDBusMethodInvocation *invocation,
                                   const gchar           *path,
                                   const gchar           *attributes,
                                   guint                  flags)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (storage));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  flags &= G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS;

  g_file_query_info_async (file,
                           attributes,
                           flags,
                           G_PRIORITY_DEFAULT,
                           cancellable,
                           bonsaid_storage_handle_query_info_cb,
                           g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsaid_storage_handle_monitor (BonsaiStorage         *storage,
                                GDBusMethodInvocation *invocation,
                                const gchar           *path,
                                guint                  flags)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autofree gchar *object_path = NULL;
  g_autoptr(GFileMonitor) monitor = NULL;
  g_autoptr(BonsaiFileMonitor) wrapper = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  BonsaiClient *client;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (storage));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  if (!(client = get_client_from_invocation (invocation, &error)))
    return_val_with_error (invocation, error, TRUE);

  flags &= (G_FILE_MONITOR_WATCH_MOUNTS |
            G_FILE_MONITOR_SEND_MOVED |
            G_FILE_MONITOR_WATCH_HARD_LINKS |
            G_FILE_MONITOR_WATCH_MOVES);

  if (!(monitor = g_file_monitor (file, flags, cancellable, &error)))
    return_val_with_error (invocation, error, TRUE);

  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Storage/FileMonitor/");
  wrapper = bonsai_server_file_monitor_new (monitor, self->root);

  if (!bonsai_client_export (client,
                             G_DBUS_INTERFACE_SKELETON (wrapper),
                             object_path,
                             &error))
    return_val_with_error (invocation, error, TRUE);

  bonsai_storage_complete_monitor (NULL,
                                   g_steal_pointer (&invocation),
                                   object_path);

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_enumerate_children_cb (GObject      *object,
                                       GAsyncResult *result,
                                       gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(BonsaiFileEnumerator) wrapper = NULL;
  g_autoptr(GFileEnumerator) enumerator = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *object_path = NULL;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(enumerator = g_file_enumerate_children_finish (file, result, &error)))
    return_with_error (invocation, error);

  if (!(client = get_client_from_invocation (invocation, &error)))
    return_with_error (invocation, error);

  wrapper = bonsai_server_file_enumerator_new (enumerator);
  object_path = bonsai_object_path_random ("/org/gnome/Bonsai/Storage/FileEnumerator/");

  if (!bonsai_client_export (client,
                             G_DBUS_INTERFACE_SKELETON (wrapper),
                             object_path,
                             &error))
    return_with_error (invocation, error);

  bonsai_storage_complete_enumerate_children (NULL,
                                              g_steal_pointer (&invocation),
                                              object_path);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_enumerate_children (BonsaiStorage         *storage,
                                           GDBusMethodInvocation *invocation,
                                           const gchar           *path,
                                           const gchar           *attributes,
                                           guint                  flags)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) file = NULL;
  GCancellable *cancellable;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (storage));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (path != NULL);

  if (attributes == NULL || *attributes == '\0')
    attributes = G_FILE_ATTRIBUTE_STANDARD_NAME;

  flags &= (G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS);

  cancellable = get_cancellable_from_invocation (invocation);

  if (!(file = bonsaid_storage_get_file (self, path, &error)))
    return_val_with_error (invocation, error, TRUE);

  g_file_enumerate_children_async (file,
                                   attributes,
                                   flags,
                                   G_PRIORITY_DEFAULT,
                                   cancellable,
                                   bonsaid_storage_enumerate_children_cb,
                                   g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_do_copy (GTask        *task,
                         gpointer      source_object,
                         gpointer      task_data,
                         GCancellable *cancellable)
{
  g_autoptr(GError) error = NULL;
  CopyMove *copy = task_data;

  BONSAI_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (copy != NULL);
  g_assert (G_IS_FILE (copy->source));
  g_assert (G_IS_FILE (copy->dest));

  if (!g_file_copy (copy->source,
                    copy->dest,
                    copy->flags,
                    cancellable,
                    copy->progress ? progress_callback : NULL,
                    copy->progress,
                    &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&copy->invocation), error);
  else
    bonsai_storage_complete_move (source_object, g_steal_pointer (&copy->invocation));

  if (copy->progress)
    bonsai_progress_call_finished (copy->progress, NULL, NULL, NULL);

  g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_copy (BonsaiStorage         *storage,
                             GDBusMethodInvocation *invocation,
                             const gchar           *source,
                             const gchar           *dest,
                             guint                  flags,
                             const gchar           *progress_path)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(BonsaiProgress) progress = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) source_file = NULL;
  g_autoptr(GFile) dest_file = NULL;
  g_autoptr(GTask) task = NULL;
  CopyMove *copy;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (storage));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(source_file = bonsaid_storage_get_file (self, source, &error)))
    return_val_with_error (invocation, error, TRUE);

  if (!(dest_file = bonsaid_storage_get_file (self, dest, &error)))
    return_val_with_error (invocation, error, TRUE);

  flags &= (G_FILE_COPY_OVERWRITE |
            G_FILE_COPY_BACKUP |
            G_FILE_COPY_NOFOLLOW_SYMLINKS |
            G_FILE_COPY_ALL_METADATA |
            G_FILE_COPY_NO_FALLBACK_FOR_MOVE |
            G_FILE_COPY_TARGET_DEFAULT_PERMS);

  if (progress_path && g_variant_is_object_path (progress_path))
    {
      GDBusConnection *connection;

      connection = g_dbus_method_invocation_get_connection (invocation);
      progress = bonsai_progress_proxy_new_sync (connection,
                                                 (G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
                                                  G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
                                                  G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START),
                                                 NULL,
                                                 progress_path,
                                                 NULL, NULL);
    }

  copy = g_slice_new0 (CopyMove);
  copy->invocation = g_steal_pointer (&invocation);
  copy->source = g_steal_pointer (&source_file);
  copy->dest = g_steal_pointer (&dest_file);
  copy->flags = flags;
  copy->progress = g_steal_pointer (&progress);

  task = g_task_new (storage, NULL, NULL, NULL);
  g_task_set_task_data (task, copy, (GDestroyNotify)copy_move_free);
  g_task_run_in_thread (task, bonsaid_storage_do_copy);

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_storage_do_move (GTask        *task,
                         gpointer      source_object,
                         gpointer      task_data,
                         GCancellable *cancellable)
{
  g_autoptr(GError) error = NULL;
  CopyMove *move = task_data;

  BONSAI_ENTRY;

  g_assert (G_IS_TASK (task));
  g_assert (move != NULL);
  g_assert (G_IS_FILE (move->source));
  g_assert (G_IS_FILE (move->dest));

  if (!g_file_move (move->source,
                    move->dest,
                    move->flags,
                    cancellable,
                    move->progress ? progress_callback : NULL,
                    move->progress,
                    &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&move->invocation), error);
  else
    bonsai_storage_complete_move (source_object, g_steal_pointer (&move->invocation));

  if (move->progress)
    bonsai_progress_call_finished (move->progress, NULL, NULL, NULL);

  g_task_return_boolean (task, TRUE);

  BONSAI_EXIT;
}

static gboolean
bonsaid_storage_handle_move (BonsaiStorage         *storage,
                             GDBusMethodInvocation *invocation,
                             const gchar           *source,
                             const gchar           *dest,
                             guint                  flags,
                             const gchar           *progress_path)
{
  BonsaidStorage *self = (BonsaidStorage *)storage;
  g_autoptr(BonsaiProgress) progress = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) source_file = NULL;
  g_autoptr(GFile) dest_file = NULL;
  g_autoptr(GTask) task = NULL;
  CopyMove *move;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (storage));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(source_file = bonsaid_storage_get_file (self, source, &error)))
    return_val_with_error (invocation, error, TRUE);

  if (!(dest_file = bonsaid_storage_get_file (self, dest, &error)))
    return_val_with_error (invocation, error, TRUE);

  flags &= (G_FILE_COPY_OVERWRITE |
            G_FILE_COPY_BACKUP |
            G_FILE_COPY_NOFOLLOW_SYMLINKS |
            G_FILE_COPY_ALL_METADATA |
            G_FILE_COPY_NO_FALLBACK_FOR_MOVE |
            G_FILE_COPY_TARGET_DEFAULT_PERMS);

  if (progress_path && g_variant_is_object_path (progress_path))
    {
      GDBusConnection *connection;

      connection = g_dbus_method_invocation_get_connection (invocation);
      progress = bonsai_progress_proxy_new_sync (connection,
                                                 (G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
                                                  G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
                                                  G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START),
                                                 NULL,
                                                 progress_path,
                                                 NULL, NULL);
    }

  move = g_slice_new0 (CopyMove);
  move->invocation = g_steal_pointer (&invocation);
  move->source = g_steal_pointer (&source_file);
  move->dest = g_steal_pointer (&dest_file);
  move->flags = flags;
  move->progress = g_steal_pointer (&progress);

  task = g_task_new (storage, NULL, NULL, NULL);
  g_task_set_task_data (task, move, (GDestroyNotify)copy_move_free);
  g_task_run_in_thread (task, bonsaid_storage_do_move);

  BONSAI_RETURN (TRUE);
}

static void
storage_iface_init (BonsaiStorageIface *iface)
{
  iface->handle_create = bonsaid_storage_handle_create;
  iface->handle_read = bonsaid_storage_handle_read;
  iface->handle_delete = bonsaid_storage_handle_delete;
  iface->handle_trash = bonsaid_storage_handle_trash;
  iface->handle_replace = bonsaid_storage_handle_replace;
  iface->handle_append_to = bonsaid_storage_handle_append_to;
  iface->handle_query_info = bonsaid_storage_handle_query_info;
  iface->handle_monitor = bonsaid_storage_handle_monitor;
  iface->handle_enumerate_children = bonsaid_storage_handle_enumerate_children;
  iface->handle_copy = bonsaid_storage_handle_copy;
  iface->handle_move = bonsaid_storage_handle_move;
}

G_DEFINE_TYPE_WITH_CODE (BonsaidStorage, bonsaid_storage, BONSAI_TYPE_STORAGE_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_STORAGE, storage_iface_init))

/**
 * bonsaid_storage_new:
 *
 * Create a new #BonsaidStorage.
 *
 * Returns: (transfer full): a newly created #BonsaidStorage
 */
BonsaidStorage *
bonsaid_storage_new (GFile *root)
{
  g_return_val_if_fail (!root || G_IS_FILE (root), NULL);

  return g_object_new (BONSAID_TYPE_STORAGE,
                       "root", root,
                       NULL);
}

static void
bonsaid_storage_root_changed_cb (BonsaidStorage *self,
                                 const gchar    *key,
                                 GSettings      *settings)
{
  g_autofree gchar *root = NULL;
  g_autofree gchar *canonical = NULL;
  g_autoptr(GFile) file = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (self));
  g_assert (G_IS_SETTINGS (settings));

  root = g_settings_get_string (settings, "root");
  canonical = g_canonicalize_filename (root, g_get_home_dir ());
  file = g_file_new_for_path (canonical);

  if (!g_file_query_exists (file, NULL))
    {
      g_autoptr(GError) error = NULL;

      if (!g_file_make_directory (file, NULL, &error))
        g_warning ("Failed to create storage directory: %s", error->message);
    }

  if (g_set_object (&self->root, file))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ROOT]);

  BONSAI_EXIT;
}

static void
bonsaid_storage_constructed (GObject *object)
{
  BonsaidStorage *self = (BonsaidStorage *)object;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_STORAGE (self));

  if (self->root == NULL)
    {
      g_signal_connect_object (self->settings,
                               "changed::root",
                               G_CALLBACK (bonsaid_storage_root_changed_cb),
                               self,
                               G_CONNECT_SWAPPED);
      bonsaid_storage_root_changed_cb (self, "root", self->settings);
    }

  G_OBJECT_CLASS (bonsaid_storage_parent_class)->constructed (object);

  BONSAI_EXIT;
}

static void
bonsaid_storage_finalize (GObject *object)
{
  BonsaidStorage *self = (BonsaidStorage *)object;

  g_clear_object (&self->root);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (bonsaid_storage_parent_class)->finalize (object);
}

static void
bonsaid_storage_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  BonsaidStorage *self = BONSAID_STORAGE (object);

  switch (prop_id)
    {
    case PROP_ROOT:
      g_value_set_object (value, self->root);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsaid_storage_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  BonsaidStorage *self = BONSAID_STORAGE (object);

  switch (prop_id)
    {
    case PROP_ROOT:
      self->root = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsaid_storage_class_init (BonsaidStorageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = bonsaid_storage_constructed;
  object_class->finalize = bonsaid_storage_finalize;
  object_class->get_property = bonsaid_storage_get_property;
  object_class->set_property = bonsaid_storage_set_property;

  /**
   * BonsaidStorage:root:
   *
   * The :root property is the root directory of the storage.
   *
   * Since: 0.2
   */
  properties [PROP_ROOT] =
    g_param_spec_object ("root",
                         "Root",
                         "The root of the storage",
                         G_TYPE_FILE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsaid_storage_init (BonsaidStorage *self)
{
  self->settings = g_settings_new ("org.gnome.bonsai.daemon.storage");
}

/**
 * bonsaid_storage_get_root:
 * @self: a #BonsaidStorage
 *
 * Gets the :root property.
 *
 * Returns: (transfer none): a #BonsaidStorage or %NULL
 *
 * Since: 0.2
 */
GFile *
bonsaid_storage_get_root (BonsaidStorage *self)
{
  g_return_val_if_fail (BONSAID_IS_STORAGE (self), NULL);

  return self->root;
}
