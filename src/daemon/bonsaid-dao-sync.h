/* bonsaid-dao-sync.h
 *
 * Copyright 2019-2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libbonsai-dao.h>

#include "ipc-dao-sync.h"

G_BEGIN_DECLS

#define BONSAID_TYPE_DAO_SYNC (bonsaid_dao_sync_get_type())

G_DECLARE_FINAL_TYPE (BonsaidDaoSync, bonsaid_dao_sync, BONSAID, DAO_SYNC, IpcDaoSyncSkeleton)

BonsaidDaoSync *bonsaid_dao_sync_new (void);

G_END_DECLS
