/* bonsaid-enrollment.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-enrollment"

#include "config.h"

#include <libbonsai.h>

#include "bonsaid-application.h"
#include "bonsaid-enrollment-manager.h"
#include "bonsaid-pairing-manager.h"
#include "bonsaid-enrollment.h"
#include "bonsaid-utils.h"

struct _BonsaidEnrollment
{
  BonsaiEnrollmentSkeleton parent_instance;
};

static gboolean
bonsaid_enrollment_handle_enroll (BonsaiEnrollment      *enrollment,
                                  GDBusMethodInvocation *invocation,
                                  const gchar           *pem)
{
  BonsaidEnrollment *self = (BonsaidEnrollment *)enrollment;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_ENROLLMENT (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (pem != NULL);

  if (!(certificate = g_tls_certificate_new_from_pem (pem, -1, &error)))
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
    }
  else
    {
      bonsaid_enrollment_manager_enroll (bonsaid_enrollment_manager_get_default (),
                                         certificate);
      bonsai_enrollment_complete_enroll (enrollment, g_steal_pointer (&invocation));
    }

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsaid_enrollment_handle_unenroll (BonsaiEnrollment      *enrollment,
                                    GDBusMethodInvocation *invocation,
                                    const gchar           *pem)
{
  BonsaidEnrollment *self = (BonsaidEnrollment *)enrollment;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_ENROLLMENT (self));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (pem != NULL);

  if (!(certificate = g_tls_certificate_new_from_pem (pem, -1, &error)))
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
    }
  else
    {
      bonsaid_enrollment_manager_unenroll (bonsaid_enrollment_manager_get_default (),
                                           certificate);
      bonsai_enrollment_complete_unenroll (enrollment, g_steal_pointer (&invocation));
    }

  BONSAI_RETURN (TRUE);
}

static void
enrollment_iface_init (BonsaiEnrollmentIface *iface)
{
  iface->handle_enroll = bonsaid_enrollment_handle_enroll;
  iface->handle_unenroll = bonsaid_enrollment_handle_unenroll;
}

G_DEFINE_TYPE_WITH_CODE (BonsaidEnrollment, bonsaid_enrollment, BONSAI_TYPE_ENROLLMENT_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_ENROLLMENT, enrollment_iface_init))

/**
 * bonsaid_enrollment_new:
 *
 * Create a new #BonsaidEnrollment.
 *
 * Returns: (transfer full): a newly created #BonsaidEnrollment
 */
BonsaidEnrollment *
bonsaid_enrollment_new (void)
{
  return g_object_new (BONSAID_TYPE_ENROLLMENT, NULL);
}

static void
bonsaid_enrollment_pairing_requested_cb (BonsaidEnrollment        *self,
                                         const gchar              *pem,
                                         GVariant                 *info,
                                         const gchar              *token,
                                         BonsaidEnrollmentManager *enrollment)
{
  BONSAI_ENTRY;

  g_assert (BONSAID_IS_ENROLLMENT (self));
  g_assert (pem != NULL);
  g_assert (info != NULL);
  g_assert (token != NULL);
  g_assert (BONSAID_IS_ENROLLMENT_MANAGER (enrollment));

  bonsai_enrollment_emit_pairing_requested (BONSAI_ENROLLMENT (self), pem, info, token);

  BONSAI_EXIT;
}

static void
bonsaid_enrollment_class_init (BonsaidEnrollmentClass *klass)
{
}

static void
bonsaid_enrollment_init (BonsaidEnrollment *self)
{
  static const BonsaidMethodAuth method_auth[] = {
    { "Enroll",   BONSAI_REALM_AUTHORIZED, TRUE },
    { "Unenroll", BONSAI_REALM_AUTHORIZED, TRUE },
    { NULL }
  };

  bonsaid_set_method_auth (G_DBUS_INTERFACE_SKELETON (self), method_auth);

  g_signal_connect_object (bonsaid_enrollment_manager_get_default (),
                           "pairing-requested",
                           G_CALLBACK (bonsaid_enrollment_pairing_requested_cb),
                           self,
                           G_CONNECT_SWAPPED);
}
