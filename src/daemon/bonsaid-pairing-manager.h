/* bonsaid-pairing-manager.h
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libbonsai.h>

G_BEGIN_DECLS

#define BONSAID_TYPE_PAIRING_MANAGER (bonsaid_pairing_manager_get_type())

G_DECLARE_FINAL_TYPE (BonsaidPairingManager, bonsaid_pairing_manager, BONSAID, PAIRING_MANAGER, GObject)

BonsaidPairingManager *bonsaid_pairing_manager_get_default   (void);
gboolean               bonsaid_pairing_manager_get_pairing   (BonsaidPairingManager   *self);
void                   bonsaid_pairing_manager_begin         (BonsaidPairingManager   *self);
void                   bonsaid_pairing_manager_end           (BonsaidPairingManager   *self);
void                   bonsaid_pairing_manager_hash_paired   (BonsaidPairingManager   *self,
                                                              const gchar             *hash);
gchar                 *bonsaid_pairing_manager_request_token (BonsaidPairingManager   *self,
                                                              BonsaiClient            *client,
                                                              GTlsCertificate         *certificate,
                                                              GError                 **error);
void                   bonsaid_pairing_manager_pair_async    (BonsaidPairingManager   *self,
                                                              BonsaiClient            *client,
                                                              GVariant                *info,
                                                              GCancellable            *cancellable,
                                                              GAsyncReadyCallback      callback,
                                                              gpointer                 user_data);
gboolean               bonsaid_pairing_manager_pair_finish   (BonsaidPairingManager   *self,
                                                              GAsyncResult            *result,
                                                              GError                 **error);

G_END_DECLS
