/* bonsaid-application.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-application"

#include "config.h"

#include <libbonsai.h>

#include "bonsaid-application.h"
#include "bonsaid-dao-sync.h"
#include "bonsaid-enrollment.h"
#include "bonsaid-enrollment-manager.h"
#include "bonsaid-isolation.h"
#include "bonsaid-pairing.h"
#include "bonsaid-pairing-manager.h"
#include "bonsaid-storage.h"

struct _BonsaidApplication
{
  GApplication       parent_instance;
  BonsaiServer      *server;
  BonsaidEnrollment *enrollment;
  BonsaidPairing    *pairing;
};

G_DEFINE_TYPE (BonsaidApplication, bonsaid_application, G_TYPE_APPLICATION)

static void
bonsaid_application_activate (GApplication *app)
{
  BonsaidApplication *self = (BonsaidApplication *)app;

  g_assert (BONSAID_IS_APPLICATION (self));
}

static BonsaiAuthorization
on_authorize_identity_cb (BonsaidApplication *self,
                          BonsaiIdentity     *identity,
                          BonsaiServer       *server)
{
  BonsaidEnrollmentManager *enrollment;
  BonsaidPairingManager *pairing;
  BonsaiAuthorization auth;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_APPLICATION (self));
  g_assert (BONSAI_IS_IDENTITY (identity));
  g_assert (BONSAI_IS_SERVER (server));

  /* See if the client should have full authorization */
  enrollment = bonsaid_enrollment_manager_get_default ();
  auth = bonsaid_enrollment_manager_authorize_identity (enrollment, identity);
  g_return_val_if_fail (auth == BONSAI_AUTHORIZATION_NOT_AUTHORIZED ||
                        auth == BONSAI_AUTHORIZATION_AUTHORIZED,
                        BONSAI_AUTHORIZATION_NOT_AUTHORIZED);
  if (auth == BONSAI_AUTHORIZATION_AUTHORIZED)
    BONSAI_RETURN (BONSAI_AUTHORIZATION_AUTHORIZED);

  /* If we're in pairing mode, we can allow that */
  pairing = bonsaid_pairing_manager_get_default ();
  if (bonsaid_pairing_manager_get_pairing (pairing))
    BONSAI_RETURN (BONSAI_AUTHORIZATION_PAIRING);

  BONSAI_RETURN (BONSAI_AUTHORIZATION_NOT_AUTHORIZED);
}

static void
on_client_added_cb (BonsaidApplication *self,
                    BonsaiClient       *client,
                    BonsaiServer       *server)
{
  BonsaiRealm realm;

  BONSAI_ENTRY;

  g_assert (BONSAID_APPLICATION (self));
  g_assert (BONSAI_CLIENT (client));

  realm = bonsai_client_get_realm (client);

  if (realm == BONSAI_REALM_ANONYMOUS)
    {
      g_autoptr(BonsaidPairing) pairing = bonsaid_pairing_new ();

      bonsai_client_export (client,
                            G_DBUS_INTERFACE_SKELETON (pairing),
                            "/org/gnome/Bonsai/Pairing",
                            NULL);

    }
  else if (realm == BONSAI_REALM_AUTHORIZED)
    {
      g_autoptr(BonsaidPairing) pairing = bonsaid_pairing_new ();
      g_autoptr(BonsaidEnrollment) enrollment = bonsaid_enrollment_new ();
      g_autoptr(BonsaidStorage) storage = bonsaid_storage_new (NULL);
      g_autoptr(BonsaidDaoSync) dao_sync = bonsaid_dao_sync_new ();
      g_autoptr(BonsaidIsolation) isolation = bonsaid_isolation_new ();

      bonsai_client_export (client,
                            G_DBUS_INTERFACE_SKELETON (isolation),
                            "/org/gnome/Bonsai/Isolation",
                            NULL);
      bonsai_client_export (client,
                            G_DBUS_INTERFACE_SKELETON (pairing),
                            "/org/gnome/Bonsai/Pairing",
                            NULL);
      bonsai_client_export (client,
                            G_DBUS_INTERFACE_SKELETON (enrollment),
                            "/org/gnome/Bonsai/Enrollment",
                            NULL);
      bonsai_client_export (client,
                            G_DBUS_INTERFACE_SKELETON (storage),
                            "/org/gnome/Bonsai/Storage",
                            NULL);
      bonsai_client_export (client,
                            G_DBUS_INTERFACE_SKELETON (dao_sync),
                            "/org/gnome/Bonsai/Dao/Sync",
                            NULL);
    }

  BONSAI_EXIT;
}

static void
bonsaid_application_load_certificate_cb (GObject      *object,
                                         GAsyncResult *result,
                                         gpointer      user_data)
{
  g_autoptr(BonsaidApplication) self = user_data;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (BONSAID_IS_APPLICATION (self));

  g_application_release (G_APPLICATION (self));

  if (!(certificate = bonsai_tls_certificate_new_from_files_or_generate_finish (result, &error)))
    {
      g_printerr ("Cannot load TLS certificate: %s", error->message);
      BONSAI_EXIT;
    }

  self->server = bonsai_server_new (certificate);

  g_signal_connect_object (self->server,
                           "authorize-identity",
                           G_CALLBACK (on_authorize_identity_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->server,
                           "client-added",
                           G_CALLBACK (on_client_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  /* TODO: actually use config and don't listen everywhere. */
  if (!g_socket_listener_add_inet_port (G_SOCKET_LISTENER (self->server),
                                        BONSAI_DEFAULT_PORT,
                                        NULL,
                                        &error))
    {
      g_printerr ("Failed to listen on default port: %s", error->message);
      return;
    }

  g_socket_service_start (G_SOCKET_SERVICE (self->server));
  g_application_hold (G_APPLICATION (self));

  BONSAI_EXIT;
}

static void
bonsaid_application_startup (GApplication *app)
{
  BonsaidApplication *self = (BonsaidApplication *)app;
  g_autofree gchar *public_key_path = NULL;
  g_autofree gchar *private_key_path = NULL;

  public_key_path = g_build_filename (g_get_user_config_dir (), "bonsaid", "public.key", NULL);
  private_key_path = g_build_filename (g_get_user_config_dir (), "bonsaid", "private.key", NULL);

  g_application_hold (app);

  bonsai_tls_certificate_new_from_files_or_generate_async (public_key_path,
                                                           private_key_path,
                                                           "US",
                                                           "GNOME:Bonsai",
                                                           NULL,
                                                           bonsaid_application_load_certificate_cb,
                                                           g_object_ref (self));

  G_APPLICATION_CLASS (bonsaid_application_parent_class)->startup (app);
}

static gboolean
bonsaid_application_dbus_register (GApplication     *app,
                                   GDBusConnection  *connection,
                                   const gchar      *object_path,
                                   GError          **error)
{
  BonsaidApplication *self = (BonsaidApplication *)app;

  g_assert (BONSAID_IS_APPLICATION (self));
  g_assert (G_IS_DBUS_CONNECTION (connection));
  g_assert (object_path != NULL);

  self->enrollment = bonsaid_enrollment_new ();
  self->pairing = bonsaid_pairing_new ();

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->pairing),
                                    connection,
                                    "/org/gnome/Bonsai/Pairing",
                                    NULL);
  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->enrollment),
                                    connection,
                                    "/org/gnome/Bonsai/Enrollment",
                                    NULL);

  return G_APPLICATION_CLASS (bonsaid_application_parent_class)->
      dbus_register (app, connection, object_path, error);
}

static void
bonsaid_application_finalize (GObject *object)
{
  BonsaidApplication *self = (BonsaidApplication *)object;

  g_clear_object (&self->server);

  G_OBJECT_CLASS (bonsaid_application_parent_class)->finalize (object);
}

static void
bonsaid_application_class_init (BonsaidApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = bonsaid_application_finalize;

  app_class->activate = bonsaid_application_activate;
  app_class->startup = bonsaid_application_startup;
  app_class->dbus_register = bonsaid_application_dbus_register;
}

static void
bonsaid_application_init (BonsaidApplication *self)
{
}

BonsaidApplication *
bonsaid_application_new (void)
{
  return g_object_new (BONSAID_TYPE_APPLICATION,
                       "application-id", "org.gnome.Bonsai.Daemon",
                       "flags", G_APPLICATION_DEFAULT_FLAGS,
                       NULL);
}
