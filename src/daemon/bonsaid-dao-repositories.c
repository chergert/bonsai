/* bonsaid-dao-repositories.c
 *
 * Copyright 2020 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-dao-repositories"

#include "config.h"

#include <errno.h>

#include "bonsaid-dao-repositories.h"

struct _BonsaidDaoRepositories
{
  GObject     parent_instance;
  GMutex      mutex;
  GHashTable *repositories;
};

G_DEFINE_TYPE (BonsaidDaoRepositories, bonsaid_dao_repositories, G_TYPE_OBJECT)

static void
bonsaid_dao_repositories_load (BonsaidDaoRepositories *self)
{
  g_autofree gchar *repos_path = NULL;
  g_autoptr(GMutexLocker) locker = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GDir) dir = NULL;
  const gchar *name;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_DAO_REPOSITORIES (self));
  g_assert (self->repositories != NULL);

  locker = g_mutex_locker_new (&self->mutex);

  repos_path = g_build_filename (g_get_user_data_dir (), "bonsaid", "dao", NULL);

  if (g_mkdir_with_parents (repos_path, 0750) != 0)
    {
      int errsv = errno;
      g_warning ("Failed to create DAO repositories directory: %s",
                 g_strerror (errsv));
      BONSAI_EXIT;
    }

  if (!(dir = g_dir_open (repos_path, 0, &error)))
    {
      g_warning ("Failed to open DAO repositories directory: %s",
                 error->message);
      BONSAI_EXIT;
    }

  while ((name = g_dir_read_name (dir)))
    {
      g_autofree gchar *path = g_build_filename (repos_path, name, NULL);
      g_autoptr(BonsaiDaoRepository) repository = NULL;

      /* Only continue if this looks like it is a repository directory */
      if (!g_application_id_is_valid (name) || !g_file_test (path, G_FILE_TEST_IS_DIR))
        continue;

      if ((repository = bonsai_dao_repository_new_for_path (name, path, NULL, &error)))
        g_hash_table_insert (self->repositories, g_strdup (name), g_steal_pointer (&repository));
      else
        g_warning ("Failed to open DAO repository \"%s\": %s", name, error->message);
    }

  BONSAI_EXIT;
}

BonsaidDaoRepositories *
bonsaid_dao_repositories_get_default (void)
{
  static BonsaidDaoRepositories *instance;

  if (g_once_init_enter (&instance))
    {
      BonsaidDaoRepositories *self = g_object_new (BONSAID_TYPE_DAO_REPOSITORIES, NULL);
      bonsaid_dao_repositories_load (self);
      g_once_init_leave (&instance, self);
    }

  return instance;
}

static void
bonsaid_dao_repositories_finalize (GObject *object)
{
  BonsaidDaoRepositories *self = (BonsaidDaoRepositories *)object;

  g_clear_pointer (&self->repositories, g_hash_table_unref);
  g_mutex_clear (&self->mutex);

  G_OBJECT_CLASS (bonsaid_dao_repositories_parent_class)->finalize (object);
}

static void
bonsaid_dao_repositories_class_init (BonsaidDaoRepositoriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsaid_dao_repositories_finalize;
}

static void
bonsaid_dao_repositories_init (BonsaidDaoRepositories *self)
{
  g_mutex_init (&self->mutex);
  self->repositories = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

/**
 * bonsaid_dao_repositories_get_repository:
 *
 * Returns: (transfer full) (nullable): a #BonsaiDaoRepository or %NULL
 */
BonsaiDaoRepository *
bonsaid_dao_repositories_get_repository (BonsaidDaoRepositories *self,
                                         const gchar            *repository_id)
{
  BonsaiDaoRepository *ret;

  g_return_val_if_fail (BONSAID_IS_DAO_REPOSITORIES (self), NULL);
  g_return_val_if_fail (repository_id != NULL, NULL);

  g_mutex_lock (&self->mutex);
  if ((ret = g_hash_table_lookup (self->repositories, repository_id)))
    {
      g_object_ref (ret);
    }
  else if (g_application_id_is_valid (repository_id))
    {
      g_autofree gchar *path = NULL;
      g_autoptr(BonsaiDaoRepository) repository = NULL;

      /* Create the repository on demand. We probably want to check
       * at some point that the peer is allowed to do this, but that
       * will need to be fit into a larger overall isolation strategy.
       */

      path = g_build_filename (g_get_user_data_dir (), "bonsaid", "dao", repository_id, NULL);
      if ((repository = bonsai_dao_repository_new_for_path (repository_id, path, NULL, NULL)))
        g_hash_table_insert (self->repositories, g_strdup (repository_id), g_object_ref (repository));
      ret = g_steal_pointer (&repository);
    }
  g_mutex_unlock (&self->mutex);

  return g_steal_pointer (&ret);
}
