/* bonsaid-pairing.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-pairing"

#include "config.h"

#include "bonsaid-application.h"
#include "bonsaid-pairing.h"
#include "bonsaid-pairing-manager.h"
#include "bonsaid-utils.h"

struct _BonsaidPairing
{
  BonsaiPairingSkeleton parent_instance;
};

static gboolean
bonsaid_pairing_handle_begin (BonsaiPairing         *pairing,
                              GDBusMethodInvocation *invocation)
{
  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING (pairing));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  bonsaid_pairing_manager_begin (bonsaid_pairing_manager_get_default ());
  bonsai_pairing_complete_begin (pairing, g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsaid_pairing_handle_end (BonsaiPairing         *pairing,
                            GDBusMethodInvocation *invocation)
{
  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING (pairing));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  bonsaid_pairing_manager_end (bonsaid_pairing_manager_get_default ());
  bonsai_pairing_complete_end (pairing, g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static void
bonsaid_pairing_pair_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  BonsaidPairingManager *pairing = (BonsaidPairingManager *)object;
  g_autoptr(GDBusMethodInvocation) invocation = user_data;
  g_autoptr(GError) error = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING_MANAGER (pairing));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!bonsaid_pairing_manager_pair_finish (pairing, result, &error))
    g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
  else
    bonsai_pairing_complete_pair (NULL, g_steal_pointer (&invocation));

  BONSAI_EXIT;
}

static gboolean
bonsaid_pairing_handle_pair (BonsaiPairing         *pairing,
                             GDBusMethodInvocation *invocation,
                             GVariant              *info)
{
  g_autoptr(GError) error = NULL;
  GDBusConnection *connection;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING (pairing));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));
  g_assert (info != NULL);

  if (!(connection = g_dbus_method_invocation_get_connection (invocation)) ||
      !(client = bonsai_client_from_connection (connection)))
    g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                           G_DBUS_ERROR,
                                           G_DBUS_ERROR_ACCESS_DENIED,
                                           "RequestToken may only be called from remote devices");
  else
    bonsaid_pairing_manager_pair_async (bonsaid_pairing_manager_get_default (),
                                        client,
                                        info,
                                        NULL,
                                        bonsaid_pairing_pair_cb,
                                        g_steal_pointer (&invocation));

  BONSAI_RETURN (TRUE);
}

static gboolean
bonsaid_pairing_handle_request_token (BonsaiPairing         *pairing,
                                      GDBusMethodInvocation *invocation,
                                      const gchar           *certificate_pem)
{
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *token = NULL;
  GDBusConnection *connection;
  BonsaiClient *client;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING (pairing));
  g_assert (G_IS_DBUS_METHOD_INVOCATION (invocation));

  if (!(connection = g_dbus_method_invocation_get_connection (invocation)) ||
      !(client = bonsai_client_from_connection (connection)))
    {
      g_dbus_method_invocation_return_error (g_steal_pointer (&invocation),
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "RequestToken may only be called from remote devices");
      BONSAI_RETURN (TRUE);
    }

  if (!(certificate = g_tls_certificate_new_from_pem (certificate_pem, -1, &error)))
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_RETURN (TRUE);
    }

  token = bonsaid_pairing_manager_request_token (bonsaid_pairing_manager_get_default (),
                                                 client,
                                                 certificate,
                                                 &error);

  if (token == NULL)
    {
      g_dbus_method_invocation_return_gerror (g_steal_pointer (&invocation), error);
      BONSAI_RETURN (TRUE);
    }

  bonsai_pairing_complete_request_token (pairing,
                                         g_steal_pointer (&invocation),
                                         token);

  BONSAI_RETURN (TRUE);
}

static void
pairing_iface_init (BonsaiPairingIface *iface)
{
  iface->handle_begin = bonsaid_pairing_handle_begin;
  iface->handle_end = bonsaid_pairing_handle_end;
  iface->handle_pair = bonsaid_pairing_handle_pair;
  iface->handle_request_token = bonsaid_pairing_handle_request_token;
}

G_DEFINE_TYPE_WITH_CODE (BonsaidPairing, bonsaid_pairing, BONSAI_TYPE_PAIRING_SKELETON,
                         G_IMPLEMENT_INTERFACE (BONSAI_TYPE_PAIRING, pairing_iface_init))

/**
 * bonsaid_pairing_new:
 *
 * Create a new #BonsaidPairing.
 *
 * Returns: (transfer full): a newly created #BonsaidPairing
 */
BonsaidPairing *
bonsaid_pairing_new (void)
{
  return g_object_new (BONSAID_TYPE_PAIRING, NULL);
}

static void
bonsaid_pairing_finalize (GObject *object)
{
  G_OBJECT_CLASS (bonsaid_pairing_parent_class)->finalize (object);
}

static void
bonsaid_pairing_class_init (BonsaidPairingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsaid_pairing_finalize;
}

static void
bonsaid_pairing_init (BonsaidPairing *self)
{
  static const BonsaidMethodAuth method_auth[] = {
    { "Begin",         BONSAI_REALM_AUTHORIZED, TRUE },
    { "End",           BONSAI_REALM_AUTHORIZED, TRUE },
    { "Pair",          BONSAI_REALM_ANONYMOUS,  FALSE },
    { "RequestToken",  BONSAI_REALM_ANONYMOUS,  FALSE },
    { NULL }
  };

  bonsaid_set_method_auth (G_DBUS_INTERFACE_SKELETON (self), method_auth);

  g_object_bind_property (bonsaid_pairing_manager_get_default (),
                          "pairing",
                          self,
                          "pairing",
                          G_BINDING_SYNC_CREATE);
}
