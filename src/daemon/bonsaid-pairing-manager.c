/* bonsaid-pairing-manager.c
 *
 * Copyright 2019 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bonsaid-pairing-manager"

#include "config.h"

#include "bonsaid-enrollment-manager.h"
#include "bonsaid-pairing-manager.h"

#define PAIRING_TIMEOUT_SECONDS 60

struct _BonsaidPairingManager
{
  GObject  parent_instance;
  GArray  *client_tokens;
  guint    timeout_source;
  guint    pairing : 1;
};

typedef struct
{
  gchar           *token;
  gchar           *hash;
  GTlsCertificate *certificate;
  BonsaiClient    *client;
  GTask           *task;
} ClientToken;

enum {
  PROP_0,
  PROP_PAIRING,
  N_PROPS
};

G_DEFINE_TYPE (BonsaidPairingManager, bonsaid_pairing_manager, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static void
clear_client_token (gpointer data)
{
  ClientToken *ct = data;

  g_clear_pointer (&ct->token, g_free);
  g_clear_pointer (&ct->hash, g_free);
  g_clear_object (&ct->certificate);
  g_clear_object (&ct->client);

  if (ct->task)
    {
      g_task_return_new_error (ct->task,
                               G_IO_ERROR,
                               G_IO_ERROR_CANCELLED,
                               "Task was cancelled");
      g_clear_object (&ct->task);
    }
}

static void
bonsaid_pairing_manager_finalize (GObject *object)
{
  BonsaidPairingManager *self = (BonsaidPairingManager *)object;

  g_clear_handle_id (&self->timeout_source, g_source_remove);
  g_clear_pointer (&self->client_tokens, g_array_unref);

  G_OBJECT_CLASS (bonsaid_pairing_manager_parent_class)->finalize (object);
}

static void
bonsaid_pairing_manager_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  BonsaidPairingManager *self = BONSAID_PAIRING_MANAGER (object);

  switch (prop_id)
    {
    case PROP_PAIRING:
      g_value_set_boolean (value, bonsaid_pairing_manager_get_pairing (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bonsaid_pairing_manager_class_init (BonsaidPairingManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bonsaid_pairing_manager_finalize;
  object_class->get_property = bonsaid_pairing_manager_get_property;

  properties [PROP_PAIRING] =
    g_param_spec_boolean ("pairing",
                          "Pairing",
                          "If the manager is in pairing mode",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  
  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bonsaid_pairing_manager_init (BonsaidPairingManager *self)
{
  self->client_tokens = g_array_new (FALSE, FALSE, sizeof (ClientToken));
  g_array_set_clear_func (self->client_tokens, clear_client_token);
}

/**
 * bonsaid_pairing_manager_get_default:
 *
 * Get the pairing manager used to add new devices.
 *
 * Returns: (transfer none): a #BonsaidPairingManager
 */
BonsaidPairingManager *
bonsaid_pairing_manager_get_default (void)
{
  static BonsaidPairingManager *instance;

  if (instance == NULL)
    {
      instance = g_object_new (BONSAID_TYPE_PAIRING_MANAGER, NULL);
      g_object_add_weak_pointer (G_OBJECT (instance), (gpointer *)&instance);
    }

  return instance;
}

static void
bonsaid_pairing_manager_client_closed_cb (BonsaidPairingManager *self,
                                          BonsaiClient          *client)
{
  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING_MANAGER (self));
  g_assert (BONSAI_IS_CLIENT (client));

  for (guint i = 0; i < self->client_tokens->len; i++)
    {
      ClientToken *ct = &g_array_index (self->client_tokens, ClientToken, i);

      if (ct->client == client)
        {
          g_autoptr(GTask) old_task = g_steal_pointer (&ct->task);

          if (old_task != NULL)
            g_task_return_new_error (old_task,
                                     G_IO_ERROR,
                                     G_IO_ERROR_CONNECTION_CLOSED,
                                     "The connection was closed");
          g_array_remove_index (self->client_tokens, i);
          BONSAI_EXIT;
        }
    }

  BONSAI_EXIT;
}

gchar *
bonsaid_pairing_manager_request_token (BonsaidPairingManager  *self,
                                       BonsaiClient           *client,
                                       GTlsCertificate        *certificate,
                                       GError                **error)
{
  ClientToken ct;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAID_IS_PAIRING_MANAGER (self), NULL);
  g_return_val_if_fail (BONSAI_IS_CLIENT (client), NULL);
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE (certificate), NULL);

  if (bonsai_client_get_realm (client) != BONSAI_REALM_ANONYMOUS)
    {
      g_set_error (error,
                   G_DBUS_ERROR,
                   G_DBUS_ERROR_ACCESS_DENIED,
                   "Only anonymous clients may request a token");
      BONSAI_RETURN (NULL);
    }

  for (guint i = 0; i < self->client_tokens->len; i++)
    {
      const ClientToken *ele = &g_array_index (self->client_tokens, ClientToken, i);

      if (ele->client == client)
        BONSAI_RETURN (g_strdup (ele->token));
    }

  g_signal_connect_object (client,
                           "closed",
                           G_CALLBACK (bonsaid_pairing_manager_client_closed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  ct.client = g_object_ref (client);
  ct.token = g_uuid_string_random ();
  ct.hash = bonsai_tls_certificate_get_hash (certificate);
  ct.certificate = g_object_ref (certificate);
  ct.task = NULL;

  g_array_append_val (self->client_tokens, ct);

  BONSAI_RETURN (g_strdup (ct.token));
}

void
bonsaid_pairing_manager_pair_async (BonsaidPairingManager *self,
                                    BonsaiClient          *client,
                                    GVariant              *info,
                                    GCancellable          *cancellable,
                                    GAsyncReadyCallback    callback,
                                    gpointer               user_data)
{
  BonsaidEnrollmentManager *enrollment;
  g_autoptr(GTask) task = NULL;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING_MANAGER (self));
  g_assert (BONSAI_IS_CLIENT (client));
  g_assert (info != NULL);
  g_assert (g_variant_is_of_type (info, G_VARIANT_TYPE_VARDICT));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bonsaid_pairing_manager_pair_async);
  g_task_set_name (task, "[BonsaidPairingManager:pair]");

  for (guint i = 0; i < self->client_tokens->len; i++)
    {
      ClientToken *ct = &g_array_index (self->client_tokens, ClientToken, i);

      if (ct->client == client)
        {
          g_autoptr(GTask) old_task = g_steal_pointer (&ct->task);

          ct->task = g_steal_pointer (&task);

          if (old_task)
            g_task_return_new_error (task,
                                     G_IO_ERROR,
                                     G_IO_ERROR_CANCELLED,
                                     "The task was cancelled");

          enrollment = bonsaid_enrollment_manager_get_default ();

          BONSAI_TRACE_MSG ("Requesting pairing for hash %s", ct->hash);

          bonsaid_enrollment_manager_request_pairing (enrollment,
                                                      ct->certificate,
                                                      info,
                                                      ct->token);

          BONSAI_EXIT;
        }
    }

  if (task != NULL)
    g_task_return_new_error (task,
                             G_IO_ERROR,
                             G_IO_ERROR_NOT_FOUND,
                             "No token registered for client");

  BONSAI_EXIT;
}

gboolean
bonsaid_pairing_manager_pair_finish (BonsaidPairingManager *self,
                                     GAsyncResult *result,
                                     GError **error)
{
  gboolean ret;

  BONSAI_ENTRY;

  g_return_val_if_fail (BONSAID_IS_PAIRING_MANAGER (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  BONSAI_RETURN (ret);
}

void
bonsaid_pairing_manager_hash_paired (BonsaidPairingManager *self,
                                     const gchar           *hash)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAID_IS_PAIRING_MANAGER (self));
  g_return_if_fail (hash != NULL);

  for (guint i = 0; i < self->client_tokens->len; i++)
    {
      ClientToken *ct = &g_array_index (self->client_tokens, ClientToken, i);

      if (strcasecmp (ct->hash, hash) == 0)
        {
          g_autoptr(GTask) task = g_steal_pointer (&ct->task);

          if (task != NULL)
            g_task_return_boolean (task, TRUE);

          g_array_remove_index (self->client_tokens, i);

          BONSAI_EXIT;
        }
    }

  BONSAI_EXIT;
}

gboolean
bonsaid_pairing_manager_get_pairing (BonsaidPairingManager *self)
{
  g_return_val_if_fail (BONSAID_IS_PAIRING_MANAGER (self), FALSE);

  return self->pairing;
}

static gboolean
bonsaid_pairing_manager_timeout_cb (gpointer data)
{
  BonsaidPairingManager *self = data;

  BONSAI_ENTRY;

  g_assert (BONSAID_IS_PAIRING_MANAGER (self));

  self->timeout_source = 0;
  bonsaid_pairing_manager_end (self);

  BONSAI_RETURN (G_SOURCE_REMOVE);
}

void
bonsaid_pairing_manager_begin (BonsaidPairingManager *self)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAID_IS_PAIRING_MANAGER (self));

  if (bonsaid_pairing_manager_get_pairing (self))
    BONSAI_EXIT;

  g_return_if_fail (self->pairing == FALSE);
  g_return_if_fail (self->timeout_source == 0);

  self->pairing = TRUE;
  self->timeout_source = g_timeout_add_seconds (PAIRING_TIMEOUT_SECONDS,
                                                bonsaid_pairing_manager_timeout_cb,
                                                self);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAIRING]);

  BONSAI_EXIT;
}

void
bonsaid_pairing_manager_end (BonsaidPairingManager *self)
{
  BONSAI_ENTRY;

  g_return_if_fail (BONSAID_IS_PAIRING_MANAGER (self));

  if (!bonsaid_pairing_manager_get_pairing (self))
    BONSAI_EXIT;

  g_clear_handle_id (&self->timeout_source, g_source_remove);
  self->pairing = FALSE;
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PAIRING]);

  for (guint i = self->client_tokens->len; i > 0; i--)
    {
      ClientToken *ct = &g_array_index (self->client_tokens, ClientToken, i - 1);
      g_autoptr(GTask) task = g_steal_pointer (&ct->task);

      if (task != NULL)
        g_task_return_new_error (ct->task,
                                 G_DBUS_ERROR,
                                 G_DBUS_ERROR_TIMED_OUT,
                                 "Pairing timed out.");

      g_array_remove_index (self->client_tokens, i - 1);
    }

  BONSAI_EXIT;
}
